package com.vnptit5.ptgp.ldtbxh.dao;

import java.util.List;
import java.util.Map;

/**
 * @author trinhctn
 */

public interface DanhMucDao {
    List<Map<String, Object>> danhMucHoanCanhGiaDinh();
    List<Map<String, Object>> danhMucNhomQuyen();
    List<Map<String, Object>> danhMucChucDanh();
    List<Map<String, Object>> danhMucDonVi(String maTinhThanh,
                                           String maQuanHuyen, String maXaPhuong);
    List<Map<String, Object>> danhMucTatCaDonVi();
    List<Map<String, Object>> danhMucPhongBan(int maDonVi);
    List<Map<String, Object>> danhMucChiTietHoanCanh();
    List<Map<String, Object>> danhMucChiTietHoanCanhTheoLoai(Integer maLoaiHoanCanh, Integer maCap);
    List<Map<String, Object>> danhMucChiTietHuongTroGiup();
    List<Map<String, Object>> danhMucChiTietHTG();
    List<Map<String, Object>> danhMucChiTietHC(Integer maLoaiHoanCanh, Integer cap);
    List<Map<String, Object>> danhMucPhanLoaiHoNgheo();
    List<Map<String, Object>> danhMucChiTietHoNgheo();
    List<Map<String, Object>> danhMucDanToc();
    List<Map<String, Object>> danhMucTinhTrangNhaO();
    List<Map<String, Object>> danhMucLoaiHoXi();
    List<Map<String, Object>> danhMucLoaiDien();
    List<Map<String, Object>> danhMucSoHuuNha();
    List<Map<String, Object>> danhMucNuocSinhHoat();
    List<Map<String, Object>> danhMucQuanHe();
    List<Map<String, Object>> danhMucDoiTuong(int idLoaiDoiTuong);
    List<Map<String, Object>> danhMucCapHoc();
    List<Map<String, Object>> danhMucTrinhDo();
    List<Map<String, Object>> danhMucDTCS();
    List<Map<String, Object>> danhMucTinhTrangViecLam();
    List<Map<String, Object>> danhMucTinhTrangHonNhan();
    List<Map<String, Object>> danhMucLoaiDoiTuong(Integer idLoaiCS, Integer doiTuong);
    List<Map<String, Object>> danhMucLoaiChiTieu();
    List<Map<String, Object>> danhMucChiTieu();
    String danhSachHoanCanhCon(Integer maHC, Integer loaiHC);
    String danhSachHuongTroGiupCon(Integer maHTG);
    List<Map<String, Object>> danhMucTrangThaiHCS();
    List<Map<String, Object>> danhMucLoaiTheBhyt();
    List<Map<String, Object>> danhMucLyDoKhongKCB();
    List<Map<String, Object>> danhMucMucDoKhuyetTat();
}
