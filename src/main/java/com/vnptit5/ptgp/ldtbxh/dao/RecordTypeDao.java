package com.vnptit5.ptgp.ldtbxh.dao;

import java.util.Map;
import java.util.List;

public interface RecordTypeDao {
    List<Map<String, Object>> getListRecordType();
}
