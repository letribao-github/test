package com.vnptit5.ptgp.ldtbxh.dao;

import com.vnptit5.ptgp.ldtbxh.dto.request.record.RecordCreateRequestDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.record.RecordRequestDto;

import com.vnptit5.ptgp.ldtbxh.dto.response.PageDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.nguoicocong.RecordCategory;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletResponse;

/*
 * Author: SonNVT
 * Date: 15/10/2020
 * */
public interface RecordDao {
    List<Map<String, Object>> searchRecord(RecordRequestDto recordRequestDto);
    PageDto searchRecordPage(RecordRequestDto recordRequestDto);
    void exportReport001(RecordRequestDto recordRequestDto, HttpServletResponse response);
    RecordCategory getCategory();
    boolean createProfileMartyrs(RecordCreateRequestDto recordRequestDto);
    List<Map<String, Object>> getProfileInfo(int hoSoID);
    boolean update(RecordCreateRequestDto request);
}
