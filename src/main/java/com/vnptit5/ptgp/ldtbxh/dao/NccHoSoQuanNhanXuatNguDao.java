package com.vnptit5.ptgp.ldtbxh.dao;

import com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong.HoSoQuanNhanXuatNguDto;

import java.util.List;
import java.util.Map;

/*
 * Author: huylt.tgg
 * Date: 19/10/2020
 * */
public interface NccHoSoQuanNhanXuatNguDao {
    Map<String, Object> getThongTinHoSoByID(Integer hosoID);
    List<Map<String, Object>> getThongtinTroCapPhuCapByID(Integer hosoID);
    List<Map<String, Object>> getQuaTrinhCongTacByID(Integer hosoID);
    List<Map<String, Object>> getTroCapKhiQuaDoiByID(Integer hosoID);
    int addThongTinHoSo(HoSoQuanNhanXuatNguDto hoSoQuanNhanXuatNguDto);
    int addThongTinTroCapPhuCap(HoSoQuanNhanXuatNguDto dto);
    int addQuaTrinhCongTac(HoSoQuanNhanXuatNguDto dto);
    int addTroCapKhiQuaDoi(HoSoQuanNhanXuatNguDto dto);
    void updateThongTinHoSo(HoSoQuanNhanXuatNguDto dto);
    void updateThongTinTroCapPhuCap(HoSoQuanNhanXuatNguDto dto);
    void updateQuaTrinhCongTac(HoSoQuanNhanXuatNguDto dto);
    void updateTroCapKhiQuaDoi(HoSoQuanNhanXuatNguDto dto);
    void deleteThongTinHoSo(Integer hosoID);
    void deleteThongtinTroCapPhuCap(Integer thongtinchedoID);
    void deleteQuaTrinhCongTac(Integer quatrinhcongtacID);
    void deleteTroCapKhiQuaDoi(Integer ID);
}
