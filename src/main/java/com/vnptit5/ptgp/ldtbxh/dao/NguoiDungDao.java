package com.vnptit5.ptgp.ldtbxh.dao;

import com.vnptit5.ptgp.ldtbxh.dto.request.nguoidung.CapNhatNguoiDungDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoidung.ThemMoiNguoiDungDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoidung.TimKiemNguoiDungDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.nguoidung.ChiTietNguoiDungDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.CustomException;

import java.util.List;
import java.util.Map;

/**
 * @author trinhctn
 *
 */
public interface NguoiDungDao {
    Map<String, Object> findByUsername(String tenDangNhap) throws CustomException;
    Map themMoiNguoiDung(ThemMoiNguoiDungDto dto);
    void themMoiPhanQuyen(int  maNguoiDung, int maNhomQuyen);
    void xoaPhanQuyen(String tenDangNhap);
    List<Map<String, Object>> danhSachNhomQuyenNguoiDung(String tenDangNhap);
    void xoaNguoiDung(String tenDangNhap);
    void khoiPhucNguoiDung(String tenDangNhap);
    void suaNguoiDung(CapNhatNguoiDungDto dto);
    List<Map<String, Object>> danhSachNguoiDung(TimKiemNguoiDungDto timKiemNguoiDungDto);
    ChiTietNguoiDungDto chiTietNguoiDung(String tenDangNhap);
}
