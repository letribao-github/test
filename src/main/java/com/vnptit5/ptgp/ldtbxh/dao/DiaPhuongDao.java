package com.vnptit5.ptgp.ldtbxh.dao;


import com.vnptit5.ptgp.ldtbxh.dto.request.diaphuong.CapNhatThonDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.diaphuong.ThemMoiThonDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.diaphuong.TimKiemDiaPhuongDto;

import java.util.List;
import java.util.Map;

/**
 * @author trinhctn
 */
public interface DiaPhuongDao {
    List<Map<String, Object>> danhSachTinh();
    List<Map<String, Object>> danhSachHuyen(String maTinh);
    List<Map<String, Object>> danhSachXa(String maTinh, String maHuyen);
    List<Map<String, Object>> danhSachThon(String maTinh, String maHuyen, String maXa);
    List<Map<String, Object>> timKiemDiaPhuong(TimKiemDiaPhuongDto dto);
    int themMoiThon(ThemMoiThonDto dto);
    void suaThon(CapNhatThonDto dto);
    void khoiPhucThon(String maThon);
    void xoaThon(String maThon);
    Map<String, Object> thongTinThong(String maThon);
    Map<String, Object> thongTinXa(String maXa);
}
