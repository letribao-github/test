package com.vnptit5.ptgp.ldtbxh.dao;

import java.util.List;
import java.util.Map;

public interface DanTocDao {
    List<Map<String, Object>> getListDanToc();
}
