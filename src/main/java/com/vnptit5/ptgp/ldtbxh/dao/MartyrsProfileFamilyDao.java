package com.vnptit5.ptgp.ldtbxh.dao;

import com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong.MartyrsProfileFamilyInfoDto;
import java.util.List;
import java.util.Map;

public interface MartyrsProfileFamilyDao {
    List<Map<String, Object>> getFamilyInfo(Integer hoSoID);
    boolean updateFamilyInfo(MartyrsProfileFamilyInfoDto request);
}
