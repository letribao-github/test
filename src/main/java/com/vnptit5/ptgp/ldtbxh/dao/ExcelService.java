package com.vnptit5.ptgp.ldtbxh.dao;

import java.io.File;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

public interface ExcelService {

    /**
     * Export single data to excel (Use FileUtil common)
     * @param response
     * @param dto
     * @author ToiTV
     */
    <T> boolean writeSingleDataToExcelTemplate(HttpServletResponse response, T dto);

    <T> boolean writeListDataToExcelTemplate(HttpServletResponse response, List<T> listDataInSheet, Class<T> typeClass);

    <T> boolean writeListDataToMultiSheet(HttpServletResponse response, T multiSheetData);

    void response(HttpServletResponse response, File file);
}