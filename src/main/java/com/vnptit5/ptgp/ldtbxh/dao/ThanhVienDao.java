package com.vnptit5.ptgp.ldtbxh.dao;

import com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach.ChiTietChinhSachThanhVienDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.thanhvien.*;
import com.vnptit5.ptgp.ldtbxh.dto.response.DoiTuongHuongCSDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.PageDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.ThanhVienSearchDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.ThongTinThanhVienChiTietDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.thanhvien.ThongTinChungThanhVienDto;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

/**
 * @author trinhctn
 */
public interface ThanhVienDao {
    int themMoiThongTinBTXH(ThongTinBTXHDto dto);

    int capNhatThanhVien(CapNhatThanhVienDto dto);

    int xoaThanhVien(Integer idNhanKhau, Integer maHoGD);

    int khoiPhucThanhVien(Integer idNhanKhau, Integer maHoGD);

    void capNhatThongTinBTXH(ThongTinBTXHDto dto);

    int themMoiThongTinHoNgheo(ThongTinHoNgheoDto dto);

    void capNhatThongTinHoNgheoThanhVien(ThongTinHoNgheoDto dto);

    int themMoiThanhVien(ThemMoiThanhVienDto dto);

    List<Map<String, Object>> danhSachThanhVien(Integer maHo);

    List<ThanhVienSearchDto.DataList> TimKiemThanhVienHoGiaDinh(TimKiemThanhVienDto dtnk);

    PageDto timKiemThanhVienPhanTrang(TimKiemThanhVienPhanTrangDto dto);

    ChiTietChinhSachThanhVienDto chiTietHuongCS(int maThanhVien, Integer idHuongCS);

    List<Map<String, Object>> chinhSachTheoThanhVien(int maThanhVien, Integer idHuongCS);

    ThongTinThanhVienChiTietDto thongTinThanhVienChiTiet(int maThanhVien, Integer idHuongCS);

    ThongTinChungThanhVienDto thongTinChungThanhVien(int maThanhVien);

    DoiTuongHuongCSDto danhSachHuongCS(Integer maHo, Boolean HGD, Integer trangThaiHuong);

    Map<String, Object> tongTienHuongCS_HGD(Integer maHoGD, Boolean HGD);

    void ChuyenThanhVien(ThongTinChuyenTVDto ctv);

    void ExportMemberList(TimKiemThanhVienDto tktvdto, HttpServletResponse response);

    Map<String, Object> tongTienHuongCS_ThanhVien(Integer idNhanKhau, Integer idHuongCS);

    DoiTuongHuongCSDto chinhSachThanhVienDangHuong(Integer maThanhVien, Integer idHuongCS);
}
