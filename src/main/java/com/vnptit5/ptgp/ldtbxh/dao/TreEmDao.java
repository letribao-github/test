package com.vnptit5.ptgp.ldtbxh.dao;

import com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh.CapNhatHoGiaDinhDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh.ThemMoiHoGiaDinhDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.treem.*;
import com.vnptit5.ptgp.ldtbxh.dto.response.PageDto;

import java.util.List;
import java.util.Map;

/**
 * @author trinhctn
 */
public interface TreEmDao {
    int themMoiTreEm(CapNhatTreEmDto dto);
    void themMoiTreEmChiTietHoanCanh(int maTreEm, int maChiTietHoanCanh);
    void xoaTreEmChiTietHoanCanh(int maTreEm);
    void themMoiTreEmChiTietHuongTroGiup(int maTreEm, int maChiTietHuongTroGiup);
    void xoaTreEmChiTietHuongTroGiup(int maTreEm);
    void suaTreEm(CapNhatTreEmDto dto);
    List<Map<String, Object>> danhSachTreEm(TimKiemTreEmDto dto);
    PageDto quanLyTreEmHoanCanh(QuanLyTreEmHoanCanhDto dto);
    PageDto quanLyTreEmHuongTroGiup(QuanLyTreEmHuongTroGiupDto dto);
    List<Map<String, Object>> danhMucHoanCanhTheoLoaiHC(int maLoaiHoanCanh, int capHoanCanh);
    List<Map<String, Object>> danhMucChiTieuTheoLoai(int maLoaiChiTieu, int capChiTieu);
    List<Map<String, Object>> danhMucHuongTroGiup(int capHuongTroGiup);
    Map<String, Object> chiTietTreEm(int maTreEm, Integer maLoaiHoanCanh);
    List<Map<String, Object>> danhSachTreEmHoanCanh(int maTreEm, Integer maLoaiHoanCanh);
    List<Map<String, Object>> danhSachTreEmHuongTroGiup(int maTreEm);
    int themMoiHoGiaDinh(ThemMoiHoGiaDinhDto dto);
    void capNhatHoGiaDinh(CapNhatHoGiaDinhDto giaDinhDto);
    List<Map<String, Object>> baoCaoDanhSachTreEmBinhThuong(BaoCaoTreEmDto baoCaoTreEmDto);
    List<Map<String, Object>> baoCaoDanhSachTreEm(BaoCaoTreEmDto baoCaoTreEmDto);
    List<Map<String, Object>> baocaothongkeTEHCDB(int year);
    List<Map<String, Object>> baoCaoDanhSachTreEmTheoChiTieu(TreEmTheoChiTieuDto dto);
    List<Map<String, Object>> baocaothongkeTETheoNam();
    Map<String, Object> chiTietChiTieu(Integer maLoaiCT);
    List<Map<String, Object>> chiTieuCha(Integer maChiTieu);
    void xoaTreEm(int maTreEm);
    void khoiPhucTreEm(int maTreEm);
    Integer getID_Cha_CTHC(Integer maChiTietHC);
    Integer getID_Cha_CTHTG(Integer maChiTietHTG);

    List<Map<String, Object>> danhSachTreEmHoanCanhNhieuTE(String dsMaTreEm, int maLoaiHoanCanh);
    PageDto danhSachTreEmPhanTrang(TimKiemTreEmDto dto);
    List<Map<String, Object>> danhSachTreEmHuongTroGiupNhieuTE(String dsMaTreEm);
}
