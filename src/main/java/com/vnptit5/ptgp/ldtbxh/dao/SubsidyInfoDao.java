package com.vnptit5.ptgp.ldtbxh.dao;

import com.vnptit5.ptgp.ldtbxh.dto.request.subsidy.SubsidyDtoRequest;
import java.util.List;
import java.util.Map;

public interface SubsidyInfoDao {
    List<Map<String, Object>> getInfo(int hoSoID);
    List<Map<String, Object>> getCategory();
    boolean insertOrUpdate(SubsidyDtoRequest request);
    boolean delete(int thongTinCheDoID);
}
