package com.vnptit5.ptgp.ldtbxh.dao;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong.*;
import com.vnptit5.ptgp.ldtbxh.dto.request.treem.TimKiemTreEmDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.PageDto;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;
import java.util.Map;

public interface NCCThongTinHoSoDao {

    int tmThongTinHSNCC (TTNguoicocongCM TTNguoicocongCM );

    int updateTTHSNCC   (TTNguoicocongCMUpdate TTNguoicocongCMUpdate );

    void deleteTTHSNCC  (Integer HoSoNguoiCoCongGiupCMID);

    Map<String, Object>       selectTTHSNCC  (Integer HosoID);

    List<Map<String, Object>> selectTTTrocap (Integer HosoID);

    List<Map<String, Object>> selectDinhkem  (Integer HosoID);

    // insert trang dinh kem - nguoi co cong giup do cach mang
    int insertNCCTrocapDinhkem (NCCTroCapInsertDinhKem NCCTroCapInsertDinhKem);
    // update trang dinh kem - nguoi co cong giup do cach mang
    int updatenccDinhKem (NCCTroCapInsertDinhKem NCCTroCapInsertDinhKem);
    // delete trang dinh kem - nguoi co cong giup do cach mang
    void deletenccDinhKem  (Integer HosoID);

    // insert trang tro cap - nguoi co cong giup do cach mang
    int insertNCCTroCap (NCCTroCapInsert NCCTroCapInsert );

    // update trang tro cap - nguoi co cong giup do cach mang
    int updateNCCTroCap (NCCTroCapUpdate NCCTroCapUpdate );

    // delete trang tro cap - nguoi co cong giup do cach mang
    void deleteNCCTrocap  (Integer HosoID, Integer ThongTinCheDoID);

    // select danh muc che do duoc huong

    List<Map<String, Object>> selectDMDH  ();

    //select danh muc che do

    List<Map<String, Object>> selectDMTinhtrangcanhan ();

}
