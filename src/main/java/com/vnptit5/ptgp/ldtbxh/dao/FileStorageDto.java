package com.vnptit5.ptgp.ldtbxh.dao;

import java.util.List;
import java.util.Map;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

/**
 * Created by Benjamin Lam on 10/21/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */
public interface FileStorageDto {
    String uploadAttachedProfile(MultipartFile file, Integer heroID, String fileName, String folderName, String note);

    List<Map<String, Object>> getListAttachedFiles(Integer heroID);

    int deleteAttachedFile(Integer fileID);

    Resource downloadAttachedFile(String fileName, String folderName);
}
