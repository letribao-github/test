package com.vnptit5.ptgp.ldtbxh.dao;

import com.vnptit5.ptgp.ldtbxh.dto.request.household.HouseHoldPostDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.household.HouseHoldPutDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.household.MemberHouseHoldPostDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.household.MemberHouseHoldPutDto;

import java.util.List;
import java.util.Map;

/**
 * Created by Benjamin Lam on 10/14/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */

public interface HouseHoldDao {

    int addNewHouseHold(HouseHoldPostDto houseHoldPostDto);

    int addNewHouseHoldMember(MemberHouseHoldPostDto memberHouseHoldPostDto);

    int updateHouseHold(HouseHoldPutDto houseHoldPutDto);

    int updateHouseHoldMember(MemberHouseHoldPutDto memberHouseHoldPutDto);

    int deleteHouseHold(String houseHoldID, String reason);

    int deleteMemberInHouseHold(String houseHoldID, int memberID, String reason);

    Map<String, Object> getHouseHoldOwner(String houseHoldID);

    List<Map<String, Object>> getHouseHoldMember(String houseHoldID);

    List<Map<String, Object>> getListHouseHold();

    List<Map<String, Object>> getListArmyLevel();

    List<Map<String, Object>> getListPoliceLevel();

    List<Map<String, Object>> getListNationLevel();
}
