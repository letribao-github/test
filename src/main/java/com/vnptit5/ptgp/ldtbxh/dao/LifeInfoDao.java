package com.vnptit5.ptgp.ldtbxh.dao;

import com.vnptit5.ptgp.ldtbxh.dto.request.lifeinfo.LifeInfoPostDto;

import java.util.List;
import java.util.Map;

/**
 * Created by Benjamin Lam on 10/19/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */
public interface LifeInfoDao {

    Map<String, Object> getHeroLifeInfo(Integer heroID);

    List<Map<String, Object>> getListIncomeType();

    List<Map<String, Object>> getListHouseStatus();

    int modifyHeroLifeInfo(LifeInfoPostDto lifeInfoPostDto);
}
