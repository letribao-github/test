package com.vnptit5.ptgp.ldtbxh.dao;

import com.vnptit5.ptgp.ldtbxh.dto.request.CatChetNhanKhauDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach.ChiTietChinhSachThanhVienDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh.*;
import com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh.chitra.ChiTraDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.thanhvien.ThemMoiThanhVienDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.thanhvien.DanhSachNhanKhauHuongChinhSachDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.PageDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.ThongKeChiTraChiTietDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.ThongKeChiTraDto;

import java.util.List;
import java.util.Map;

/**
 * @author trinhctn
 */

public interface HoGiaDinhDao {
    List<Map<String, Object>> danhSachChiTra(ChiTraDto chiTraDto);
    void chiTra(Integer idNhanKhau, Integer soTienHuong, String namHuong, String thangHuong);
    void khoiPhucChiTra(Integer idNhanKhau, Integer soTienHuong, String namHuong, String thangHuong);
    List<Map<String, Object>> tongTienChiTra(String maTinh, String maHuyen, String maXa, String maThon,
                                             String namHuong, String thangHuong);
    List<ThongKeChiTraDto> thongKeChiTra(String maTinh, String maHuyen, String maXa, String maThon,
                                         String namHuong, String thangHuong);
    List<ThongKeChiTraChiTietDto> thongKeChiTraChiTiet(String maXa, String maThon, String namHuong,
                                                       String thangHuong);
    List<Map<String, Object>> thongKeChiTraChiTietDoiTuong(Integer idNhanKhau, String namHuong, String thangHuong);
    int themMoiNhanKhau(ThemMoiThanhVienDto nhanKhauDto);
    int themMoiDacDiemHGD(DacDiemHGDDto hgdDto);
    int themMoiHGD(ThemMoiHoGiaDinhDto dto);
    void capNhatHGD(CapNhatHoGiaDinhDto dto);
    int xoaHGD(Integer maHoGD);
    int khoiPhucHGD(Integer maHoGD);
    void capNhatHGDDacDiem(int maHoGD, int idDacDiem);
    void themMoiQuanHeGiaDinh(Integer maHoGD, String maQuanHe, Integer idNhanKhau);
    Map<String, Object> thongTinNhanKhau(String maDinhDanh);
    Map<String, Object> thongTinHGD(Integer maHo);
    PageDto dsNhanKhauHuongChinhSach(DanhSachNhanKhauHuongChinhSachDto dto);
    List<Map<String, Object>> timKiemHoGiaDinh(TimKiemHoGiaDinhBTXHDto TimKiemHoGiaDinhBTXHDto);
    PageDto timKiemHoGiaDinhPhanTrang(TimKiemHoGiaDinhPhanTrangDto dto);
    int themMoiChinhSachDoiTuong(CapNhatChinhSachDoiTuong dto);
    int capNhatChinhSachDoiTuong(CapNhatChinhSachDoiTuong dto);
    List<Map<String, Object>> danhSachCatChet(DanhSachCatChetDto dto);
    int catChetNhanKhau(CatChetNhanKhauDto dto);
    void chuyenVungHGD(ChuyenVungHoGiaDinhDto dto);
    ChiTietChinhSachThanhVienDto chinhSachDoiTuong(Integer idHuongChinhSach, Integer idNhanKhau);
}
