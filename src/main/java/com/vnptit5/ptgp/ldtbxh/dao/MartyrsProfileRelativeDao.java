package com.vnptit5.ptgp.ldtbxh.dao;

import com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong.MartyrsProfileRelativeDto;
import java.util.List;
import java.util.Map;

public interface MartyrsProfileRelativeDao {
    List<Map<String, Object>> getRelativeInfo(int hoSoID);
    boolean updateRelativeInfo(MartyrsProfileRelativeDto request);
    boolean deleteRelativeInfo(int thanNhanLietSyID);
}
