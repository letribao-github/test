package com.vnptit5.ptgp.ldtbxh.dao;

import com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach.SuaChinhSachDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach.SuaDoiTuongDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach.ThemChinhSachDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach.ThemDoiTuongDto;

import java.util.List;
import java.util.Map;

public interface ChinhSachDao {
    Map<String, Object> thongTinChinhSachTheoDoiTuong(Integer idLoaiCS, Integer idDoiTuong);

    List<Map<String, Object>> getLoaiChinhSachs(Integer doiTuong);

    List<Map<String, Object>> findLoaiChinhSach(String name);

    List<Map<String, Object>> danhMucChinhSach(Integer doiTuong);

    List<Map<String, Object>> getAllLoaiDoiTuong();

    int themMoiChinhSach(ThemChinhSachDto csdto);

    void updatePolicy(SuaChinhSachDto dto);

    List<Map<String, Object>> searchDoiTuong(Integer idLoaiCs, String tenDoiTuong);

    int themDoiTuong(ThemDoiTuongDto dto);

    void updateDoiTuong(SuaDoiTuongDto dto);

    Map<String, Object> getRelatedInfoDoiTuong(Integer idDoiTuong);

    Map<String, Object> findByIdChinhSach(Integer idChinhSach);

    Map<String, Object> findByIdDoiTuong(Integer idDoiTuong);
}
