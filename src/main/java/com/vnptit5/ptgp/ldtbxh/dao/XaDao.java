package com.vnptit5.ptgp.ldtbxh.dao;

import java.util.Map;
import java.util.List;

public interface XaDao {
    List<Map<String, Object>> getListXa(Integer huyenID);
}
