package com.vnptit5.ptgp.ldtbxh.dao;

import com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong.MartyrsProfileDeathBenefitsDto;
import java.util.List;
import java.util.Map;

/**
 * @author ToiTV
 * 2020-10-22
 */
public interface MartyrsProfileDeathBenefitsDao {

    /** Get list death benefits information */
    List<Map<String, Object>> getDeathInfo(int hoSoID);

    /** Update death benefits information */
    boolean updateDeathBenefitsInfo(MartyrsProfileDeathBenefitsDto request);

    /** Delete death benefits information */
    boolean deleteDeathBenefitsInfo(int id);
}
