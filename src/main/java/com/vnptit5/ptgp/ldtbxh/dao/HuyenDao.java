package com.vnptit5.ptgp.ldtbxh.dao;

import java.util.Map;
import java.util.List;

public interface HuyenDao {
    List<Map<String, Object>> getListHuyen(Integer tinhID);
    List<Map<String, Object>> getListTinh();
}
