package com.vnptit5.ptgp.ldtbxh.authentication;

import com.vnptit5.ptgp.ldtbxh.dao.NguoiDungDao;
import com.vnptit5.ptgp.ldtbxh.dto.response.nguoidung.ChiTietNguoiDungDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author trinhctn
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private NguoiDungDao nguoiDungDao;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Map<String, Object> user = nguoiDungDao.findByUsername(username);
        CustomUserDetails customUserDetails = new CustomUserDetails(user);
        if (user == null || !customUserDetails.isEnabled()){
            throw new UsernameNotFoundException("Người dùng không tồn tại");
        }
        return new CustomUserDetails(user);
    }
    public ChiTietNguoiDungDto getCurrentUser() {
        CustomUserDetails user = (CustomUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        user.getUser().put("danhSachNhomQuyen", nguoiDungDao.danhSachNhomQuyenNguoiDung(user.getUsername()));
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(user.getUser(), ChiTietNguoiDungDto.class);
    }
}
