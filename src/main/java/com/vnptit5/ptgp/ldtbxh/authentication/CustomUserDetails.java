package com.vnptit5.ptgp.ldtbxh.authentication;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.Map;

/**
 * @author trinhctn
 */
@Data
@AllArgsConstructor
public class CustomUserDetails implements UserDetails {

    private Map<String, Object> user;

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getUsername() {
        return user.get("TEN_DANG_NHAP").toString();
    }

    @Override
    public String getPassword() {
        return user.get("MAT_KHAU").toString();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return (boolean) user.get("TRANG_THAI");
    }

}
