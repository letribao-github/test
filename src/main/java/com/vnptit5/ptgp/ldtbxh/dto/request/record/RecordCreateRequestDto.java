package com.vnptit5.ptgp.ldtbxh.dto.request.record;

import java.util.Date;
import javax.annotation.Nullable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/*
 * Author: ToiTV
 * Date: 18/10/2020
 * */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Nullable
public class RecordCreateRequestDto {
    private int         LoaiHoSoID;

    @Nullable
    private int         BanSao;

    @Nullable
    private String      SoHoSoTinhQuanLy;

    @Nullable
    private String      SoHoSoBoQuanLy;

    private int         HuyenTiepNhanID;
    private int         XaTiepNhanID;

    @Nullable
    private String      CumToTiepNhan;

    @Nullable
    private int         HoSoChuyenDenTuID;

    @Nullable
    private Date        ThoiGianChuyenDen;

    @Nullable
    private int         HoSoChuyenDiID;

    @Nullable
    private Date        ThoiGianChuyenDi;

    private String      HoVaTen;

    @Nullable
    private String      BiDanh;

    @Nullable
    private Date        NgaySinh;

    private int         GioiTinh;
    private int         DanTocID;

    @Nullable
    private String      QueQuan;

    @Nullable
    private Date        NgayNhapNgu;

    @Nullable
    private String      CapBacTruocKhiHySinh;

    @Nullable
    private String      LietSyLaAnhHung;

    @Nullable
    private String      DonViKhiHySinh;

    @Nullable
    private Date        NgayHySinh;

    @Nullable
    private int         ThoiKyHySinhID;

    @Nullable
    private String      TruongHopHySinh;

    @Nullable
    private String      NoiHySinh;

    @Nullable
    private int         ThoCung;

    @Nullable
    private String      GiayBaoTuSo;

    @Nullable
    private String      DonViCapGiayBaoTu;

    @Nullable
    private String      SoBangToQuocGhiCong;

    @Nullable
    private String      QuyetDinhCapBangSo;

    @Nullable
    private int         DoiTuongID;

    @Nullable
    private int         TongSoLietSyTrongGiaDinhID;

    @Nullable
    private String      ThuocHoGiaDinh;

    @Nullable
    private String      CanBoQuanLy;

    @Nullable
    private String      ViTriLuuTruHoSo;

    @Nullable
    private String      CacCheDoKhac;

    @Nullable
    private int         HoSoID;
}
