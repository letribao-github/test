package com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CapNhatChinhSachDoiTuong {
    private Integer idHuongChinhSach;
    private Integer idDoiTuong;
    private Integer idChinhSach;
    private Float heSo;
    private Integer mucHuong;
    private String ngayHuong;
    private Integer soTienHuong;
    private Integer ttHuong;
    private Integer idNhanKhau;
    private String tenNguoiNhan;
    private String ghiChu;
    private String soQuyetDinhHuong;
    private String ngayKyQDHuong;
    private String soSoLanhTien;
    private String soQuyetDinhThoiHuong;
    private String ngayKyQDThoiHuong;
    private String ngayThoiHuong;
    private String lyDoThoiHuong;
}
