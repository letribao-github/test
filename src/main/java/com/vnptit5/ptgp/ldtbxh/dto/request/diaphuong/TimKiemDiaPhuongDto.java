package com.vnptit5.ptgp.ldtbxh.dto.request.diaphuong;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TimKiemDiaPhuongDto {
    private String maTinhThanh;
    private String maQuanHuyen;
    private String maXaPhuong;
    private String tenXaPhuong;
    private Boolean trangThai;
}
