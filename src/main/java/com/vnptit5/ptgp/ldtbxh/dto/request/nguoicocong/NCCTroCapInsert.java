package com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class NCCTroCapInsert
{
    private Integer hosoID;
    private String  nguoiDuocHuongTroCap;
    private String  quanHeVoiNguoiCoCong;
    private String  namSinhNguoiHuong;
    private String  nguyenQuanNguoiHuong;
    private String  truQuanNguoiHuong;
    private Integer maiTangPhi;
    private Integer troCapCacThang;
    private String  thoiGianHuongTroCap;
    private String  quyetDinhHuongTroCap;
    private String  ngayRaQuyetDinhHuongTroCap;
    private String  noiRaQuyetDinhHuongTroCap;
    private Integer soTienTroCap;
    private String  truyLinhTuNgay;
    private String  truyLinhDenNgay;
    private Integer soTienTruyLinh;

}
