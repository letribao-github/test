package com.vnptit5.ptgp.ldtbxh.dto.request.thanhvien;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author trinhctn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ThongTinBTXHDto {
    private Integer maThanhVien;

    private Boolean nhanTCXHThang;
    private String nguoiNuoiDuong;
    private String thongTinCha;
    private String thongTinMe;
    private Integer tinhTrangHonNhan;
    private String thanhVienLaCon;
    private Boolean thamGiaLamViec;
    private Boolean tuPhucVu;
    private String quaTrinhHoatDong;
    private Boolean vanDong;
    private Boolean nghe;
    private Boolean nhin;
    private Boolean thanKinh;
    private Boolean triTue;
    private Boolean dangTatKhac;
    private Integer mucDoKhuyetTat;
    private String kinhNghiemChamSocDT;
    private Boolean anTu;
    private Boolean benhManTinh;
    private String viPham;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date ngayNhiemHIV;
}
