package com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TraCuuHoGiaDinhDto {
    private String maTinh;
    private String maHo;
    private Integer page;
    private Integer size;
    private String propertyName;
    private String direction;
    private String phoneNumber;
    private Boolean check;

}
