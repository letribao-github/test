package com.vnptit5.ptgp.ldtbxh.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class CatChetNhanKhauDto {
    public Integer maThanhVien;
    public String tenNguoiNhan;
}
