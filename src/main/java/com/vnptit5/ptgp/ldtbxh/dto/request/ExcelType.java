package com.vnptit5.ptgp.ldtbxh.dto.request;

import org.springframework.http.MediaType;

/**
 * @author trinhctn
 */
public enum ExcelType {

    XLS, XLSX;

    public MediaType getMediaType() {
        if (this == XLS)
            return MediaType.valueOf("application/vnd.ms-excel");
        else if (this == XLSX)
            return MediaType.valueOf("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        return null;
    }

}
