package com.vnptit5.ptgp.ldtbxh.dto.request.thanhvien;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ThongTinChuyenTVDto {
    Integer maHo;
    String maQuanHe;
    Integer idNhanKhau;
}
