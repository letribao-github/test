package com.vnptit5.ptgp.ldtbxh.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ThongKeChiTraDto {
//    private Integer maThonXom;
//    private Integer maXaPhuong;
    private Integer maXaPhuongBhxh;
    private String tenXaPhuong;
    private Integer maThonXomBhxh;
    private String tenThonXom;
    private Integer tongTien;
    private Integer tongTienDaChi;
    private Integer tongTienConLai;
}
