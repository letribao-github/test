
package com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ThemChinhSachDto {
    private String tenChinhSach;
    private String maChinhSach;
    private Boolean trangThai;
    private Integer mucHuong;
    private Boolean isHGD;
}
