package com.vnptit5.ptgp.ldtbxh.dto.response.nguoicocong;

public class RecordReport {
    private String HoSoID;

    private String LoaiHoSoTen;

    private String CumToTiepNhan;

    private String SoHoSoTinhQuanLy;

    private String NamSinh;

    private String Gender;

    private String SoBHYT;

    private String TaiKhoanCapNhat;
}
