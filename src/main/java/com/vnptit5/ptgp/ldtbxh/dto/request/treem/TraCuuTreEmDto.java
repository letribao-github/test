package com.vnptit5.ptgp.ldtbxh.dto.request.treem;

import lombok.AllArgsConstructor;
import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * @author trinhctn
 */
@Data
@AllArgsConstructor
public class TraCuuTreEmDto {
    @NotBlank(message = "Mã tỉnh không được để trống")
    private String maTinh;

    @NotBlank(message = "Mã huyện không được để trống")
    private String maHuyen;

    private String maXa;

    private String maThon;
    private String hoTen;
    private String soCmnd;
    private String ngaySinh;
    private String maSo;
    private String maThe;
    private int page;
    private int size;
    private String propertyName;
    private String direction;
    private String phoneNumber;
    private Boolean check;
}
