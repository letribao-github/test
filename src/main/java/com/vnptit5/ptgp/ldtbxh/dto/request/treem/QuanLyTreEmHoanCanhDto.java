package com.vnptit5.ptgp.ldtbxh.dto.request.treem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuanLyTreEmHoanCanhDto {
    private String maTinhThanh;
    private String maQuanHuyen;
    private String maXaPhuong;
    private String maThonXom;
    private Integer maLoaiHoanCanh;
    private Integer maChiTietHoanCanh;
    private String tenTreEm;
    private Integer pageNumber;
    private Integer pageSize;
    private Integer maHoGiaDinh;
}
