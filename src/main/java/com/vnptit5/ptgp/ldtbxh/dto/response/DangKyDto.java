package com.vnptit5.ptgp.ldtbxh.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DangKyDto {
    private int statusCode;
    private String thongBao;
    private int id;
}
