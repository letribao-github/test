package com.vnptit5.ptgp.ldtbxh.dto.request.nguoidung;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class TimKiemNguoiDungDto {
    private String hoTen;
    private Integer maPhongBan;
    private Integer maChucDanh;
    private Boolean trangThai;
    private Integer maDonVi;
}
