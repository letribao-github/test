package com.vnptit5.ptgp.ldtbxh.dto.request.treem;

import com.vnptit5.ptgp.ldtbxh.dto.request.ExcelType;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class BaoCaoTreEmDto {
    private String maTinh;
    private String maHuyen;
    private String maXa;
    private String maThon;
    private Integer tuoiTu;
    private Integer denTuoi;
    private String tuNgay;
    private String denNgay;
    private ExcelType excelType;
}
