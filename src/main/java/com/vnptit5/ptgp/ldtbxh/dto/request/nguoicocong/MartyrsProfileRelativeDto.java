package com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ToiTV
 * 2020-10-22
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MartyrsProfileRelativeDto {

    private Integer     thanNhanLietSyID;

    private String      hoVaTen;

    private Date        namSinh;

    private Integer     moiQuanHeID;

    private String      moiQuanHe;

    private String      choOHienNay;

    private String      ngheNghiep;

    private String      chucVu;

    private Integer     troCapMotLan;

    private Integer     troCapCoBan;

    private Integer     troCapNuoiDuong;

    private String      chuaHuong;

    private Date        truyLinhTu;

    private Integer     tongSoTienTruyLinh;

    private String      ghiChu;

    private Integer     hoSoID;

    private Integer     conSong;
}
