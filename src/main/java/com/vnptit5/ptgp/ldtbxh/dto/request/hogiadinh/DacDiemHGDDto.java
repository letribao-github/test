package com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DacDiemHGDDto {
    private Integer idDacDiemHGD;
    private Integer tinhTrangNhaO;
    private Integer loaiHoXi;
    private Integer loaiDien;
    private Integer soNguoiTrongHo;
    private Integer soHuuNha;
    private Integer diemB1;
    private Integer dienTichNha;
    private Integer diemB2;
    private Boolean dienThoai;
    private Boolean internet;
    private Boolean tivi;
    private Boolean radio;
    private Boolean mayTinh;
    private Boolean loa;
    private Integer nuocSinhHoat;
    private Boolean hoTroYTe;
    private Boolean hoTroTinhDung;
    private Boolean hoTroNhaO;
    private Boolean hoTroSanXuat;
    private Boolean hoTroGiaoDuc;
    private Integer soNguoiHuongTCXH;
}
