package com.vnptit5.ptgp.ldtbxh.dto.response;

/**
 * @author trinhctn
 */
public class CustomException extends RuntimeException {

    public CustomException(String message) {
        super(message);
    }

}
