package com.vnptit5.ptgp.ldtbxh.dto.request.thanhvien;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ThongTinHoNgheoDto {
    private Integer maThanhVien;

    private Integer trinhDoHocVan;
    private Integer doiTuongChinhSach;
    private Integer khamChuaBenh;
    private Boolean mangThai;
    private Boolean doiTuongBTXH;
    private Integer tinhTrangViecLam;
    private Integer lyDoKhongKCB;
    private String ngayDuSinh;
    private Boolean doiTuongBTXHNgoaiHo;
}
