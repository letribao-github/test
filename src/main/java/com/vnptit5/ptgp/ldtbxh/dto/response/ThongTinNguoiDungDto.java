package com.vnptit5.ptgp.ldtbxh.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ThongTinNguoiDungDto {
    private String hoTen;
    private String tenPhongBan;
    private String tenDangNhap;
    private String tenChucDanh;
    private String sdt;
    private String quyenXuLy;
    private boolean trangThai;
}
