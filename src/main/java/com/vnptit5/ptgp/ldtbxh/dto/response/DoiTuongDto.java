package com.vnptit5.ptgp.ldtbxh.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DoiTuongDto {
    private Integer idDoiTuong;
    private String tenDoiTuong;
    private Float heSo;
    private Integer idLoaiDoiTuong;
    private Boolean hgd;
    private Boolean trangThai;
    private Integer idChinhSach;
}
