package com.vnptit5.ptgp.ldtbxh.dto.request.thanhvien;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DanhSachNhanKhauHuongChinhSachDto {
    private Integer maTinh;
    private Integer maHuyen;
    private Integer maXa;
    private Integer maThon;
    private String tenNhanKhau;
    private Integer trangThai;
    private Integer maLoaiChinhSach;
    private List<Integer> dsDoiTuong;
    private Integer pageNumber;
    private Integer pageSize;
}
