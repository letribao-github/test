package com.vnptit5.ptgp.ldtbxh.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.Date;

/**
 * @author trinhctn
 */
@Data
@AllArgsConstructor
public class ThongTinTreEmDto {
    private String maHo;
    private String maThe;
    private String hoTen;
    private Date ngaySinh;
    private String maDinhDanh;
    private String noiKs;
    private String soCmnd;
    private String diaChi;
    private String maXa;
    private String maSoBhxh;
    private String soBhxhCu;
    private String maDvi;
    private String tenDvi;
    private Integer maTinhThanh;
    private Integer maQuanHuyen;
    private Integer maXaPhuong;
    private Integer maThonXom;
}
