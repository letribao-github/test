package com.vnptit5.ptgp.ldtbxh.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Benjamin Lam on 10/21/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UploadFileResponseDto {
    private String fileName;
    private String fileDownloadUri;
    private String fileType;
    private long size;
}
