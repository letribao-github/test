

package com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ThemDoiTuongDto {
    private String tenDoiTuong;
    private Double mucHuong;
    private Integer idLoaiCs;
    private Integer idDoiTuong;
}
