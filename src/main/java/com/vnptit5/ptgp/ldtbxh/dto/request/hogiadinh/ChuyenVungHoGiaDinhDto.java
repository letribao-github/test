package com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ChuyenVungHoGiaDinhDto {
    private String maTinh;
    private String maHuyen;
    private String maXa;
    private String maThon;
    private Integer maHoGD;
}
