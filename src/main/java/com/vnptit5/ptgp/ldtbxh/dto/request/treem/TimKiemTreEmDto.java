package com.vnptit5.ptgp.ldtbxh.dto.request.treem;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */

@Data
@NoArgsConstructor
public class TimKiemTreEmDto {
    private String maTinhThanh;
    private String maQuanHuyen;
    private String maXaPhuong;
    private String maThonXom;
    private String tenTreEm;
    private String tenChaMe;
    private Integer maHoGiaDinh;
    private Boolean trangThai;
    private Integer gioiTinh;
    private Boolean capNhat;
    private Integer pageNumber;
    private Integer pageSize;
}
