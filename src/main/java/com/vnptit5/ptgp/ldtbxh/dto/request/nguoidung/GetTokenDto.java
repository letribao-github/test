package com.vnptit5.ptgp.ldtbxh.dto.request.nguoidung;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @author trinhctn
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class GetTokenDto {

    @NotBlank(message = "Tên đăng nhập không được để trống")
    private String tenDangNhap;

    @NotBlank(message = "Mật khẩu không được để trống")
    private String matKhau;

}
