package com.vnptit5.ptgp.ldtbxh.dto.response.household;

import com.vnptit5.ptgp.ldtbxh.dto.pojo.household.MemberHouseHoldGetDto;
import com.vnptit5.ptgp.ldtbxh.dto.pojo.household.OwnerHouseHoldGetDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import java.util.List;

/**
 * Created by Benjamin Lam on 10/14/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HouseHoldGetResponseDto {
    private OwnerHouseHoldGetDto owner;
    private List<MemberHouseHoldGetDto> memberList;
}