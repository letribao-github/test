package com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DanhSachCatChetDto {
    private Integer maTinh;
    private Integer maHuyen;
    private Integer maXa;
    private Integer maThon;
    private Integer idNhanKhau;
    private String  tenNhanKhau;
}
