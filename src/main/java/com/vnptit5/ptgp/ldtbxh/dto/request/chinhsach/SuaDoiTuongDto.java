
package com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class  SuaDoiTuongDto  {
    private Integer idDoiTuong;
    private String tenDoiTuong;
    private float heSo;
    private Boolean isHGD;
    private Boolean trangThai;
}

