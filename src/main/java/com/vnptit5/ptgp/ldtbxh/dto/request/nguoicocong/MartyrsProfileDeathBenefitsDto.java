package com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ToiTV
 * 2020-10-22
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MartyrsProfileDeathBenefitsDto {

    private Integer     iD;

    private Integer     hoSoID;

    private String      hoTenNguoiHuong;

    private String      qdSo;

    private Integer     namSinhNguoiHuong;

    private Integer     quanHeVoiNguoiCoCong;

    private String      nguyenQuanNguoiHuong;

    private String      truQuanNguoiHuong;

    private Integer     maiTangPhi;

    private Integer     troCapCacThang;

    private Date        ngayRaQD;

    private String      noiRaQD;
}
