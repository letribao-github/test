package com.vnptit5.ptgp.ldtbxh.dto.response.lifeinfo;

import com.vnptit5.ptgp.ldtbxh.dto.base.GetResponseBaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * Created by Benjamin Lam on 10/20/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class IncomeTypeHomeStatusGetResponseDto {
    List<GetResponseBaseDto> listIncomeType;
    List<GetResponseBaseDto> listHomeStatus;
}
