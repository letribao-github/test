package com.vnptit5.ptgp.ldtbxh.dto.request.nguoidung;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author trinhctn
 */
@Data
@NoArgsConstructor
public class CapNhatNguoiDungDto {
    private int maNguoiDung;
    private int maPhongBan;
    private int maChucDanh;
    private String tenDangNhap;
    private String matKhau;
    private String hoTen;
    private String sdt;
    private boolean trangThai;
    private Boolean them;
    private Boolean sua;
    private Boolean xoa;
    private Boolean xem;
    private List<Integer> dsNhomQuyen;
}

