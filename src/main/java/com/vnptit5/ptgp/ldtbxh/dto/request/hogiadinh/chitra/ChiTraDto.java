package com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh.chitra;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ChiTraDto {
    private String maTinh;
    private String maHuyen;
    private String maXa;
    private String maThon;
    private Boolean trangthaiLanh;
    private String namHuong;
    private String thangHuong;
}
