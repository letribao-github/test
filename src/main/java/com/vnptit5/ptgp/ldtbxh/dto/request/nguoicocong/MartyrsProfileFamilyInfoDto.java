package com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ToiTV
 * 2020-10-22
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MartyrsProfileFamilyInfoDto {

    private Integer     hoSoLietSyID;

    private Date        hoTroTienSuDungDatNam;

    private Integer     tongSoTienHoTroSuDungDat;

    private Date        duocHoaGiaNhaNam;

    private Integer     tongSoTienCapSuaChuaNha;

    private String      chuaDuocHoTro;

    private String      tinhTrangSucKhoe;

    private String      conLietSyDangDiHoc;

    private String      conLietSyDenTuoiKhongDiHoc;

    private String      conLietSyBiTanTat;

    private String      hoTenNguoiThoCung;

    private Integer     quanHeVoiLietSy;

    private Date        namSinhNguoiThoCung;

    private String      choONguoiThoCung;

    private Integer     nguonGocHoSoID;

    private Date        namChuyenDen;

    private Integer     hoSoChuyenDiTinh;

    private Date        namChuyenDi;

    private String ghiChu;
}
