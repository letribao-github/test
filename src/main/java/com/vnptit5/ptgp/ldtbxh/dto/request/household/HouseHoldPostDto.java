package com.vnptit5.ptgp.ldtbxh.dto.request.household;

import com.vnptit5.ptgp.ldtbxh.dto.base.HouseHoldBaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by Benjamin Lam on 10/14/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HouseHoldPostDto extends HouseHoldBaseDto {
    private String identityNumber;
    private String profileNumber;
    private String mainAddressNumber;
    private String paperNumber;
    private String ownerName;
    private String ownerNickName;
    private String citizenID;
    private Boolean gender;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date ownerBirthday;
    private String mainAddress;
    private String oldAddress;
    private Integer nation;
    private String nationality;
    private String job;
    private String beforeAddress;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date signedDate;
    private String signedOfficerName;
    private Integer signedOfficerLevel;
    private String mainOfficerName;
    private Integer mainOfficerLevel;
    private Integer mainOfficerOrganLevel;
}