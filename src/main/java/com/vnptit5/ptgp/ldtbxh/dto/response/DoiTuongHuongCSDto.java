package com.vnptit5.ptgp.ldtbxh.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;

/**
 * @author trinhctn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DoiTuongHuongCSDto {
//    private Integer idHuongChinhSach;
//    private Integer tenDoiTuong;
//    private Integer heSo;
//    private String tenChinhSach;
//    private String ngayHuong;
//    private Long soTienHuong;
//    private String ttHuong;
//    private String tenNhanKhau;
//    private Integer maHoGiaDinh;
    private Integer tongTienHuong;
    private Collection<?> data;
}
