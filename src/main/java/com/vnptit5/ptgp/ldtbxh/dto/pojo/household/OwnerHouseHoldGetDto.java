package com.vnptit5.ptgp.ldtbxh.dto.pojo.household;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by Benjamin Lam on 10/15/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class OwnerHouseHoldGetDto {
    private String identityNumber;
    private String profileNumber;
    private String mainAddressNumber;
    private String paperNumber;
    private String ownerName;
    private String ownerNickName;
    private String citizenID;
    private Boolean gender;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date ownerBirthday;
    private String mainAddress;
    private String oldAddress;
    private String nation;
    private String nationality;
    private String job;
    private String beforeAddress;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date signedDate;
    private String signedOfficerName;
    private String signedOfficerLevel;
    private String mainOfficerName;
    private String mainOfficerLevel;
    private String mainOfficerOrganLevel;
    private boolean isDelete;
    private String reasonDelete;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createdDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date updatedDate;
}