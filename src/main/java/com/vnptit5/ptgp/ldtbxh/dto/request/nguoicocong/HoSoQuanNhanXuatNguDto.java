package com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
/*
 * Author: huylt.tgg
 * Date: 19/10/2020
 * */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HoSoQuanNhanXuatNguDto {
    // Thong tin ho so
    private Integer hosoid;
    private Integer bansao;
    private String sohosotinhquanly;
    private String sohosoboquanly;
    private Integer loaihosoid;
    private Integer huyentiepnhanid;
    private Integer xatiepnhanid;
    private String cumtotiepnhan;
    private String hovaten;
    private String bidanh;
    private String ngaysinh;
    private String namsinh;
    private Integer gioitinh;
    private Integer dantocid;
    private String quequan;
    private String choohiennay;
    private String cacchedokhac;
    private Integer hosochuyendentuid;
    private String thoigianchuyenden;
    private Integer hosochuyendiid;
    private String thoigianchuyendi;
    private String taikhoancapnhat;
    private String vitriluutruhoso;
    private String sobhyt;
    private String ngayvaodang;
    private String chinhthuc;
    private String ngaynhapngu;
    private String donvinhapngu;
    private String noinhapngu;
    private String ngayxuatngu;
    private String donvixuatngu;
    private String ngaytaingu;
    private String donvitaingu;
    private String ngayphucvien;
    private String donviphucvien;
    private String capbacphucvienxuatngu;
    private String chucvuphucvienxuatngu;
    private String donviphucvienxuatngu;
    private String tongsothoigianphucvu;
    private String sonamquydoi;
    private String nghenghiepphucvienxuatngu;
    private String thoigianochientruongk;
    private String ngaytutran;
    private String soquyetdinhmaitangphi;
    private String sotienmaitangphi;
    private Integer chedoquannhanid;
    private String giaytoconluugiu;
    private Integer thoikyid;
    private Integer tinhtranghiennayid;
    private Integer doituonghuongchedobhyt;
    // Thong tin tro cap phu cap
    private Integer thongtinchedoid;
    private String thoigianhuongtrocap;
    private String quyetdinhhuongtrocap;
    private String ngayraquyetdinhhuongtrocap;
    private String noiraquyetdinhhuongtrocap;
    private Integer sotientrocap;
    private String truylinhtungay;
    private String truylinhdenngay;
    private Integer sotientruylinh;
    // Thong tin qua trinh cong tac
    private Integer quatrinhcongtacid;
    private String tungay;
    private String denngay;
    private String capbac;
    private String chucvu;
    private String donvicongtac;
    private String diabancongtac;
    // Tro Cap Khi Qua Doi
    private Integer id;
    private String hotennguoihuong;
    private String namsinhnguoihuong;
    private Integer quanhevoinguoicocong;
    private String nguyenquannguoihuong;
    private String truquannguoihuong;
    private Integer maitangphi;
    private Integer trocapcacthang;
    private String qdso;
    private String ngayraqd;
    private String noiraqd;


}
