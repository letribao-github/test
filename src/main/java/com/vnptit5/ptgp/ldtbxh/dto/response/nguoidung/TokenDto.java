package com.vnptit5.ptgp.ldtbxh.dto.response.nguoidung;

import lombok.Data;

import java.time.LocalDateTime;
import java.util.Date;

/**
 * @author trinhctn
 */
@Data
public class TokenDto {
    private String accessToken;
    private String tokenType = "Bearer";
    private Date expiresIn;
    private Date issueAt;
    private int statusCode;

    public TokenDto(String accessToken, Date expiresIn, Date issueAt) {
        this.accessToken = accessToken;
        this.expiresIn = expiresIn;
        this.issueAt = issueAt;
    }
}
