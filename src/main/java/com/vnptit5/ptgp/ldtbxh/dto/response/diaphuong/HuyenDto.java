package com.vnptit5.ptgp.ldtbxh.dto.response.diaphuong;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class HuyenDto {
    private String maTinhThanh;
    private String maQuanHuyen;
    private String tenQuanHuyen;
    private String maTinhBhxh;
    private String maQuanHuyenBhxh;

}
