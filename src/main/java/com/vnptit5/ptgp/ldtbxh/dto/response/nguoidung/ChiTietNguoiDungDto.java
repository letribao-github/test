package com.vnptit5.ptgp.ldtbxh.dto.response.nguoidung;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author trinhctn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChiTietNguoiDungDto {
    private Integer maNguoiDung;
    private String tenDangNhap;
    private Boolean trangThai;
    private String hoTen;
    private String sdt;
    private String quyenXuLy;
    private String tenPhongBan;
    private String tenChucDanh;
    private String tenDonVi;
    private Integer capBacDv;
    private String maXaPhuong;
    private String maXaPhuongBhxh;
    private String tenXaPhuong;
    private String maQuanHuyen;
    private String maQuanHuyenBhxh;
    private String tenQuanHuyen;
    private String maTinhThanh;
    private String maTinhBhxh;
    private String tenTinhThanh;
    private Boolean them;
    private Boolean sua;
    private Boolean xoa;
    private Boolean xem;
    private Integer maPhongBan;
    private Integer maChucDanh;
    private Integer maDonVi;
    private List<?> danhSachNhomQuyen;
}
