package com.vnptit5.ptgp.ldtbxh.dto.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Benjamin Lam on 10/16/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class GetResponseBaseDto {
    private Integer levelID;
    private String levelName;
}
