package com.vnptit5.ptgp.ldtbxh.dto.response.nguoicocong;

import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ToiTV
 * 2020-10-18
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class RecordCategory {
    private List<Map<String, Object>> PROFILE_TYPE;
    private List<Map<String, Object>> PROVINCE;
    private List<Map<String, Object>> NATION;
    private List<Map<String, Object>> PERIOD;
    private List<Map<String, Object>> SUBJECT;
    private List<Map<String, Object>> TOTAL_MARTYRS;
}
