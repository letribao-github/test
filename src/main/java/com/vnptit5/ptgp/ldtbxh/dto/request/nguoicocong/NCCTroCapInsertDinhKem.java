package com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NCCTroCapInsertDinhKem
{
    private Integer hosoID;
    private String  tenFile;
    private Date    ngayCapNhat;
    private String  ghiChu;
    private String  duongDan;
    private String  thuMuc;
}
