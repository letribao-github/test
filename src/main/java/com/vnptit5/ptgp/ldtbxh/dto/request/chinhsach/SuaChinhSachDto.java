package com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class SuaChinhSachDto {
    private Integer idChinhSach;
    private String tenChinhSach;
    private String maChinhSach;
    private Float mucHuong;
    private Boolean trangThai;
    private Boolean isHGD;

}
