package com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ThemMoiHoGiaDinhDto {
    private String maTinh;
    private String maHuyen;
    private String maXa;
    private String maThon;
    private Integer khuVuc;
    private String diaChi;
    private Integer soHoKhau;
    private String maHoBhxh;
    private Integer maHoanCanhGiaDinh;

    private Integer maChiTietHoNgheo;
    private Integer phanLoaiHoNgheo;
    private String ghiChu;
    private String tenChuHo;
    private String idChuHo;
    private String quanHeChuHo;
    private String idDacDiemHoGiaDinh;
    private String soCmndChuHo;
    private String soDienThoaiChuHo;

    private Integer idDacDiemHGD;
    private Integer tinhTrangNhaO;
    private Integer loaiHoXi;
    private Integer loaiDien;
    private Integer soNguoiTrongHo;
    private Integer soHuuNha;
    private Integer diemB1;
    private Integer dienTichNha;
    private Integer diemB2;
    private Boolean dienThoai;
    private Boolean internet;
    private Boolean tivi;
    private Boolean radio;
    private Boolean mayTinh;
    private Boolean loa;
    private Integer nuocSinhHoat;
    private Boolean hoTroYTe;
    private Boolean hoTroTinhDung;
    private Boolean hoTroNhaO;
    private Boolean hoTroSanXuat;
    private Boolean hoTroGiaoDuc;
    private Integer soNguoiHuongTCXH;

    //    private String tenNhanKhau;
    private String ngaySinh;
    private Integer danToc;
    private Integer gioiTinh;
    //    private String soCmnd;
    private String maDinhDanh;

    private Integer idNhanKhau;
}
