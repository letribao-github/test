package com.vnptit5.ptgp.ldtbxh.dto.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @author trinhctn
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class DonViDto {
    private Integer maTinhThanh;
    private Integer maQuanHuyen;
    private Integer maXaPhuong;
}
