package com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder

public class NCCTTTroCapPhanTrang
{
    private Integer hosoID;
    private Integer pageSize;
    private Integer pageNumber;
    private Integer soDong;
    private Integer soTrang;
}
