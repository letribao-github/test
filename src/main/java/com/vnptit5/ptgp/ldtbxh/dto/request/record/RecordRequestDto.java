package com.vnptit5.ptgp.ldtbxh.dto.request.record;

import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

/*
 * Author: SonNVT
 * Date: 15/10/2020
 * */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Nullable
public class RecordRequestDto {
    @Nullable
    public Integer recordTypeId;
    @Nullable
    public String numRecordProvince;
    @Nullable
    public Integer provinceId;
    @Nullable
    public Integer districtId;
    @Nullable
    public Integer townId;
    @Nullable
    public String fullName;
    @Nullable
    public String birthDay;
    @Nullable
    public Integer gender;
    @Nullable
    public Integer ethnicId;
    @Nullable
    public Boolean isCopy;
    @Nullable
    public String numHealthInsurance;
    @NotNull
    public Integer page = 1;
    @NotNull
    public Integer pageSize;
}
