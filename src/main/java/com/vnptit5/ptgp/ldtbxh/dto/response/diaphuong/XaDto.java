package com.vnptit5.ptgp.ldtbxh.dto.response.diaphuong;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class XaDto {
    private String maQuanHuyen;
    private String maXaPhuong;
    private String tenXaPhuong;
    private String maQuanHuyenBhxh;
    private String maXaPhuongBhxh;
}