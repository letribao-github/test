package com.vnptit5.ptgp.ldtbxh.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DonViDto {
    private Integer maDonVi;
    private String tenDonVi;
    private Integer idCha;
    private String capBacDv;
    private Integer maXaPhuong;
    private Integer maQuanHuyen;
    private Integer maTinhThanh;
    private Boolean trangThai;

}
