package com.vnptit5.ptgp.ldtbxh.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Collection;
import java.util.Map;

/**
 * @author trinhctn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ThongTinThanhVienChiTietDto {
    private Collection<?> chinhSachThanhVien;
    private Map<String, Object> thongTinHoNgheo;
    private Map<String, Object> thongTinBTXH;
    private Map<String, Object> thongTinChung;
}
