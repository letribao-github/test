package com.vnptit5.ptgp.ldtbxh.dto.request.treem;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author trinhctn
 */
@Data
@NoArgsConstructor
public class CapNhatTreEmDto {
    private Integer maTreEm;
    private String tenTreEm;
    private String ngaySinh;
    private String danToc;
    private String gioiTinh;
    private String trinhDo;
    private String tinhTrangHoc;
    private String maSoBhxh;
    private String maSoBhxhCu;
    private String maDonViBhxh;
    private String tenDonViBhxh;
    private String diaChi;
    private String noiKhaiSinh;
    private String soCmnd;
    private String maDinhDanh;
    private String maTreEmChuoi;
    private String maTinhThanh;
    private String maQuanHuyen;
    private String maXaPhuong;
    private String maThon;
    private List<Integer> dsMaHoanCanh;
    private List<Integer> dsMaChiTietHuongTroGiup;
    private String hoTenCha;
    private String hoTenMe;
    private String hoTenNguoiNuoiDuong;
    private String sdtGiaDinh;
    private Integer maHoGiaDinh;
    private Boolean capNhat;
    private Integer maHoanCanhGiaDinh;
    private String maHoGiaDinhBhxh;
    private String tenNguoiGiamHo;
    private String ghiChu;
}
