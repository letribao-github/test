package com.vnptit5.ptgp.ldtbxh.dto.export;

import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExport;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportAreas;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportForm;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportGeneralConfig;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportList;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportList.Direction;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ToiTV
 * 2020-10-17
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ExcelExportGeneralConfig(exportNameCombine = "Profile", exportName = "Profile", template = "DS_DONVI.xlsx", startRow = 12, hasSampleRow = true, isEvaluateFormula = false)
public class RecordExcelExportDto {

    @ExcelExportForm(sheetIndex = 0, cell = "J9")
    private String name;

    @ExcelExportForm(sheetIndex = 0, cell = "J10")
    private String date;

    @ExcelExportForm(startRow=12, sheetIndex = 0)
    List<RecordData> data;

    @Data
    public static class RecordData {
        @ExcelExport(index = 2)
        private String HoSoId;

        @ExcelExport(index = 3)
        private String HoVaTen;

        @ExcelExport(index = 4)
        private String LoaiHoSoTen;

        @ExcelExport(index = 5)
        private String CumToTiepNhan;

        @ExcelExport(index = 6)
        private String SoHoSoTinhQuanLy;

        @ExcelExport(index = 7)
        private String NamSinh;

        @ExcelExport(index = 8)
        private String Gender;

        @ExcelExport(index = 9)
        private String SoBHYT;

        @ExcelExport(index = 10)
        private String TaiKhoanCapNhat;
    }
}
