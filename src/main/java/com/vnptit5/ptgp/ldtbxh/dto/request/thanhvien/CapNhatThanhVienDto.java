package com.vnptit5.ptgp.ldtbxh.dto.request.thanhvien;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @author trinhctn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class CapNhatThanhVienDto {
    private Integer idNhanKhau;

    //Thông tin chung
    private Integer maHoGiaDinh;
    private String tenNhanKhau;
    private String ngaySinh;
    private String diaChi;
    private String maTinh;
    private String maHuyen;
    private String maXa;
    private String maThon;
    private Integer danToc;
    private Integer gioiTinh;
    private String soCmnd;
    private String maDinhDanh;
    private Boolean tinhTrangDiHoc;
    private Integer capHoc;
    private Boolean theBHYT;
    private Integer loaiTheBHYT;
    private String soTheBHYT;
    private String quanHeChuHo;

    private Integer maThanhVien;

    //Thông tin hộ nghèo
    private Integer idThongTinHN;
    private Integer trinhDoHocVan;
    private Integer doiTuongChinhSach;
    private Integer khamChuaBenh;
    private Boolean mangThai;
    private Boolean doiTuongBTXH;
    private Integer tinhTrangViecLam;
    private Integer lyDoKhongKCB;
    private String ngayDuSinh;
    private Boolean doiTuongBTXHNgoaiHo;

    //Thông tin bảo trợ xã hội
    private Integer idThongTinBTXH;
    private Boolean nhanTCXHThang;
    private String nguoiNuoiDuong;
    private String thongTinCha;
    private String thongTinMe;
    private Integer tinhTrangHonNhan;
    private String thanhVienLaCon;
    private Boolean thamGiaLamViec;
    private Boolean tuPhucVu;
    private String quaTrinhHoatDong;
    private Boolean vanDong;
    private Boolean nghe;
    private Boolean noi;
    private Boolean nhin;
    private Boolean thanKinh;
    private Boolean triTue;
    private Boolean dangTatKhac;
    private Integer mucDoKhuyetTat;
    private String kinhNghiemChamSocDT;
    private Boolean anTu;
    private Boolean benhManTinh;
    private String viPham;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date ngayNhiemHIV;
}
