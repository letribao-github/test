package com.vnptit5.ptgp.ldtbxh.dto.response.attachedfile;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.File;
import java.util.Date;

/**
 * Created by Benjamin Lam on 10/22/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AttachedFileListGetResponseDto {
    private Integer fileID;
    private String fileName;
    private String originalFileName;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updatedDate;
    private String fileType;
    private String note;
    private String folder;

    @JsonIgnore
    private String path;

    @JsonIgnore
    public String toFullFilePath() {
        return path + File.separatorChar + originalFileName;
    }
}
