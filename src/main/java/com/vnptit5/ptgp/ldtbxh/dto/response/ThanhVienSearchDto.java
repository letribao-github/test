package com.vnptit5.ptgp.ldtbxh.dto.response;

import com.vnptit5.ptgp.ldtbxh.dto.export.RecordExcelExportDto;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExport;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportForm;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportGeneralConfig;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
@ExcelExportGeneralConfig(exportNameCombine = "Profile", exportName = "Profile", template = "02.BAOCAOTV.xlsx", startRow = 12, hasSampleRow = true, isEvaluateFormula = false)
public class ThanhVienSearchDto {

    @ExcelExportForm(startRow=1, sheetIndex = 0)
    List<DataList> data;


    @Data
    public static class DataList {
        @ExcelExport(index = 0)
        private String MA_HO_GIA_DINH;

        @ExcelExport(index = 1)
        private String ID_NHAN_KHAU;

        @ExcelExport(index = 2)
        private String TEN_NHAN_KHAU;

        @ExcelExport(index = 3)
        private String NGAY_SINH;

        @ExcelExport(index = 4)
        private String GIOI_TINH;

        @ExcelExport(index = 5)
        private String DAN_TOC;

        @ExcelExport(index = 6)
        private String TEN_PHUONG_XA;

        @ExcelExport(index = 7)
        private String TEN_THON_XOM;

    }
}