package com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author trinhctn
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TimKiemHoGiaDinhPhanTrangDto {
    private String maTinh;
    private String maHuyen;
    private String maXa;
    private String maThon;
    private String soCMNDChuHo;
    private String tenChuHo;
    private Boolean doiTuongBTXH;
    private Boolean nguoiNhanTCXH;
    private List<Integer> phanLoaiHo;
    private Integer trangThaiXoa;
    private Integer pageNumber;
    private Integer pageSize;

}
