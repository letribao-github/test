package com.vnptit5.ptgp.ldtbxh.dto.response;

import io.swagger.v3.oas.models.security.SecurityScheme;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DanhSachTreEmDto {
    private Integer maTreEm;
    private String tenTreEm;
    private String ngaySinh;
    private String danToc;
    private String gioiTinh;
    private String trinhDo;
    private String tinhTrangHocTap;
    private String ngayTao;
//    private String ngayCapNhat;
    private Integer maHoGiaDinh;
    private Boolean trangThai;
    private String maHoGiaDinhBhxh;
    private String maSoBhxh;
    private String maSoBhxhCu;
    private String diaChi;
    private String noiKhaiSinh;
    private String soCmnd;
    private String maDinhDanh;
    private String maTreEmChuoi;
    private String hoTenCha;
    private String hoTenMe;
    private Integer maThonXom;
    private Integer maXaPhuong;
    private Integer maTinhThanh;
    private Integer maQuanHuyen;
    private Integer maHoanCanhGiaDinh;
    private String tenThonXom;
    private String tenXaPhuong;
    private String tenQuanHuyen;
    private Boolean capNhat;

}
