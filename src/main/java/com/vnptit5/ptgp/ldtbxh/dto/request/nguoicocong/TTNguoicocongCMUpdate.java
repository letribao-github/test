package com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TTNguoicocongCMUpdate
{
    private Integer HoSoNguoiCoCongGiupCMID;
    private Integer banSao;
    private String soHoSoTinhQuanLy;
    private String soHoSoBoQuanLy;
    private Integer quanHuyenTiepNhan;
    private String cumTo;
    private Integer hoSoChuyenDenTuID;
    private Integer hoSoChuyenDiID;
    private String hoSoQuanLy;
    private Integer phuongXaTiepNhan;
    private String soBHXH;
    private Integer thoiGianChuyenDen;
    private Integer thoiGianChuyenDi;
    private String hoVaTen;
    private String biDanh;
    private Integer ngaySinh;
    private Integer gioiTinh;
    private Integer danToc;
    private String queQuan;
    private String choOHiennay;
    private String duocNhaNuocPhongTang;
    private String quyetDinhSo;
    private Integer ngayRaQuyetDinh;
    private Integer thangRaQuyetDinh;
    private Integer namRaQuyetDinh;
    private Integer cheDoDuocHuong;
    private String hoanCanhHienTai;
    private Integer thoiKy;
    private String viTriLuuHoSo;
    private String cacCheDoKhac;
    private String tinhTrangCanhan;
}
