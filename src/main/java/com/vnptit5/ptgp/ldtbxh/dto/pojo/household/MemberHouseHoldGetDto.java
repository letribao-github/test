package com.vnptit5.ptgp.ldtbxh.dto.pojo.household;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by Benjamin Lam on 10/14/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MemberHouseHoldGetDto {
    private String memberId;
    private String identityNumber;
    private String memberName;
    private String memberNickName;
    private String citizenID;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date memberBirthday;
    private Boolean memberGender;
    private String memberNation;
    private String memberNationality;
    private String memberJob;
    private String relationship;
    private String beforeAddress;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date signedDate;
    private String signedOfficerName;
    private String signedOfficerLevel;
    private String mainOfficerName;
    private String mainOfficerLevel;
    private String mainOfficerOrganLevel;
    private boolean isDelete;
    private String reasonDelete;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date createdDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date updatedDate;
}