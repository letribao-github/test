package com.vnptit5.ptgp.ldtbxh.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ThongKeChiTraChiTietDto {
    private Integer maHoGiaDinh;
    private Integer idChuHo;
    private String tenChuHo;
    private Integer soTienHuong;
    private Integer tongSoTienHuong;
    private Integer idNhanKhau;
}
