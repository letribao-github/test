package com.vnptit5.ptgp.ldtbxh.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PhongBanDto {
    private Integer maDonVi;
    private Integer maPhongBan;
    private String tenPhongBan;
}
