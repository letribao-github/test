package com.vnptit5.ptgp.ldtbxh.dto.request.subsidy;

import java.util.Date;
import javax.annotation.Nullable;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ToiTV
 * 2020-10-21
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SubsidyDtoRequest {

    @Nullable
    private int thongTinCheDoID;

    @NotNull
    private int hoSoID;

    @Nullable
    private int maiTangPhi;

    @Nullable
    private Date namSinhNguoiHuong;

    @Nullable
    private Date ngayRaQuyetDinhHuongTroCap;

    @NotNull
    private String nguoiDuocHuongTroCap;

    @Nullable
    private String nguyenQuanNguoiHuong;

    @Nullable
    private String noiRaQuyetDinhHuongTroCap;

    @Nullable
    private String quanHeVoiNguoiCoCong;

    @Nullable
    private String quyetDinhHuongTroCap;

    @Nullable
    private int soTienTroCap;

    @Nullable
    private int soTienTruyLinh;

    @Nullable
    private Date thoiGianHuongTroCap;

    @Nullable
    private int troCapCacThang;

    @Nullable
    private String truQuanNguoiHuong;

    @Nullable
    private Date truyLinhDenNgay;

    @Nullable
    private Date truyLinhTuNgay;
}
