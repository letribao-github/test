package com.vnptit5.ptgp.ldtbxh.dto.request.diaphuong;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CapNhatThonDto {
    private String maXaPhuong;
    private String maThonXom;
    private String tenThonXom;
}
