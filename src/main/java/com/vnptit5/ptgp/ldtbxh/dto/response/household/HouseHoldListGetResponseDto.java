package com.vnptit5.ptgp.ldtbxh.dto.response.household;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Benjamin Lam on 10/15/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class HouseHoldListGetResponseDto {
    private String identityNumber;
    private String profileNumber;
    private String mainAddress;
    private String paperNumber;
    private String ownerName;
    private Integer memberNumber;
    private String citizenID;
}