package com.vnptit5.ptgp.ldtbxh.dto.request.household;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by Benjamin Lam on 10/15/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
public class MemberHouseHoldPutDto {
    private Integer memberId;
    private String identityNumber;
    private String memberName;
    private String memberNickName;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date memberBirthday;
    private Boolean memberGender;
    private Integer memberNation;
    private String memberNationality;
    private String memberJob;
    private String relationship;
    private String beforeAddress;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date signedDate;
    private String signedOfficerName;
    private Integer signedOfficerLevel;
    private String mainOfficerName;
    private Integer mainOfficerLevel;
    private Integer mainOfficerOrganLevel;
}