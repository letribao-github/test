package com.vnptit5.ptgp.ldtbxh.dto.response.lifeinfo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Benjamin Lam on 10/20/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LifeInfoGetResponseDto {
    private String heroName;
    private String heroBurial;
    private String subsidizeNumber;
    private String subsidizeDate;
    private String decisionLevel;
    private String familyIncome;
    private Integer familyMember;
    private String averageIncome;
    private Integer incomeType;
    private String loanProgram;
    private Integer loanYear;
    private String loanMoney;
    private Integer savingYear;
    private String savingMoney;
    private Integer employedNumber;
    private Integer unemployedNumber;
    private String sponsor;
    private Integer homeStatus;
    private Integer homeYear;
}
