package com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TraCuuTheoMaDinhDanh {
    private String maTinh;
    private String maHuyen;
    private String maXa;
    private String maDinhDanh;
}
