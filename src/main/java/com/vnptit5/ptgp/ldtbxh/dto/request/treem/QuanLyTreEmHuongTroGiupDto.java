package com.vnptit5.ptgp.ldtbxh.dto.request.treem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuanLyTreEmHuongTroGiupDto {
    private String maTinhThanh;
    private Integer maQuanHuyen;
    private Integer maXaPhuong;
    private String maThonXom;
    private String tenTreEm;
    private Integer maLoaiHuongTroGiup;
    private Integer maChiTietHuongTroGiup;
    private Integer pageNumber;
    private Integer pageSize;
}
