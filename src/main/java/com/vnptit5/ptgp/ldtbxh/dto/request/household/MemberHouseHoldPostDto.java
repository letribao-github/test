package com.vnptit5.ptgp.ldtbxh.dto.request.household;

import com.vnptit5.ptgp.ldtbxh.dto.base.HouseHoldBaseDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Created by Benjamin Lam on 10/14/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */

@EqualsAndHashCode(callSuper = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
public class MemberHouseHoldPostDto extends HouseHoldBaseDto {
    private String identityNumber;
    private String memberName;
    private String memberNickName;
    private String citizenID;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date memberBirthday;
    private Boolean memberGender;
    private Integer memberNation;
    private String memberNationality;
    private String memberJob;
    private String relationship;
    private String beforeAddress;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date signedDate;
    private String signedOfficerName;
    private Integer signedOfficerLevel;
    private String mainOfficerName;
    private Integer mainOfficerLevel;
    private Integer mainOfficerOrganLevel;
}