package com.vnptit5.ptgp.ldtbxh.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author trinhctn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ThonDto {
    private String maXaPhuong;
    private String maThonXom;
    private String tenThonXom;
    private Boolean trangThai;
    private String maTinhBhxh;
    private String maQuanHuyenBhxh;
    private String maXaPhuongBhxh;
    private String maThonXomBhxh;
}
