package com.vnptit5.ptgp.ldtbxh.dto.request.thanhvien;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TimKiemThanhVienDto {
    private String maTinh;
    private String maHuyen;
    private String maXa;
    private String maThon;
    private String soCmnd;
    private String tenThanhVien;
    private List<Integer> phanLoaiHo;
    private List<Integer> chinhSach;
    private Boolean doiTuongBTXH;
    private Boolean nguoiNhanTCXH;
}
