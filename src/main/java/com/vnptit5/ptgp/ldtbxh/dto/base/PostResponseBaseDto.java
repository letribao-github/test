package com.vnptit5.ptgp.ldtbxh.dto.base;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by Benjamin Lam on 10/14/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PostResponseBaseDto {
    private int statusCode;
    private String message;
}
