package com.vnptit5.ptgp.ldtbxh.utils.excel.annotation;

import java.lang.annotation.*;

/**
 * @author ToiTV
 * 2020-10-17
 */

@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Target(value = { ElementType.FIELD, ElementType.METHOD, ElementType.ANNOTATION_TYPE })
public @interface ExcelExportMultiSheet {

    int sheetIndex() default 0;

    int startRow() default 0;

}
