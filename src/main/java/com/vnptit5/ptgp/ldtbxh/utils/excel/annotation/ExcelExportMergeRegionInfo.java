package com.vnptit5.ptgp.ldtbxh.utils.excel.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author ToiTV
 * 2020-10-17
 */
@Target(value = { ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelExportMergeRegionInfo {

    String offsetRow();

    String offsetColumn();

    String styleId();

    String value();

}