package com.vnptit5.ptgp.ldtbxh.utils.excel.annotation;

import java.lang.annotation.*;

/**
 * @author ToiTV
 * 2020-10-17
 */
@Target(value = { ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(ExcelExportAreaActionConfig.List.class)
public @interface ExcelExportAreaActionConfig {

    enum Action {
        ON_EQUAL_CLEAR, ON_EQUAL_RETAIN
    }

    int sheetIndex();

    String fromCell();

    String toCell();

    Action action();

    String conditionValue();

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.FIELD })
    @Documented
    @interface List {
        ExcelExportAreaActionConfig[] value();
    }
}
