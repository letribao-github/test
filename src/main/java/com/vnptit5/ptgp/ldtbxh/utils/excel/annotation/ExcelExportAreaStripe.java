package com.vnptit5.ptgp.ldtbxh.utils.excel.annotation;

import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportList.Direction;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author ToiTV
 * 2020-10-17
 */
@Target(value = { ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelExportAreaStripe {

    String colorIdField();

    String offsetColumnField();

    String offsetRowField();

    Direction direction();

}
