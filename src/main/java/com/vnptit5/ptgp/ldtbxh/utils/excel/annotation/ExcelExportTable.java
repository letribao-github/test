package com.vnptit5.ptgp.ldtbxh.utils.excel.annotation;

import java.lang.annotation.*;

/**
 * @author ToiTV
 * 2020-10-17
 */
@Target(value = { ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(ExcelExportTable.List.class)
public @interface ExcelExportTable {

    int sheetIndex();

    int startRow();

    ColumnSchema columnSchema();

    /**
     * default []
     */
    String[] columnNameFixed() default {};

    /**
     * default "A"
     */
    String startColumnName() default "A";

    /**
     * default "A"
     */
    String endColumnName() default "A";

    /**
     * default 1
     */
    int columnWidth() default 1;

    /**
     * The maximum number of row in Excel template available.<br>
     * To disable this check,<br>
     * set 0, or a negative value (e.g. -1).<br>
     */
    int rowCount() default 0;

    int regionOrder() default 0;

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.FIELD })
    @Documented
    @interface List {
        ExcelExportTable[] value();
    }

    static enum ColumnSchema {
        FIXED,
        AUTO;
    }
}

