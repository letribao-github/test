package com.vnptit5.ptgp.ldtbxh.utils.excel.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author ToiTV
 * 2020-10-17
 */
@Target(value = { ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(ExcelExportGeneralConfig.List.class)
public @interface ExcelExportGeneralConfig {

    String exportNameCombine() default "";

    String exportName();

    String template();

    int startRow() default 1;

    boolean hasSampleRow() default false;

    boolean isEvaluateFormula() default true;

    int[] lockSheetIndex() default {};

    String styleTemplateName() default "";

    Class<?>[] groups() default {};

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.TYPE })
    @Documented
    @interface List {
        ExcelExportGeneralConfig[] value();
    }
}