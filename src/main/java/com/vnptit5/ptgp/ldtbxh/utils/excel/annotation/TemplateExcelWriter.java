package com.vnptit5.ptgp.ldtbxh.utils.excel.annotation;

import com.vnptit5.ptgp.ldtbxh.common.CommonConstant;
import com.vnptit5.ptgp.ldtbxh.utils.FileUtil;
import com.vnptit5.ptgp.ldtbxh.utils.StringUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.net.URL;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.ClientAnchor;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFDataValidation;
import org.apache.poi.xssf.usermodel.XSSFDataValidationConstraint;
import org.apache.poi.xssf.usermodel.XSSFDataValidationHelper;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import com.google.common.io.Files;
import lombok.extern.slf4j.Slf4j;

/**
 * @author ToiTV
 * 2020-10-17
 */
@Slf4j
@Component(value = "templateExcelWriter")
public class TemplateExcelWriter {

    @Value("${excel.sheet.error}")
    private String folderTemp;

    public <T> File setSingleDataToTemplate(T data) {
        Class<?> classType = data.getClass();
        ExcelExportGeneralConfig config = classType.getAnnotation(ExcelExportGeneralConfig.class);
//        if (config == null) {
//            throw new ApplicationException("E018", "");
//        }
        String templateName = this.getTemplateName(classType);
        File file = null;
        File templateFile = this.getTemplateFile(templateName);
        try (Workbook workbook = this.openWorkbook(templateFile)) {
            Sheet sheet = workbook.getSheetAt(0);
            this.setSingleDataToSheet(data, sheet, config.hasSampleRow());
            file = this.writeToFile(templateFile, workbook, config);
        } catch (Exception e) {
            log.error("" + e);
            if (templateFile != null) {
                boolean deleteSuccess = templateFile.delete();
                log.debug("Delete file success: " + deleteSuccess);
            }
//            throw new ApplicationException("E018", e.getMessage());
        }
        return file;
    }

    /**
     * Set list data to template excel file
     * @return
     */
    public <T> File setListDataToTemplate(List<T> listDataInSheet, Class<T> type) {
        ExcelExportGeneralConfig config = type.getAnnotation(ExcelExportGeneralConfig.class);
//        if (config == null) {
//            throw new ApplicationException("E018", "Annotation ExcelExportGeneralConfig is null");
//        }
        String templateName = this.getTemplateName(type);
        File file = null;
        File templateFile = this.getTemplateFile(templateName);
        try (Workbook workbook = this.openWorkbook(templateFile)) {
            Sheet sheet = workbook.getSheetAt(0);
            boolean hasSampleRow = config.hasSampleRow();
            this.setDataInListToTemplate(listDataInSheet, config.startRow(), sheet,hasSampleRow, type);
            file = this.writeToFile(templateFile, workbook, config);
        } catch (Exception e) {
            log.error("" + e);
            if (templateFile != null) {
                boolean deleteSuccess = templateFile.delete();
                log.debug("Delete file success: " + deleteSuccess);
            }
//            throw new ApplicationException("E018", e.getMessage());
        }
        return file;
    }

    /**
     * Set multi data to multi sheet
     */
    @SuppressWarnings("unchecked")
    public <T> File setListDataToMultiSheet(T multiSheetData) {
        Class<T> type = (Class<T>) multiSheetData.getClass();
        ExcelExportGeneralConfig config = type.getAnnotation(ExcelExportGeneralConfig.class);
//        if (config == null) {
//            throw new ApplicationException("E018", "Annotation ExcelExportGeneralConfig is null");
//        }
        String templateName = this.getTemplateName(type);
        File file = null;
        File templateFile = this.getTemplateFile(templateName);
        try (Workbook workbook = this.openWorkbook(templateFile)) {
            this.readMultiSheetObject(multiSheetData, workbook, config.hasSampleRow());
            file = this.writeToFile(templateFile, workbook, config);
        } catch (Exception e) {
            e.printStackTrace();
            log.error("" + e);
            if (templateFile != null) {
                boolean deleteSuccess = templateFile.delete();
                log.debug("Delete file success: " + deleteSuccess);
            }
//            throw new ApplicationException("E018", e.getMessage());
        }
        return file;
    }



    /**
     * Open workbook
     * @return
     */
    public Workbook openWorkbook(File file) {
        Workbook wb = null;
        try (InputStream is = new FileInputStream(file)) {
            wb = new XSSFWorkbook(is);
        } catch (EncryptedDocumentException | IOException e) {
            log.error("Handle exception: ", e);
//            throw new ApplicationException(HttpStatus.NOT_ACCEPTABLE, e.getMessage());
        }
        return wb;
    }

    /**
     * Get template file by name
     * @return
     */
    public File getTemplateFile(String fileName) {
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        File tempFile = null;
        try {
            URL resource = classloader.getResource(fileName);
            String templatePath = URLDecoder.decode(resource.getFile(), "UTF-8");
            File file = new File(templatePath);
            String tempPathFile = folderTemp + StringUtil.genUniqueText() + ".xlsx";
            tempFile = new File(tempPathFile);
            Files.copy(file, tempFile);
        } catch (IOException e) {
            log.error("" + e.getMessage());
//            throw new ApplicationException("E018", e.getMessage());
        }
        return tempFile;
    }

    public void createCellandCopyStyle(Row templateRow, Row newRow) {
        if (null == templateRow) {
            return;
        }
        int lastCellNum = templateRow.getLastCellNum();
        for (int i = 0; i < lastCellNum; i++) {
            Cell cellTemplate = templateRow.getCell(i);
            if (cellTemplate != null) {
                getCellWithCreate(newRow, i).setCellStyle(cellTemplate.getCellStyle());
            }
        }
    }

    public Cell getCellWithCreate(Row row, int colIndex) {
        Cell cell = row.getCell(colIndex);
        if (cell == null) {
            cell = row.createCell(colIndex);
        }
        return cell;
    }

    public void getCellAndWriteValue(Object value, Row row, int columnIndex, ValueDefinition def) {
        if(value == null || value.toString().isEmpty()) {
            return;
        }

        Cell cell = row.getCell(columnIndex);
        if(cell == null) {
            cell = row.createCell(columnIndex);
        }
        this.setValueToCell(value, cell, def);
    }


    public void writeValueToCell(Object value, Cell cell, ValueDefinition def) {
        if(value == null || value.toString().isEmpty()) {
            return;
        }
        this.setValueToCell(value, cell, def);
    }

    /**
     * Get cell at cell name address
     * @param sheet
     * @param cellName
     * @return
     */
    public Cell getCellAtPosition(Sheet sheet, final String cellName) {
        CellReference cr = new CellReference(cellName);
        Row row = sheet.getRow(cr.getRow());
        int colIndex = cr.getCol();
        Cell cell = row.getCell(colIndex);
        if (cell == null) {
            cell = row.createCell(colIndex);
        }
        return cell;
    }

    @SuppressWarnings("rawtypes")
    public void setQrCodeToCell(Cell cell, Object value, int width, int height) {
        if (cell == null || value == null || StringUtils.isBlank(value.toString())) {
            return;
        }
        Sheet sheet = cell.getRow().getSheet();
        Workbook wb = sheet.getWorkbook();
        byte[] qr = FileUtil.generateQRCodeToByte(value.toString(), width, height);
        int qrId = wb.addPicture(qr, Workbook.PICTURE_TYPE_PNG);

        Drawing drawing = sheet.createDrawingPatriarch();
        CreationHelper helper = wb.getCreationHelper();
        ClientAnchor anchor = helper.createClientAnchor();
        anchor.setCol1(cell.getColumnIndex());
        anchor.setRow1(cell.getRowIndex());

        Picture picture = drawing.createPicture(anchor, qrId);
        picture.resize();
    }

    /**
     * Write workbook to file and ouput response
     */
    protected File writeToFile(File file, Workbook wb, ExcelExportGeneralConfig config) {
        try (FileOutputStream outputStream = new FileOutputStream(file)) {
            // Init all cell format with formula
            if (config.isEvaluateFormula()) {
                XSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
            }
            wb.write(outputStream);
        } catch (IOException e) {
            log.error("" + e);
//            throw new ApplicationException("E018", e.getMessage());
        }
        return file;
    }

    protected <T> String getTemplateName(Class<T> type) {
        ExcelExportGeneralConfig config = type.getAnnotation(ExcelExportGeneralConfig.class);
//        if (config == null) {
//            throw new ApplicationException("E018", "Annotation ExcelExportGeneralConfig is null");
//        }
        return CommonConstant.EXCEL_TEMPLATE_FOLDER + config.template();
    }

    protected <T> void readMultiSheetObject(T multiSheetData, Workbook workbook, boolean hasSampleRow) {
        final Field[] fields = multiSheetData.getClass().getDeclaredFields();
        ExcelExportMultiSheet fa;
        try {
            for (Field field : fields) {
                if (field.isAnnotationPresent(ExcelExportMultiSheet.class)) {
                    fa = field.getAnnotation(ExcelExportMultiSheet.class);
                    Sheet sheet = workbook.getSheetAt(fa.sheetIndex());
                    int rowIndex = fa.startRow();
                    // Get list in field
                    Object data;
                    if (field.getType().isAssignableFrom(ArrayList.class)) {
                        data = FieldUtils.readField(field, multiSheetData, true);
                        ArrayList<?> list = (ArrayList<?>) data; // cast to ArrayList<>
                        if (CollectionUtils.isNotEmpty(list)) {
                            this.setDataInListToTemplate(list, rowIndex, sheet,hasSampleRow, list.get(0).getClass());
                        }
                    } else {
                        data = FieldUtils.readField(field, multiSheetData, true);
                        this.setSingleDataToSheet(data, sheet, hasSampleRow);
                    }
                }
            }
        } catch (IllegalAccessException e) {
            log.error("" + e);
//            throw new ApplicationException("E018", e.getMessage());
        }
    }

    protected <T> void setSingleDataToSheet(T data, Sheet sheet, boolean hasSamepleRow) {
        if (data == null) {
            return;
        }

        Class<?> classType = data.getClass();
        final Field[] fields = classType.getDeclaredFields();
        ExcelExportForm fa;
        Object value;
        try {
            for (Field field : fields) {
                if (field.isAnnotationPresent(ExcelExportForm.class)) {
                    fa = field.getAnnotation(ExcelExportForm.class);
                    if (field.getType().isAssignableFrom(ArrayList.class)) {
                        int startRow = fa.startRow();
                        ArrayList<?> list = (ArrayList<?>) FieldUtils.readField(field, data, true);
                        if (CollectionUtils.isNotEmpty(list)) {
                            this.setDataInListToTemplate(list, startRow, sheet,hasSamepleRow, list.get(0).getClass());
                        }
                    } else {
                        String cellAddress = fa.cell();
                        String[] cellAddressArray = fa.cellArray();
                        ValueDefinition def = field.getAnnotation(ValueDefinition.class);
                        if (StringUtils.isNotBlank(cellAddress)) {
                            Cell cell = this.getCellAtPosition(sheet, cellAddress);
                            value = FieldUtils.readField(field, data, true);
                            writeValueToCell(value, cell, def);
                        } else if (cellAddressArray.length > 0) {
                            for (String address : cellAddressArray) {
                                Cell cell = this.getCellAtPosition(sheet, address);
                                value = FieldUtils.readField(field, data, true);
                                this.writeValueToCell(value, cell, def);
                            }
                        }
                    }
                } else if (field.isAnnotationPresent(ExcelExportQRConfig.class)) { // QR code
                    ExcelExportQRConfig qrCodeAnotation = field.getAnnotation(ExcelExportQRConfig.class);
                    String cellAddress = qrCodeAnotation.cell();
                    if (StringUtils.isNotBlank(cellAddress)) {
                        Cell cell = this.getCellAtPosition(sheet, cellAddress);
                        value = FieldUtils.readField(field, data, true);
                        this.setQrCodeToCell(cell, value, qrCodeAnotation.width(), qrCodeAnotation.height());
                    }
                }
            }
        } catch (IllegalArgumentException | IllegalAccessException e) {
//            throw new ApplicationException("E018", e.getMessage());
        }
    }

    protected void setDataInListToTemplate(List<?> listDataInSheet, int startRow, Sheet sheet
            , boolean hasSampleRow, Class<?> type) {
        if (CollectionUtils.isEmpty(listDataInSheet)) {
            return;
        }
        int rowSampleIndex = startRow;
        Row rowSample = sheet.getRow(rowSampleIndex);
        // If rowSample is null, create row
        if (rowSample == null) {
            rowSample = sheet.createRow(rowSampleIndex);
        }
        int rowIndex = rowSampleIndex;
        for (Object data : listDataInSheet) {
            if (rowIndex == rowSampleIndex) {
                this.setDataFromObject(data, rowSample, type);
                rowIndex++;
                continue;
            }

            Row row = sheet.getRow(rowIndex);
            if (row == null) {
                row = sheet.createRow(rowIndex);
            }

            if (hasSampleRow) {
                this.createCellandCopyStyle(rowSample, row);
            }

            this.setDataFromObject(data, row, type);
            rowIndex++;
        }
    }

    /**
     * Set data from fields of object
     * @param data
     * @param row
     * @param type
     */
    protected void setDataFromObject(Object data, Row row, Class<?> type) {
        final Field[] fields = type.getDeclaredFields();
        ExcelExport fa;
        for (Field field : fields) {
            if (field.isAnnotationPresent(ExcelExport.class)) {
                fa = field.getAnnotation(ExcelExport.class);
                int columnIndex = fa.index();
                int[] columnIndexArray = fa.indexArray();
                try {
                    Object value = FieldUtils.readField(field, data, true);
                    ValueDefinition def = field.getAnnotation(ValueDefinition.class);
                    if (columnIndexArray.length > 0) {
                        for (int index : columnIndexArray) {
                            this.getCellAndWriteValue(value, row, index, def);
                        }
                    } else {
                        this.getCellAndWriteValue(value, row, columnIndex, def);
                    }
                } catch (IllegalArgumentException | IllegalAccessException e) {
//                    throw new ApplicationException("E018", e.getMessage());
                }
            }
        }
    }

    protected void createDropdown(Sheet sheet, String[] listValues, CellRangeAddressList range) {
        if (sheet instanceof XSSFSheet) {
            final XSSFDataValidationHelper dvHelper =
                    new XSSFDataValidationHelper((XSSFSheet)sheet);
            final XSSFDataValidationConstraint dvConstraint =
                    (XSSFDataValidationConstraint)dvHelper
                            .createExplicitListConstraint(listValues);
            final XSSFDataValidation validation =
                    (XSSFDataValidation)dvHelper.createValidation(dvConstraint, range);
            validation.setShowErrorBox(true);
            sheet.addValidationData(validation);
        } else {
            final DVConstraint dvConstraint =
                    DVConstraint.createExplicitListConstraint(listValues);
            final DataValidation dataValidation = new HSSFDataValidation(range, dvConstraint);
            dataValidation.setSuppressDropDownArrow(false);
            sheet.addValidationData(dataValidation);
        }
    }

    /**
     *
     * @param value
     * @param cell
     * @param def
     */
    protected void setValueToCell(Object value, Cell cell, ValueDefinition def) {
        if (value instanceof Date) {
            cell.setCellValue((Date) value);
        }
        if (value instanceof Double || value instanceof BigDecimal || value instanceof Integer || value instanceof BigInteger) {
            BigDecimal tmp = new BigDecimal(value.toString());
            if (def != null) {
                try {
                    tmp = tmp.setScale(def.decimalScale(), def.roundingMode());
                } catch (Exception e) {
                    tmp = tmp.setScale(def.decimalScale(), RoundingMode.HALF_EVEN);
                }
            }
            cell.setCellValue(tmp.doubleValue());
        } else if (value instanceof String) {
            cell.setCellValue((String) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        }
    }
}
