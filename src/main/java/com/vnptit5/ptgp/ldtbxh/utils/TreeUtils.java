package com.vnptit5.ptgp.ldtbxh.utils;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author trinhctn
 */
public class TreeUtils {

    public static void buildTree(List < List<Map<String, Object>> > tree, String keyId, String keyIdParent) {
        for (int i = 0; i<tree.size(); i++) {
            List<Map<String, Object>> parent = tree.get(i);
            List<Map<String, Object>> child = i < tree.size() - 1 ? tree.get(i+1) : new ArrayList<>();
            Multimap<Integer, Map<String, Object>> group = ArrayListMultimap.create();
            child.forEach(e -> {
                int idParent = Integer.parseInt(e.get(keyIdParent).toString());
                group.put(idParent, e);
            });
            parent.forEach(e -> {
                int id = Integer.parseInt(e.get(keyId).toString());
                Collection<Map<String, Object>> children = group.get(id);
                e.put("CON", children);
            });
        }
    }

    public static List<Map<String, Object>> buildList(List<Map<String, Object>> childList, List<Map<String, Object>> parentlist,
                                                      String keyId, String keyIdParent,
                                                      String sheetName){
        Multimap<Integer, Map<String, Object>> group = ArrayListMultimap.create();
        childList.forEach(e -> {
            int idParent = Integer.parseInt(e.get(keyIdParent).toString());
            group.put(idParent, e);
        });

        parentlist.forEach(e -> {
            int id = Integer.parseInt(e.get(keyId).toString());
            Collection<Map<String, Object>> children = group.get(id);
            e.put("DATA", children);
            e.put("SHEET_NAME", sheetName);
        });
        return parentlist;
    }

}
