package com.vnptit5.ptgp.ldtbxh.utils.excel.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author ToiTV
 * 2020-10-17
 */

@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Target(value = { ElementType.TYPE })
public @interface ExcelExportPrintAreaInfo {

    String name();

    String fromRow();

    String toRow();

    String fromColumn();

    String toColumn();

}
