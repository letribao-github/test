package com.vnptit5.ptgp.ldtbxh.utils;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.io.Files;
import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.QRCodeWriter;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportAreaActionConfig;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportAreaActionConfig.Action;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportAreaElement;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportAreaStripe;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportAreas;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportColorArea;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportForm;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportGeneralConfig;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportLineBreak;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportList;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportList.Direction;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportMergeRegion;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportMergeRegionInfo;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportOrder;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportParams;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportPrintArea;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportPrintAreaInfo;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportQRConfig;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportTable;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportTable.ColumnSchema;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportWaterfall;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ValueDefinition;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.CRC32;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.imageio.ImageIO;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.multipdf.PDFMergerUtility;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.rendering.ImageType;
import org.apache.pdfbox.rendering.PDFRenderer;
import org.apache.poi.poifs.crypt.HashAlgorithm;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Picture;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.ShapeTypes;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFCreationHelper;
import org.apache.poi.xssf.usermodel.XSSFDrawing;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator;
import org.apache.poi.xssf.usermodel.XSSFName;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFSimpleShape;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jodconverter.DocumentConverter;
import org.jodconverter.LocalConverter;
import org.jodconverter.office.LocalOfficeManager;
import org.jodconverter.office.OfficeManager;
//import org.omg.CORBA.portable.ApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author ToiTV
 * 2020-10-17
 */
@Slf4j
@Component
@SuppressWarnings("unused")
public class FileUtil {

    @Value("${folder.temp.timeToLive}")
    private int tempFileTTL;

    @Value("${excel.sheet.password}")
    private String excelSheetPassword;
    @Value("${excel.sheet.error}")
    private String excelSheetError;

    private static File TempFolder;
    private static File downloadingCacheFolder;
    private File templateExcelFolder;

    private Random random = new Random();

    class OfficePool {
        private final int SIZE;
        private final int RETRY;
        private int omIndex = -1;
        private List<OfficeManager> OM = new ArrayList<>();
        private List<DocumentConverter> DC = new ArrayList<>();
        private List<Integer> portListValidated = new ArrayList<>();

        public OfficePool(Iterable<String> ports, String officeHome, String templateProfileDir, Integer retry) {
            this.RETRY = retry;

            StringBuilder portNames = new StringBuilder();
            Set<Integer> portSet = new HashSet<>();
            for (String portString : ports) {
                if (StringUtils.isBlank(portString))
                    continue;
                Integer portNum = null;
                try {
                    portNum = Integer.parseInt(portString);
                } catch (Exception e) {
                }
                if (portNum == null)
                    continue;
                if (portSet.contains(portNum))
                    continue;
                else
                    portSet.add(portNum);

                portListValidated.add(portNum);
                portNames.append(portNum).append(" ");
            }

            this.SIZE = portListValidated.size();
            log.info("Creating OfficeManager on [" + portNames.toString() + "] ...");
            for (int i = 0; i < this.SIZE; i++) {
                LocalOfficeManager.Builder builder = LocalOfficeManager.builder();
                builder.portNumbers(portListValidated.get(i));
                builder.officeHome(officeHome);
                builder.templateProfileDir(templateProfileDir);
                OfficeManager officeManager = builder.build();
                OM.add(officeManager);
                DC.add(LocalConverter.make(officeManager));
            }
        }

        public void start() {
            for (OfficeManager om : OM) {
                try {
                    om.start();
                } catch (Exception e) {
                    log.error("", e);
                }
            }
        }

        public void stop() {
            for (OfficeManager om : OM) {
                try {
                    om.stop();
                } catch (Exception e) {
                    log.error("", e);
                }
            }
        }

        public synchronized DocumentConverter getConverter() {
            omIndex++;
            if (omIndex == SIZE)
                omIndex = 0;
            log.info("** OfficeManager on Port[" + portListValidated.get(omIndex) + "]");
            return DC.get(omIndex);
        }
    }

    private OfficePool officePool;

    /**
     * 32767
     */
    public static int EXCEL_CELL_STRING_MAX_SIZE = 32767;
    /**
     * # DATA TOO LONG !
     */
    private static String DATA_TOO_LONG = "# DATA TOO LONG !";

    @Autowired
    public FileUtil(
            @Value("src/main/resources/template/excel/") String folderTemp,
            @Value("src/main/resources/template/excel/") String folderTempDownload,
            @Value("src/main/resources/template/excel/") String templateExcel,
            @Value("8100") String portNumber,
//            @Value("C:/Program Files (x86)/OpenOffice 4") String officeHome,
//            @Value("C:/Users/${username}/AppData/Roaming/OpenOffice/4") String templateProfileDir,
            @Value("5") Integer retries) {
        try {
            log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            log.info("++++++++++++++++++++++++++++++          FILE UTIL          ++++++++++++++++++++++++++++++++");
            log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            log.info("FileUtil starting...");
            templateExcelFolder = new File(templateExcel);
            if (!templateExcelFolder.isDirectory())
                throw new Exception("FileUtil init has failed.");

            log.info("folder.temp : " + folderTemp);
            TempFolder = new File(folderTemp);
            if (!TempFolder.exists() || !TempFolder.isDirectory()) {
                log.info("TempFolder is not existed. Creating..");
                boolean result = TempFolder.mkdir();
                log.info("Result : " + result);
                if (result) {
                    log.info("canRead : " + TempFolder.canRead());
                    log.info("canWrite : " + TempFolder.canWrite());
                    log.info("canExecute : " + TempFolder.canExecute());
                }
            } else {
                log.info("TempFolder is existed.");
                log.info("canRead : " + TempFolder.canRead());
                log.info("canWrite : " + TempFolder.canWrite());
                log.info("canExecute : " + TempFolder.canExecute());
            }
            downloadingCacheFolder = new File(folderTempDownload);
            if (!downloadingCacheFolder.exists() || !downloadingCacheFolder.isDirectory()) {
                downloadingCacheFolder.mkdirs();
            }

            if (StringUtils.isNotBlank(portNumber)) {
//                List<String> ports = new ArrayList<>();
//                for (String port : portNumber.split(","))
//                    ports.add(port);
//                officePool = new OfficePool(ports, officeHome, templateProfileDir, retries);
            }

            log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            log.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        } catch (Exception e) {
            log.error("FileUtil init has failed.", e);
        }
    }

    @PostConstruct
    public void postConstruct() {
        try {
//            officePool.start();
        } catch (Exception e) {
            log.error("", e);
        }
    }

    @PreDestroy
    public void preDestroy() {
        try {
            officePool.stop();
        } catch (Exception e) {
            log.error("", e);
        }
    }

    public File getTempFile(String fileName) {
        if (StringUtils.isBlank(fileName))
            return null;
        File file = new File(TempFolder, fileName);
        if (file != null && file.exists())
            return file;
        return null;
    }

    public String clearAllTempFile() {
        StringBuilder sb = new StringBuilder();
        for (File file : TempFolder.listFiles()) {
            if (!file.isFile())
                continue;
            try {
                String delLog = "Deleting file : [" + file.getName() + "] : ";
                boolean result = file.delete();
                delLog += result;
                log.info(delLog);
                sb.append(delLog + "\n");
            } catch (Exception e) {
            }
        }
        return sb.toString();
    }

    public enum Extension {
        PDF("pdf"), PNG("png"), ZIP("zip"), XLSX("xlsx"), XLS("xls"), XLSM("xlsm"), UNKNOWN("");

        private String value;

        Extension(String value) {
            this.value = value;
        }

        static public Extension get(String extension) {
            if (StringUtils.isBlank(extension))
                return Extension.UNKNOWN;
            for (Extension ext : Extension.values())
                if (ext.value.equalsIgnoreCase(extension))
                    return ext;
            return Extension.UNKNOWN;
        }

        /**
         * In case extension can not be found, will return Extension.UNKNOWN
         */
        static public Extension getFromName(String name) {
            return get(getExtensionOnly(name));
        }
    }

    private static String getNameOnly(String name) {
        if (StringUtils.isBlank(name))
            return "";
        int lastDotIndex = name.lastIndexOf(".");
        return lastDotIndex == -1 ? name : name.substring(0, lastDotIndex);
    }

    private static String getExtensionOnly(String name) {
        if (StringUtils.isBlank(name))
            return "";
        int lastDotIndex = name.lastIndexOf(".");
        return lastDotIndex == -1 ? name : name.substring(lastDotIndex + 1, name.length());
    }

    enum IsUnique {
        TRUE, FALSE, UNKNOWN
    }

    private static final String DATA_DIVIDER = "$$";
    private static final String DATA_DIVIDER_REGEX = "\\$\\$";
    private static final String DATA_REGEX = DATA_DIVIDER_REGEX + "(.+?)" + DATA_DIVIDER_REGEX;
    private static final Pattern DATA_PATTERN = Pattern.compile(DATA_REGEX);

    private IsUnique isUniqueFormatted(String name) {
        String nameOnly = getNameOnly(name);
        Matcher dataMatcher = DATA_PATTERN.matcher(nameOnly);
        if (dataMatcher.find())
            return IsUnique.TRUE;
        else
            return IsUnique.FALSE;
    }

    /**
     * File name must not contains dot "."<br>
     * file_name.pdf, fileName.xlsx, ...
     *
     * @param
     * @return
     */
    private String genUniqueName(String name) {
        String result = null;
        if (StringUtils.isBlank(name))
            return result;

        String nameOnly = getNameOnly(name);
        String extension = getExtensionOnly(name);

        int randNum = 100000 + random.nextInt(999999);
        result = DATA_DIVIDER + nameOnly + DATA_DIVIDER + System.currentTimeMillis() + randNum
                + ("".equals(extension) ? "" : "." + extension);
        return result;
    }

    /**
     * file name generated by genUniqueName
     *
     * @param
     * @return
     */
    private String revertUniqueName(String name) {
        String revertedName = "";
        if (StringUtils.isBlank(name))
            return revertedName;

        String nameOnly = getNameOnly(name);
        String extension = getExtensionOnly(name);

        if ("".equals(nameOnly))
            return revertedName;

        Matcher dataMatcher = DATA_PATTERN.matcher(nameOnly);
        if (dataMatcher.find())
            revertedName = dataMatcher.group(1);

        return revertedName + ("".equals(extension) ? "" : "." + extension);
    }

    private static String addIndexToFileName(String name, int index) {
        if (StringUtils.isBlank(name))
            return "";

        String nameOnly = getNameOnly(name);
        String extension = getExtensionOnly(name);

        return nameOnly + "_" + index + ("".equals(extension) ? "" : "." + extension);
    }

    private String changeExtension(String name, Extension newExt) {
        if (StringUtils.isBlank(name))
            return "";

        return getNameOnly(name) + (newExt != null ? ("." + newExt.value) : "");
    }

    public static class TempFile extends File {
        private static final long serialVersionUID = -2780778674496516447L;
        @Getter
        private String businessName;
        @Getter
        private Extension extension;

        /**
         *
         * @param systemName   system unique name
         * @param businessName display name
         * @throws IOException
         */
        private TempFile(String systemName, String businessName, Extension extension) {
            super(TempFolder, systemName);
            this.businessName = businessName;
            this.extension = extension;
        }

        public String indexBusinessName(int index) {
            return FileUtil.addIndexToFileName(this.businessName, index);
        }
    }

    public static class ExcelPosition {
        private int row = 0;
        private int column = 0;

        public ExcelPosition() {
        }

        /**
         * Example:<br>
         * ExcelPosition("A1")<br>
         * ExcelPosition("AH20")<br>
         *
         * @param cellName The name of the cell
         */
        public ExcelPosition(String cellName) {
            String[] part = cellName.split("(?<=\\D)(?=\\d)");
            if (part.length != 2) {
                log.error("Cell name invalid.[" + cellName + "]");
                return;
            }
            this.row = Integer.valueOf(part[1]) - 1;
            this.column = CellReference.convertColStringToIndex(part[0]);
        }

        /**
         * Example:<br>
         * ExcelPosition("A", 1)<br>
         * ExcelPosition("AH", 20)<br>
         *
         * @param columnName Name of the column
         * @param row        The number index of the row
         */
        public ExcelPosition(String columnName, int row) {
            this.column = CellReference.convertColStringToIndex(columnName);
            this.row = row;
        }

        private ExcelPosition(int column, int row) {
            this.column = column;
            this.row = row;
        }

        public void nextRow() {
            stepRow(1);
        }

        public void stepRow(int step) {
            this.row += step;
        }

        public void nextColumn() {
            stepColumn(1);
        }

        public void stepColumn(int step) {
            this.column += step;
        }

        public ExcelPosition offset(int offsetColumn, int offsetRow) {
            return new ExcelPosition(this.column + offsetColumn, this.row + offsetRow);
        }

        public String getCellName() {
            return CellReference.convertNumToColString(this.column) + String.valueOf(this.row + 1);
        }

        public static Map<Integer, List<Integer>> deduce(String fromPos, String toPos) {
            return deduce(new ExcelPosition(fromPos), new ExcelPosition(toPos));
        }

        public static Map<Integer, List<Integer>> deduce(ExcelPosition fromPos, ExcelPosition toPos) {
            Map<Integer, List<Integer>> result = new HashMap<>();

            int fromColumn = fromPos.column;
            int toColumn = toPos.column;
            int columnSize = toColumn - fromColumn + 1;

            int fromRow = fromPos.row;
            int toRow = toPos.row;
            int rowSize = toRow - fromRow + 1;

            for (int y = 0; y < rowSize; y++) {
                int rowNum = fromRow + y;
                List<Integer> rowList = new ArrayList<>();
                result.put(rowNum, rowList);
                for (int x = 0; x < columnSize; x++) {
                    rowList.add(fromColumn + x);
                }
            }
            return result;
        }
    }

    public boolean isExcelFileName(String name, Extension targetExt) {
        Extension ext = Extension.getFromName(name);
        if (targetExt != null) {
            if (targetExt == ext)
                return true;
            else
                return false;
        } else {
            switch (ext) {
                case XLS:
                case XLSX:
                case XLSM:
                    return true;
                default:
                    return false;
            }
        }
    }

    public TempFile multipartToFile(MultipartFile multipartFile) {
        String systemName = String.valueOf(System.currentTimeMillis() + "_" + (100000 + random.nextInt(999999)));
        TempFile tmpFile = new TempFile(systemName, multipartFile.getOriginalFilename(), Extension.UNKNOWN);

        try (FileOutputStream fos = new FileOutputStream(tmpFile)) {
            fos.write(multipartFile.getBytes());
        } catch (Exception e) {
            log.error("TempFile generation has failed.", e);
        }
        return tmpFile;
    }

    private TempFile createTempFile(String name, Extension extension) {
        TempFile file = null;
        try {
            name = StringUtils.isBlank(name) ? "tmp." + extension.value : name;
            file = new TempFile(genUniqueName(name), name, extension);
            file.createNewFile();
        } catch (Exception e) {
            log.error("", e);
        }
        return file;
    }

    public TempFile createZipFile(String name) {
        return createTempFile(name, Extension.ZIP);
    }

    public TempFile createPdfFile(String name) {
        return createTempFile(name, Extension.PDF);
    }

    public TempFile createExcelFile(String name) {
        return createTempFile(name, Extension.XLSX);
    }

    public TempFile createCopy(File sourceFile) {
        try {
            String sourceFileName = sourceFile.getName();
            TempFile copy = createTempFile(sourceFileName, Extension.getFromName(sourceFileName));
            Files.copy(sourceFile, copy);
            return copy;
        } catch (Exception e) {
            log.error("createCopy has failed.", e);
            return null;
        }
    }

    public TempFile createCopy(TempFile sourceFile) {
        try {
            String sourceFileName = sourceFile.getBusinessName();
            TempFile copy = createTempFile(sourceFileName, Extension.getFromName(sourceFileName));
            Files.copy(sourceFile, copy);
            return copy;
        } catch (Exception e) {
            log.error("createCopy has failed.", e);
            return null;
        }
    }

    public XSSFWorkbook getWorkbook(TempFile file) {
        try (FileInputStream fis = new FileInputStream(file);) {
            XSSFWorkbook wb = new XSSFWorkbook(fis);
            return wb;
        } catch (Exception e) {
            log.error("", e);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public <T> TempFile exportExcelToFile(T dto, boolean recalculateAfter, Class<?>... groups) {

        Set<Class<?>> groupSet = new HashSet<>();
        for (Class<?> group : groups)
            if (group != null)
                groupSet.add(group);

        ExcelExportGeneralConfig[] configs = dto.getClass().getAnnotationsByType(ExcelExportGeneralConfig.class);
        ExcelExportGeneralConfig config = null;
        if (configs.length == 1) {
            config = configs[0];
        } else if (configs.length > 1) {
            configBreak: for (ExcelExportGeneralConfig e : configs) {
                for (Class<?> clazz : e.groups()) {
                    if (groupSet.contains(clazz)) {
                        config = e;
                        break configBreak;
                    }
                }
            }
        }
        if (config == null || StringUtils.isBlank(config.exportName())) {
            log.error("exportToExcelTemplate : ExcelExportGeneralConfig invalid.");
            return null;
        }

        HashMap<String, String> paramMap = null;
        try {
            for (Field field : dto.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                if (field.isAnnotationPresent(ExcelExportParams.class) && field.getType().isAssignableFrom(Map.class)) {
                    paramMap = (HashMap<String, String>) field.get(dto);
                }
            }
        } catch (Exception e) {
        }
        if (paramMap == null)
            paramMap = new HashMap<>();

        String templateName = config.template();

        String exportName = replaceParam(paramMap, config.exportName());

        TempFile genFile = createTempFile(exportName + "." + Extension.XLSX.value, Extension.XLSX);
        TempFile genFile2 = createTempFile(exportName + "." + Extension.XLSX.value, Extension.XLSX);
        XSSFWorkbook wb = null;
        try (FileInputStream genFIS = new FileInputStream(genFile);
                FileOutputStream genFOS = new FileOutputStream(genFile2);) {

            File templateFile = new File(templateExcelFolder, templateName);
            Files.copy(templateFile, genFile);
            wb = new XSSFWorkbook(genFIS);
            genFIS.close();

            Map<Integer, CellStyle> cellStyleMap = new HashMap<>();
            if (StringUtils.isNotBlank(config.styleTemplateName())) {
                XSSFSheet styleSheet = wb.getSheet(config.styleTemplateName());
                if (styleSheet != null) {
                    Row row0 = styleSheet.getRow(0);
                    if (row0 != null) {
                        for (Cell cell : row0) {
                            cellStyleMap.put((int) cell.getNumericCellValue(), cell.getCellStyle());
                        }
                    }
                }
            }

            // =================================================================================================
            // Common export
            for (Field field : dto.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                Object cellValue = field.get(dto);
                ValueDefinition def = field.getAnnotation(ValueDefinition.class);

                if (cellValue == null)
                    continue;

                // ExcelExportForm
                ExcelExportForm[] allFormATs = field.getAnnotationsByType(ExcelExportForm.class);
                List<ExcelExportForm> formATs = new ArrayList<>();
                formAtLoop: for (ExcelExportForm e : allFormATs) {
                    if (e.groups().length == 0) {
                        formATs.add(e);
                    } else {
                        for (Class<?> clazz : e.groups()) {
                            if (groupSet.contains(clazz)) {
                                formATs.add(e);
                                continue formAtLoop;
                            }
                        }
                    }
                }
                if (formATs.size() > 0) {
                    processForm(wb, formATs, cellValue, def);
                }

                // ExcelExportTable
                ExcelExportTable[] tableColumnAtAll = field.getAnnotationsByType(ExcelExportTable.class);
                if (tableColumnAtAll.length > 0 && cellValue instanceof List<?>
                        && ((List<Object>) cellValue).size() > 0) {
                    processTable(wb, tableColumnAtAll, cellValue, def);
                }

                // ExcelExportList
                ExcelExportList[] listColumnAtAll = field.getAnnotationsByType(ExcelExportList.class);
                if (listColumnAtAll.length > 0 && cellValue instanceof List<?>
                        && ((List<Object>) cellValue).size() > 0) {
                    processList(wb, listColumnAtAll, cellValue, def);
                }

                // ExcelExportWaterfall
                ExcelExportWaterfall waterfallAt = field.getAnnotation(ExcelExportWaterfall.class);
                if (waterfallAt != null && cellValue instanceof Map && ((Map<Object, Object>) cellValue).size() > 0) {
                    processWaterfall(wb, waterfallAt, cellValue, def);
                }

                // ExcelExportAreas
                ExcelExportAreas[] areasATs = field.getAnnotationsByType(ExcelExportAreas.class);
                ExcelExportAreas areasAT = null;
                if (areasATs.length == 1) {
                    areasAT = areasATs[0];
                } else if (configs.length > 1) {
                    areaAtBreak: for (ExcelExportAreas e : areasATs) {
                        for (Class<?> clazz : e.groups()) {
                            if (groupSet.contains(clazz)) {
                                areasAT = e;
                                break areaAtBreak;
                            }
                        }
                    }
                }
                if (areasAT != null) {
                    processAreas(wb, areasAT, cellValue, cellStyleMap);
                }

                // ExcelExportRangeElement
                ExcelExportPrintArea printAreaAt = field.getAnnotation(ExcelExportPrintArea.class);
                if (printAreaAt != null) {
                    processPrintArea(wb, printAreaAt, cellValue, def);
                }

                // ExcelExportMergeRegion
                ExcelExportMergeRegion[] mergeAtAll = field.getDeclaredAnnotationsByType(ExcelExportMergeRegion.class);
                List<ExcelExportMergeRegion> mergeATs = new ArrayList<>();
                mergeRegionAtLoop: for (ExcelExportMergeRegion e : mergeAtAll) {
                    if (e.groups().length == 0) {
                        mergeATs.add(e);
                    } else {
                        for (Class<?> clazz : e.groups()) {
                            if (groupSet.contains(clazz)) {
                                mergeATs.add(e);
                                continue mergeRegionAtLoop;
                            }
                        }
                    }
                }
                if (mergeATs.size() > 0) {
                    processMergeRegion(wb, mergeATs, cellValue, cellStyleMap, def);
                }

                // ExcelExportWaterfall
                ExcelExportLineBreak lineBreakAt = field.getAnnotation(ExcelExportLineBreak.class);
                if (lineBreakAt != null && cellValue instanceof List && ((List<Object>) cellValue).size() > 0) {
                    processLineBreak(wb, lineBreakAt, cellValue, def);
                }
            }

            // =================================================================================================
            // ExcelExportAreaActionConfig
            for (Field field : dto.getClass().getDeclaredFields()) {
                field.setAccessible(true);
                Object cellValue = field.get(dto);
                ValueDefinition def = field.getAnnotation(ValueDefinition.class);

                ExcelExportAreaActionConfig[] areaActionAts = field
                        .getAnnotationsByType(ExcelExportAreaActionConfig.class);
                if (areaActionAts.length > 0) {
                    processAreaActionConfig(wb, areaActionAts, cellValue, def);
                }
            }

            // =================================================================================================
            // ExcelExportQRConfig
            ExcelExportQRConfig[] excelExportQRConfig = dto.getClass().getAnnotationsByType(ExcelExportQRConfig.class);
            if (excelExportQRConfig.length > 0) {
                processQrConfig(wb, excelExportQRConfig, dto);
            }

            // =================================================================================================
            if (recalculateAfter) {
                try {
                    XSSFFormulaEvaluator.evaluateAllFormulaCells(wb);
                } catch (Exception e) {
                    log.error("", e);
                }
            }
            // =================================================================================================
            for (Sheet sheet : wb) {
                sheet.setZoom(100);
            }
            // =================================================================================================
            // Lock sheet
            if (config.lockSheetIndex().length > 0) {
                Set<Integer> sheetSet = new HashSet<>();
                for (int index : config.lockSheetIndex())
                    sheetSet.add(index);

                for (int index : sheetSet) {
                    XSSFSheet sheet = wb.getSheetAt(index);

                    sheet.lockSelectLockedCells(true);
                    sheet.lockSelectUnlockedCells(true);
                    sheet.setSheetPassword(excelSheetPassword, HashAlgorithm.md5);
                    sheet.enableLocking();

                    wb.lockStructure();
                }
            }
            // =================================================================================================
            wb.write(genFOS);
            genFOS.flush();

        } catch (Exception e) {
            log.error("exportToExcelTemplate has failed.", e);
            try {
                if (genFile != null)
                    genFile.delete();
            } catch (Exception e2) {
            }
            try {
                if (genFile2 != null)
                    genFile2.delete();
            } catch (Exception e2) {
            }
            genFile2 = null;
        } finally {
            try {
                if (wb != null)
                    wb.close();
            } catch (Exception e2) {
            }
            try {
                if (genFile != null)
                    genFile.delete();
            } catch (Exception e2) {
            }
        }
        return genFile2;
    }

    private void processForm(XSSFWorkbook wb, List<ExcelExportForm> formATs, Object cellValue, ValueDefinition def) {
        for (ExcelExportForm at : formATs) {
            XSSFSheet sheet = wb.getSheetAt(at.sheetIndex());
            String cellName = at.cell();

            ExcelPosition pos = new ExcelPosition(cellName);
            setCellValue(sheet, pos, cellValue, def);
        }
    }

    @SuppressWarnings("unchecked")
    private void processTable(XSSFWorkbook wb, ExcelExportTable[] tableColumnAtAll, Object cellValue,
            ValueDefinition def) {
        try {
            List<Object> valueList = (List<Object>) cellValue;
            // prepare column according to columnIndex inside target Object
            Class<?> valueClazz = valueList.get(0).getClass();
            List<Field> valueFieldList = new ArrayList<>();
            for (Field valueField : valueClazz.getDeclaredFields()) {
                valueField.setAccessible(true);
                if (valueField.getAnnotation(ExcelExportOrder.class) != null)
                    valueFieldList.add(valueField);
            }
            Collections.sort(valueFieldList, new Comparator<Field>() {
                @Override
                public int compare(Field field1, Field field2) {
                    ExcelExportOrder columnAt1 = field1.getAnnotation(ExcelExportOrder.class);
                    ExcelExportOrder columnAt2 = field2.getAnnotation(ExcelExportOrder.class);
                    return columnAt1.order() - columnAt2.order();
                }
            });

            Map<Integer, List<ExcelExportTable>> tableAtPerSheetMap = new HashMap<>();
            for (ExcelExportTable at : tableColumnAtAll) {
                int sheetIndex = at.sheetIndex();
                List<ExcelExportTable> columnAtList = tableAtPerSheetMap.get(sheetIndex);
                if (columnAtList == null) {
                    columnAtList = new ArrayList<>();
                    tableAtPerSheetMap.put(sheetIndex, columnAtList);
                }
                columnAtList.add(at);
            }

            // For each sheet
            for (Map.Entry<Integer, List<ExcelExportTable>> entry : tableAtPerSheetMap.entrySet()) {
                int sheetIndex = entry.getKey();
                XSSFSheet sheet = wb.getSheetAt(sheetIndex);

                List<ExcelExportTable> sameSheetRegionAtList = entry.getValue();
                Collections.sort(sameSheetRegionAtList, new Comparator<ExcelExportTable>() {
                    @Override
                    public int compare(ExcelExportTable col1, ExcelExportTable col2) {
                        return col1.regionOrder() - col2.regionOrder();
                    }
                });

                // For each region of the table in the same sheet
                int valueListPointer = 0;
                regionBreak: for (ExcelExportTable at : sameSheetRegionAtList) {

                    List<Integer> columnIndexList = new ArrayList<>();

                    ColumnSchema schema = at.columnSchema();
                    switch (schema) {
                        case FIXED:
                            if (at.columnNameFixed() == null || at.columnNameFixed().length == 0) {
                                log.error("columnSchema is FIXED but columnNameFixed array is not configured.");
                                continue;
                            }
                            for (String columnName : at.columnNameFixed())
                                columnIndexList.add(CellReference.convertColStringToIndex(columnName));
                            break;
                        case AUTO:
                            if (StringUtils.isBlank(at.startColumnName()) || StringUtils.isBlank(at.endColumnName())) {
                                log.error("columnSchema is AUTO but startColumnName/endColumnName is not configured.");
                                continue;
                            }
                            int startColumn = CellReference.convertColStringToIndex(at.startColumnName().trim());
                            int endColumn = CellReference.convertColStringToIndex(at.endColumnName().trim());
                            int columnWidth = at.columnWidth();
                            if ((endColumn - startColumn) % columnWidth != 0) {
                                log.error("startColumn/endColumn and columnWidth are not configured properly.");
                                continue;
                            }
                            int columnPointer = startColumn;
                            while (columnPointer <= endColumn) {
                                columnIndexList.add(columnPointer);
                                columnPointer += columnWidth;
                            }
                            break;
                    }

                    if (columnIndexList.size() != valueFieldList.size()) {
                        log.error("Number of column and number of field is not equal.");
                        continue;
                    }

                    int currentRow = at.startRow() - 1;
                    int rowCount = at.rowCount() < 1 ? valueList.size() : at.rowCount();
                    for (int i = 0; i < rowCount; i++) {
                        if (valueListPointer == valueList.size())
                            break regionBreak;

                        Object value = valueList.get(valueListPointer);

                        int columnIndexPointer = 0;
                        for (Field valueField : valueFieldList) {
                            if (columnIndexPointer == columnIndexList.size())
                                break;

                            int currentColumn = columnIndexList.get(columnIndexPointer);
                            ExcelPosition pos = new ExcelPosition(currentColumn, currentRow);
                            setCellValue(sheet, pos, valueField.get(value),
                                    valueField.getAnnotation(ValueDefinition.class));

                            columnIndexPointer++;
                        }
                        currentRow++;
                        valueListPointer++;
                    }
                }
            }
        } catch (Exception e) {
            log.error("", e);
        }
    }

    @SuppressWarnings("unchecked")
    private void processWaterfall(XSSFWorkbook wb, ExcelExportWaterfall waterfallAt, Object cellValue,
            ValueDefinition def) {
        Map<Object, List<Object>> valueMap;
        try {
            valueMap = (Map<Object, List<Object>>) cellValue;
        } catch (Exception e) {
            return;
        }

        int sheetIndex = waterfallAt.sheetIndex();
        XSSFSheet sheet = wb.getSheetAt(sheetIndex);
        String startCell = waterfallAt.startCell();
        ExcelPosition pos = new ExcelPosition(startCell);
        int startRow = pos.row;

        // For each column
        for (Map.Entry<Object, List<Object>> entry : valueMap.entrySet()) {
            Object key = entry.getKey();
            List<Object> values = entry.getValue();

            // First row is Key
            pos.row = startRow;
            setCellValue(sheet, pos, key, def);

            // For each element of this column
            for (Object value : values) {
                pos.nextRow();
                setCellValue(sheet, pos, value, def);
            }

            // Assign name for Range
            if (waterfallAt.setRangeName()) {
                String columnName = CellReference.convertNumToColString(pos.column);
                int rowFrom = startRow + 2;
                int rowTo = startRow + values.size() + 1;
                // E.g: '縲舌??繧ｹ繧ｿ縲大霧讌ｭ諡?轤ｹ繝ｦ繝ｼ繧ｶ邨櫁ｾｼ'!$E$2:$E$17
                String range = "'縲舌??繧ｹ繧ｿ縲大霧讌ｭ諡?轤ｹ繝ｦ繝ｼ繧ｶ邨櫁ｾｼ'!$" + columnName + "$" + rowFrom + ":$" + columnName + "$"
                        + rowTo;

                XSSFName name = wb.createName();
                name.setNameName(String.valueOf(key));
                name.setRefersToFormula(range);
            }
            pos.nextColumn();
        }
    }

    @SuppressWarnings("unchecked")
    private void processList(XSSFWorkbook wb, ExcelExportList[] listColumnAtAll, Object cellValue,
            ValueDefinition def) {
        List<Object> valueList = (List<Object>) cellValue;

        Map<Integer, List<ExcelExportList>> listAtPerSheetMap = new HashMap<>();
        for (ExcelExportList at : listColumnAtAll) {
            int sheetIndex = at.sheetIndex();
            List<ExcelExportList> columnAtList = listAtPerSheetMap.get(sheetIndex);
            if (columnAtList == null) {
                columnAtList = new ArrayList<>();
                listAtPerSheetMap.put(sheetIndex, columnAtList);
            }
            columnAtList.add(at);
        }

        // For each sheet
        for (Map.Entry<Integer, List<ExcelExportList>> sheetEntry : listAtPerSheetMap.entrySet()) {
            int sheetIndex = sheetEntry.getKey();
            List<ExcelExportList> atList = sheetEntry.getValue();
            XSSFSheet sheet = wb.getSheetAt(sheetIndex);
            if (sheet == null)
                continue;

            Map<Integer, List<ExcelExportList>> sameSheetRegionGroupAtMap = new HashMap<>();
            for (ExcelExportList at : atList) {
                int regionGroup = at.regionGroup();
                List<ExcelExportList> list = sameSheetRegionGroupAtMap.get(regionGroup);
                if (list == null) {
                    list = new ArrayList<>();
                    sameSheetRegionGroupAtMap.put(regionGroup, list);
                }
                list.add(at);
            }

            // for each region group of this sheet
            // 1 region group :
            // a group of regions, placed in different position in this sheet
            // 1 region group represent a complete list of this value list
            // Multiple region groups will display a multiple duplication this value list
            for (Map.Entry<Integer, List<ExcelExportList>> entry : sameSheetRegionGroupAtMap.entrySet()) {
                List<ExcelExportList> sameRegionGroupAtList = entry.getValue();
                Collections.sort(sameRegionGroupAtList, new Comparator<ExcelExportList>() {
                    @Override
                    public int compare(ExcelExportList col1, ExcelExportList col2) {
                        return col1.regionOrder() - col2.regionOrder();
                    }
                });

                // For each region of the this group
                // 1 region : a continuous array of cell which present a part of the total value
                // list
                // region size is limited by the size of annotation
                // if size < 0, the region's size is not limited
                int valueCount = 0;
                regionBreak: for (ExcelExportList at : sameRegionGroupAtList) {

                    String startCell = at.startCell();
                    int size = at.size() < 0 ? valueList.size() : at.size();
                    ExcelPosition pos = new ExcelPosition(startCell);
                    List<Integer> columnIndexList = new ArrayList<>();

                    for (int i = 0; i < size; i++) {
                        if (valueCount == valueList.size())
                            break regionBreak;

                        setCellValue(sheet, pos, valueList.get(valueCount), def);

                        if (at.direction() == Direction.HORIZONTAL)
                            pos.nextColumn();
                        else if (at.direction() == Direction.VERTICAL)
                            pos.nextRow();
                        valueCount++;
                    }
                }
            }
        }
    }

    private void processAreaActionConfig(XSSFWorkbook wb, ExcelExportAreaActionConfig[] areaActionAts, Object cellValue,
            ValueDefinition def) {
        for (ExcelExportAreaActionConfig at : areaActionAts) {
            String conditionValue = at.conditionValue();
            Action action = at.action();
            if ((action == Action.ON_EQUAL_CLEAR && conditionValue.equalsIgnoreCase((String) cellValue))
                    || (action == Action.ON_EQUAL_RETAIN && !conditionValue.equalsIgnoreCase((String) cellValue))) {
                XSSFSheet sheet = wb.getSheetAt(at.sheetIndex());
                Map<Integer, List<Integer>> area = ExcelPosition.deduce(at.fromCell(), at.toCell());
                for (Map.Entry<Integer, List<Integer>> entry : area.entrySet()) {
                    Row row = sheet.getRow(entry.getKey());
                    if (row == null)
                        continue;
                    for (Integer columnNum : entry.getValue()) {
                        Cell cell = row.getCell(columnNum);
                        if (cell == null)
                            continue;
                        cell.setCellValue("");
                        // switch (cell.getCellTypeEnum()) {
                        // case BOOLEAN:
                        // cell.setCellValue(false);
                        // break;
                        // case NUMERIC:
                        // cell.setCellValue(0);
                        // break;
                        // case STRING:
                        // cell.setCellValue("");
                        // break;
                        // default:
                        // cell.setCellType(CellType.STRING);
                        // cell.setCellValue("");
                        // break;
                        // }
                    }
                }
            }
        }
    }

    private void processQrConfig(XSSFWorkbook wb, ExcelExportQRConfig[] excelExportQRConfig, Object dto) {
        try {
            for (ExcelExportQRConfig QRConfigAT : excelExportQRConfig) {
                Field field = dto.getClass().getDeclaredField(QRConfigAT.field());
                field.setAccessible(true);
                String value = (String) field.get(dto);
                if (StringUtils.isNotBlank(value)) {
                    byte[] qr = generateQRCodeToByte(value, QRConfigAT.width(), QRConfigAT.height());
                    int qrId = wb.addPicture(qr, Workbook.PICTURE_TYPE_PNG);

                    XSSFSheet sheet = wb.getSheetAt(QRConfigAT.sheetIndex());
                    if (sheet == null)
                        continue;
                    XSSFDrawing drawing = sheet.createDrawingPatriarch();
                    XSSFCreationHelper helper = wb.getCreationHelper();
                    XSSFClientAnchor anchor = helper.createClientAnchor();
                    ExcelPosition pos = new ExcelPosition(QRConfigAT.cell());
                    anchor.setCol1(pos.column);
                    anchor.setRow1(pos.row);

                    Picture picture = drawing.createPicture(anchor, qrId);
                    picture.resize();
                }
            }
        } catch (Exception e) {
            log.error("", e);
        }
    }

    @SuppressWarnings("unchecked")
    private void processAreas(XSSFWorkbook wb, ExcelExportAreas areasAT, Object cellValue,
            Map<Integer, CellStyle> cellStyleMap) {
        List<Object> areas = null;

        if (cellValue instanceof List<?>) {
            if (((List<Object>) cellValue).size() == 0)
                return;
            areas = (List<Object>) cellValue;
        } else {
            areas = new ArrayList<>(1);
            areas.add(cellValue);
        }

        XSSFSheet sheet = wb.getSheetAt(areasAT.sheetIndex());
        if (sheet == null)
            return;
        ExcelPosition areaStartPos = new ExcelPosition(areasAT.startCell());

        // Check the first area for Stripe
        Object firstArea = areas.get(0);
        ExcelExportAreaStripe stripeAT = firstArea.getClass().getAnnotation(ExcelExportAreaStripe.class);

        // Normal Area
        if (stripeAT == null) {
            int size = areasAT.size() < 0 ? areas.size() : areasAT.size();
            int count = 0;
            // for each Area
            for (Object area : areas) {
                if (count == size)
                    break;

                if (area != null) {
                    // for each variable of this area
                    for (Field field : area.getClass().getDeclaredFields()) {
                        field.setAccessible(true);
                        if (field.getAnnotation(ExcelExportAreaElement.class) == null)
                            continue;

                        ExcelExportAreaElement elementAT = field.getAnnotation(ExcelExportAreaElement.class);
                        ExcelExportColorArea styleElementAT = field.getAnnotation(ExcelExportColorArea.class);
                        ValueDefinition def = field.getAnnotation(ValueDefinition.class);
                        ExcelPosition elementPos = areaStartPos.offset(elementAT.offsetColumn(), elementAT.offsetRow());

                        try {
                            Object value = field.get(area);
                            if (value == null)
                                continue;

                            if (field.getType().isAssignableFrom(List.class)) {
                                List<Object> elements = (List<Object>) value;
                                if (elements.size() == 0)
                                    continue;

                                int elementSize = elementAT.size() < 0 ? elements.size() : elementAT.size();
                                int elementCount = 0;

                                for (Object element : elements) {
                                    if (elementCount == elementSize)
                                        break;

                                    if (element != null) {
                                        if (styleElementAT != null) {
                                            setCellStyle(sheet, elementPos, cellStyleMap, (Integer) element);
                                        } else {
                                            setCellValue(sheet, elementPos, element, def);
                                        }
                                    }

                                    elementCount++;
                                    if (elementAT.direction() == Direction.VERTICAL)
                                        elementPos.nextRow();
                                    else if (elementAT.direction() == Direction.HORIZONTAL)
                                        elementPos.nextColumn();
                                }
                            } else {
                                setCellValue(sheet, elementPos, value, def);
                            }
                        } catch (Exception e) {
                            log.error("", e);
                        }
                    }
                }

                count++;
                if (areasAT.direction() == Direction.VERTICAL)
                    areaStartPos.stepRow(areasAT.offset());
                else if (areasAT.direction() == Direction.HORIZONTAL)
                    areaStartPos.stepColumn(areasAT.offset());
            }
        }
        // Stripe Area
        else {
            try {
                Field colorIdField = firstArea.getClass().getDeclaredField(stripeAT.colorIdField());
                colorIdField.setAccessible(true);
                Field offsetColumnField = firstArea.getClass().getDeclaredField(stripeAT.offsetColumnField());
                offsetColumnField.setAccessible(true);
                Field offsetRowField = firstArea.getClass().getDeclaredField(stripeAT.offsetRowField());
                offsetRowField.setAccessible(true);

                // for each Area
                for (Object area : areas) {
                    ExcelPosition stripeStartPos = areaStartPos.offset((int) offsetColumnField.get(area),
                            (int) offsetRowField.get(area));

                    List<Integer> colorIdList = (List<Integer>) colorIdField.get(area);

                    // for each color cell
                    for (Integer colorId : colorIdList) {

                        setCellStyle(sheet, stripeStartPos, cellStyleMap, colorId);

                        if (stripeAT.direction() == Direction.VERTICAL)
                            stripeStartPos.nextRow();
                        else if (stripeAT.direction() == Direction.HORIZONTAL)
                            stripeStartPos.nextColumn();
                    }
                }
            } catch (Exception e) {
                log.error("", e);
            }
        }
    }

    private void processPrintArea(XSSFWorkbook wb, ExcelExportPrintArea rangeAT, Object cellValue,
            ValueDefinition def) {
        try {
            ExcelExportPrintAreaInfo infoAT = cellValue.getClass().getAnnotation(ExcelExportPrintAreaInfo.class);
            if (infoAT == null)
                return;

            Field nameField = cellValue.getClass().getDeclaredField(infoAT.name());
            nameField.setAccessible(true);
            Field fromRowField = cellValue.getClass().getDeclaredField(infoAT.fromRow());
            fromRowField.setAccessible(true);
            Field toRowField = cellValue.getClass().getDeclaredField(infoAT.toRow());
            toRowField.setAccessible(true);
            Field fromColumnField = cellValue.getClass().getDeclaredField(infoAT.fromColumn());
            fromColumnField.setAccessible(true);
            Field toColumnField = cellValue.getClass().getDeclaredField(infoAT.toColumn());
            toColumnField.setAccessible(true);

            XSSFSheet sheet = wb.getSheetAt(rangeAT.sheetIndex());
            if (sheet == null)
                return;

            String name = (String) nameField.get(cellValue);
            Integer fromRow = (Integer) fromRowField.get(cellValue);
            Integer toRow = (Integer) toRowField.get(cellValue);
            String fromColumn = CellReference.convertNumToColString((Integer) fromColumnField.get(cellValue));
            String toColumn = CellReference.convertNumToColString((Integer) toColumnField.get(cellValue));

            // E.g: =荳頑｣滂ｽ樊惠螳?!$A$1:$KQ$64
            String reference = "$" + fromColumn + "$" + (fromRow + 1) + ":$" + toColumn + "$" + (toRow + 1);
            wb.setPrintArea(rangeAT.sheetIndex(), reference);

        } catch (Exception e) {
            log.error("", e);
        }
    }

    private void processMergeRegion(XSSFWorkbook wb, List<ExcelExportMergeRegion> mergeATs, Object cellValue,
            Map<Integer, CellStyle> cellStyleMap, ValueDefinition def) {
        try {
            ExcelExportMergeRegionInfo infoAT = cellValue.getClass().getAnnotation(ExcelExportMergeRegionInfo.class);
            if (infoAT == null)
                return;

            Field offsetColumnField = cellValue.getClass().getDeclaredField(infoAT.offsetColumn());
            offsetColumnField.setAccessible(true);
            Field offsetRowField = cellValue.getClass().getDeclaredField(infoAT.offsetRow());
            offsetRowField.setAccessible(true);
            Field styleIdField = cellValue.getClass().getDeclaredField(infoAT.styleId());
            styleIdField.setAccessible(true);
            Field valueField = cellValue.getClass().getDeclaredField(infoAT.value());
            valueField.setAccessible(true);

            int offsetColumn = offsetColumnField.getInt(cellValue);
            int offsetRow = offsetRowField.getInt(cellValue);
            int styleId = styleIdField.getInt(cellValue);
            String value = null;
            if (valueField.get(cellValue) != null)
                value = String.valueOf(valueField.get(cellValue));

            Map<Integer, List<ExcelExportMergeRegion>> tableAtPerSheetMap = new HashMap<>();
            for (ExcelExportMergeRegion at : mergeATs) {
                int sheetIndex = at.sheetIndex();
                List<ExcelExportMergeRegion> columnAtList = tableAtPerSheetMap.get(sheetIndex);
                if (columnAtList == null) {
                    columnAtList = new ArrayList<>();
                    tableAtPerSheetMap.put(sheetIndex, columnAtList);
                }
                columnAtList.add(at);
            }

            // For each sheet
            for (Map.Entry<Integer, List<ExcelExportMergeRegion>> entry : tableAtPerSheetMap.entrySet()) {
                int sheetIndex = entry.getKey();
                List<ExcelExportMergeRegion> sameSheetRegionAtList = entry.getValue();

                XSSFSheet sheet = wb.getSheetAt(sheetIndex);
                if (sheet == null)
                    continue;

                // For each region of the table in the same sheet
                regionBreak: for (ExcelExportMergeRegion at : sameSheetRegionAtList) {

                    ExcelPosition startPos = new ExcelPosition(at.startCell());
                    ExcelPosition pos = new ExcelPosition(at.startCell());

                    while (pos.row <= startPos.row + offsetRow) {
                        pos.column = startPos.column;
                        while (pos.column <= startPos.column + offsetColumn) {
                            setCellStyle(sheet, pos, cellStyleMap, styleId);
                            pos.nextColumn();
                        }
                        pos.nextRow();
                    }

                    CellRangeAddress cellRangeAddress = new CellRangeAddress(startPos.row, startPos.row + offsetRow,
                            startPos.column, startPos.column + offsetColumn);
                    sheet.addMergedRegion(cellRangeAddress);

                    setCellValue(sheet, startPos, value, def);
                }
            }
        } catch (Exception e) {
            log.error("", e);
        }
    }

    @SuppressWarnings("unchecked")
    private void processLineBreak(XSSFWorkbook wb, ExcelExportLineBreak lineBreakAt, Object cellValue,
            ValueDefinition def) {
        List<Integer> rowList;
        try {
            rowList = (List<Integer>) cellValue;
        } catch (Exception e) {
            return;
        }

        int sheetIndex = lineBreakAt.sheetIndex();
        XSSFSheet sheet = wb.getSheetAt(sheetIndex);
        int startColumn = CellReference.convertColStringToIndex(lineBreakAt.startColumn());
        int endColumn = CellReference.convertColStringToIndex(lineBreakAt.endColumn());
        int rowOffset = lineBreakAt.rowOffset();

        // For each row
        for (Integer row : rowList) {
            CreationHelper helper = wb.getCreationHelper();
            XSSFClientAnchor anchor = (XSSFClientAnchor) helper.createClientAnchor();
            anchor.setRow1(row + rowOffset);
            anchor.setRow2(row + rowOffset);
            anchor.setCol1(startColumn);
            anchor.setCol2(endColumn);

            XSSFDrawing drawing = (XSSFDrawing) sheet.createDrawingPatriarch();
            XSSFSimpleShape shape = drawing.createSimpleShape(anchor);
            shape.setShapeType(ShapeTypes.LINE);
            shape.setLineWidth(2);
            shape.setLineStyle(0);
            shape.setLineStyleColor(0, 0, 0);
        }
    }

    private Cell getCellByName(XSSFSheet sheet, String name) {
        ExcelPosition pos = new ExcelPosition(name);
        Row row = sheet.getRow(pos.row);
        if (row == null)
            return null;
        Cell cell = row.getCell(pos.column);
        if (cell == null)
            return null;
        return cell;
    }

    private void setStringCellValue(Cell cell, String value) {
        if (cell != null && value != null) {
            if (value.length() >= EXCEL_CELL_STRING_MAX_SIZE) {
                cell.setCellValue(DATA_TOO_LONG);
            } else {
                cell.setCellValue(value);
            }
        }
    }

    private void setCellValue(XSSFSheet sheet, ExcelPosition pos, Object value, ValueDefinition def) {
        if (value == null)
            return;

        Row row = sheet.getRow(pos.row);
        if (row == null)
            row = sheet.createRow(pos.row);
        Cell cell = row.getCell(pos.column);
        if (cell == null) {
            cell = row.createCell(pos.column);
            cell.setCellType(CellType.STRING);
        }

        if (value instanceof Date) {
            Date date = (Date) value;
            if (def != null) {
                if (StringUtils.isBlank(def.dateFormat())) {
                    cell.setCellValue(date);
                } else {
                    String dateTime = "";
                    dateTime = DatetimeUtil.formatDateToString(date, def.dateFormat());
                    setStringCellValue(cell, dateTime);
                }
            } else {
                cell.setCellValue(date);
            }
        } else if (value instanceof Number) {
            if (value instanceof Double || value instanceof BigDecimal) {
                BigDecimal tmp = value instanceof Double ? new BigDecimal((Double) value) : (BigDecimal) value;
                if (def != null) {
                    try {
                        tmp = tmp.setScale(def.decimalScale(), def.roundingMode());
                    } catch (Exception e) {
                        tmp = tmp.setScale(def.decimalScale(), RoundingMode.HALF_EVEN);
                    }
                }
                cell.setCellValue(tmp.doubleValue());
            } else if (value instanceof Integer) {
                Integer tmp = (Integer) value;
                cell.setCellValue(tmp);
            } else if (value instanceof BigInteger) {
                BigInteger tmp = (BigInteger) value;
                cell.setCellValue(tmp.intValue());
            }
            cell.setCellType(CellType.NUMERIC);
        } else if (value instanceof String) {
            setStringCellValue(cell, (String) value);
            cell.setCellType(CellType.STRING);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
            cell.setCellType(CellType.BOOLEAN);
        }
    }

    private void setCellStyle(XSSFSheet sheet, ExcelPosition pos, Map<Integer, CellStyle> cellStyleMap,
            Integer cellStyleId) {
        if (cellStyleId == null)
            return;

        Row row = sheet.getRow(pos.row);
        if (row == null)
            row = sheet.createRow(pos.row);
        Cell cell = row.getCell(pos.column);
        if (cell == null) {
            cell = row.createCell(pos.column);
            cell.setCellType(CellType.STRING);
        }
        cell.setCellStyle(cellStyleMap.get(cellStyleId));
    }

    private String replaceParam(Map<String, String> params, String input) {
        String result = new String(input);

        Matcher m = Pattern.compile("\\{([\\w]+)\\}").matcher(input);
        while (m.find()) {
            String key = m.group(1);
            if (params.containsKey(key) && StringUtils.isNotBlank(params.get(key)))
                result = result.replaceAll("\\{" + key + "\\}", params.get(key));
        }
        return result;
    }

    public static class ExcelTarget {
        private Map<Integer, List<ExcelPosition>> targetMap = new HashMap<>();

        public void add(int sheetIndex, String cellAddress) {
            List<ExcelPosition> posList = targetMap.get(sheetIndex);
            if (posList == null) {
                posList = new ArrayList<>();
                targetMap.put(sheetIndex, posList);
            }
            ExcelPosition pos = new ExcelPosition(cellAddress);
            posList.add(pos);
        }

        public void add(int sheetIndex, int row, int column) {
            List<ExcelPosition> posList = targetMap.get(sheetIndex);
            if (posList == null) {
                posList = new ArrayList<>();
                targetMap.put(sheetIndex, posList);
            }
            ExcelPosition pos = new ExcelPosition(column, row);
            posList.add(pos);
        }
    }

    public TempFile markCellError(XSSFWorkbook wb, ExcelTarget newTarget, TempFile sourceFile, List<String> errorList) {
        String originalName = sourceFile.getBusinessName();
        TempFile resultFile = createTempFile(originalName, Extension.getFromName(originalName));
        try (FileOutputStream fos = new FileOutputStream(resultFile);) {
            ObjectMapper mapper = new ObjectMapper();
            mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
            mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

            // ===========================================================================
            // Clear error
            XSSFSheet errorShet = wb.getSheet(excelSheetError);
            if (errorShet != null && errorShet.getRow(0) != null && errorShet.getRow(0).getCell(0) != null
                    && StringUtils.isNotBlank(errorShet.getRow(0).getCell(0).getStringCellValue())) {

                Font noErrorFont = wb.createFont();
                noErrorFont.setColor(IndexedColors.BLACK.getIndex());
                noErrorFont.setBold(false);

                String oldError = errorShet.getRow(0).getCell(0).getStringCellValue().trim();
                ExcelTarget oldTarget = mapper.readValue(errorShet.getRow(0).getCell(0).getStringCellValue(),
                        ExcelTarget.class);

                for (Map.Entry<Integer, List<ExcelPosition>> entry : oldTarget.targetMap.entrySet()) {
                    Integer sheetIndex = entry.getKey();
                    List<ExcelPosition> posList = entry.getValue();
                    XSSFSheet sheet = wb.getSheetAt(sheetIndex);

                    for (ExcelPosition pos : posList) {
                        Row row = sheet.getRow(pos.row);
                        if (row == null)
                            continue;
                        Cell cell = row.getCell(pos.column);
                        if (cell == null)
                            continue;
                        cell.getCellStyle().setFont(noErrorFont);
                    }
                }
            }

            for (Row row : errorShet)
                errorShet.removeRow(row);

            // ===========================================================================
            // Mark error
            Font errorFont = wb.createFont();
            errorFont.setColor(IndexedColors.RED.getIndex());
            errorFont.setBold(true);

            for (Map.Entry<Integer, List<ExcelPosition>> entry : newTarget.targetMap.entrySet()) {
                Integer sheetIndex = entry.getKey();
                List<ExcelPosition> posList = entry.getValue();
                XSSFSheet sheet = wb.getSheetAt(sheetIndex);

                for (ExcelPosition pos : posList) {
                    Row row = sheet.getRow(pos.row);
                    if (row == null)
                        continue;
                    Cell cell = row.getCell(pos.column);
                    if (cell == null)
                        continue;
                    CellStyle cellStyle = wb.createCellStyle();
                    cellStyle.cloneStyleFrom(cell.getCellStyle());
                    cellStyle.setFont(errorFont);
                    cell.setCellStyle(cellStyle);
                }
            }

            // ===========================================================================
            // Record error
            if (errorShet == null)
                errorShet = wb.createSheet(excelSheetError);

            // =====================================
            // error mapping
            Row row = errorShet.getRow(0);
            if (row == null)
                row = errorShet.createRow(0);
            Cell cell = row.getCell(0);
            if (cell == null)
                cell = row.createCell(0, CellType.STRING);
            String errorValue = mapper.writeValueAsString(newTarget);
            setStringCellValue(cell, errorValue);

            // =====================================
            // logging error
            int logIndex = 1;
            for (String errorLog : errorList) {
                row = errorShet.getRow(logIndex);
                if (row == null)
                    row = errorShet.createRow(logIndex);

                Cell cellRowIndex = row.getCell(0);
                if (cellRowIndex == null)
                    cellRowIndex = row.createCell(0, CellType.STRING);

                Cell cellContent = row.getCell(1);
                if (cellContent == null)
                    cellContent = row.createCell(1, CellType.STRING);

                String[] errorArr = errorLog.split("-");
                if (errorArr.length == 1) {
                    setStringCellValue(cellContent, errorArr[0]);
                } else if (errorArr.length == 2) {
                    setStringCellValue(cellRowIndex, errorArr[0]);
                    setStringCellValue(cellContent, errorArr[1]);
                }
                logIndex++;
            }

            // =====================================
            // locking error sheet
            errorShet.lockSelectLockedCells(true);
            errorShet.lockSelectUnlockedCells(true);
            errorShet.setSheetPassword(excelSheetPassword, HashAlgorithm.md5);
            errorShet.enableLocking();
            wb.lockStructure();

            // ===========================================================================
            // Write file

            wb.write(fos);
            fos.flush();
        } catch (Exception e) {
            resultFile = null;
            log.error("markCell has failed.", e);
        } finally {
            if (wb != null)
                try {
                    wb.close();
                } catch (Exception e1) {
                }
            if (sourceFile != null)
                try {
                    sourceFile.delete();
                } catch (Exception e1) {
                }
        }
        return resultFile;
    }

    public TempFile convertExcelToPdfAuto(File inputExcel, boolean deleteAfter, int sheetIndex)
//            throws ApplicationException
    {
//        if (officePool == null)
//            throw new ApplicationException("OfficeManagerPool is not available");

        TempFile genPdfFile = null;
        if (inputExcel == null)
            return genPdfFile;

        String genPdfName = changeExtension(
                inputExcel instanceof TempFile ? ((TempFile) inputExcel).getBusinessName() : inputExcel.getName(),
                Extension.PDF);
        genPdfFile = createTempFile(genPdfName, Extension.PDF);

        TempFile tmpExcel = createTempFile(null, Extension.XLSX);

        XSSFWorkbook inputWb = null;
        try (FileInputStream inputFis = new FileInputStream(inputExcel);
                FileOutputStream tmpFos = new FileOutputStream(tmpExcel);) {
            inputWb = new XSSFWorkbook(inputFis);
            inputFis.close();
            for (int i = inputWb.getNumberOfSheets() - 1; i > -1; i--) {
                if (i != sheetIndex)
                    inputWb.removeSheetAt(i);
            }
            inputWb.write(tmpFos);
            inputWb.close();

            boolean isDone = false;
            int count = 0;
            Exception exception = null;
            log.info("convertExcelToPdfAuto - Start");
            log.info("File : " + genPdfName);
            while (!isDone && count < officePool.SIZE * officePool.RETRY) {
                count++;
                if (count % officePool.SIZE == 0)
                    Thread.sleep(2000);
                try {
                    officePool.getConverter().convert(tmpExcel).to(genPdfFile).execute();
                    isDone = true;
                } catch (Exception e) {
                    log.error("Attempted " + count + " has failed. Retry.");
                    exception = e;
                    isDone = false;
                }
            }
            log.info("convertExcelToPdfAuto - End");
            if (!isDone)
                throw exception;
        } catch (Exception e) {
            log.error("convertExcelToPdfAuto has failed.", e);
            try {
                if (genPdfFile != null) {
                    genPdfFile.delete();
                }
            } catch (Exception e2) {
            }
        } finally {
            try {
                if (tmpExcel != null) {
                    tmpExcel.delete();
                }
            } catch (Exception e2) {
            }
            try {
                if (inputExcel != null && deleteAfter) {
                    inputExcel.delete();
                }
            } catch (Exception e2) {
            }
        }
        return genPdfFile;
    }

    public TempFile mergePdfToPdfFile(List<TempFile> fileList, String name, boolean deleteAfter) {
        TempFile genFile = null;
        if (fileList == null || fileList.size() == 0 || StringUtils.isBlank(name))
            return genFile;

        genFile = createTempFile(name, Extension.PDF);
        if (genFile == null)
            return genFile;
        List<FileInputStream> fisList = new ArrayList<>();
        try (FileOutputStream generatedFOS = new FileOutputStream(genFile)) {
            PDFMergerUtility PDFmerger = new PDFMergerUtility();
            PDFmerger.setDestinationStream(generatedFOS);

            for (TempFile file : fileList) {
                FileInputStream fis = new FileInputStream(file);
                fisList.add(fis);
                PDFmerger.addSource(fis);
            }
            PDFmerger.mergeDocuments(null);
        } catch (Exception e) {
            log.error("mergePdfToFile has failed.", e);
            try {
                if (genFile != null)
                    genFile.delete();
            } catch (Exception e2) {
            }
        } finally {
            for (FileInputStream fis : fisList) {
                try {
                    if (fis != null)
                        fis.close();
                } catch (Exception e2) {
                }
            }
            if (deleteAfter) {
                for (File file : fileList) {
                    try {
                        if (file != null)
                            file.delete();
                    } catch (Exception e2) {
                    }
                }
            }
        }
        return genFile;
    }

    public TempFile generateQRCodeToFile(String input, int width, int height) {
        TempFile genFile = null;
        try {
            genFile = createTempFile("QR.png", Extension.PNG);

            Map<EncodeHintType, Object> hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
            hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
            hints.put(EncodeHintType.MARGIN, 1); /* default = 4 */

            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            BitMatrix bitMatrix = qrCodeWriter.encode(input, BarcodeFormat.QR_CODE, width, height, hints);

            MatrixToImageWriter.writeToPath(bitMatrix, "PNG", genFile.toPath());
        } catch (Exception e) {
            log.error("generateQRCode has failed.", e);
            try {
                if (genFile != null) {
                    genFile.delete();
                }
            } catch (Exception e2) {
            }
        }
        return genFile;
    }

    public static byte[] generateQRCodeToByte(String input, int width, int height) {
        ByteArrayOutputStream pngOutputStream = null;
        byte[] pngData = null;

        Map<EncodeHintType, Object> hints = new EnumMap<EncodeHintType, Object>(EncodeHintType.class);
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8");
        hints.put(EncodeHintType.MARGIN, 1); /* default = 4 */

        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = qrCodeWriter.encode(input, BarcodeFormat.QR_CODE, width, height, hints);
            pngOutputStream = new ByteArrayOutputStream();
            MatrixToImageWriter.writeToStream(bitMatrix, "PNG", pngOutputStream);
            pngData = pngOutputStream.toByteArray();
        } catch (WriterException | IOException e) {
            log.error("generateQRCode has failed.", e);
//            throw new ApplicationException("E018", e.getMessage());
        } finally {
            try {
                if (pngOutputStream != null) {
                    pngOutputStream.close();
                }
            } catch (IOException e) {
                log.error("generateQRCode has failed.", e);
//                throw new ApplicationException("E018", e.getMessage());
            }
        }
        return pngData;
    }

    public String decodeQRCodeFromImage(File qrCodeimage) {
        try {
            BufferedImage bufferedImage = ImageIO.read(qrCodeimage);
            LuminanceSource source = new BufferedImageLuminanceSource(bufferedImage);
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

            Result result = new MultiFormatReader().decode(bitmap);
            return result.getText();
        } catch (NotFoundException e) {
            log.error("There is no QR code in the image", e);
        } catch (Exception e) {
            log.error("decodeQRCodeFromImage has failed.", e);
        }
        return null;
    }

    /**
     *
     * @param pdfFile
     * @param pageIndex   (start from 1)
     * @param deleteAfter
     * @return
     */
    public String decodeQRCodeFromPdf(File pdfFile, int pageIndex, boolean deleteAfter) {
        PDDocument document = null;
        try {
            document = PDDocument.load(pdfFile);
            PDFRenderer pdfRenderer = new PDFRenderer(document);
            BufferedImage bufferedImage = pdfRenderer.renderImageWithDPI(pageIndex - 1, 300, ImageType.RGB);

            LuminanceSource source = new BufferedImageLuminanceSource(bufferedImage);
            BinaryBitmap bitmap = new BinaryBitmap(new HybridBinarizer(source));

            Result result = new MultiFormatReader().decode(bitmap);
            return result.getText();
        } catch (NotFoundException e) {
            log.error("There is no QR code in the file at page " + pageIndex, e);
        } catch (Exception e) {
            log.error("decodeQRCodeFromPdf has failed.", e);
        } finally {
            try {
                if (document != null) {
                    document.close();
                }
            } catch (Exception e2) {
            }
            try {
                if (pdfFile != null && deleteAfter) {
                    pdfFile.delete();
                }
            } catch (Exception e2) {
            }
        }
        return null;
    }

    // At 01:00:00am every day
    @Scheduled(cron = "0 0 1 * * ?")
    private void tempFileAutoCleaning() {
        log.info(" * * * * * * * * * * * * * * * * * * * ");
        log.info(" * * * * * * * * * * * * * * * * * * * ");
        log.info(" *   Temp folder auto cleaning ..    * ");
        log.info(" * * * * * * * * * * * * * * * * * * * ");
        long curentTime = System.currentTimeMillis();
        long TTL = tempFileTTL * 60 * 60 * 1000;
        log.info("Allowed Time to live : " + TTL + " milliseconds.");
        log.info(" * * * * * * * * * * * * * * * * * * * ");
        if (TempFolder != null && TempFolder.listFiles() != null) {
            for (File file : TempFolder.listFiles()) {
                if (!file.isFile())
                    continue;

                BasicFileAttributes attr = null;
                try {
                    attr = java.nio.file.Files.readAttributes(file.toPath(), BasicFileAttributes.class);
                } catch (IOException e1) {
                    log.warn("Can not read temp file " + file.getName());
                }
                if (attr != null) {
                    long liveTime = curentTime - attr.creationTime().toMillis();
                    if (liveTime > TTL) {
                        try {
                            if (file.delete()) {
                                log.info("Deleting file : [" + file.getName() + "]. Created time : "
                                        + attr.creationTime().toString() + ". Lived time : " + liveTime
                                        + " milliseconds.");
                            }
                        } catch (Exception e) {
                            log.warn("Can not delete temp file " + file.getName());
                        }
                    }
                }
            }
            // clean up temporary physical cache folder for downloading file

            for (File file : TempFolder.listFiles()) {
                if (file.isFile())
                    continue;

                try {
                    long liveTime = curentTime - Long.parseLong(file.getName());
                    if (liveTime > TTL) {
                        if (file.delete()) {
                            log.debug("Clear tmp cache folder " + file.getName());
                        }
                    }
                } catch (Exception e) {
                    log.warn("Can not delete temp cache folder " + file.getName());
                }
            }
        }

        if (downloadingCacheFolder != null && downloadingCacheFolder.listFiles() != null) {
            for (File file : downloadingCacheFolder.listFiles()) {
                if (file.isFile())
                    continue;

                try {
                    BasicFileAttributes attr = java.nio.file.Files.readAttributes(file.toPath(),
                            BasicFileAttributes.class);

                    long liveTime = curentTime - attr.creationTime().toMillis();
                    if (liveTime > TTL) {
                        if (file.delete()) {
                            log.debug("Clear download cache folder " + file.getName());
                        }
                    }
                } catch (Exception e) {
                    log.warn("Can not delete download cache folder " + file.getName());
                }
            }
        }
        log.info(" * * * * * * * * * * * * * * * * * * * ");
    }

    // @Scheduled(cron = "[Seconds] [Minutes] [Hours] [Day of month] [Month] [Day of
    // week]")
    // @Scheduled(cron = "0 0 18 ? * SUN")//12 AM each Sunday
    @Scheduled(cron = "0 30 0 * * ?") // 0:30 AM each day (after new log file created)
    private void saveAccessLogToDatabase() {

    }

    public String encodeFileToStringBase64(File file, boolean deleteAfter) {
        String result = null;
        if (file == null)
            return result;
        try (FileInputStream fis = new FileInputStream(file)) {
            result = Base64.encodeBase64String(IOUtils.toByteArray(fis));
        } catch (Exception e) {
            log.error("", e);
        } finally {
            if (deleteAfter) {
                try {
                    file.delete();
                } catch (Exception e2) {
                }
            }
        }
        return result;
    }

    private class CRC16 {
        public String genHexString(File file) {
            if (file == null)
                return null;
            try (FileInputStream fis = new FileInputStream(file)) {
                int[] table = { 0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241, 0xC601, 0x06C0, 0x0780,
                        0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440, 0xCC01, 0x0CC0, 0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81,
                        0x0E40, 0x0A00, 0xCAC1, 0xCB81, 0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841, 0xD801, 0x18C0, 0x1980,
                        0xD941, 0x1B00, 0xDBC1, 0xDA81, 0x1A40, 0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01, 0x1DC0, 0x1C80,
                        0xDC41, 0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0, 0x1680, 0xD641, 0xD201, 0x12C0, 0x1380,
                        0xD341, 0x1100, 0xD1C1, 0xD081, 0x1040, 0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281,
                        0x3240, 0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441, 0x3C00, 0xFCC1, 0xFD81,
                        0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41, 0xFA01, 0x3AC0, 0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881,
                        0x3840, 0x2800, 0xE8C1, 0xE981, 0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41, 0xEE01, 0x2EC0, 0x2F80,
                        0xEF41, 0x2D00, 0xEDC1, 0xEC81, 0x2C40, 0xE401, 0x24C0, 0x2580, 0xE541, 0x2700, 0xE7C1, 0xE681,
                        0x2640, 0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0, 0x2080, 0xE041, 0xA001, 0x60C0, 0x6180,
                        0xA141, 0x6300, 0xA3C1, 0xA281, 0x6240, 0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480,
                        0xA441, 0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41, 0xAA01, 0x6AC0, 0x6B80,
                        0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840, 0x7800, 0xB8C1, 0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80,
                        0xBA41, 0xBE01, 0x7EC0, 0x7F80, 0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40, 0xB401, 0x74C0, 0x7580,
                        0xB541, 0x7700, 0xB7C1, 0xB681, 0x7640, 0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101, 0x71C0, 0x7080,
                        0xB041, 0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0, 0x5280, 0x9241, 0x9601, 0x56C0, 0x5780,
                        0x9741, 0x5500, 0x95C1, 0x9481, 0x5440, 0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81,
                        0x5E40, 0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841, 0x8801, 0x48C0, 0x4980,
                        0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40, 0x4E00, 0x8EC1, 0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80,
                        0x8C41, 0x4400, 0x84C1, 0x8581, 0x4540, 0x8701, 0x47C0, 0x4680, 0x8641, 0x8201, 0x42C0, 0x4380,
                        0x8341, 0x4100, 0x81C1, 0x8081, 0x4040, };

                byte[] bytes = IOUtils.toByteArray(fis);
                int crc = 0x0000;
                for (byte b : bytes)
                    crc = (crc >>> 8) ^ table[(crc ^ b) & 0xff];
                return Integer.toHexString(crc);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    public String genCRC16inHexString(File file) {
        CRC16 crc = new CRC16();
        return crc.genHexString(file);
    }

    public String genCRC32inHexString(File file) {
        Integer crcValue = null;
        if (file == null || !file.exists() || !file.isFile())
            return null;
        try (FileInputStream fis = new FileInputStream(file)) {

            CRC32 crc = new CRC32();
            int data = fis.read();
            while (data != -1) {
                crc.update(data);
                data = fis.read();
            }
            crcValue = (int) crc.getValue();
            return Integer.toHexString(crcValue);
        } catch (IOException e) {
            log.error("", e);
            return null;
        }
    }

    /**
     * Convert File to byte[]
     *
     * @param file
     * @return
     */
    public static byte[] convertFileToByteArray(File file) {
        byte[] bytesArray = null;
        if (null == file) {
            return bytesArray;
        }

        try (FileInputStream fileInputStream = new FileInputStream(file)) {
            bytesArray = new byte[(int) file.length()];
            // read file into bytes[]
            fileInputStream.read(bytesArray);

        } catch (IOException e) {
            log.error("", e);
//            throw new ApplicationException(e);
        }
        return bytesArray;
    }
}
