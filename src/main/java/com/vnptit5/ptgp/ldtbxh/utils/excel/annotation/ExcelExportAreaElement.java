package com.vnptit5.ptgp.ldtbxh.utils.excel.annotation;

import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.ExcelExportList.Direction;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author ToiTV
 * 2020-10-17
 */
@Target(value = { ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelExportAreaElement {

    int offsetColumn();

    int offsetRow();

    int size() default -1;

    Direction direction() default Direction.HORIZONTAL;

}

