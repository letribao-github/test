package com.vnptit5.ptgp.ldtbxh.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * @author ToiTV
 * 2020-10-17
 */


@Slf4j
public class StringUtil {

    /**
     * Convert string to ISO-2022-JP charactor
     *
     * @param targetStr
     * @return
     */
    public static String convertISO2022JPCharacters(String targetStr) {

        if (targetStr == null) {
            return null;
        }

        char[] ch = targetStr.toCharArray();

        for (int i = 0; i < ch.length; i++) {
            switch (ch[i]) {

                // '-'(Double byte hyphen)
                case '\u2015':
                    ch[i] = '\u2014';
                    break;
                // '－'(Double byte minus)
                case '\uff0d':
                    ch[i] = '\u2212';
                    break;
                // '～'(Tilde)
                case '\uff5e':
                    ch[i] = '\u301c';
                    break;
                // '∥'(Pipe)
                case '\u2225':
                    ch[i] = '\u2016';
                    break;
                // '￠'(Cent symbol)
                case '\uffe0':
                    ch[i] = '\u00A2';
                    break;
                // '￡'(Pound symbol)
                case '\uffe1':
                    ch[i] = '\u00A3';
                    break;
                // '￢' (Negation symbol)
                case '\uffe2':
                    ch[i] = '\u00AC';
                    break;
                default:
                    break;
            }
        }

        return String.valueOf(ch);
    }

    /**
     * Convert to escapse special characters (Ex: % _ ! ?) to search like in native query
     *
     * @param str
     * @return
     */
    public static String escapeSpecialCharacterSqlQuery(String str) {
        String result = StringUtils.EMPTY;
        if (StringUtils.isNoneBlank(str)) {
            String formatValue = StringUtils.trimToEmpty(str);
            result = formatValue.replace("%", "\\%")
                    .replace("_", "\\_")
                    .replace("[", "\\[")
                    .replace("]", "\\]");
        }
        return result;
    }

    /**
     * Build LIKE query String
     *
     * @param value
     *            the key word value
     * @return string query with append %%
     */
    public static String getLikeQueryString(String value) {
        // Trim and remove space
        String formatValue = StringUtils.trimToEmpty(value);
        return "%" + StringUtil.escapeSpecialCharacterSqlQuery(formatValue) + "%";
    }

    /**
     * Concat the list of string with the given delimiter
     *
     * @param delimiter
     * @param values
     * @return
     */
    public static String concatStringNotEmpty(String delimiter, String... values) {
        if (values.length > 0 && delimiter != null) {
            List<String> listNotEmptyValue = new ArrayList<>();
            for (String str : values) {
                if (StringUtils.isNotEmpty(str)) {
                    listNotEmptyValue.add(str);
                }
            }
            return StringUtils.join(listNotEmptyValue, delimiter);
        }
        return null;
    }

    /**
     * File name must not contains dot "."<br>
     * file_name.pdf, fileName.xlsx, ...
     *
     * @param
     * @return
     */
    public static String genUniqueText() {
        return String.format("%s", RandomStringUtils.randomAlphanumeric(8));
    }
}
