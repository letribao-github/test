package com.vnptit5.ptgp.ldtbxh.utils.excel.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author ToiTV
 * 2020-10-17
 */
@Target(value = { ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(ExcelExportList.List.class)
public @interface ExcelExportList {

    int sheetIndex();

    String startCell();

    int size();

    int regionGroup();

    int regionOrder() default 0;

    static enum Direction {
        HORIZONTAL,
        VERTICAL;
    }

    Direction direction();

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.FIELD })
    @Documented
    @interface List {
        ExcelExportList[] value();
    }
}

