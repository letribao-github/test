package com.vnptit5.ptgp.ldtbxh.utils.excel.annotation;

import java.lang.annotation.*;

/**
 * @author ToiTV
 * 2020-10-17
 */
@Target(value = { ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(ExcelExportForm.List.class)
public @interface ExcelExportForm {

    int sheetIndex();

    String cell() default "";

    String[] cellArray() default {};

    int startRow() default 0; // for list object

    Class<?>[] groups() default {};

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.FIELD })
    @Documented
    @interface List {
        ExcelExportForm[] value();
    }
}
