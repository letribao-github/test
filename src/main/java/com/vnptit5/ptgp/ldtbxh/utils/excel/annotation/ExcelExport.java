package com.vnptit5.ptgp.ldtbxh.utils.excel.annotation;

import java.lang.annotation.*;

/**
 * @author ToiTV
 * 2020-10-17
 */

@Retention(RetentionPolicy.RUNTIME)
@Inherited
@Target(value = { ElementType.FIELD, ElementType.METHOD, ElementType.ANNOTATION_TYPE })
public @interface ExcelExport {

    String columnLabel() default "";

    int index() default -1;

    int[] indexArray() default {};

    int startRow() default 0;

    boolean isDropdownList() default false;
}

