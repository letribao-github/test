package com.vnptit5.ptgp.ldtbxh.utils.excel.annotation;

import java.lang.annotation.*;

/**
 * @author ToiTV
 * 2020-10-17
 */
@Target(value = { ElementType.TYPE, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(ExcelExportQRConfig.List.class)
public @interface ExcelExportQRConfig {

    int sheetIndex();

    String field() default "";

    String cell();

    int width();

    int height();

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.TYPE })
    @Documented
    @interface List {
        ExcelExportQRConfig[] value();
    }
}