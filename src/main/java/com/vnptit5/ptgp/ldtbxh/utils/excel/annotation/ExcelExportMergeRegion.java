package com.vnptit5.ptgp.ldtbxh.utils.excel.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Repeatable;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author ToiTV
 * 2020-10-17
 */
@Target(value = { ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Repeatable(ExcelExportMergeRegion.List.class)
public @interface ExcelExportMergeRegion {

    int sheetIndex();

    String startCell();

    Class<?>[] groups() default {};

    @Retention(RetentionPolicy.RUNTIME)
    @Target({ ElementType.FIELD })
    @Documented
    @interface List {
        ExcelExportMergeRegion[] value();
    }

}
