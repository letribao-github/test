package com.vnptit5.ptgp.ldtbxh.utils;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import org.springframework.beans.factory.annotation.Value;

/**
 * @author ToiTV
 * 2020-10-17
 */

@Slf4j
public class DatetimeUtil {

    /**
     * yyyy-MM-dd
     */
    public static final String DATE_FORMAT_3             = "yyyy-MM-dd";
    /**
     * yyyy/MM/dd
     */
    public static final String DATE_FORMAT_4             = "yyyy/MM/dd";
    public static final String DATE_FORMAT_SQL           = "'%Y/%m/%d'";
    /**
     * M/d/yyyy
     */
    public static final String DATE_FORMAT_5             = "M/d/yyyy";
    /**
     * yyyyMMdd
     */
    public static final String DATE_FORMAT_6             = "yyyyMMdd";

    /**
     * dd/MM/yyyy
     */
    public static final String DATE_FORMAT_7             = "dd/MM/yyyy";

    /**
     * yyyy-MM-dd HH:mm:ss
     */
    public static final String DATE_TIME_FORMAT_1 = "yyyy-MM-dd HH:mm:ss";
    /**
     * yyyyMMddHHmmss
     */
    public static final String DATE_TIME_FORMAT_2 = "yyyyMMddHHmmss";

    /**
     * yyyy-MM-dd HH:mm:ss
     */
    public static final String DATE_TIME_FORMAT_3 = "dd/MM/yyyy HH:mm:ss";

    @Value("${spring.jackson.time-zone}")
    private static final String timeZone = "Asia/Ho_Chi_Minh";

    public static String formatDateToString(Date date, String pattern) {
        if (null == date)
            return "";
        if (StringUtils.isBlank(pattern))
            pattern = DATE_FORMAT_3;
        return new SimpleDateFormat(pattern).format(date);
    }

    private DatetimeUtil() {
    }


    public static Date getCurrentDatetime() {
        return Calendar.getInstance().getTime();
    }

    public static String getCurrentDatetimeInFormat(String format) {
        if (StringUtils.isBlank(format))
            format = DATE_FORMAT_3;
        DateFormat df = new SimpleDateFormat(format);
        df.setTimeZone(TimeZone.getTimeZone(timeZone));
        return df.format(getCurrentDatetime());
    }

    /**
     * Client cast missing time value will lead date losing 1 day
     *
     * @return Date without time
     */
    @Deprecated
    public static Date getCurrentDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.MILLISECOND, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.HOUR, 0);
        return cal.getTime();
    }

    public static int getCalendarField(int field) {
        Calendar cal = Calendar.getInstance();
        return cal.get(field);
    }

    public static int getYear(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.YEAR);
    }

    public static int getMonth(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return (cal.get(Calendar.MONTH) + 1);
    }

    @SneakyThrows
    public static Date parseStringToDateWithFormat(String dateString, String format) {
        DateFormat df = new SimpleDateFormat(format);
        Date resultDate = null;
        if (StringUtils.isNotBlank(dateString)) {
            resultDate = df.parse(dateString);
        }
        return resultDate;
    }

    @SneakyThrows
    public static Date dateFormat(Date date, String format) {
        SimpleDateFormat df = new SimpleDateFormat(format);
        Date resultDate = null;
        if (date != null) {
            resultDate = df.parse(df.format(date));
        }
        return resultDate;
    }

    public static boolean isValidDate(String input, String formatString) {
        try {
            SimpleDateFormat format = new SimpleDateFormat(formatString);
            format.setLenient(false);
            format.parse(input);
        } catch (ParseException | IllegalArgumentException ex) {
            log.debug("", ex);
            return false;
        }
        return true;
    }

    public static Date addDays(Date date, int days) {
        if (date == null) return null;
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); // minus number would decrement the days
        return cal.getTime();
    }

    public static Date addMonths(Date date, int months) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, months); // minus number would decrement the months
        return cal.getTime();
    }

    public static int subDate(String dateFrom, String dateTo, String dateFormat) {
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern(dateFormat);
        int result = 0;
        LocalDate locateDateFrom = LocalDate.parse(dateFrom, dateTimeFormatter);
        LocalDate locateDateTo = LocalDate.parse(dateTo, dateTimeFormatter);
        Duration diff = Duration.between(locateDateFrom.atStartOfDay(), locateDateTo.atStartOfDay());
        result = (int) diff.toDays();
        return result;
    }

    public static int subDate(Date dateFrom, Date dateTo, String dateFormat) {
        String dateStringFrom = DatetimeUtil.formatDateToString(dateFrom, dateFormat);
        String dateStringTo = DatetimeUtil.formatDateToString(dateTo, dateFormat);
        return subDate(dateStringFrom, dateStringTo, dateFormat);
    }

    public static boolean isSameDate(Date date1, Date date2, boolean truncateTime) {
        if (date1 == null && date2 == null) {
            return true;
        } else if (date1 != null && date2 != null) {
            if (!truncateTime) {
                return DateUtils.isSameDay(date1, date2);
            } else {
                Calendar cal1 = Calendar.getInstance();
                cal1.setTime(date1);
                cal1.set(Calendar.HOUR, 0);
                cal1.set(Calendar.MINUTE, 0);
                cal1.set(Calendar.SECOND, 0);
                cal1.set(Calendar.MILLISECOND, 0);

                Calendar cal2 = Calendar.getInstance();
                cal2.setTime(date2);
                cal2.set(Calendar.HOUR, 0);
                cal2.set(Calendar.MINUTE, 0);
                cal2.set(Calendar.SECOND, 0);
                cal2.set(Calendar.MILLISECOND, 0);

                return DateUtils.isSameDay(cal1, cal2);
            }
        } else {
            return false;
        }
    }
}
