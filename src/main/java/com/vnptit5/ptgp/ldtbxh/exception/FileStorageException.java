package com.vnptit5.ptgp.ldtbxh.exception;

/**
 * Created by Benjamin Lam on 10/21/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */
public class FileStorageException extends RuntimeException {
    public FileStorageException(String message) {
        super(message);
    }

    public FileStorageException(String message, Throwable cause) {
        super(message, cause);
    }
}
