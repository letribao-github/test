package com.vnptit5.ptgp.ldtbxh.property;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * Created by Benjamin Lam on 10/21/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */
@ConfigurationProperties(prefix = "file")
public class FileStorageProperties {
    private String uploadDir;

    public String getUploadDir() {
        return uploadDir;
    }

    public void setUploadDir(String uploadDir) {
        this.uploadDir = uploadDir;
    }
}
