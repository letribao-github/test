package com.vnptit5.ptgp.ldtbxh.client;

import com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh.TraCuuHoGiaDinhDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.treem.TraCuuTreEmDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh.TraCuuTheoMaDinhDanh;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author trinhctn
 */
@FeignClient(name = "VndisClient", url = "http://egw.vndis.vn/api")
public interface VndisClient {

    @PostMapping("/token/take")
    public ResponseEntity<Map<String, Object>> takeToken(@RequestBody Map<String, Object> body);

    @PostMapping("/res/traCuu")
    public ResponseEntity<List<Map<String, Object>>> getTreEmPhatSinh(@RequestHeader HttpHeaders httpHeaders, @RequestBody TraCuuTreEmDto traCuuTreEmDto);

    @PostMapping("res/traCuuHo")
    public ResponseEntity<List<Map<String, Object>>> traCuuHoGiaDinh(@RequestHeader HttpHeaders httpHeaders, @RequestBody TraCuuHoGiaDinhDto hoGiaDinhDto);

    @PostMapping("res/traCuuMaDinhDanh")
    public ResponseEntity<Map<String, Object>> traCuuNhanh(@RequestHeader HttpHeaders httpHeaders, @RequestBody Map<String, Object> body);

    @PostMapping("res/traCuuHoTheoMaDinhDanh")
    public ResponseEntity<List<Map<String, Object>>> traCuuHoTheoMaDinhDanh(@RequestHeader HttpHeaders httpHeaders, @RequestBody TraCuuTheoMaDinhDanh dto);
}
