package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.dao.MartyrsProfileFamilyDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong.MartyrsProfileFamilyInfoDto;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * @author ToiTV
 * 2020-10-22
 */

@Repository
public class MartyrsProfileFamilyDaoImpl implements MartyrsProfileFamilyDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Get family information
     * @param hoSoID
     * @return
     */
    @Override
    public List<Map<String, Object>> getFamilyInfo(Integer hoSoID) {
        try {
            String sql = "{call ncc.sp_HoSoLietSi_FamilyInfo(?)}";
            List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, hoSoID);
            return result;
        } catch (Exception ex) {
            return new ArrayList<>();
        }
    }

    /**
     * Update family information
     * @param request
     * @return
     */
    @Override
    public boolean updateFamilyInfo(MartyrsProfileFamilyInfoDto request) {
        try {
            if (request.getHoSoLietSyID() == 0) {
                return false;
            }

            String sql = "{call ncc.sp_HoSoLietSy_Family_Update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
            jdbcTemplate.update(sql,
                    request.getHoSoLietSyID(), request.getHoTroTienSuDungDatNam(), request.getTongSoTienHoTroSuDungDat(),
                    request.getDuocHoaGiaNhaNam(), request.getTongSoTienCapSuaChuaNha(), request.getChuaDuocHoTro(),
                    request.getTinhTrangSucKhoe(), request.getConLietSyDangDiHoc(), request.getConLietSyDenTuoiKhongDiHoc(),
                    request.getConLietSyBiTanTat(), request.getHoTenNguoiThoCung(), request.getQuanHeVoiLietSy(),
                    request.getNamSinhNguoiThoCung(), request.getChoONguoiThoCung(), request.getNguonGocHoSoID(),
                    request.getNamChuyenDen(), request.getHoSoChuyenDiTinh(), request.getNamChuyenDi(),
                    request.getGhiChu());
            return true;
        } catch (Exception ex) {
            return false;
        }
    }


}
