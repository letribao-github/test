package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.dao.SubsidyInfoDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.subsidy.SubsidyDtoRequest;
import com.vnptit5.ptgp.ldtbxh.dto.response.CustomException;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/*
 * Author: ToiTV
 * Date: 21/10/2020
 * */
@Repository
public class SubsidyInfoDaoImpl implements SubsidyInfoDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Map<String, Object>> getCategory() {
        String sql = "{call ncc.sp_DanhMuc_MoQuanHeGiaDinh()}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql);
        return result;
    }

    @Override
    public boolean insertOrUpdate(SubsidyDtoRequest request) {
        try {
            String sql = "{call ncc.sp_SubsidyInfo_Insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
            jdbcTemplate.update(sql, request.getThongTinCheDoID(),
                    request.getHoSoID(), request.getThoiGianHuongTroCap(), request.getQuyetDinhHuongTroCap(),
                    request.getNgayRaQuyetDinhHuongTroCap(), request.getNoiRaQuyetDinhHuongTroCap(),
                    request.getSoTienTroCap(), request.getTruyLinhTuNgay(), request.getTruyLinhDenNgay(),
                    request.getSoTienTruyLinh(), request.getNguoiDuocHuongTroCap(), request.getNamSinhNguoiHuong(),
                    request.getQuanHeVoiNguoiCoCong(), request.getNguyenQuanNguoiHuong(), request.getTruQuanNguoiHuong(),
                    request.getMaiTangPhi());
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    @Override
    public List<Map<String, Object>> getInfo(int hoSoID) {
        String sql = "{call ncc.sp_SubsidyInfo_lookUpByID(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, hoSoID);
        return result;
    }

    @Override
    public boolean delete(int thongTinCheDoID) {
        try {
            String sql = "{call ncc.sp_SubsidyInfo_Delete(?)}";
            jdbcTemplate.update(sql, thongTinCheDoID);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}
