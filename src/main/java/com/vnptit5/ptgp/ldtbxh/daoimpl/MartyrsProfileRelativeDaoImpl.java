package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.dao.MartyrsProfileRelativeDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong.MartyrsProfileRelativeDto;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class MartyrsProfileRelativeDaoImpl implements MartyrsProfileRelativeDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Get relative information
     * @param hoSoID
     * @return
     */
    @Override
    public List<Map<String, Object>> getRelativeInfo(int hoSoID) {
        try {
            String sql = "{call ncc.sp_HoSoLietSy_Relative_Info(?)}";
            List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, hoSoID);
            return result;
        } catch (Exception ex) {
            return new ArrayList<>();
        }
    }

    /**
     * Update relative information
     * @param request
     * @return
     */
    @Override
    public boolean updateRelativeInfo(MartyrsProfileRelativeDto request) {
        try {
            String sql = "{call ncc.sp_HoSoLietSy_Relative_Update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
            jdbcTemplate.update(sql,
                    request.getThanNhanLietSyID(), request.getHoVaTen(),
                    request.getNamSinh(), request.getMoiQuanHeID(),
                    request.getChoOHienNay(), request.getNgheNghiep(),
                    request.getChucVu(), request.getTroCapMotLan(),
                    request.getTroCapCoBan(), request.getTroCapNuoiDuong(),
                    request.getChuaHuong(), request.getTruyLinhTu(),
                    request.getTongSoTienTruyLinh(), request.getGhiChu(),
                    request.getConSong(), request.getHoSoID());
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * Delete relative information
     * @param thanNhanLietSyID
     * @return
     */
    @Override
    public boolean deleteRelativeInfo(int thanNhanLietSyID) {
        try {
            String sql = "{call ncc.sp_HoSoLietSy_Relative_Delete(?)}";
            jdbcTemplate.update(sql, thanNhanLietSyID);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}
