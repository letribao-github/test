package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.dao.DanTocDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/*
 * Author: hungnguyenthanh
 * Date: 15/10/2020
 * */

@Repository
public class DanTocDaoImpl implements DanTocDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Map<String, Object>> getListDanToc() {
        String listDanToc = "{call ncc.sp_DanhMuc_DanToc()}";
        return jdbcTemplate.queryForList(listDanToc);
    }
}
