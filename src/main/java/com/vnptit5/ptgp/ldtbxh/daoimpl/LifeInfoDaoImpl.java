package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.dao.LifeInfoDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.lifeinfo.LifeInfoPostDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by Benjamin Lam on 10/19/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */
@Repository
public class LifeInfoDaoImpl implements LifeInfoDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Map<String, Object> getHeroLifeInfo(Integer heroID) {
        String sql = "{call ncc.sp_HeroLifeInfo_lookUpByID(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, heroID);
        if (result.size() == 0) {
            throw new CustomException("Thông tin đời sống liệt sĩ không tồn tại");
        }
        return result.get(0);
    }

    @Override
    public List<Map<String, Object>> getListIncomeType() {
        String sql = "{call ncc.sp_FamilyIncomeType_getAll()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> getListHouseStatus() {
        String sql = "{call ncc.sp_HousingSituation_getAll()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public int modifyHeroLifeInfo(LifeInfoPostDto lifeInfoPostDto) {
        String sql = "{call ncc.sp_HeroLifeInfo_addNew(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        int affectRow = jdbcTemplate.update(sql, lifeInfoPostDto.getHeroID(), lifeInfoPostDto.getHeroName(),
                lifeInfoPostDto.getHeroBurial(), lifeInfoPostDto.getSubsidizeNumber(), lifeInfoPostDto.getSubsidizeDate(),
                lifeInfoPostDto.getDecisionLevel(), lifeInfoPostDto.getFamilyIncome(), lifeInfoPostDto.getFamilyMember(),
                lifeInfoPostDto.getAverageIncome(), lifeInfoPostDto.getIncomeType(), lifeInfoPostDto.getLoanProgram(),
                lifeInfoPostDto.getLoanYear(), lifeInfoPostDto.getLoanMoney(), lifeInfoPostDto.getSavingYear(),
                lifeInfoPostDto.getSavingMoney(), lifeInfoPostDto.getEmployedNumber(), lifeInfoPostDto.getUnemployedNumber(),
                lifeInfoPostDto.getSponsor(), lifeInfoPostDto.getHomeYear(), lifeInfoPostDto.getHomeYear());

        if (affectRow == 0) {
            throw new CustomException("Cập nhật thất bại");
        }

        return affectRow;
    }
}
