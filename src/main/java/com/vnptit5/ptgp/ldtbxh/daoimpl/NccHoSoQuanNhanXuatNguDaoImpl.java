package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.dao.NccHoSoQuanNhanXuatNguDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong.HoSoQuanNhanXuatNguDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/*
 * Author: huylt.tgg
 * Date: 19/10/2020
 * */

@Repository
public class NccHoSoQuanNhanXuatNguDaoImpl implements NccHoSoQuanNhanXuatNguDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Override
    public Map<String, Object> getThongTinHoSoByID(Integer hosoID) {
        String sql = "{call ncc.sp_HoSoQuanNhanXuatNgu_selectThongTinHoSo(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, hosoID);
        if (result.size() == 0) {
            throw new CustomException("Thông tin hồ sơ không tồn tại");
        }
        return result.get(0);
    }

    @Override
    public List<Map<String, Object>> getThongtinTroCapPhuCapByID(Integer hosoID) {
        String sql = "{call ncc.sp_HoSoQuanNhanXuatNgu_selectTTTroCapPhuCap(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, hosoID);
        if (result.size() == 0) {
            throw new CustomException("Thông tin trợ cấp phụ cấp không tồn tại");
        }
        return result;
    }

    @Override
    public List<Map<String, Object>> getQuaTrinhCongTacByID(Integer hosoID) {
        String sql = "{call ncc.sp_HoSoQuanNhanXuatNgu_selectQuaTrinhCongTac(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, hosoID);
        if (result.size() == 0) {
            throw new CustomException("Thông tin quá trình công tác không tồn tại");
        }
        return result;
    }

    @Override
    public List<Map<String, Object>> getTroCapKhiQuaDoiByID(Integer hosoID) {
        String sql = "{call ncc.sp_HoSoQuanNhanXuatNgu_selectTroCapKhiQuaDoi(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, hosoID);
        if (result.size() == 0) {
            throw new CustomException("Thông tin trợ cấp khi qua đời không tồn tại");
        }
        return result;
    }

    @Override
    public int addThongTinHoSo(HoSoQuanNhanXuatNguDto hoSoQuanNhanXuatNguDto) {
        String sql = "{call ncc.sp_HoSoQuanNhanXuatNgu_insertThongTinHoSo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        List<SqlParameter> params = Arrays.asList(new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.VARCHAR), new SqlParameter(Types.INTEGER),
                                                  new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                                                  new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.VARCHAR), new SqlParameter(Types.INTEGER),
                                                  new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR),
                                                  new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                                                  new SqlOutParameter("P_HOSOQUANNHANID", Types.INTEGER), new SqlOutParameter("P_HOSOID", Types.INTEGER));

        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setInt(1, hoSoQuanNhanXuatNguDto.getBansao());
            callableStatement.setString(2, hoSoQuanNhanXuatNguDto.getSohosotinhquanly());
            callableStatement.setString(3, hoSoQuanNhanXuatNguDto.getSohosoboquanly());
            callableStatement.setInt(4, hoSoQuanNhanXuatNguDto.getLoaihosoid());
            callableStatement.setInt(5, hoSoQuanNhanXuatNguDto.getHuyentiepnhanid());
            callableStatement.setInt(6, hoSoQuanNhanXuatNguDto.getXatiepnhanid());
            callableStatement.setString(7, hoSoQuanNhanXuatNguDto.getCumtotiepnhan());
            callableStatement.setString(8, hoSoQuanNhanXuatNguDto.getHovaten());
            callableStatement.setString(9, hoSoQuanNhanXuatNguDto.getBidanh());
            callableStatement.setString(10, hoSoQuanNhanXuatNguDto.getNgaysinh());
            callableStatement.setString(11, hoSoQuanNhanXuatNguDto.getNamsinh());
            callableStatement.setInt(12, hoSoQuanNhanXuatNguDto.getGioitinh());
            callableStatement.setInt(13, hoSoQuanNhanXuatNguDto.getDantocid());
            callableStatement.setString(14, hoSoQuanNhanXuatNguDto.getQuequan());
            callableStatement.setString(15, hoSoQuanNhanXuatNguDto.getChoohiennay());
            callableStatement.setString(16, hoSoQuanNhanXuatNguDto.getCacchedokhac());
            callableStatement.setInt(17, hoSoQuanNhanXuatNguDto.getHosochuyendentuid());
            callableStatement.setString(18, hoSoQuanNhanXuatNguDto.getThoigianchuyenden());
            callableStatement.setInt(19, hoSoQuanNhanXuatNguDto.getHosochuyendiid());
            callableStatement.setString(20, hoSoQuanNhanXuatNguDto.getThoigianchuyendi());
            callableStatement.setString(21, hoSoQuanNhanXuatNguDto.getTaikhoancapnhat());
            callableStatement.setString(22, hoSoQuanNhanXuatNguDto.getVitriluutruhoso());
            callableStatement.setString(23, hoSoQuanNhanXuatNguDto.getSobhyt());
            callableStatement.setString(24, hoSoQuanNhanXuatNguDto.getNgayvaodang());
            callableStatement.setString(25, hoSoQuanNhanXuatNguDto.getChinhthuc());
            callableStatement.setString(26, hoSoQuanNhanXuatNguDto.getNgaynhapngu());
            callableStatement.setString(27, hoSoQuanNhanXuatNguDto.getDonvinhapngu());
            callableStatement.setString(28, hoSoQuanNhanXuatNguDto.getNoinhapngu());
            callableStatement.setString(29, hoSoQuanNhanXuatNguDto.getNgayxuatngu());
            callableStatement.setString(30, hoSoQuanNhanXuatNguDto.getDonvixuatngu());
            callableStatement.setString(31, hoSoQuanNhanXuatNguDto.getNgaytaingu());
            callableStatement.setString(32, hoSoQuanNhanXuatNguDto.getDonvitaingu());
            callableStatement.setString(33, hoSoQuanNhanXuatNguDto.getNgayphucvien());
            callableStatement.setString(34, hoSoQuanNhanXuatNguDto.getDonviphucvien());
            callableStatement.setString(35, hoSoQuanNhanXuatNguDto.getCapbacphucvienxuatngu());
            callableStatement.setString(36, hoSoQuanNhanXuatNguDto.getChucvuphucvienxuatngu());
            callableStatement.setString(37, hoSoQuanNhanXuatNguDto.getDonviphucvienxuatngu());
            callableStatement.setString(38, hoSoQuanNhanXuatNguDto.getTongsothoigianphucvu());
            callableStatement.setString(39, hoSoQuanNhanXuatNguDto.getSonamquydoi());
            callableStatement.setString(40, hoSoQuanNhanXuatNguDto.getNghenghiepphucvienxuatngu());
            callableStatement.setString(41, hoSoQuanNhanXuatNguDto.getThoigianochientruongk());
            callableStatement.setString(42, hoSoQuanNhanXuatNguDto.getNgaytutran());
            callableStatement.setString(43, hoSoQuanNhanXuatNguDto.getSoquyetdinhmaitangphi());
            callableStatement.setString(44, hoSoQuanNhanXuatNguDto.getSotienmaitangphi());
            callableStatement.setInt(45, hoSoQuanNhanXuatNguDto.getChedoquannhanid());
            callableStatement.setString(46, hoSoQuanNhanXuatNguDto.getGiaytoconluugiu());
            callableStatement.setInt(47, hoSoQuanNhanXuatNguDto.getThoikyid());
            callableStatement.setInt(48, hoSoQuanNhanXuatNguDto.getTinhtranghiennayid());
            callableStatement.setInt(49, hoSoQuanNhanXuatNguDto.getDoituonghuongchedobhyt());
            return callableStatement;
        }, params);

        return Integer.parseInt(result.get("P_HOSOID").toString());
    }

    @Override
    public int addThongTinTroCapPhuCap(HoSoQuanNhanXuatNguDto dto) {
        String sql = "{call ncc.sp_HoSoQuanNhanXuatNgu_insertTTTroCapPhuCap(?,?,?,?,?,?,?,?,?,?)}";
        List<SqlParameter> params = Arrays.asList(new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlOutParameter("P_THONGTINCHEDOID", Types.INTEGER));

        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setInt(1, dto.getHosoid());
            callableStatement.setString(2, dto.getThoigianhuongtrocap());
            callableStatement.setString(3, dto.getQuyetdinhhuongtrocap());
            callableStatement.setString(4, dto.getNgayraquyetdinhhuongtrocap());
            callableStatement.setString(5, dto.getNoiraquyetdinhhuongtrocap());
            callableStatement.setString(6, String.valueOf(dto.getSotientrocap()));
            callableStatement.setString(7, dto.getTruylinhtungay());
            callableStatement.setString(8, dto.getTruylinhdenngay());
            callableStatement.setString(9, String.valueOf(dto.getSotientruylinh()));
            return callableStatement;
        }, params);

        return Integer.parseInt(result.get("P_THONGTINCHEDOID").toString());
    }

    @Override
    public int addQuaTrinhCongTac(HoSoQuanNhanXuatNguDto dto) {
        String sql = "{call ncc.sp_HoSoQuanNhanXuatNgu_insertQuaTrinhCongTac(?,?,?,?,?,?,?,?)}";
        List<SqlParameter> params = Arrays.asList(new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlOutParameter("P_QUATRINHCONGTACID", Types.INTEGER));

        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setInt(1, dto.getHosoid());
            callableStatement.setString(2, dto.getTungay());
            callableStatement.setString(3, dto.getDenngay());
            callableStatement.setString(4, dto.getCapbac());
            callableStatement.setString(5, dto.getChucvu());
            callableStatement.setString(6, dto.getDonvicongtac());
            callableStatement.setString(7, dto.getDiabancongtac());

            return callableStatement;
        }, params);

        return Integer.parseInt(result.get("P_QUATRINHCONGTACID").toString());

    }

    @Override
    public int addTroCapKhiQuaDoi(HoSoQuanNhanXuatNguDto dto) {
        String sql = "{call ncc.sp_HoSoQuanNhanXuatNgu_insertTroCapKhiQuaDoi(?,?,?,?,?,?,?,?,?,?,?,?)}";
        List<SqlParameter> params = Arrays.asList(new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlOutParameter("P_ID", Types.INTEGER));

        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setInt(1, dto.getHosoid());
            callableStatement.setString(2, dto.getHotennguoihuong());
            callableStatement.setString(3, dto.getNamsinhnguoihuong());
            callableStatement.setInt(4, dto.getQuanhevoinguoicocong());
            callableStatement.setString(5, dto.getNguyenquannguoihuong());
            callableStatement.setString(6, dto.getTruquannguoihuong());
            callableStatement.setString(7, String.valueOf(dto.getMaitangphi()));
            callableStatement.setString(8, String.valueOf(dto.getTrocapcacthang()));
            callableStatement.setString(9, dto.getQdso());
            callableStatement.setString(10, dto.getNgayraqd());
            callableStatement.setString(11, dto.getNoiraqd());


            return callableStatement;
        }, params);

        return Integer.parseInt(result.get("P_ID").toString());
    }

    @Override
    public void updateThongTinHoSo(HoSoQuanNhanXuatNguDto dto) {
        String sql = "{call ncc.sp_HoSoQuanNhanXuatNgu_updateThongTinHoSo(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        jdbcTemplate.update(sql,
                    dto.getHosoid(),
                    dto.getBansao(),
                    dto.getSohosotinhquanly(),
                    dto.getSohosoboquanly(),
                    dto.getLoaihosoid(),
                    dto.getHuyentiepnhanid(),
                    dto.getXatiepnhanid(),
                    dto.getCumtotiepnhan(),
                    dto.getHovaten(),
                    dto.getBidanh(),
                    dto.getNgaysinh(),
                    dto.getNamsinh(),
                    dto.getGioitinh(),
                    dto.getDantocid(),
                    dto.getQuequan(),
                    dto.getChoohiennay(),
                    dto.getCacchedokhac(),
                    dto.getHosochuyendentuid(),
                    dto.getThoigianchuyenden(),
                    dto.getHosochuyendiid(),
                    dto.getThoigianchuyendi(),
                    dto.getTaikhoancapnhat(),
                    dto.getVitriluutruhoso(),
                    dto.getSobhyt(),
                    dto.getNgayvaodang(),
                    dto.getChinhthuc(),
                    dto.getNgaynhapngu(),
                    dto.getDonvinhapngu(),
                    dto.getNoinhapngu(),
                    dto.getNgayxuatngu(),
                    dto.getDonvixuatngu(),
                    dto.getNgaytaingu(),
                    dto.getDonvitaingu(),
                    dto.getNgayphucvien(),
                    dto.getDonviphucvien(),
                    dto.getCapbacphucvienxuatngu(),
                    dto.getChucvuphucvienxuatngu(),
                    dto.getDonviphucvienxuatngu(),
                    dto.getTongsothoigianphucvu(),
                    dto.getSonamquydoi(),
                    dto.getNghenghiepphucvienxuatngu(),
                    dto.getThoigianochientruongk(),
                    dto.getNgaytutran(),
                    dto.getSoquyetdinhmaitangphi(),
                    dto.getSotienmaitangphi(),
                    dto.getChedoquannhanid(),
                    dto.getGiaytoconluugiu(),
                    dto.getThoikyid(),
                    dto.getTinhtranghiennayid(),
                    dto.getDoituonghuongchedobhyt());
    }

    @Override
    public void updateThongTinTroCapPhuCap(HoSoQuanNhanXuatNguDto dto) {
        String sql = "{call ncc.sp_HoSoQuanNhanXuatNgu_updateTTTroCapPhuCap(?,?,?,?,?,?,?,?,?)}";
        jdbcTemplate.update(sql,
                dto.getThongtinchedoid(),
                dto.getThoigianhuongtrocap(),
                dto.getQuyetdinhhuongtrocap(),
                dto.getNgayraquyetdinhhuongtrocap(),
                dto.getNoiraquyetdinhhuongtrocap(),
                dto.getSotientrocap(),
                dto.getTruylinhtungay(),
                dto.getTruylinhdenngay(),
                dto.getSotientruylinh());

    }

    @Override
    public void updateQuaTrinhCongTac(HoSoQuanNhanXuatNguDto dto) {
        String sql = "{call ncc.sp_HoSoQuanNhanXuatNgu_updateQuaTrinhCongTac(?,?,?,?,?,?,?)}";
        jdbcTemplate.update(sql,
                dto.getQuatrinhcongtacid(),
                dto.getTungay(),
                dto.getDenngay(),
                dto.getCapbac(),
                dto.getChucvu(),
                dto.getDonvicongtac(),
                dto.getDiabancongtac());

    }

    @Override
    public void updateTroCapKhiQuaDoi(HoSoQuanNhanXuatNguDto dto) {
        String sql = "{call ncc.sp_HoSoQuanNhanXuatNgu_updateTroCapKhiQuaDoi(?,?,?,?,?,?,?,?,?,?,?)}";
        jdbcTemplate.update(sql,
                dto.getId(),
                dto.getHotennguoihuong(),
                dto.getNamsinhnguoihuong(),
                dto.getQuanhevoinguoicocong(),
                dto.getNguyenquannguoihuong(),
                dto.getTruquannguoihuong(),
                dto.getMaitangphi(),
                dto.getTrocapcacthang(),
                dto.getQdso(),
                dto.getNgayraqd(),
                dto.getNoiraqd());
    }

    @Override
    public void deleteThongTinHoSo(Integer hosoID) {
        String sql = "{call ncc.sp_HoSoQuanNhanXuatNgu_deleteThongTinHoSo(?)}";
        jdbcTemplate.update(sql, hosoID);
    }

    @Override
    public void deleteThongtinTroCapPhuCap(Integer thongtinchedoID) {
        String sql = "{call ncc.sp_HoSoQuanNhanXuatNgu_deleteTTTroCapPhuCap(?)}";
        jdbcTemplate.update(sql, thongtinchedoID);
    }

    @Override
    public void deleteQuaTrinhCongTac(Integer quatrinhcongtacID) {
        String sql = "{call ncc.sp_HoSoQuanNhanXuatNgu_deleteQuaTrinhCongTac(?)}";
        jdbcTemplate.update(sql, quatrinhcongtacID);
    }

    @Override
    public void deleteTroCapKhiQuaDoi(Integer ID) {
        String sql = "{call ncc.sp_HoSoQuanNhanXuatNgu_deleteTroCapKhiQuaDoi(?)}";
        jdbcTemplate.update(sql, ID);
    }
}
