package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.dao.HuyenDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/*
 * Author: hungnguyenthanh
 * Date: 15/10/2020
 * */

@Repository
public class HuyenDaoImpl implements HuyenDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Map<String, Object>> getListHuyen(Integer tinhID) {
        String listHuyen = "{call ncc.sp_DanhMuc_Huyen(?)}";
        return jdbcTemplate.queryForList(listHuyen,tinhID);
    }

    @Override
    public List<Map<String, Object>> getListTinh() {
        String listTinh = "{call ncc.sp_DanhMuc_Tinh()}";
        return jdbcTemplate.queryForList(listTinh);
    }
}
