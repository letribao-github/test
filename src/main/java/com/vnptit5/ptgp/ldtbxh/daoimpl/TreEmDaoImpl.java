package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.vnptit5.ptgp.ldtbxh.dao.DanhMucDao;
import com.vnptit5.ptgp.ldtbxh.dao.DiaPhuongDao;
import com.vnptit5.ptgp.ldtbxh.dao.TreEmDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh.CapNhatHoGiaDinhDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh.ThemMoiHoGiaDinhDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.treem.*;
import com.vnptit5.ptgp.ldtbxh.dto.response.CustomException;
import com.vnptit5.ptgp.ldtbxh.dto.response.PageDto;
import com.vnptit5.ptgp.ldtbxh.utils.TreeUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author trinhctn
 */
@Repository
public class TreEmDaoImpl  implements TreEmDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DiaPhuongDao diaPhuongDao;

    @Autowired
    private DanhMucDao danhMucDao;


    @Override
    @Transactional
    public int themMoiTreEm(CapNhatTreEmDto dto) {
        ThemMoiHoGiaDinhDto giaDinhDto = ThemMoiHoGiaDinhDto.builder().maTinh(dto.getMaTinhThanh())
                .maHuyen(dto.getMaQuanHuyen()).maXa(dto.getMaXaPhuong()).maThon(dto.getMaThon()).diaChi(dto.getDiaChi())
                .maHoanCanhGiaDinh(dto.getMaHoanCanhGiaDinh()).build();
        // thêm mới hộ gia đình
        int maHoGiaDinh = themMoiHoGiaDinh(giaDinhDto);
        String sql = "{call spTreEm_ThemMoi(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        List<SqlParameter> params = Arrays.asList(new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlOutParameter("P_OUT", Types.INTEGER));
        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setObject(1, dto.getTenTreEm());
            callableStatement.setObject(2, dto.getNgaySinh());
            callableStatement.setObject(3, dto.getDanToc(), Types.VARCHAR);
            callableStatement.setObject(4, dto.getGioiTinh(), Types.VARCHAR);
            callableStatement.setObject(5, dto.getTrinhDo(), Types.VARCHAR);
            callableStatement.setObject(6, dto.getTinhTrangHoc(), Types.VARCHAR);
            callableStatement.setObject(7, dto.getMaSoBhxh(), Types.VARCHAR);
            callableStatement.setObject(8, dto.getMaSoBhxhCu(), Types.VARCHAR);
            callableStatement.setObject(9, dto.getMaDonViBhxh(), Types.VARCHAR);
            callableStatement.setObject(10, dto.getTenDonViBhxh(), Types.VARCHAR);
            callableStatement.setObject(11, dto.getDiaChi(), Types.VARCHAR);
            callableStatement.setObject(12, dto.getNoiKhaiSinh(), Types.VARCHAR);
            callableStatement.setObject(13, dto.getSoCmnd(), Types.VARCHAR);
            callableStatement.setObject(14, dto.getMaDinhDanh(), Types.VARCHAR);
            callableStatement.setObject(15, dto.getMaTreEmChuoi(), Types.VARCHAR);
            callableStatement.setObject(16, dto.getMaTinhThanh(), Types.VARCHAR);
            callableStatement.setObject(17, dto.getMaQuanHuyen(), Types.VARCHAR);
            callableStatement.setObject(18, dto.getMaXaPhuong(), Types.VARCHAR);
            callableStatement.setObject(19, dto.getMaThon(), Types.VARCHAR);
            callableStatement.setObject(20, dto.getHoTenCha(), Types.VARCHAR);
            callableStatement.setObject(21, dto.getHoTenMe(), Types.VARCHAR);
            callableStatement.setObject(22, dto.getHoTenNguoiNuoiDuong(), Types.VARCHAR);
            callableStatement.setObject(23, dto.getSdtGiaDinh(), Types.VARCHAR);
            callableStatement.setObject(24, dto.getMaHoGiaDinh() == null ? maHoGiaDinh : dto.getMaHoGiaDinh(),
                    Types.INTEGER);
            callableStatement.setObject(25, dto.getCapNhat(), Types.BOOLEAN);
            callableStatement.setObject(26, dto.getMaHoGiaDinhBhxh(), Types.VARCHAR);
            callableStatement.setObject(27, dto.getTenNguoiGiamHo(), Types.VARCHAR);
            callableStatement.setObject(28, dto.getGhiChu(), Types.VARCHAR);
            return callableStatement;
        }, params);

        int maTreEm = Integer.parseInt(result.get("P_OUT").toString());
        if (maTreEm == 0) {
            throw new CustomException("Người dùng đã tồn tại");
        }
        dto.getDsMaHoanCanh().forEach(maHoanCanh -> {
            themMoiTreEmChiTietHoanCanh(maTreEm, maHoanCanh);
        });
        dto.getDsMaChiTietHuongTroGiup().forEach(maChiTietHuongTroGiup -> {
            themMoiTreEmChiTietHuongTroGiup(maTreEm, maChiTietHuongTroGiup);
        });
        return maTreEm;
    }

    @Override
    public void themMoiTreEmChiTietHoanCanh(int maTreEm, int maChiTietHoanCanh) {
        String sql = "{call spTreEm_ChiTietHoanCanh_Them(?, ?)}";
        jdbcTemplate.update(sql, maTreEm, maChiTietHoanCanh);
    }

    @Override
    public void xoaTreEmChiTietHoanCanh(int maTreEm) {
        String sql = "{call spTreEm_ChiTietHoanCanh_Xoa(?)}";
        jdbcTemplate.update(sql, maTreEm);
    }

    @Override
    public void themMoiTreEmChiTietHuongTroGiup(int maTreEm, int maChiTietHuongTroGiup) {
        String sql = "{call spTreEm_ChiTietHuongTroGiup_Them(?, ?)}";
        jdbcTemplate.update(sql, maTreEm, maChiTietHuongTroGiup);
    }

    @Override
    public void xoaTreEmChiTietHuongTroGiup(int maTreEm) {
        String sql = "{call spTreEm_ChiTietHuongTroGiup_Xoa(?)}";
        jdbcTemplate.update(sql, maTreEm);
    }

    @Override
    public void suaTreEm(CapNhatTreEmDto dto) {
        String sql = "{call spTreEm_CapNhat(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        jdbcTemplate.update(sql, dto.getMaTreEm(), dto.getTenTreEm(), dto.getNgaySinh(),
                dto.getDanToc(), dto.getGioiTinh(), dto.getTrinhDo(), dto.getTinhTrangHoc(),
                dto.getDiaChi(), dto.getMaThon(), dto.getHoTenCha(), dto.getHoTenMe(),
                dto.getHoTenNguoiNuoiDuong(), dto.getSdtGiaDinh(), dto.getCapNhat(),
                dto.getTenNguoiGiamHo(), dto.getMaHoGiaDinh(), dto.getSoCmnd(),
                dto.getNoiKhaiSinh(), dto.getGhiChu());
        CapNhatHoGiaDinhDto giaDinhDto = CapNhatHoGiaDinhDto.builder()
                .maTinh(dto.getMaTinhThanh())
                .maHuyen(dto.getMaQuanHuyen()).maXa(dto.getMaXaPhuong())
                .maThon(dto.getMaThon()).diaChi(dto.getDiaChi())
                .maHoanCanhGiaDinh(dto.getMaHoanCanhGiaDinh()).build();
        capNhatHoGiaDinh(giaDinhDto);
        xoaTreEmChiTietHoanCanh(dto.getMaTreEm());
        dto.getDsMaHoanCanh().forEach(maHoanCanh -> {
            themMoiTreEmChiTietHoanCanh(dto.getMaTreEm(), maHoanCanh);
        });

        xoaTreEmChiTietHuongTroGiup(dto.getMaTreEm());
        dto.getDsMaChiTietHuongTroGiup().forEach(maChiTietHuongTroGiup -> {
            themMoiTreEmChiTietHuongTroGiup(dto.getMaTreEm(), maChiTietHuongTroGiup);
        });

    }

    @Override
    public List<Map<String, Object>> danhSachTreEm(TimKiemTreEmDto dto) {
        String sql = "{call spTreEm_DanhSach(?,?,?,?,?,?,?,?,?,?)}";
        return jdbcTemplate.queryForList(sql,
                dto.getMaTinhThanh(), dto.getMaQuanHuyen(),
                dto.getMaXaPhuong(), dto.getMaThonXom(),
                dto.getTenTreEm(), dto.getTenChaMe(),
                dto.getMaHoGiaDinh(), dto.getTrangThai(),
                dto.getGioiTinh(), dto.getCapNhat());
    }

    /**
     * Kết quả trả về là 1 multiMap: - key: các mã hoàn cảnh cấp 1 - value: các mã
     * hoàn cảnh là con, cháu, ... của mã hoàn cảnh đó (bao gồm chính nó)
     */
    private Multimap<String, String> getMaChiTietHoanCanhCap1(int maLoaiHoanCanh) {
        List<List<Map<String, Object>>> listDanhMucHoanCanhTheoLoaiHC = new ArrayList<>();
        for (int cap = 1; cap <= 3; cap++) {
            listDanhMucHoanCanhTheoLoaiHC.add(danhMucHoanCanhTheoLoaiHC(maLoaiHoanCanh, cap));
        }

        Multimap<String, String> temp = ArrayListMultimap.create();

        for (int cap = listDanhMucHoanCanhTheoLoaiHC.size() - 1; cap >= 0; cap--) {
            List<Map<String, Object>> child = listDanhMucHoanCanhTheoLoaiHC.get(cap);
            child.forEach(e -> {
                String maChiTietHoanCanh = e.get("MA_CHI_TIET_HOAN_CANH").toString();
                temp.put(maChiTietHoanCanh, maChiTietHoanCanh);
                if (e.get("ID_CHA") != null) {
                    String idCha = e.get("ID_CHA").toString();
                    temp.putAll(idCha, temp.get(maChiTietHoanCanh));
                }
            });
        }

        Multimap<String, String> result = ArrayListMultimap.create();
        listDanhMucHoanCanhTheoLoaiHC.get(0).forEach(chiTietHc -> {
            String key = chiTietHc.get("MA_CHI_TIET_HOAN_CANH").toString();
            result.putAll(key, temp.get(key));
        });
        return result;
    }

    private Multimap<String, String> getMaChiTieuCap1(int maLoaiHoanCanh) {
        List<List<Map<String, Object>>> listDanhMucHoanCanhTheoLoaiHC = new ArrayList<>();
        for (int cap = 1; cap <= 3; cap++) {
            listDanhMucHoanCanhTheoLoaiHC.add(danhMucChiTieuTheoLoai(maLoaiHoanCanh, cap));
        }

        Multimap<String, String> temp = ArrayListMultimap.create();

        for (int cap = listDanhMucHoanCanhTheoLoaiHC.size() - 1; cap >= 0; cap--) {
            List<Map<String, Object>> child = listDanhMucHoanCanhTheoLoaiHC.get(cap);
            child.forEach(e -> {
                String maChiTietHoanCanh = e.get("MA_CHI_TIET_HOAN_CANH").toString();
                temp.put(maChiTietHoanCanh, maChiTietHoanCanh);
                if (e.get("ID_CHA") != null) {
                    String idCha = e.get("ID_CHA").toString();
                    temp.putAll(idCha, temp.get(maChiTietHoanCanh));
                }
            });
        }

        Multimap<String, String> result = ArrayListMultimap.create();
        listDanhMucHoanCanhTheoLoaiHC.get(0).forEach(chiTietHc -> {
            String key = chiTietHc.get("MA_CHI_TIET_HOAN_CANH").toString();
            result.putAll(key, temp.get(key));
        });
        return result;
    }

    private Multimap<String, String> getMaChiTietHuongTroGiupCap1() {
        List<List<Map<String, Object>>> listDanhMucHuongTroGiupTheoLoaiHC = new ArrayList<>();
        for (int cap = 1; cap <= 3; cap++) {
            listDanhMucHuongTroGiupTheoLoaiHC.add(danhMucHuongTroGiup(cap));
        }

        Multimap<String, String> temp = ArrayListMultimap.create();

        for (int cap = listDanhMucHuongTroGiupTheoLoaiHC.size() - 1; cap >= 0; cap--) {
            List<Map<String, Object>> child = listDanhMucHuongTroGiupTheoLoaiHC.get(cap);
            child.forEach(e -> {
                String maChiTietHuongTroGiup = e.get("MA_CHI_TIET_HUONG_TRO_GIUP").toString();
                temp.put(maChiTietHuongTroGiup, maChiTietHuongTroGiup);
                if (e.get("ID_CHA") != null) {
                    String idCha = e.get("ID_CHA").toString();
                    temp.putAll(idCha, temp.get(maChiTietHuongTroGiup));
                }
            });
        }

        Multimap<String, String> result = ArrayListMultimap.create();
        listDanhMucHuongTroGiupTheoLoaiHC.get(0).forEach(chiTietHtg -> {
            String key = chiTietHtg.get("MA_CHI_TIET_HUONG_TRO_GIUP").toString();
            result.putAll(key, temp.get(key));
        });
        return result;
    }

    @Override
    public PageDto quanLyTreEmHoanCanh(QuanLyTreEmHoanCanhDto dto) {
        String sql = "{call spTreEm_QuanLyHoanCanh_PhanTrang(?,?,?,?,?,?,?,?,?,?,?,?)}";
        String danhSachHC = danhMucDao.danhSachHoanCanhCon(dto.getMaChiTietHoanCanh(), dto.getMaLoaiHoanCanh());
        List<SqlParameter> params = Arrays.asList(
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.INTEGER),
                new SqlOutParameter("P_SODONG", Types.INTEGER),
                new SqlOutParameter("P_SOTRANG", Types.INTEGER));
        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setObject(1, dto.getMaTinhThanh(), Types.VARCHAR);
            callableStatement.setObject(2, dto.getMaQuanHuyen(), Types.VARCHAR);
            callableStatement.setObject(3, dto.getMaXaPhuong(), Types.VARCHAR);
            callableStatement.setObject(4, dto.getMaThonXom(), Types.VARCHAR);
            callableStatement.setObject(5, dto.getMaLoaiHoanCanh(), Types.INTEGER);
            callableStatement.setObject(6, danhSachHC, Types.VARCHAR);
            callableStatement.setObject(7, dto.getTenTreEm(), Types.VARCHAR);
            callableStatement.setObject(8, dto.getPageSize(), Types.INTEGER);
            callableStatement.setObject(9, dto.getPageNumber(), Types.INTEGER);
            callableStatement.setObject(10, dto.getMaHoGiaDinh(), Types.INTEGER);
            return callableStatement;
        }, params);
        int totalPage = Integer.parseInt(result.get("P_SOTRANG").toString());
        int totalRows = Integer.parseInt(result.get("P_SODONG").toString());
        ModelMapper modelMapper = new ModelMapper();
        List<Map<String, Object>> data = (List) result.get("#result-set-2");
        if (data.size() > 0) {
            /* lấy ra toàn bộ chi tiết hoàn cảnh cấp 1 */
            List<Map<String, Object>> dsChiTietHoanCanhCap1 = danhMucHoanCanhTheoLoaiHC(dto.getMaLoaiHoanCanh(), 1);
            /* Lấy các mã chi tiết hoàn cảnh con của các chi tiết hoàn cảnh cấp 1 */
            Multimap<String, String> maChiTietHoanCanhCap1 = getMaChiTietHoanCanhCap1(dto.getMaLoaiHoanCanh());
            data.forEach(treEm -> {
                /* Copy danh sách mã hoàn cảnh cấp 1 để put vào từng trẻ en */
                List<Map<String, Object>> dshc = dsChiTietHoanCanhCap1.stream().map(HashMap::new)
                        .collect(Collectors.toList());
                /* Lấy ra các mã chi tiết hoàn cảnh của trẻ em */
                List<String> dsMaChiTietHC;
                if (treEm.get("DS_MA_CHI_TIET_HOAN_CANH") != null
                        && !treEm.get("DS_MA_CHI_TIET_HOAN_CANH").toString().isEmpty()) {
                    String dsMaChiTietHCString = treEm.get("DS_MA_CHI_TIET_HOAN_CANH").toString();
                    dsMaChiTietHC = Arrays.asList(dsMaChiTietHCString.split(","));
                } else {
                    dsMaChiTietHC = new ArrayList<>();
                }

                /*
                 * Đối với từng chi tiết hoàn cảnh cấp 1 thì kiểm tra xem trẻ em có thuộc mã này
                 * hay không
                 */
                dshc.forEach(cthc -> {
                    /*
                     * maChiTietHoanCanhCap1.get(maCthc): lấy ra các mã con của chi tiết hoàn cảnh
                     */
                    String maCthc = cthc.get("MA_CHI_TIET_HOAN_CANH").toString();
                    boolean value = !Collections.disjoint(dsMaChiTietHC, maChiTietHoanCanhCap1.get(maCthc));
                    cthc.put("VALUE", value);
                });
                treEm.put("DSHC", dshc);
            });
        }
        return PageDto.builder()
                .totalPage(totalPage).totalRows(totalRows)
                .data(data)
                .pageNumber(dto.getPageNumber()).pageSize(dto.getPageSize())
                .build();
    }

    @Override
    public Integer getID_Cha_CTHC(Integer maChiTietHC) {
        String sql = "{call spHoanCanh_ChiTiet(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, maChiTietHC);
        if (result.size() == 0) {
            return 0;
        } else
            return (Integer) result.get(0).get("ID_CHA");
    }

    @Override
    public PageDto quanLyTreEmHuongTroGiup(QuanLyTreEmHuongTroGiupDto dto) {
        String sql = "{call spTreEm_QuanLyHuongTroGiup_PhanTrang(?,?,?,?,?,?,?,?,?,?)}";
        String danhSachHTG = danhMucDao.danhSachHuongTroGiupCon(dto.getMaChiTietHuongTroGiup());
        List<SqlParameter> params = Arrays.asList(
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                new SqlOutParameter("P_SODONG", Types.INTEGER),
                new SqlOutParameter("P_SOTRANG", Types.INTEGER));
        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setObject(1, dto.getMaTinhThanh(), Types.VARCHAR);
            callableStatement.setObject(2, dto.getMaQuanHuyen(), Types.VARCHAR);
            callableStatement.setObject(3, dto.getMaXaPhuong(), Types.VARCHAR);
            callableStatement.setObject(4, dto.getMaThonXom(), Types.VARCHAR);
            callableStatement.setObject(5, danhSachHTG, Types.VARCHAR);
            callableStatement.setObject(6, dto.getTenTreEm(), Types.VARCHAR);
            callableStatement.setObject(7, dto.getPageSize(), Types.INTEGER);
            callableStatement.setObject(8, dto.getPageNumber(), Types.INTEGER);
            return callableStatement;
        }, params);
        int totalPage = Integer.parseInt(result.get("P_SOTRANG").toString());
        int totalRows = Integer.parseInt(result.get("P_SODONG").toString());
        List<Map<String, Object>> data = (List) result.get("#result-set-2");
        if (data.size() > 0) {
            try {
                /* lấy ra toàn bộ chi tiết hưởng trợ giúp cấp 1 */
                List<Map<String, Object>> dsChiTietHuongTroGiupCap1 = danhMucHuongTroGiup(1);
                /*
                 * Lấy các mã chi tiết hưởng trọ giúp con của các chi tiết hưởng trợ giúp cấp 1
                 */
                Multimap<String, String> maChiTietHuongTroGiupCap1 = getMaChiTietHuongTroGiupCap1();
                data.forEach(treEm -> {
                    /* Copy danh sách mã hưởng trợ giúp cấp 1 để put vào từng trẻ en */
                    List<Map<String, Object>> dshtg = dsChiTietHuongTroGiupCap1.stream().map(HashMap::new)
                            .collect(Collectors.toList());
                    /* Lấy ra các mã chi tiết hưởng trợ giúp của trẻ em */
                    List<String> dsMaChiTietHTG;
                    if (treEm.get("DS_MA_CHI_TIET_HUONG_TRO_GIUP") != null
                            && !treEm.get("DS_MA_CHI_TIET_HUONG_TRO_GIUP").toString().isEmpty()) {
                        String dsMaChiTietHTGString = treEm.get("DS_MA_CHI_TIET_HUONG_TRO_GIUP").toString();
                        dsMaChiTietHTG = Arrays.asList(dsMaChiTietHTGString.split(","));
                    } else {
                        dsMaChiTietHTG = new ArrayList<>();
                    }

                    /*
                     * Đối với từng chi tiết hưởng trợ giúp cấp 1 thì kiểm tra xem trẻ em có thuộc
                     * mã này hay không
                     */
                    dshtg.forEach(cthtg -> {
                        /*
                         * maChiTietHuongTroGiupCap1.get(maCthtg): lấy ra các mã con của chi tiết hoàn
                         * cảnh
                         */
                        String maCthtg = cthtg.get("MA_CHI_TIET_HUONG_TRO_GIUP").toString();
                        boolean value = !Collections.disjoint(dsMaChiTietHTG, maChiTietHuongTroGiupCap1.get(maCthtg));
                        cthtg.put("VALUE", value);
                    });
                    treEm.put("DSHC", dshtg);
                });
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return PageDto.builder()
                .totalPage(totalPage).totalRows(totalRows)
                .data(data)
                .pageNumber(dto.getPageNumber()).pageSize(dto.getPageSize())
                .build();
    }

    @Override
    public Integer getID_Cha_CTHTG(Integer maChiTietHTG) {
        String sql = "{call spHuongTroGiup_ChiTiet(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, maChiTietHTG);
        if (result.size() == 0) {
            return 0;
        } else
            return (Integer) result.get(0).get("ID_CHA");
    }

    @Override
    public List<Map<String, Object>> danhSachTreEmHoanCanhNhieuTE(String dsMaTreEm, int maLoaiHoanCanh) {
        String sql = "{call spTreEm_ChiTietHoanCanh_DanhSach_Nhieu_TE(?,?)}";
        return jdbcTemplate.queryForList(sql, dsMaTreEm, maLoaiHoanCanh);
    }

    @Override
    public List<Map<String, Object>> danhSachTreEmHuongTroGiupNhieuTE(String dsMaTreEm) {
        String sql = "{call spTreEm_ChiTietHuongTroGiup_DanhSach_Nhieu_TE(?)}";
        return jdbcTemplate.queryForList(sql, dsMaTreEm);
    }

    @Override
    public List<Map<String, Object>> danhMucHoanCanhTheoLoaiHC(int maLoaiHoanCanh, int capHoanCanh) {
        String sql = "{call spHoanCanh_DanhSach_TheoLoai(?,?)}";
        return jdbcTemplate.queryForList(sql, maLoaiHoanCanh, capHoanCanh);
    }

    @Override
    public List<Map<String, Object>> danhMucChiTieuTheoLoai(int maLoaiChiTieu, int capChiTieu) {
        String sql = "{call tre_em.sp_ChiTieu_DanhSach_TheoLoai(?,?)}";
        return jdbcTemplate.queryForList(sql, maLoaiChiTieu, capChiTieu);
    }

    @Override
    public List<Map<String, Object>> danhMucHuongTroGiup(int capHuongTroGiup) {
        String sql = "{call spHuongTroGiup_DanhSach(?)}";
        return jdbcTemplate.queryForList(sql, capHuongTroGiup);
    }

    @Override
    public PageDto danhSachTreEmPhanTrang(TimKiemTreEmDto dto) {
        String sql = "{call spTreEm_DanhSachPhanTrang(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        List<SqlParameter> params = Arrays.asList(new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.INTEGER), new SqlParameter(Types.BOOLEAN),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.BOOLEAN), new SqlOutParameter("P_SODONG", Types.INTEGER),
                new SqlOutParameter("P_SOTRANG", Types.INTEGER));
        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setObject(1, dto.getMaTinhThanh(), Types.INTEGER);
            callableStatement.setObject(2, dto.getMaQuanHuyen(), Types.INTEGER);
            callableStatement.setObject(3, dto.getMaXaPhuong(), Types.INTEGER);
            callableStatement.setObject(4, dto.getMaThonXom(), Types.INTEGER);
            callableStatement.setString(5, dto.getTenTreEm());
            callableStatement.setString(6, dto.getTenChaMe());
            callableStatement.setObject(7, dto.getMaHoGiaDinh(), Types.INTEGER);
            callableStatement.setObject(8, dto.getTrangThai(), Types.BOOLEAN);
            callableStatement.setObject(9, dto.getGioiTinh(), Types.VARCHAR);
            callableStatement.setObject(10, dto.getPageSize(), Types.INTEGER);
            callableStatement.setObject(11, dto.getPageNumber(), Types.INTEGER);
            callableStatement.setObject(12, dto.getCapNhat(), Types.BOOLEAN);
            return callableStatement;
        }, params);
        int totalPage = Integer.parseInt(result.get("P_SOTRANG").toString());
        int totalRows = Integer.parseInt(result.get("P_SODONG").toString());
        ModelMapper modelMapper = new ModelMapper();
        List<Map<String, Object>> temp = (List) result.get("#result-set-1");
        PageDto pageDto = PageDto.builder().totalRows(totalRows).totalPage(totalPage).pageSize(dto.getPageSize())
                .pageNumber(dto.getPageNumber()).data(temp).build();
        return pageDto;
    }

    @Override
    public void xoaTreEm(int maTreEm) {
        String sql = "{call spTreEm_Xoa(?)}";
        jdbcTemplate.update(sql, maTreEm);
    }

    @Override
    public void khoiPhucTreEm(int maTreEm) {
        String sql = "{call spTreEm_KhoiPhuc(?)}";
        jdbcTemplate.update(sql, maTreEm);
    }

    @Override
    public Map<String, Object> chiTietTreEm(int maTreEm, Integer maLoaiHoanCanh) {
        String sql = "{call spTreEm_ChiTiet(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, maTreEm);
        if (result.size() == 0) {
            throw new CustomException("Trẻ em không tồn tại");
        }
        result.get(0).put("DS_HOAN_CANH", danhSachTreEmHoanCanh(maTreEm, maLoaiHoanCanh));
        result.get(0).put("DS_HUONG_TRO_GIUP", danhSachTreEmHuongTroGiup(maTreEm));
        return result.get(0);
    }

    @Override
    public List<Map<String, Object>> danhSachTreEmHoanCanh(int maTreEm, Integer maLoaiHoanCanh) {
        String sql = "{call spTreEm_ChiTietHoanCanh_DanhSach(?,?)}";
        return jdbcTemplate.queryForList(sql, maTreEm, maLoaiHoanCanh);
    }

    @Override
    public List<Map<String, Object>> danhSachTreEmHuongTroGiup(int maTreEm) {
        String sql = "{call spTreEm_ChiTietHuongTroGiup_DanhSach(?)}";
        return jdbcTemplate.queryForList(sql, maTreEm);
    }

    @Override
    public int themMoiHoGiaDinh(ThemMoiHoGiaDinhDto dto) {
        String sql = "{call spHoGiaDinh_ThemMoi(?,?,?,?,?,?,?,?,?,?)}";
        List<SqlParameter> params = Arrays.asList(new SqlParameter(Types.VARCHAR), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.INTEGER), new SqlOutParameter("P_OUT", Types.INTEGER));
        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setObject(1, dto.getMaTinh(), Types.VARCHAR);
            callableStatement.setObject(2, dto.getMaHuyen(), Types.INTEGER);
            callableStatement.setObject(3, dto.getMaXa(), Types.INTEGER);
            callableStatement.setObject(4, dto.getMaThon(), Types.VARCHAR);
            callableStatement.setObject(5, dto.getKhuVuc(), Types.INTEGER);
            callableStatement.setObject(6, dto.getMaHoBhxh(), Types.VARCHAR);
            callableStatement.setObject(7, dto.getDiaChi(), Types.VARCHAR);
            callableStatement.setObject(8, dto.getSoHoKhau(), Types.INTEGER);
            callableStatement.setObject(9, dto.getMaHoanCanhGiaDinh() == null ? 3 : dto.getMaHoanCanhGiaDinh(),
                    Types.INTEGER);
            return callableStatement;
        }, params);
        int maHoGiaDinh = Integer.parseInt(result.get("P_OUT").toString());
        if (maHoGiaDinh == 0) {
            throw new CustomException("Gia đình đã tồn tại");
        }
        return maHoGiaDinh;
    }

    @Override
    public void capNhatHoGiaDinh(CapNhatHoGiaDinhDto dto) {
        String sql = "{call spHoGiaDinh_CapNhat(?,?,?,?,?,?,?,?,?,?)}";
        jdbcTemplate.update(sql, dto.getMaHoGiaDinh(), dto.getMaTinh(), dto.getMaHuyen(), dto.getMaXa(),
                dto.getMaThon(), dto.getKhuVuc(), dto.getMaHoBhxh(), dto.getDiaChi(), dto.getSoHoKhau(),
                dto.getMaHoanCanhGiaDinh());
    }

    @Override
    public List<Map<String, Object>> baoCaoDanhSachTreEmBinhThuong(BaoCaoTreEmDto dto) {
        String sql = "{call tre_em.spKhaiThac_DanhSachTreEmBinhThuong(?,?,?,?,?,?,?,?)}";
        List<Map<String, Object>> DSTreEmBinhThuong = jdbcTemplate.queryForList(sql, dto.getMaTinh(), dto.getMaHuyen(),
                dto.getMaXa(), dto.getMaThon(), dto.getTuoiTu(),
                dto.getDenTuoi(), dto.getTuNgay(), dto.getDenNgay());
        return customeDanhSachTEBaoCao(DSTreEmBinhThuong,
                dto.getMaTinh(), dto.getMaHuyen(),
                dto.getMaXa(), dto.getMaThon());
    }

    @Override
    public List<Map<String, Object>> baoCaoDanhSachTreEm(BaoCaoTreEmDto dto) {
        String sql = "{call tre_em.spKhaiThac_DanhSachTreEm(?,?,?,?,?,?,?,?)}";
        List<Map<String, Object>> DSTreEm = jdbcTemplate.queryForList(
                sql, dto.getMaTinh(), dto.getMaHuyen(),
                dto.getMaXa(), dto.getMaThon(), dto.getTuoiTu(),
                dto.getDenTuoi(), dto.getTuNgay(), dto.getDenNgay());
        return customeDanhSachTEBaoCao(DSTreEm,
                dto.getMaTinh(), dto.getMaHuyen(),
                dto.getMaXa(), dto.getMaThon());
    }

    private List<Map<String, Object>> customeDanhSachTEBaoCao(List<Map<String, Object>> dsTE,
                                                              String maTinh, String maHuyen,
                                                              String maXa, String maThon){
        if (maXa == null){
            List<Map<String, Object>> danhSachXa = diaPhuongDao.danhSachXa(maTinh,maHuyen);
            return TreeUtils.buildList(dsTE, danhSachXa, "MA_XA_PHUONG_BHXH",
                    "MA_XA_BHXH", "TEN_XA_PHUONG");
        }
        if (maThon == null){
            List<Map<String, Object>> danhSachThon = diaPhuongDao.danhSachThon(maTinh, maHuyen, maXa);
            return TreeUtils.buildList(dsTE, danhSachThon, "MA_THON_XOM_BHXH",
                    "MA_THON_BHXH", "TEN_THON_XOM");
        }
        String sqlThon = "{call spDiaPhuong_Thon_ThongTin(?)}";
        List<Map<String, Object>> thon = jdbcTemplate.queryForList(sqlThon, maThon);
        return TreeUtils.buildList(dsTE, thon,
                "MA_THON_XOM", "MA_THON_XOM", "TEN_THON_XOM");
    };

    @Override
    public List<Map<String, Object>> baoCaoDanhSachTreEmTheoChiTieu(TreEmTheoChiTieuDto dto) {
        String sql = "{call tre_em.sp_KhaiThac_DanhSachTreEmTheoChiTieu(?,?,?,?,?,?,?,?,?,?)}";
        String danhSachHC = danhMucDao.danhSachHoanCanhCon(dto.getMaChiTietHC(), dto.getMaLoaiHC());
        List<Map<String, Object>> danhSachTETheoChiTieu = jdbcTemplate.queryForList(sql,
                dto.getMaTinh(), dto.getMaHuyen(), dto.getMaXa(), dto.getMaThon(),
                dto.getMaLoaiHC(), danhSachHC,
                dto.getTuoiTu(), dto.getDenTuoi(),
                dto.getTuNgay(), dto.getDenNgay());
        if (danhSachTETheoChiTieu.size() > 0) {
            /* lấy ra toàn bộ chi tiết hoàn cảnh cấp 1 */
            List<Map<String, Object>> dsChiTietHoanCanhCap1 = danhMucChiTieuTheoLoai(dto.getMaLoaiHC(), 1);
            /* Lấy các mã chi tiết hoàn cảnh con của các chi tiết hoàn cảnh cấp 1 */
            Multimap<String, String> maChiTietHoanCanhCap1 = getMaChiTieuCap1(dto.getMaLoaiHC());

            danhSachTETheoChiTieu.forEach(treEm -> {
                /* Copy danh sách mã hoàn cảnh cấp 1 để put vào từng trẻ en */
                List<Map<String, Object>> dshc =
                        dto.getMaChiTietHC() == null ?
                        dsChiTietHoanCanhCap1.stream().map(HashMap::new)
                        .collect(Collectors.toList())
                        : chiTieuCha(dto.getMaChiTietHC());
                /* Lấy ra các mã chi tiết hoàn cảnh của trẻ em */
                List<String> dsMaChiTietHC;
                if (treEm.get("DS_MA_CHI_TIET_HOAN_CANH") != null
                        && !treEm.get("DS_MA_CHI_TIET_HOAN_CANH").toString().isEmpty()) {
                    String dsMaChiTietHCString = treEm.get("DS_MA_CHI_TIET_HOAN_CANH").toString();
                    dsMaChiTietHC = Arrays.asList(dsMaChiTietHCString.split(","));
                } else {
                    dsMaChiTietHC = new ArrayList<>();
                }

                /*
                 * Đối với từng chi tiết hoàn cảnh cấp 1 thì kiểm tra xem trẻ em có thuộc mã này
                 * hay không
                 */
                dshc.forEach(cthc -> {
                    /*
                     * maChiTietHoanCanhCap1.get(maCthc): lấy ra các mã con của chi tiết hoàn cảnh
                     */
                    String maCthc = cthc.get("MA_CHI_TIET_HOAN_CANH").toString();
                    boolean value = !Collections.disjoint(dsMaChiTietHC, maChiTietHoanCanhCap1.get(maCthc));
                    cthc.put("VALUE", value);
                });
                treEm.put("DSHC", dshc);
            });
        }

        if (dto.getMaXa() == null){
            List<Map<String, Object>> danhSachXa = diaPhuongDao.danhSachXa(dto.getMaTinh(), dto.getMaHuyen());
            return TreeUtils.buildList(danhSachTETheoChiTieu, danhSachXa, "MA_XA_PHUONG_BHXH",
                    "MA_XA_BHXH", "TEN_XA_PHUONG");
        }
        if (dto.getMaThon() == null){
            List<Map<String, Object>> danhSachThon = diaPhuongDao.danhSachThon(dto.getMaTinh(), dto.getMaHuyen(), dto.getMaXa());
            return TreeUtils.buildList(danhSachTETheoChiTieu, danhSachThon, "MA_THON_XOM_BHXH",
                    "MA_THON_BHXH", "TEN_THON_XOM");
        }
        String sqlThon = "{call spDiaPhuong_Thon_ThongTin(?)}";
        List<Map<String, Object>> thon = jdbcTemplate.queryForList(sqlThon, dto.getMaThon());
        return TreeUtils.buildList(danhSachTETheoChiTieu, thon,
                "MA_THON_XOM", "MA_THON_XOM", "TEN_THON_XOM");
    }
    @Override
    public List<Map<String, Object>> baocaothongkeTETheoNam() {
        String sql = "{call tre_em.son_baocao_2()}";
        List<Map<String, Object>> data = jdbcTemplate.queryForList(sql);
        List<Map<String, Object>> Temp0 = new ArrayList<Map<String, Object>>();
        Temp0.add(data.get(1));
        Temp0.add(data.get(2));
        List<Map<String, Object>> Temp3 = new ArrayList<Map<String, Object>>();
        Temp3.add(data.get(4));
        Temp3.add(data.get(5));
        List<Map<String, Object>> Temp6 = new ArrayList<Map<String, Object>>();
        Temp6.add(data.get(7));
        Temp6.add(data.get(8));
        List<Map<String, Object>> Temp9 = new ArrayList<Map<String, Object>>();
        Temp9.add(data.get(10));
        Temp9.add(data.get(11));
        List<Map<String, Object>> Temp12 = new ArrayList<Map<String, Object>>();
        Temp12.add(data.get(13));
        Temp12.add(data.get(14));
        List<Map<String, Object>> Temp15 = new ArrayList<Map<String, Object>>();
        Temp15.add(data.get(16));
        Temp15.add(data.get(17));
        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        result.add(data.get(0));
        result.get(0).put("CON", Temp0);
        result.add(data.get(3));
        result.get(1).put("CON", Temp3);
        result.add(data.get(6));
        result.get(2).put("CON", Temp6);
        result.add(data.get(9));
        result.get(3).put("CON", Temp9);
        result.add(data.get(12));
        result.get(4).put("CON", Temp12);
        result.add(data.get(15));
        result.get(5).put("CON", Temp15);
        result.add(data.get(18));
        result.add(data.get(19));
        result.add(data.get(20));
        result.add(data.get(21));
        result.add(data.get(22));
        result.add(data.get(23));
        result.add(data.get(24));
        result.add(data.get(25));
        result.add(data.get(26));
        List<Map<String, Object>> Temp26 = new ArrayList<Map<String, Object>>();
        for (int i = 27; i < data.size(); i++) {
            Temp26.add(data.get(i));
        }
        result.get(14).put("CON", Temp26);
        return result;
    }

    @Override
    public Map<String, Object> chiTietChiTieu(Integer maLoaiCT) {
        String sqlTenLoaiCT = "{call tre_em.sp_LoaiChiTieu_ChiTiet(?)}";
        return jdbcTemplate.queryForMap(sqlTenLoaiCT, maLoaiCT);
    }

    @Override
    public List<Map<String, Object>> chiTieuCha(Integer maChiTieu) {
        String sqlTenLoaiCT = "{call tre_em.sp_ChiTieu_Cha(?)}";
        return jdbcTemplate.queryForList(sqlTenLoaiCT, maChiTieu);
    }
    @Override
    public List<Map<String, Object>> baocaothongkeTEHCDB(int year) {

        String sql = "{call tre_em.son_baocao(?)}";
        List<Map<String, Object>> data = jdbcTemplate.queryForList(sql,year);
        List<Map<String, Object>> infoTreEmParent = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> infoTreEmChild = new ArrayList<Map<String, Object>>();
        if(data.size()>10){
 if(data.size() >3)
        {
 infoTreEmChild.add(data.get(3));
        }
                infoTreEmChild.add(data.get(1));
                        infoTreEmChild.add(data.get(2));

        if(data.size() >4)
        {
 infoTreEmChild.add(data.get(4));
        }
        if(data.size() >5)
        {
 infoTreEmChild.add(data.get(5));
        }
      
        }
                 infoTreEmParent.add(data.get(0));
        infoTreEmParent.get(0).put("CON", infoTreEmChild);
        infoTreEmParent.get(0).replace("noi_dung", "I " + infoTreEmParent.get(0).get("noi_dung").toString());
        Integer countTreEmChild = 1;
        for (int i = 0; i < infoTreEmChild.size(); i++) {
            infoTreEmChild.get(i).replace("noi_dung",
                    countTreEmChild.toString() + " " + infoTreEmChild.get(i).get("noi_dung").toString());
            countTreEmChild++;
        }


        List<Map<String, Object>> result = new ArrayList<Map<String, Object>>();
        List<List<Map<String, Object>>> dsCapHoanCanh = new ArrayList<>();
        List<Map<String, Object>> capHC1 = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> capHC2 = new ArrayList<Map<String, Object>>();
        List<Map<String, Object>> capHC3 = new ArrayList<Map<String, Object>>();
        Map<String, Object> loaiHoancanh282 = new HashMap<String, Object>();
        List<Map<String, Object>> tempLoaiHoancanh282 = new ArrayList<Map<String, Object>>();
        loaiHoancanh282.put("noi_dung", "II Trẻ em có hoàn cảnh đặc biệt");
        loaiHoancanh282.put("id", "2");
        Map<String, Object> loaiHoancanh283 = new HashMap<String, Object>();
        List<Map<String, Object>> tempLoaiHoancanh283 = new ArrayList<Map<String, Object>>();
        loaiHoancanh283.put("noi_dung", "III Trẻ em có nguy cơ rơi vào hoàn cảnh đặc biệt");
        loaiHoancanh283.put("id", "3");
        Map<String, Object> loaiHoancanh284 = new HashMap<String, Object>();
        List<Map<String, Object>> tempLoaiHoancanh284 = new ArrayList<Map<String, Object>>();
        loaiHoancanh284.put("noi_dung", "IV Trẻ em có hoàn cảnh khác");
        loaiHoancanh284.put("id", "4");
        for (int i = 10; i < data.size(); i++) {
            data.get(i).replace("noi_dung", data.get(i).get("noi_dung").toString()
                    .substring(data.get(i).get("noi_dung").toString().indexOf(" ") + 1));
            if (Integer.parseInt(data.get(i).get("CAP_HOAN_CANH").toString()) == 1) {
                capHC1.add(data.get(i));
            }

            if (Integer.parseInt(data.get(i).get("CAP_HOAN_CANH").toString()) == 2) {
                capHC2.add(data.get(i));
            }
            if (Integer.parseInt(data.get(i).get("CAP_HOAN_CANH").toString()) == 3) {
                capHC3.add(data.get(i));
            }
        }
        dsCapHoanCanh.add(capHC1);
        dsCapHoanCanh.add(capHC2);
        dsCapHoanCanh.add(capHC3);
        buildTree(dsCapHoanCanh, "MA_CHI_TIET_HOAN_CANH", "ID_CHA");
        for (int i = 0; i < dsCapHoanCanh.get(0).size(); i++) {
            if (Integer.parseInt(dsCapHoanCanh.get(0).get(i).get("MA_LOAI_HOAN_CANH").toString()) == 282) {

                tempLoaiHoancanh282.add(dsCapHoanCanh.get(0).get(i));
            }
            if (Integer.parseInt(dsCapHoanCanh.get(0).get(i).get("MA_LOAI_HOAN_CANH").toString()) == 283) {
                tempLoaiHoancanh283.add(dsCapHoanCanh.get(0).get(i));
            }
            if (Integer.parseInt(dsCapHoanCanh.get(0).get(i).get("MA_LOAI_HOAN_CANH").toString()) == 284) {
                tempLoaiHoancanh284.add(dsCapHoanCanh.get(0).get(i));
            }
        }
        loaiHoancanh282.put("CON", tempLoaiHoancanh282);
        loaiHoancanh283.put("CON", tempLoaiHoancanh283);
        loaiHoancanh284.put("CON", tempLoaiHoancanh284);
        Integer countloaiHoancanh282 = 1;
        for (int i = 0; i < tempLoaiHoancanh282.size(); i++) {
            if (tempLoaiHoancanh282.get(i).get("CON") != null) {
                tempLoaiHoancanh282.get(i).replace("noi_dung",
                        countloaiHoancanh282.toString() + " " + tempLoaiHoancanh282.get(i).get("noi_dung").toString());
                countloaiHoancanh282++;
            }
        }
        Integer countloaiHoancanh283 = 1;
        for (int i = 0; i < tempLoaiHoancanh283.size(); i++) {
            if (tempLoaiHoancanh283.get(i).get("CON") != null) {
                tempLoaiHoancanh283.get(i).replace("noi_dung",
                        countloaiHoancanh283.toString() + " " + tempLoaiHoancanh283.get(i).get("noi_dung").toString());
                countloaiHoancanh283++;
            }
        }
        Integer countloaiHoancanh284 = 1;
        for (int i = 0; i < tempLoaiHoancanh284.size(); i++) {
            if (tempLoaiHoancanh284.get(i).get("CON") != null) {
                tempLoaiHoancanh284.get(i).replace("noi_dung",
                        countloaiHoancanh284.toString() + " " + tempLoaiHoancanh284.get(i).get("noi_dung").toString());
                countloaiHoancanh284++;
            }
        }
        result.add(infoTreEmParent.get(0));
        result.add(loaiHoancanh282);
        result.add(loaiHoancanh283);
        result.add(loaiHoancanh284);
        return result;

    }

    public static void buildTree(List<List<Map<String, Object>>> tree, String keyId, String keyIdParent) {
        for (int i = 0; i < tree.size(); i++) {
            List<Map<String, Object>> parent = tree.get(i);
            List<Map<String, Object>> child = i < tree.size() - 1 ? tree.get(i + 1) : new ArrayList<>();
            Multimap<Integer, Map<String, Object>> group = ArrayListMultimap.create();
            child.forEach(e -> {
                int idParent = Integer.parseInt(e.get(keyIdParent).toString());
                group.put(idParent, e);
            });
            parent.forEach(e -> {
                int id = Integer.parseInt(e.get(keyId).toString());
                Collection<Map<String, Object>> children = group.get(id);
                e.put("CON", children);
            });
        }
    }

}
