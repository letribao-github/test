package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.dao.HouseHoldDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.household.HouseHoldPostDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.household.HouseHoldPutDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.household.MemberHouseHoldPostDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.household.MemberHouseHoldPutDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.CustomException;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
 * Created by Benjamin Lam on 10/14/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */

@Repository
public class HouseHoldDaoImpl implements HouseHoldDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public int addNewHouseHold(HouseHoldPostDto houseHoldPostDto) {
        String sql = "{call ho_gia_dinh.sp_HouseHold_addNew(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

        int affectRow = jdbcTemplate.update(sql, houseHoldPostDto.getIdentityNumber(),
                houseHoldPostDto.getProfileNumber(), houseHoldPostDto.getMainAddressNumber(),
                houseHoldPostDto.getPaperNumber(), houseHoldPostDto.getOwnerName(),
                houseHoldPostDto.getOwnerNickName(), houseHoldPostDto.getCitizenID(),
                houseHoldPostDto.getGender(), houseHoldPostDto.getOwnerBirthday(),
                houseHoldPostDto.getMainAddress(), houseHoldPostDto.getOldAddress(),
                houseHoldPostDto.getNation(), houseHoldPostDto.getNationality(),
                houseHoldPostDto.getJob(), houseHoldPostDto.getBeforeAddress(),
                houseHoldPostDto.getSignedOfficerName(), houseHoldPostDto.getSignedOfficerLevel(),
                houseHoldPostDto.getMainOfficerName(), houseHoldPostDto.getMainOfficerLevel(),
                houseHoldPostDto.getMainOfficerOrganLevel());
        if (affectRow == 0) {
            throw new CustomException("Hộ khẩu đã tồn tại");
        }

        return affectRow;
    }

    @Override
    public int addNewHouseHoldMember(MemberHouseHoldPostDto memberHouseHoldPostDto) {
        String sql = "{call ho_gia_dinh.sp_HouseHold_addNewMember(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";

        int affectRow = jdbcTemplate.update(sql, memberHouseHoldPostDto.getIdentityNumber(),
                memberHouseHoldPostDto.getMemberName(), memberHouseHoldPostDto.getMemberNickName(),
                memberHouseHoldPostDto.getCitizenID(), memberHouseHoldPostDto.getMemberBirthday(),
                memberHouseHoldPostDto.getMemberGender(), memberHouseHoldPostDto.getMemberNation(),
                memberHouseHoldPostDto.getMemberNationality(), memberHouseHoldPostDto.getMemberJob(),
                memberHouseHoldPostDto.getRelationship(), memberHouseHoldPostDto.getBeforeAddress(),
                memberHouseHoldPostDto.getSignedOfficerName(), memberHouseHoldPostDto.getSignedOfficerLevel(),
                memberHouseHoldPostDto.getMainOfficerName(), memberHouseHoldPostDto.getMainOfficerLevel(),
                memberHouseHoldPostDto.getMainOfficerOrganLevel());

        if (affectRow == 0) {
            throw new CustomException("Hộ khẩu đã tồn tại");
        }

        return affectRow;
    }

    @Override
    public int updateHouseHold(HouseHoldPutDto houseHoldPutDto) {
        String sql = "{call ho_gia_dinh.sp_HouseHold_update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        int affectRow = jdbcTemplate.update(sql, houseHoldPutDto.getIdentityNumber(), houseHoldPutDto.getOwnerName(),
                houseHoldPutDto.getOwnerNickName(), houseHoldPutDto.getGender(), houseHoldPutDto.getOwnerBirthday(),
                houseHoldPutDto.getMainAddress(), houseHoldPutDto.getOldAddress(), houseHoldPutDto.getNation(),
                houseHoldPutDto.getNationality(), houseHoldPutDto.getJob(), houseHoldPutDto.getBeforeAddress(),
                houseHoldPutDto.getSignedDate(), houseHoldPutDto.getSignedOfficerName(), houseHoldPutDto.getSignedOfficerLevel(),
                houseHoldPutDto.getMainOfficerName(), houseHoldPutDto.getMainOfficerLevel(), houseHoldPutDto.getMainOfficerOrganLevel());

        if (affectRow == 0) {
            throw new CustomException("Cập nhật thất bại");
        }

        return affectRow;
    }

    @Override
    public int updateHouseHoldMember(MemberHouseHoldPutDto memberHouseHoldPutDto) {
        String sql = "{call ho_gia_dinh.sp_HouseHold_updateMember(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        int affectRow = jdbcTemplate.update(sql, memberHouseHoldPutDto.getMemberId(), memberHouseHoldPutDto.getIdentityNumber(),
                memberHouseHoldPutDto.getMemberName(), memberHouseHoldPutDto.getMemberNickName(),
                memberHouseHoldPutDto.getMemberBirthday(), memberHouseHoldPutDto.getMemberGender(),
                memberHouseHoldPutDto.getMemberNation(), memberHouseHoldPutDto.getMemberNationality(),
                memberHouseHoldPutDto.getMemberJob(), memberHouseHoldPutDto.getRelationship(),
                memberHouseHoldPutDto.getBeforeAddress(), memberHouseHoldPutDto.getSignedDate(),
                memberHouseHoldPutDto.getSignedOfficerName(), memberHouseHoldPutDto.getSignedOfficerLevel(),
                memberHouseHoldPutDto.getMainOfficerName(), memberHouseHoldPutDto.getMainOfficerLevel(),
                memberHouseHoldPutDto.getMainOfficerOrganLevel());

        if (affectRow == 0) {
            throw new CustomException("Cập nhật thất bại");
        }

        return affectRow;
    }

    @Override
    public int deleteHouseHold(String houseHoldID, String reason) {
        String sql = "{call ho_gia_dinh.sp_HouseHold_delete(?,?)}";
        int affectRow = jdbcTemplate.update(sql, houseHoldID, reason);
        if (affectRow == 0) {
            throw new CustomException("Xoá thất bại");
        }
        return affectRow;
    }

    @Override
    public int deleteMemberInHouseHold(String houseHoldID, int memberID, String reason) {
        String sql = "{call ho_gia_dinh.sp_HouseHold_deleteMember(?,?,?)}";
        int affectRow = jdbcTemplate.update(sql, houseHoldID, memberID, reason);
        if (affectRow == 0) {
            throw new CustomException("Xoá thất bại");
        }
        return affectRow;
    }

    @SneakyThrows
    @Override
    public Map<String, Object> getHouseHoldOwner(String houseHoldID) {
        String sql = "{call ho_gia_dinh.sp_HouseHold_lookUpByID(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, houseHoldID);
        if (result.size() == 0) {
            throw new CustomException("Hộ khẩu không tồn tại");
        }
        return result.get(0);
    }

    @Override
    public List<Map<String, Object>> getHouseHoldMember(String houseHoldID) {
        String sql = "{call ho_gia_dinh.sp_HouseHold_lookUpMemberByID(?)}";
        return jdbcTemplate.queryForList(sql, houseHoldID);
    }

    @Override
    public List<Map<String, Object>> getListHouseHold() {
        String sql = "{call ho_gia_dinh.sp_HouseHold_getAll()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> getListArmyLevel() {
        String sql = "{call ho_gia_dinh.sp_ArmyLevel_getAll()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> getListPoliceLevel() {
        String sql = "{call ho_gia_dinh.sp_PoliceOrganLevel_getAll()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> getListNationLevel() {
        String sql = "{call ho_gia_dinh.sp_nationLevel_getAll()}";
        return jdbcTemplate.queryForList(sql);
    }
}
