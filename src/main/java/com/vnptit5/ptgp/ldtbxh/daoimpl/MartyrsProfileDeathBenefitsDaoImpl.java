package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.dao.MartyrsProfileDeathBenefitsDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong.MartyrsProfileDeathBenefitsDto;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 * @author ToiTV
 * 2020-10-22
 */

@Repository
public class MartyrsProfileDeathBenefitsDaoImpl implements MartyrsProfileDeathBenefitsDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    /**
     * Get list death benefits information
     * @param hoSoID
     * @return
     */
    @Override
    public List<Map<String, Object>> getDeathInfo(int hoSoID) {
        try {
            String sql = "{call ncc.sp_HoSoLietSy_DeathBenefits_Info(?)}";
            List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, hoSoID);
            return result;
        } catch (Exception ex) {
            return new ArrayList<>();
        }
    }

    /**
     * Update death benefits information
     * @param request
     * @return
     */
    @Override
    public boolean updateDeathBenefitsInfo(MartyrsProfileDeathBenefitsDto request) {
        try {
            String sql = "{call ncc.sp_HoSoLietSy_DeathBenefits_Update(?,?,?,?,?,?,?,?,?,?,?,?)}";
            jdbcTemplate.update(sql,
                    request.getID(), request.getHoSoID(),
                    request.getHoTenNguoiHuong(), request.getNamSinhNguoiHuong(),
                    request.getQuanHeVoiNguoiCoCong(), request.getNguyenQuanNguoiHuong(),
                    request.getTruQuanNguoiHuong(), request.getMaiTangPhi(),
                    request.getTroCapCacThang(), request.getQdSo(),
                    request.getNgayRaQD(), request.getNoiRaQD());
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * Delete death benefits information
     * @param id
     * @return
     */
    @Override
    public boolean deleteDeathBenefitsInfo(int id) {
        try {
            String sql = "{call ncc.sp_HoSoLietSy_DeathBenefits_Delete(?)}";
            jdbcTemplate.update(sql, id);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}
