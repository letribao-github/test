package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.dao.XaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/*
 * Author: hungnguyenthanh
 * Date: 15/10/2020
 * */

@Repository
public class XaDaoImpl implements XaDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Map<String, Object>> getListXa(Integer huyenID) {
        String listXa = "{call ncc.sp_DanhMuc_Xa(?)}";
        return jdbcTemplate.queryForList(listXa, huyenID);
    }
}
