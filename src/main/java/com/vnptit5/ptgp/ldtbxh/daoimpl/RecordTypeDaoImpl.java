package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.dao.RecordTypeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/*
 * Author: SonNVT
 * Date: 15/10/2020
 * */

@Repository
public class RecordTypeDaoImpl implements RecordTypeDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Map<String, Object>> getListRecordType() {
        String listRecordType = "{call sp_DanhMucHoSo_LoaiHoSo()}";
        return jdbcTemplate.queryForList(listRecordType);
    }
}
