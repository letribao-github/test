package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.common.CommonConstant;
import com.vnptit5.ptgp.ldtbxh.dao.ExcelService;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.TemplateExcelWriter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.FileCopyUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;


@Service
@Slf4j
public class ExcelServiceImpl implements ExcelService {

    @Autowired
    private TemplateExcelWriter templateExcelWriter;

    /*
     * (non-Javadoc)
     * @see co.jp.oha.service.ExcelService#writeExcelFromTemplate(javax.servlet.http.HttpServletResponse, java.lang.Object, boolean)
     */
    @Override
    public <T> boolean writeSingleDataToExcelTemplate(HttpServletResponse response, T dto) {
        File file = templateExcelWriter.setSingleDataToTemplate(dto);
        this.responseFile(response, file);
        return true;

    }

    /*
     * (non-Javadoc)
     * @see co.jp.oha.service.ExcelService#writeListDataToExcelTemplate(javax.servlet.http.HttpServletResponse, java.util.List, java.lang.Class)
     */
    @Override
    public <T> boolean writeListDataToExcelTemplate(HttpServletResponse response, List<T> listDataInSheet,
            Class<T> typeClass) {
        File file = templateExcelWriter.setListDataToTemplate(listDataInSheet, typeClass);
        this.responseFile(response, file);
        return true;
    }

    /*
     * (non-Javadoc)
     * @see co.jp.oha.service.ExcelService#writeListDataToMultiSheet(javax.servlet.http.HttpServletResponse, java.lang.Object)
     */
    @Override
    public <T> boolean writeListDataToMultiSheet(HttpServletResponse response, T multiSheetData) {
        File file = templateExcelWriter.setListDataToMultiSheet(multiSheetData);
        this.responseFile(response, file);
        return true;
    }

    /*
     * (non-Javadoc)
     * @see co.jp.oha.service.ExcelService#response(javax.servlet.http.HttpServletResponse, java.io.File)
     */
    @Override
    public void response(HttpServletResponse response, File file) {
        this.responseFile(response, file);
    }

    /**
     *
     * @param response
     * @param file
     */
    private void responseFile(HttpServletResponse response, File file) {
        if (file == null) {
//            throw new ApplicationException("E134", "Export excel file fail");
        }

        try (FileInputStream fis = new FileInputStream(file)) {
            response.setContentType(CommonConstant.MimeType.EXCEL);
            response.setHeader("Content-Disposition", "attachment;");
            response.setContentLength((int) file.length());
            FileCopyUtils.copy(fis, response.getOutputStream());
            response.flushBuffer();
        } catch (IOException e) {
            log.error("" + e);
//            throw new ApplicationException("E018", e.getMessage());
        } finally {
            file.delete();
        }
    }
}

