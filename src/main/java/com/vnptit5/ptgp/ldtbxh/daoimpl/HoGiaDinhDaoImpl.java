package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.Multimap;
import com.vnptit5.ptgp.ldtbxh.dao.DiaPhuongDao;
import com.vnptit5.ptgp.ldtbxh.dao.HoGiaDinhDao;
import com.vnptit5.ptgp.ldtbxh.dao.ThanhVienDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.CatChetNhanKhauDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach.ChiTietChinhSachThanhVienDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh.*;
import com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh.chitra.ChiTraDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.thanhvien.DanhSachNhanKhauHuongChinhSachDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.thanhvien.ThemMoiThanhVienDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.CustomException;
import com.vnptit5.ptgp.ldtbxh.dto.response.PageDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.ThongKeChiTraChiTietDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.ThongKeChiTraDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author trinhctn
 */

@Repository
public class HoGiaDinhDaoImpl implements HoGiaDinhDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private DiaPhuongDao diaPhuongDao;

    @Autowired
    @Lazy
    private ThanhVienDao thanhVienDao;


    @Override
    public List<Map<String, Object>> danhSachChiTra(ChiTraDto chiTraDto) {
        String sql = "{call ho_gia_dinh.sp_ChiTra_DanhSach(?,?,?,?,?,?,?)}";
        return jdbcTemplate.queryForList(sql, chiTraDto.getMaTinh(), chiTraDto.getMaHuyen(), chiTraDto.getMaXa(),
                chiTraDto.getMaThon(), chiTraDto.getTrangthaiLanh(), chiTraDto.getNamHuong(),
                chiTraDto.getThangHuong());
    }

    @Override
    public void chiTra(Integer idNhanKhau, Integer soTienHuong, String namHuong, String thangHuong) {
        String sql = "{call ho_gia_dinh.sp_ChiTra(?,?,?)}";
        jdbcTemplate.update(sql, idNhanKhau, namHuong, thangHuong);
        String thucLanhSql = "{call ho_gia_dinh.sp_ThucLanh_ThemMoi(?,?,?)}";
        jdbcTemplate.update(thucLanhSql, idNhanKhau, soTienHuong, 1);
    }

    @Override
    public void khoiPhucChiTra(Integer idNhanKhau, Integer soTienHuong, String namHuong, String thangHuong) {
        String sql = "{call ho_gia_dinh.sp_ChiTra_KhoiPhuc(?,?,?)}";
        jdbcTemplate.update(sql, idNhanKhau, namHuong, thangHuong);
        String thucLanhSql = "{call ho_gia_dinh.sp_ThucLanh_KhoiPhuc(?,?,?)}";
        jdbcTemplate.update(thucLanhSql, idNhanKhau, soTienHuong, 0);
    }

    @Override
    public List<Map<String, Object>> tongTienChiTra(String maTinh, String maHuyen, String maXa, String maThon,
            String namHuong, String thangHuong) {
        String sql = "{call ho_gia_dinh.sp_ChiTra_TongTien(?,?,?,?,?,?)}";
        return jdbcTemplate.queryForList(sql, maTinh, maHuyen, maXa, maThon, namHuong, thangHuong);
    }

    @Override
    public List<ThongKeChiTraDto> thongKeChiTra(String maTinh, String maHuyen, String maXa, String maThon,
            String namHuong, String thangHuong) {
        List<Map<String, Object>> danhSachTongTien = tongTienChiTra(maTinh, maHuyen, maXa, maThon, namHuong,
                thangHuong);
        List<Map<String, Object>> danhSachXa = diaPhuongDao.danhSachXa(maTinh, maHuyen);
        Multimap<Integer, Map<String, Object>> group = ArrayListMultimap.create();
        danhSachTongTien.forEach(e -> {
            Integer thon = Integer.parseInt(maXa == null ? e.get("MA_XA").toString() : e.get("MA_THON").toString());
            group.put(thon, e);
        });
        if (maXa == null) {
            danhSachXa.forEach(xa -> {
                Integer id = Integer.parseInt(xa.get("MA_XA_PHUONG_BHXH").toString());
                Collection<Map<String, Object>> children = group.get(id);
                Integer tongTien = 0;
                Integer tongTienDaChi = 0;
                for (Map<String, Object> child : children) {
                    Integer so_tien_huong = Integer.parseInt(child.get("SO_TIEN_HUONG").toString());
                    tongTien += so_tien_huong;
                    if (Boolean.parseBoolean(child.get("TT_LANH").toString())) {
                        tongTienDaChi += so_tien_huong;
                    }
                }
                xa.put("TONG_TIEN", tongTien);
                xa.put("TONG_TIEN_DA_CHI", tongTienDaChi);
                xa.put("TONG_TIEN_CON_LAI", tongTien - tongTienDaChi);
            });
            ModelMapper modelMapper = new ModelMapper();
            return danhSachXa.stream().map(e -> modelMapper.map(e, ThongKeChiTraDto.class))
                    .collect(Collectors.toList());
        } else {
            List<Map<String, Object>> danhSachThon = diaPhuongDao.danhSachThon(maTinh, maHuyen, maXa);
            danhSachThon.forEach(e -> {
                Integer id = Integer.parseInt(e.get("MA_THON_XOM_BHXH").toString());
                Collection<Map<String, Object>> children = group.get(id);
                Integer tongTien = 0;
                Integer tongTienDaChi = 0;
                for (Map<String, Object> child : children) {
                    Integer so_tien_huong = Integer.parseInt(child.get("SO_TIEN_HUONG").toString());
                    tongTien += so_tien_huong;
                    if (Boolean.parseBoolean(child.get("TT_LANH").toString())) {
                        tongTienDaChi += so_tien_huong;
                    }
                }
                e.put("TONG_TIEN", tongTien);
                e.put("TONG_TIEN_DA_CHI", tongTienDaChi);
                e.put("TONG_TIEN_CON_LAI", tongTien - tongTienDaChi);
            });
            ModelMapper modelMapper = new ModelMapper();
            return danhSachThon.stream().map(e -> modelMapper.map(e, ThongKeChiTraDto.class))
                    .collect(Collectors.toList());
        }
    }

    @Override
    public List<ThongKeChiTraChiTietDto> thongKeChiTraChiTiet(String maXa, String maThon, String namHuong,
            String thangHuong) {
        String sql = "{call spDiaPhuong_Thon_ThongTin(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, maThon);
        Integer maTinh = Integer.parseInt(result.get(0).get("MA_TINH_BHXH").toString());
        Integer maHuyen = Integer.parseInt(result.get(0).get("MA_QUAN_HUYEN_BHXH").toString());
        Integer maXaBhxh = Integer.parseInt(result.get(0).get("MA_XA_PHUONG_BHXH").toString());

        String sqlThongKe = "{call ho_gia_dinh.sp_ChiTra_ThongKeChiTiet(?,?,?,?,?,?)}";

        List<Map<String, Object>> danhSachChiTiet = jdbcTemplate.queryForList(sqlThongKe, maTinh, maHuyen, maXa, maThon,
                namHuong, thangHuong);

        ModelMapper modelMapper = new ModelMapper();
        return danhSachChiTiet.stream().map(e -> modelMapper.map(e, ThongKeChiTraChiTietDto.class))
                .collect(Collectors.toList());
    }

    @Override
    public List<Map<String, Object>> thongKeChiTraChiTietDoiTuong(Integer idNhanKhau, String namHuong,
            String thangHuong) {
        String sql = "{call ho_gia_dinh.sp_ChiTra_ThongKeChiTietDoiTuong(?,?,?)}";
        return jdbcTemplate.queryForList(sql, idNhanKhau, namHuong, thangHuong);
    }

    @Override
    public int themMoiNhanKhau(ThemMoiThanhVienDto nhanKhauDto) {
        String sql = "{call ho_gia_dinh.sp_NhanKhau_ThemMoi(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        List<SqlParameter> params = Arrays.asList(new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR),
                new SqlOutParameter("P_OUT", Types.INTEGER));

        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setObject(1, nhanKhauDto.getTenNhanKhau(), Types.VARCHAR);
            callableStatement.setObject(2, nhanKhauDto.getNgaySinh(), Types.VARCHAR);
            callableStatement.setObject(3, nhanKhauDto.getDiaChi(), Types.VARCHAR);
            callableStatement.setObject(4, nhanKhauDto.getMaTinh(), Types.INTEGER);
            callableStatement.setObject(5, nhanKhauDto.getMaHuyen(), Types.INTEGER);
            callableStatement.setObject(6, nhanKhauDto.getMaXa(), Types.INTEGER);
            callableStatement.setObject(7, nhanKhauDto.getMaThon(), Types.INTEGER);
            callableStatement.setObject(8, nhanKhauDto.getDanToc(), Types.INTEGER);
            callableStatement.setObject(9, nhanKhauDto.getGioiTinh(), Types.INTEGER);
            callableStatement.setObject(10, nhanKhauDto.getSoCmnd(), Types.VARCHAR);
            callableStatement.setObject(11, nhanKhauDto.getMaDinhDanh(), Types.VARCHAR);
            callableStatement.setObject(12, nhanKhauDto.getMaHoGiaDinh(), Types.INTEGER);
            callableStatement.setObject(13, nhanKhauDto.getTinhTrangDiHoc(), Types.BOOLEAN);
            callableStatement.setObject(14, nhanKhauDto.getCapHoc(), Types.INTEGER);
            callableStatement.setObject(15, nhanKhauDto.getTheBHYT(), Types.BOOLEAN);
            callableStatement.setObject(16, nhanKhauDto.getLoaiTheBHYT(), Types.INTEGER);
            callableStatement.setObject(17, nhanKhauDto.getSoTheBHYT(), Types.VARCHAR);
            callableStatement.setObject(18, nhanKhauDto.getQuanHeChuHo(), Types.VARCHAR);
            return callableStatement;
        }, params);
        int idNhanKhau = Integer.parseInt(result.get("P_OUT").toString());
        if (idNhanKhau != 0) {
            themMoiQuanHeGiaDinh(nhanKhauDto.getMaHoGiaDinh(), nhanKhauDto.getQuanHeChuHo(), idNhanKhau);
        }
        // else {
        // Map<String, Object> thongTinNK =
        // thongTinNhanKhau(nhanKhauDto.getMaDinhDanh());
        // themMoiQuanHeGiaDinh(nhanKhauDto.getMaHoGiaDinh(),
        // nhanKhauDto.getQuanHeChuHo(),
        // Integer.parseInt(thongTinNK.get("ID_NHAN_KHAU").toString()));
        // }
        return idNhanKhau;
    }

    @Override
    public int themMoiDacDiemHGD(DacDiemHGDDto hgdDto) {
        String sql = "{call ho_gia_dinh.sp_DacDiemHGD_ThemMoi(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        List<SqlParameter> params = Arrays.asList(new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.BOOLEAN),
                new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.BOOLEAN),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.BOOLEAN),
                new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.BOOLEAN),
                new SqlParameter(Types.INTEGER), new SqlOutParameter("P_IDDACDIEMHGD", Types.INTEGER));

        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setObject(1, hgdDto.getTinhTrangNhaO(), Types.INTEGER);
            callableStatement.setObject(2, hgdDto.getLoaiHoXi(), Types.INTEGER);
            callableStatement.setObject(3, hgdDto.getLoaiDien(), Types.INTEGER);
            callableStatement.setObject(4, hgdDto.getSoNguoiTrongHo(), Types.INTEGER);
            callableStatement.setObject(5, hgdDto.getSoHuuNha(), Types.INTEGER);
            callableStatement.setObject(6, hgdDto.getDiemB1(), Types.INTEGER);
            callableStatement.setObject(7, hgdDto.getDienTichNha(), Types.INTEGER);
            callableStatement.setObject(8, hgdDto.getDiemB2(), Types.INTEGER);
            callableStatement.setObject(9, hgdDto.getDienThoai(), Types.BOOLEAN);
            callableStatement.setObject(10, hgdDto.getInternet(), Types.BOOLEAN);
            callableStatement.setObject(11, hgdDto.getTivi(), Types.BOOLEAN);
            callableStatement.setObject(12, hgdDto.getRadio(), Types.BOOLEAN);
            callableStatement.setObject(13, hgdDto.getMayTinh(), Types.BOOLEAN);
            callableStatement.setObject(14, hgdDto.getLoa(), Types.BOOLEAN);
            callableStatement.setObject(15, hgdDto.getNuocSinhHoat(), Types.INTEGER);
            callableStatement.setObject(16, hgdDto.getHoTroYTe(), Types.BOOLEAN);
            callableStatement.setObject(17, hgdDto.getHoTroTinhDung(), Types.BOOLEAN);
            callableStatement.setObject(18, hgdDto.getHoTroNhaO(), Types.BOOLEAN);
            callableStatement.setObject(19, hgdDto.getHoTroSanXuat(), Types.BOOLEAN);
            callableStatement.setObject(20, hgdDto.getHoTroGiaoDuc(), Types.BOOLEAN);
            callableStatement.setObject(21, hgdDto.getSoNguoiHuongTCXH(), Types.INTEGER);
            return callableStatement;
        }, params);
        int idDacDiemHGD = Integer.parseInt(result.get("P_IDDACDIEMHGD").toString());
        if (idDacDiemHGD == 0) {
            throw new CustomException("Đặc điểm đã tồn tại");
        }
        return idDacDiemHGD;
    }

    @Override
    public int themMoiHGD(ThemMoiHoGiaDinhDto dto) {
        String sql = "{call ho_gia_dinh.sp_HGD_ThemMoi(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        List<SqlParameter> params = Arrays.asList(
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlOutParameter("P_IDHOGIADINH", Types.INTEGER));
        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setObject(1, dto.getMaHoBhxh(), Types.INTEGER);
            callableStatement.setObject(2, dto.getMaTinh(), Types.VARCHAR);
            callableStatement.setObject(3, dto.getMaHuyen(), Types.VARCHAR);
            callableStatement.setObject(4, dto.getMaXa(), Types.VARCHAR);
            callableStatement.setObject(5, dto.getMaThon(), Types.VARCHAR);
            callableStatement.setObject(6, dto.getKhuVuc(), Types.INTEGER);
            callableStatement.setObject(7, dto.getMaHoanCanhGiaDinh(), Types.INTEGER);
            callableStatement.setObject(8, dto.getTenChuHo(), Types.VARCHAR);
            callableStatement.setObject(9, dto.getDiaChi(), Types.VARCHAR);
            callableStatement.setObject(10, dto.getSoHoKhau(), Types.VARCHAR);
            callableStatement.setObject(11, dto.getMaChiTietHoNgheo(), Types.INTEGER);
            callableStatement.setObject(12, dto.getPhanLoaiHoNgheo(), Types.INTEGER);
            callableStatement.setObject(13, dto.getSoCmndChuHo(), Types.VARCHAR);
            callableStatement.setObject(14, dto.getSoDienThoaiChuHo(), Types.VARCHAR);
            callableStatement.setObject(15, dto.getGhiChu(), Types.VARCHAR);
            return callableStatement;
        }, params);
        int idHGD = Integer.parseInt(result.get("P_IDHOGIADINH").toString());
        if (idHGD == 0) {
            throw new CustomException("Hộ gia đã tồn tại");
        } else {
            DacDiemHGDDto dacDiemHGDDto = DacDiemHGDDto.builder().tinhTrangNhaO(dto.getTinhTrangNhaO())
                    .loaiHoXi(dto.getLoaiHoXi()).loaiDien(dto.getLoaiDien()).soNguoiTrongHo(dto.getSoNguoiTrongHo())
                    .soHuuNha(dto.getSoHuuNha()).diemB1(dto.getDiemB1()).dienTichNha(dto.getDienTichNha())
                    .diemB2(dto.getDiemB2()).dienThoai(dto.getDienThoai()).internet(dto.getInternet())
                    .tivi(dto.getTivi()).radio(dto.getRadio()).mayTinh(dto.getMayTinh()).loa(dto.getLoa())
                    .nuocSinhHoat(dto.getNuocSinhHoat()).hoTroYTe(dto.getHoTroYTe())
                    .hoTroTinhDung(dto.getHoTroTinhDung()).hoTroNhaO(dto.getHoTroNhaO())
                    .hoTroSanXuat(dto.getHoTroSanXuat()).hoTroGiaoDuc(dto.getHoTroGiaoDuc())
                    .soNguoiHuongTCXH(dto.getSoNguoiHuongTCXH()).build();
            int idDacDiemHo = themMoiDacDiemHGD(dacDiemHGDDto);
            capNhatHGDDacDiem(idHGD, idDacDiemHo);

            ThemMoiThanhVienDto nhanKhauDto = ThemMoiThanhVienDto.builder()
                    .tenNhanKhau(dto.getTenChuHo()).ngaySinh(dto.getNgaySinh())
                    .diaChi(dto.getDiaChi())
                    .maTinh(dto.getMaTinh()).maHuyen(dto.getMaHuyen())
                    .maXa(dto.getMaXa()).maThon(dto.getMaThon())
                    .danToc(dto.getDanToc()).gioiTinh(dto.getGioiTinh())
                    .quanHeChuHo(dto.getQuanHeChuHo())
                    .soCmnd(dto.getSoCmndChuHo()).maDinhDanh(dto.getMaDinhDanh())
                    .maHoGiaDinh(idHGD)
                    .build();
            thanhVienDao.themMoiThanhVien(nhanKhauDto);
            return idHGD;
        }
    }

    @Override
    public void capNhatHGD(CapNhatHoGiaDinhDto dto) {
        String sqlCapNhatHGD = "{call ho_gia_dinh.sp_HGD_CapNhat(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        jdbcTemplate.update(sqlCapNhatHGD, dto.getMaHoGiaDinh(), dto.getMaTinh(), dto.getMaHuyen(), dto.getMaXa(),
                dto.getMaThon(), dto.getKhuVuc(), dto.getMaHoanCanhGiaDinh(), dto.getTenChuHo(), dto.getDiaChi(),
                dto.getSoHoKhau(), dto.getMaChiTietHoNgheo(), dto.getPhanLoaiHoNgheo(), dto.getSoCmndChuHo(),
                dto.getSoDienThoaiChuHo(), dto.getGhiChu());

        String sqlCapNhatDacDiemHGD = "{call ho_gia_dinh.sp_DacDiemHGD_CapNhat(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        jdbcTemplate.update(sqlCapNhatDacDiemHGD, dto.getIdDacDiemHGD(), dto.getTinhTrangNhaO(), dto.getLoaiHoXi(),
                dto.getLoaiDien(), dto.getSoNguoiTrongHo(), dto.getSoHuuNha(), dto.getDiemB1(), dto.getDienTichNha(),
                dto.getDiemB2(), dto.getDienThoai(), dto.getInternet(), dto.getTivi(), dto.getRadio(), dto.getMayTinh(),
                dto.getLoa(), dto.getNuocSinhHoat(), dto.getHoTroYTe(), dto.getHoTroTinhDung(), dto.getHoTroNhaO(),
                dto.getHoTroSanXuat(), dto.getHoTroGiaoDuc(), dto.getSoNguoiHuongTCXH());

        String sqlCapNhatChuHo = "{call ho_gia_dinh.sp_NhanKhau_CapNhat_ChuHo(?,?,?,?,?,?,?,?,?,?,?,?)}";
        jdbcTemplate.update(sqlCapNhatChuHo, dto.getTenChuHo(), dto.getNgaySinh(), dto.getDiaChi(), dto.getMaTinh(),
                dto.getMaHuyen(), dto.getMaXa(), dto.getMaThon(), dto.getDanToc(), dto.getGioiTinh(),
                dto.getSoCmndChuHo(), dto.getSoCmndChuHo(), dto.getIdNhanKhau());
    }

    @Override
    public int xoaHGD(Integer maHoGD) {
        String sql = "{call ho_gia_dinh.sp_HGD_Xoa(?)}";
        return jdbcTemplate.update(sql, maHoGD);
    }

    @Override
    public int khoiPhucHGD(Integer maHoGD) {
        String sql = "{call ho_gia_dinh.sp_HGD_KhoiPhuc(?)}";
        return jdbcTemplate.update(sql, maHoGD);
    }

    @Override
    public void capNhatHGDDacDiem(int maHoGiaDinh, int idDacDiem) {
        String sql = "{call ho_gia_dinh.sp_HGD_CapNhatDacDiem(?,?)}";
        jdbcTemplate.update(sql, maHoGiaDinh, idDacDiem);
    }

    @Override
    public void themMoiQuanHeGiaDinh(Integer maHoGiaDinh, String maQuanHe, Integer idNhanKhau) {
        String sql = "{call ho_gia_dinh.sp_QuanHeGiaDinh_ThemMoi(?,?,?)}";
        jdbcTemplate.update(sql, maHoGiaDinh, maQuanHe, idNhanKhau);
    }

    @Override
    public Map<String, Object> thongTinNhanKhau(String maDinhDanh) {
        String sql = "{call ho_gia_dinh.sp_NhanKhau_ThongTin(?)}";
        return jdbcTemplate.queryForList(sql, maDinhDanh).get(0);
    }



    @Override
    public Map<String, Object> thongTinHGD(Integer maHo) {
        String sql = "{call ho_gia_dinh.sp_HGD_ThongTinChung(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, maHo);
        if (result.size() == 0) {
            throw new CustomException("Hộ không tồn tại");
        }
        return result.get(0);
    }

    @Override
    public PageDto dsNhanKhauHuongChinhSach(DanhSachNhanKhauHuongChinhSachDto dto) {
        String sql = "{call ho_gia_dinh.sp_DanhSach_DoiTuong_HuongChinhSach_Thang(?,?,?,?,?,?,?,?,?,?,?,?)}";
        String listDoiTuong = null;
        if (dto.getDsDoiTuong() != null || !dto.getDsDoiTuong().isEmpty()){
            listDoiTuong = dto.getDsDoiTuong().stream().map(String::valueOf).collect(Collectors.joining(","));
        }

        List<SqlParameter> params = Arrays.asList(
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                new SqlOutParameter("P_SODONG", Types.INTEGER),
                new SqlOutParameter("P_SOTRANG", Types.INTEGER));

        String finalListDoiTuong = listDoiTuong;
        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setObject(1, dto.getMaTinh(), Types.VARCHAR);
            callableStatement.setObject(2, dto.getMaHuyen(), Types.VARCHAR);
            callableStatement.setObject(3, dto.getMaXa(), Types.VARCHAR);
            callableStatement.setObject(4, dto.getMaThon(), Types.VARCHAR);
            callableStatement.setObject(5, dto.getTenNhanKhau(), Types.VARCHAR);
            callableStatement.setObject(6, dto.getTrangThai(), Types.INTEGER);
            callableStatement.setObject(7, dto.getMaLoaiChinhSach(), Types.INTEGER);
            callableStatement.setObject(8,
                    dto.getDsDoiTuong() == null || dto.getDsDoiTuong().isEmpty()
                            ? "null" : finalListDoiTuong, Types.VARCHAR);
            callableStatement.setObject(9, dto.getPageSize(), Types.INTEGER);
            callableStatement.setObject(10, dto.getPageNumber(), Types.INTEGER);
            return callableStatement;
        }, params);
        int totalPage = Integer.parseInt(result.get("P_SOTRANG").toString());

        int totalRows = Integer.parseInt(result.get("P_SODONG").toString());

        List<Map<String, Object>> data = (List) result.get("#result-set-2");

        Multimap<Integer, Map<String, Object>> group = ArrayListMultimap.create();

        List<Map<String, Object>> houseHoldList = new ArrayList<Map<String, Object>>();

        List<Integer> idList = new ArrayList<Integer>();

        for (int i = 0; i < data.size(); i++) {
            if (data.get(i).get("MA_HO_GIA_DINH") != null) {
                int idParent = Integer.parseInt(data.get(i).get("MA_HO_GIA_DINH").toString());
                group.put(idParent, data.get(i));
                idList.add(idParent);
            }

        }

        List<Integer> idListWithoutDuplicate = idList.stream().distinct().collect(Collectors.toList());
        idListWithoutDuplicate.forEach(e -> {
            String getHouseHoldByIdSQL = "call ho_gia_dinh.sp_HGD_findbyId(?)";
            Map<String, Object> houseHold = jdbcTemplate.queryForMap(getHouseHoldByIdSQL, e);
            if (!houseHold.isEmpty()) {
                houseHoldList.add(houseHold);
            }
        });
        Integer size = houseHoldList.size();
        int temp = 0;
        for (int i = 0; i < size; i++) {
            houseHoldList.get(temp).put("isGroupBy", true);
            int id = Integer.parseInt(houseHoldList.get(temp).get("MA_HO_GIA_DINH").toString());
            Collection<Map<String, Object>> children = group.get(id);
            houseHoldList.addAll(temp + 1, children);
            temp = temp + children.size() + 1;
        }
        return PageDto.builder()
                .pageSize(dto.getPageSize())
                .pageNumber(dto.getPageNumber())
                .totalRows(totalRows)
                .totalPage(totalPage)
                .data(houseHoldList)
                .build();

    }

    @Override
    public int catChetNhanKhau(CatChetNhanKhauDto dto) {
        String sql = "{call ho_gia_dinh.sp_Nhankhau_CatChet_CapNhat(?)}";
        jdbcTemplate.update(sql, dto.getMaThanhVien());
        String sql1 = "call ho_gia_dinh.sp_HuongChinhSach_CapNhat_TrangThai_CatChet(?)";
        jdbcTemplate.update(sql1, dto.maThanhVien);
        String sql2 = "{call ho_gia_dinh.sp_HuongChinhSach_ThemMoi(?,?,?,?,?,?,?,?,?,?)}";
        String sql3 = "{call ho_gia_dinh.sp_LoaiChinhSach_findById(?)}";
        List<Map<String, Object>> policyType = jdbcTemplate.queryForList(sql3, 0);
        if (policyType.size() > 0) {
            List<SqlParameter> params = Arrays.asList(new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                    new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                    new SqlParameter(Types.DATE), new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                    new SqlParameter(Types.VARCHAR), new SqlOutParameter("P_IDHUONGCS", Types.INTEGER));
            Map result = jdbcTemplate.call(con -> {
                CallableStatement callableStatement = con.prepareCall(sql2);
                callableStatement.setObject(1, dto.getMaThanhVien(), Types.INTEGER);
                callableStatement.setObject(2, 102, Types.INTEGER);
                callableStatement.setObject(3, 21, Types.INTEGER);
                callableStatement.setObject(4, 1, Types.INTEGER);
                callableStatement.setObject(5, policyType.get(0).get("MUC_HUONG_CHUNG"), Types.INTEGER);
                callableStatement.setObject(6, java.time.LocalDate.now(), Types.DATE);
                callableStatement.setObject(7, policyType.get(0).get("MUC_HUONG_CHUNG"), Types.INTEGER);
                callableStatement.setObject(8, 2, Types.INTEGER);
                callableStatement.setObject(9, dto.getTenNguoiNhan(), Types.VARCHAR);
                return callableStatement;
            }, params);
            return Integer.parseInt(result.get("P_IDHUONGCS").toString());
        }

        else {
            return 0;
        }
    }

    @Override
    public void chuyenVungHGD(ChuyenVungHoGiaDinhDto dto) {
        String sql = "{call ho_gia_dinh.sp_HGD_ChuyenVung(?,?,?,?,?)}";
        jdbcTemplate.update(sql, dto.getMaHoGD(),
                dto.getMaTinh(), dto.getMaHuyen(),
                dto.getMaXa(), dto.getMaThon());
    }

    @Override
    public ChiTietChinhSachThanhVienDto chinhSachDoiTuong(Integer idHuongChinhSach, Integer idNhanKhau) {
        String sql = "{call ho_gia_dinh.sp_DoiTuong_HuongChinhSach(?)}";
        String sqlThongTinChung = "{call ho_gia_dinh.sp_NhanKhau_ThongTinChung(?)}";
        Map<String, Object> thongTinChung = jdbcTemplate.queryForList(sqlThongTinChung, idNhanKhau).get(0);
        return ChiTietChinhSachThanhVienDto.builder()
                .thongTinChung(thongTinChung)
                .chinhSachThanhVien(jdbcTemplate.queryForList(sql, idHuongChinhSach)).build();
    }


    @Override
    public int themMoiChinhSachDoiTuong(CapNhatChinhSachDoiTuong dto) {
        String sql = "{call ho_gia_dinh.sp_HuongChinhSach_ThemMoi(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        List<SqlParameter> params = Arrays.asList(
                new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.VARCHAR),new SqlParameter(Types.VARCHAR),new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR),new SqlParameter(Types.VARCHAR),new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR),new SqlParameter(Types.VARCHAR),new SqlParameter(Types.VARCHAR),
                new SqlOutParameter("P_IDHUONGCS", Types.INTEGER));
        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setObject(1, dto.getIdNhanKhau(), Types.INTEGER);
            callableStatement.setObject(2, dto.getIdDoiTuong(), Types.INTEGER);
            callableStatement.setObject(3, dto.getIdChinhSach(), Types.INTEGER);
            callableStatement.setObject(4, dto.getHeSo(), Types.INTEGER);
            callableStatement.setObject(5, dto.getMucHuong(), Types.INTEGER);
            callableStatement.setObject(6, dto.getNgayHuong(), Types.VARCHAR);
            callableStatement.setObject(7, dto.getSoTienHuong(), Types.INTEGER);
            callableStatement.setObject(8, dto.getTtHuong(), Types.INTEGER);
            callableStatement.setObject(9, dto.getTenNguoiNhan(), Types.VARCHAR);
            callableStatement.setObject(10, dto.getGhiChu(), Types.VARCHAR);
            callableStatement.setObject(11, dto.getSoQuyetDinhHuong(), Types.VARCHAR);
            callableStatement.setObject(12, dto.getNgayKyQDHuong(), Types.VARCHAR);
            callableStatement.setObject(13, dto.getSoSoLanhTien(), Types.VARCHAR);
            callableStatement.setObject(14, dto.getSoQuyetDinhThoiHuong(), Types.VARCHAR);
            callableStatement.setObject(15, dto.getNgayKyQDThoiHuong(), Types.VARCHAR);
            callableStatement.setObject(16, dto.getNgayThoiHuong(), Types.VARCHAR);
            callableStatement.setObject(17, dto.getLyDoThoiHuong(), Types.VARCHAR);
            return callableStatement;
        }, params);
        return Integer.parseInt(result.get("P_IDHUONGCS").toString());
    }

    @Override
    public List<Map<String, Object>> timKiemHoGiaDinh(TimKiemHoGiaDinhBTXHDto dto) {
        String sql = "{call ho_gia_dinh.sp_HGD_TimKiem(?,?,?,?,?,?,?,?,?)}";
        String dsHC = null;
        if (!dto.getPhanLoaiHo().isEmpty()){
            dsHC = dto.getPhanLoaiHo().stream().map(String::valueOf).collect(Collectors.joining(","));
        }
        return jdbcTemplate.queryForList(
                sql, dto.getMaTinh(), dto.getMaHuyen(),
                    dto.getMaXa(), dto.getMaThon(),
                    dto.getSoCMNDChuHo(), dto.getTenChuHo(),
                dto.getPhanLoaiHo().isEmpty() ? "null" : dsHC,
                dto.getNguoiNhanTCXH(), dto.getDoiTuongBTXH()
                );
    }

    @Override
    public PageDto timKiemHoGiaDinhPhanTrang(TimKiemHoGiaDinhPhanTrangDto dto) {
        String sql = "{call ho_gia_dinh.sp_HGD_TimKiem_PhanTrang(?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        String dsHoanCanh = null;
        if (!dto.getPhanLoaiHo().isEmpty()){
            dsHoanCanh = dto.getPhanLoaiHo().stream().map(String::valueOf).collect(Collectors.joining(","));
        }
        List<SqlParameter> params = Arrays.asList(
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.BOOLEAN),
                new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                new SqlOutParameter("P_SODONG", Types.INTEGER),
                new SqlOutParameter("P_SOTRANG", Types.INTEGER));
        String finalDsHoanCanh = dsHoanCanh;
        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setObject(1, dto.getMaTinh(), Types.VARCHAR);
            callableStatement.setObject(2, dto.getMaHuyen(), Types.VARCHAR);
            callableStatement.setObject(3, dto.getMaXa(), Types.VARCHAR);
            callableStatement.setObject(4, dto.getMaThon(), Types.VARCHAR);
            callableStatement.setObject(5, dto.getSoCMNDChuHo(), Types.VARCHAR);
            callableStatement.setObject(6, dto.getTenChuHo(),Types.VARCHAR);
            callableStatement.setObject(7, dto.getPhanLoaiHo().isEmpty() ? "null" : finalDsHoanCanh, Types.VARCHAR);
            callableStatement.setObject(8, dto.getNguoiNhanTCXH(), Types.BOOLEAN);
            callableStatement.setObject(9, dto.getDoiTuongBTXH(), Types.BOOLEAN);
            callableStatement.setObject(10, dto.getTrangThaiXoa(), Types.INTEGER);
            callableStatement.setObject(11, dto.getPageSize(), Types.INTEGER);
            callableStatement.setObject(12, dto.getPageNumber(), Types.INTEGER);
            return callableStatement;
        }, params);
        int totalPage = Integer.parseInt(result.get("P_SOTRANG").toString());
        int totalRows = Integer.parseInt(result.get("P_SODONG").toString());
        List<Map<String, Object>> temp = (List) result.get("#result-set-3");
        return PageDto.builder().totalRows(totalRows).totalPage(totalPage).pageSize(dto.getPageSize())
                .pageNumber(dto.getPageNumber()).data(temp).build();
    }

    public int capNhatChinhSachDoiTuong(CapNhatChinhSachDoiTuong dto) {
        String sql = "{call ho_gia_dinh.sp_HuongChinhSach_CapNhat(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        return jdbcTemplate.update(sql, dto.getIdHuongChinhSach(), dto.getIdDoiTuong(), dto.getIdChinhSach(), dto.getHeSo(),
                dto.getMucHuong(), dto.getNgayHuong(), dto.getSoTienHuong(), dto.getTtHuong(),
                dto.getGhiChu(), dto.getSoQuyetDinhHuong(), dto.getNgayKyQDHuong(),
                dto.getSoSoLanhTien(), dto.getSoQuyetDinhThoiHuong(), dto.getNgayKyQDThoiHuong(),
                dto.getNgayThoiHuong(), dto.getLyDoThoiHuong());
    }

    @Override
    public List<Map<String, Object>> danhSachCatChet(DanhSachCatChetDto ccdto) {
        String dscc = "{call ho_gia_dinh.sp_CatChet_DanhSach(?,?,?,?,?,?)}";
        return jdbcTemplate.queryForList(dscc, ccdto.getMaTinh(), ccdto.getMaHuyen(), ccdto.getMaXa(),
                ccdto.getMaThon(), ccdto.getIdNhanKhau(), ccdto.getTenNhanKhau());

    }

}
