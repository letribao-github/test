package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.dao.DiaPhuongDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.diaphuong.CapNhatThonDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.diaphuong.ThemMoiThonDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.diaphuong.TimKiemDiaPhuongDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author trinhctn
 */
@Repository
public class DiaPhuongDaoImpl implements DiaPhuongDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Map<String, Object>> danhSachTinh() {
        String sql = "{call spDiaPhuong_DSTinh()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhSachHuyen(String maTinh) {
        String sql = "{call spDiaPhuong_DSHuyen(?)}";
        return jdbcTemplate.queryForList(sql,maTinh);
    }

    @Override
    public List<Map<String, Object>> danhSachXa(String maTinh, String maHuyen) {
        String sql = "{call spDiaPhuong_DSXa(?,?)}";
        return jdbcTemplate.queryForList(sql,maTinh, maHuyen);
    }

    @Override
    public List<Map<String, Object>> danhSachThon(String maTinh, String maHuyen, String maXa) {
        String sql = "{call spDiaPhuong_DSThon(?,?,?)}";
        return jdbcTemplate.queryForList(sql,maTinh, maHuyen, maXa);
    }


    @Override
    public Map<String, Object> thongTinThong(String maThon) {
        String sql = "{call spDiaPhuong_Thon_ThongTin(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, maThon);
        if (result.size() == 0) {
            throw new CustomException("Thôn không tồn tại");
        }
        return result.get(0);
    }

    @Override
    public Map<String, Object> thongTinXa(String maXa) {
        String sql = "{call spDiaPhuong_Xa_ThongTin(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, maXa);
        if (result.size() == 0) {
            throw new CustomException("Thôn không tồn tại");
        }
        return result.get(0);
    }



    @Override
    public List<Map<String, Object>> timKiemDiaPhuong(TimKiemDiaPhuongDto dto) {
        String sql = "{call spDiaPhuong_TimKiem(?,?,?,?,?)}";
        return jdbcTemplate.queryForList(sql, dto.getMaTinhThanh(), dto.getMaQuanHuyen(),
                dto.getMaXaPhuong(), dto.getTenXaPhuong(), dto.getTrangThai());
    }
    @Override
    public int themMoiThon(ThemMoiThonDto dto) {
        String sql = "{call spDiaPhuong_ThemXom(?,?,?)}";
        List<SqlParameter> params = Arrays.asList(
                new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR),
                new SqlOutParameter("P_OUT", Types.INTEGER));
        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setString(1, dto.getTenThonXom());
            callableStatement.setString(2, dto.getMaXaPhuong());
            return callableStatement; }, params);
        return Integer.parseInt(result.get("P_OUT").toString());
    }

    @Override
    public void suaThon(CapNhatThonDto dto) {
        String sql = "{call spDiaPhuong_SuaThongTinThon(?,?)}";
        jdbcTemplate.update(sql, dto.getMaThonXom(), dto.getTenThonXom());
    }

    @Override
    public void khoiPhucThon(String maThon) {
        String sql = "{call spDiaPhuong_KhoiPhucThon(?)}";
        jdbcTemplate.update(sql, maThon);
    }

    @Override
    public void xoaThon(String maThon) {
        String sql = "{call spDiaPhuong_XoaThon(?)}";
        jdbcTemplate.update(sql, maThon);
    }

}
