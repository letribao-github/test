package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.dao.HoGiaDinhDao;
import com.vnptit5.ptgp.ldtbxh.dao.ThanhVienDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach.ChiTietChinhSachThanhVienDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.thanhvien.*;
import com.vnptit5.ptgp.ldtbxh.dto.response.*;
import com.vnptit5.ptgp.ldtbxh.dto.response.thanhvien.ThongTinChungThanhVienDto;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.TemplateExcelWriter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.sql.CallableStatement;
import java.sql.Types;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author trinhctn
 */

@Repository
public class ThanhVienDaoImpl implements ThanhVienDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private HoGiaDinhDao hoGiaDinhDao;

    @Autowired
    private TemplateExcelWriter templateExcelWriter;

    @Autowired
    private ExcelServiceImpl excelService;

    @Override
    public int xoaThanhVien(Integer idNhanKhau, Integer maHoGD) {
        String sql = "{call ho_gia_dinh.sp_NhanKhau_Xoa(?,?)}";
        return jdbcTemplate.update(sql, idNhanKhau, maHoGD);
    }

    @Override
    public int khoiPhucThanhVien(Integer idNhanKhau, Integer maHoGD) {
        String sql = "{call ho_gia_dinh.sp_NhanKhau_KhoiPhuc(?,?)}";
        return jdbcTemplate.update(sql, idNhanKhau, maHoGD);
    }

    @Override
    public int capNhatThanhVien(CapNhatThanhVienDto dto) {
        String sqlCapNhatNK = "{call ho_gia_dinh.sp_NhanKhau_CapNhat(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        int capNhatTTChung = jdbcTemplate.update(sqlCapNhatNK, dto.getIdNhanKhau(), dto.getTenNhanKhau(), dto.getNgaySinh(), dto.getDiaChi(),
                dto.getMaTinh(), dto.getMaHuyen(), dto.getMaXa(), dto.getMaThon(), dto.getDanToc(), dto.getGioiTinh(),
                dto.getSoCmnd(), dto.getMaDinhDanh(), dto.getTinhTrangDiHoc(), dto.getCapHoc(), dto.getTheBHYT(),
                dto.getLoaiTheBHYT(), dto.getSoTheBHYT());

        String sqlCapNhatHoNgheo = "{call ho_gia_dinh.sp_ThongTinHoNgheo_CapNhat(?,?,?,?,?,?,?,?,?,?)}";
        jdbcTemplate.update(sqlCapNhatHoNgheo,
                dto.getIdThongTinHN(), dto.getTrinhDoHocVan(),
                dto.getDoiTuongChinhSach(), dto.getKhamChuaBenh(),
                dto.getMangThai(), dto.getDoiTuongBTXH(),
                dto.getTinhTrangViecLam(), dto.getLyDoKhongKCB(),
                dto.getNgayDuSinh(), dto.getDoiTuongBTXHNgoaiHo());

        String sqlCapNhatBTXH = "{call ho_gia_dinh.sp_ThongTinBaoTroXaHoi_CapNhat(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        jdbcTemplate.update(sqlCapNhatBTXH, dto.getIdThongTinBTXH(), dto.getNhanTCXHThang(), dto.getNguoiNuoiDuong(),
                dto.getThongTinCha(), dto.getThongTinMe(), dto.getTinhTrangHonNhan(), dto.getThanhVienLaCon(),
                dto.getThamGiaLamViec(), dto.getTuPhucVu(), dto.getQuaTrinhHoatDong(), dto.getVanDong(), dto.getNghe(),
                dto.getNoi(), dto.getNhin(), dto.getThanKinh(), dto.getTriTue(), dto.getDangTatKhac(),
                dto.getMucDoKhuyetTat(), dto.getKinhNghiemChamSocDT(), dto.getAnTu(), dto.getBenhManTinh(),
                dto.getViPham(), dto.getNgayNhiemHIV());
        return capNhatTTChung;
    }

    @Override
    public int themMoiThongTinBTXH(ThongTinBTXHDto dto) {
        String sql = "{call ho_gia_dinh.sp_ThongTinBaoTroXaHoi_ThemMoi(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        List<SqlParameter> params = Arrays.asList(
                new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.BOOLEAN),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.BOOLEAN),
                new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.BOOLEAN),
                new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.BOOLEAN),
                new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.BOOLEAN),
                new SqlParameter(Types.BOOLEAN),
                new SqlParameter(Types.VARCHAR),new SqlParameter(Types.DATE),
                new SqlOutParameter("P_IDTTBTXH", Types.INTEGER));

        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setObject(1, dto.getNhanTCXHThang(), Types.BOOLEAN);
            callableStatement.setObject(2, dto.getNguoiNuoiDuong(), Types.VARCHAR);
            callableStatement.setObject(3, dto.getThongTinCha(), Types.VARCHAR);
            callableStatement.setObject(4, dto.getThongTinMe(), Types.VARCHAR);
            callableStatement.setObject(5, dto.getTinhTrangHonNhan(), Types.INTEGER);
            callableStatement.setObject(6, dto.getThanhVienLaCon(), Types.VARCHAR);
            callableStatement.setObject(7, dto.getThamGiaLamViec(), Types.BOOLEAN);
            callableStatement.setObject(8, dto.getTuPhucVu(), Types.BOOLEAN);
            callableStatement.setObject(9, dto.getQuaTrinhHoatDong(), Types.VARCHAR);
            callableStatement.setObject(10, dto.getVanDong(), Types.BOOLEAN);
            callableStatement.setObject(11, dto.getNghe(), Types.BOOLEAN);
            callableStatement.setObject(12, dto.getNhin(), Types.BOOLEAN);
            callableStatement.setObject(13, dto.getThanKinh(), Types.BOOLEAN);
            callableStatement.setObject(14, dto.getTriTue(), Types.BOOLEAN);
            callableStatement.setObject(15, dto.getDangTatKhac(), Types.BOOLEAN);
            callableStatement.setObject(16, dto.getMucDoKhuyetTat(), Types.INTEGER);
            callableStatement.setObject(17, dto.getKinhNghiemChamSocDT(), Types.VARCHAR);
            callableStatement.setObject(18, dto.getAnTu(), Types.BOOLEAN);
            callableStatement.setObject(19, dto.getBenhManTinh(), Types.BOOLEAN);
            callableStatement.setObject(20, dto.getViPham(), Types.VARCHAR);
            callableStatement.setObject(21, dto.getNgayNhiemHIV(), Types.DATE);
            return callableStatement;
        }, params);
        int idThongTinBTXH = Integer.parseInt(result.get("P_IDTTBTXH").toString());
        if (idThongTinBTXH == 0) {
            throw new CustomException("Đặc điểm đã tồn tại");
        }
        return idThongTinBTXH;
    }

    @Override
    public void capNhatThongTinBTXH(ThongTinBTXHDto dto) {
        int idThongTinBTXH = themMoiThongTinBTXH(dto);
        String sql = "{call ho_gia_dinh.sp_NhanKhau_ThemMoiThongTinBTXH(?,?)}";
        jdbcTemplate.update(sql, dto.getMaThanhVien(), idThongTinBTXH);
    }

    @Override
    public int themMoiThongTinHoNgheo(ThongTinHoNgheoDto dto) {
        String sql = "{call ho_gia_dinh.sp_ThongTinHoNgheo_ThemMoi(?,?,?,?,?,?,?,?,?,?)}";
        List<SqlParameter> params = Arrays.asList(new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.BOOLEAN),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.BOOLEAN), new SqlOutParameter("P_IDTHONGTINHONGHEO", Types.INTEGER));
        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setObject(1, dto.getTrinhDoHocVan(), Types.INTEGER);
            callableStatement.setObject(2, dto.getDoiTuongChinhSach(), Types.INTEGER);
            callableStatement.setObject(3, dto.getKhamChuaBenh(), Types.INTEGER);
            callableStatement.setObject(4, dto.getMangThai(), Types.BOOLEAN);
            callableStatement.setObject(5, dto.getDoiTuongBTXH(), Types.BOOLEAN);
            callableStatement.setObject(6, dto.getTinhTrangViecLam(), Types.INTEGER);
            callableStatement.setObject(7, dto.getLyDoKhongKCB(), Types.INTEGER);
            callableStatement.setObject(8, dto.getNgayDuSinh(), Types.VARCHAR);
            callableStatement.setObject(9, dto.getDoiTuongBTXHNgoaiHo(), Types.BOOLEAN);
            return callableStatement;
        }, params);
        int idThongTinHN = Integer.parseInt(result.get("P_IDTHONGTINHONGHEO").toString());
        if (idThongTinHN == 0) {
            throw new CustomException("Thông tin hộ nghèo đã tồn tại");
        }
        return idThongTinHN;
    }

    @Override
    public void capNhatThongTinHoNgheoThanhVien(ThongTinHoNgheoDto dto) {
        int idThongTinHN = themMoiThongTinHoNgheo(dto);
        String sql = "{call ho_gia_dinh.sp_NhanKhau_ThemMoiThongTinHoNgheo(?,?)}";
        jdbcTemplate.update(sql, dto.getMaThanhVien(), idThongTinHN);
    }

    @Override
    public int themMoiThanhVien(ThemMoiThanhVienDto dto) {
        int maThanhVien = hoGiaDinhDao.themMoiNhanKhau(dto);
        if (maThanhVien == 0) {
            throw new CustomException("Thành viên đã tồn tại");
        } else {
            ThongTinHoNgheoDto hoNgheoDto = ThongTinHoNgheoDto.builder()
                    .trinhDoHocVan(dto.getTrinhDoHocVan() == null ? 0 : dto.getTrinhDoHocVan())
                    .doiTuongChinhSach(dto.getDoiTuongChinhSach() == null ? 0 : dto.getDoiTuongChinhSach())
                    .khamChuaBenh(dto.getKhamChuaBenh() == null ? 0 : dto.getKhamChuaBenh())
                    .mangThai(dto.getMangThai() == null ? false : dto.getMangThai())
                    .doiTuongBTXH(dto.getDoiTuongBTXH() == null ? false : dto.getDoiTuongBTXH())
                    .tinhTrangViecLam(dto.getTinhTrangViecLam() == null ? 0 : dto.getTinhTrangViecLam())
                    .lyDoKhongKCB(dto.getLyDoKhongKCB()).ngayDuSinh(dto.getNgayMangThai())
                    .doiTuongBTXHNgoaiHo(dto.getDoiTuongBTXHNgoaiHo() == null ? false : dto.getDoiTuongBTXHNgoaiHo())
                    .maThanhVien(maThanhVien).build();
            capNhatThongTinHoNgheoThanhVien(hoNgheoDto);

            ThongTinBTXHDto thongTinBTXHDto = ThongTinBTXHDto.builder().nhanTCXHThang(dto.getNhanTCXHThang())
                    .nguoiNuoiDuong(dto.getNguoiNuoiDuong()).thongTinCha(dto.getThongTinCha())
                    .thongTinMe(dto.getThongTinMe())
                    .tinhTrangHonNhan(dto.getTinhTrangHonNhan() == null ? 0 : dto.getTinhTrangHonNhan())
                    .thanhVienLaCon(dto.getThanhVienLaCon()).thamGiaLamViec(dto.getThamGiaLamViec())
                    .tuPhucVu(dto.getTuPhucVu())
                    .quaTrinhHoatDong(dto.getQuaTrinhHoatDong())
                    .vanDong(dto.getVanDong() == null ? false : dto.getVanDong())
                    .nghe(dto.getNgheNoi() == null ? false : dto.getNgheNoi())
                    .nhin(dto.getNhin() == null ? false : dto.getNhin())
                    .thanKinh(dto.getThanKinh() == null ? false : dto.getThanKinh())
                    .triTue(dto.getTriTue() == null ? false : dto.getTriTue())
                    .dangTatKhac(dto.getDangTatKhac() == null ? false : dto.getDangTatKhac())
                    .mucDoKhuyetTat(dto.getMucDoKhuyetTat()).kinhNghiemChamSocDT(dto.getKinhNghiemChamSocDT())
                    .anTu(dto.getAnTu()).benhManTinh(dto.getBenhManTinh()).viPham(dto.getViPham())
                    .ngayNhiemHIV(dto.getNgayNhiemHIV())
                    .maThanhVien(maThanhVien).build();

            capNhatThongTinBTXH(thongTinBTXHDto);
            return maThanhVien;
        }
    }

    @Override
    public List<Map<String, Object>> danhSachThanhVien(Integer maHo) {
        String sql = "{call ho_gia_dinh.sp_HGD_DanhSachThanhVien(?)}";
        return jdbcTemplate.queryForList(sql, maHo);
    }

    @Override
    public List<ThanhVienSearchDto.DataList> TimKiemThanhVienHoGiaDinh(TimKiemThanhVienDto dtnk){
        String ttnkSql = "{call ho_gia_dinh.sp_NhanKhau_TimKiem(?,?,?,?,?,?,?,?,?,?)}";
        String dsChinhSach = null;
        String dsHoanCanh = null;
        if (dtnk.getChinhSach() != null && !dtnk.getChinhSach().isEmpty()){
            dsChinhSach = dtnk.getChinhSach().stream().map(String::valueOf).collect(Collectors.joining(","));
        }
        if (dtnk.getPhanLoaiHo() != null && dtnk.getPhanLoaiHo().size() > 0){
            dsHoanCanh = dtnk.getPhanLoaiHo().stream().map(String::valueOf).collect(Collectors.joining(","));
        }
        List<Map<String, Object>> result = jdbcTemplate.queryForList(ttnkSql,
                dtnk.getMaTinh(), dtnk.getMaHuyen(), dtnk.getMaXa(), dtnk.getMaThon(),
                dtnk.getSoCmnd(), dtnk.getTenThanhVien(),
                dtnk.getChinhSach() == null || dtnk.getChinhSach().isEmpty() ? "null" : dsChinhSach,
                dtnk.getPhanLoaiHo() == null || dtnk.getPhanLoaiHo().isEmpty() ? "null" : dsHoanCanh,
                dtnk.getNguoiNhanTCXH(), dtnk.getDoiTuongBTXH()
        );
        ModelMapper modelMapper = new ModelMapper();
        return result.stream().map(e -> modelMapper.map(e, ThanhVienSearchDto.DataList.class))
                .collect(Collectors.toList());
    }

    @Override
    public PageDto timKiemThanhVienPhanTrang(TimKiemThanhVienPhanTrangDto dto) {
        String sql = "{call ho_gia_dinh.sp_NhanKhau_TimKiem_PhanTrang(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        String dsChinhSach = null;
        String dsHoanCanh = null;
        if (!dto.getChinhSach().isEmpty()){
            dsChinhSach = dto.getChinhSach().stream().map(String::valueOf).collect(Collectors.joining(","));
        }
        if (!dto.getPhanLoaiHo().isEmpty()){
            dsHoanCanh = dto.getPhanLoaiHo().stream().map(String::valueOf).collect(Collectors.joining(","));
        }
        List<SqlParameter> params = Arrays.asList(
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.BOOLEAN),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.INTEGER),
                new SqlOutParameter("P_SODONG", Types.INTEGER),
                new SqlOutParameter("P_SOTRANG", Types.INTEGER));
        String finalDsChinhSach = dsChinhSach;
        String finalDsHoanCanh = dsHoanCanh;
        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setObject(1, dto.getMaTinh(), Types.VARCHAR);
            callableStatement.setObject(2, dto.getMaHuyen(), Types.VARCHAR);
            callableStatement.setObject(3, dto.getMaXa(), Types.VARCHAR);
            callableStatement.setObject(4, dto.getMaThon(), Types.VARCHAR);
            callableStatement.setObject(5, dto.getSoCmnd(), Types.VARCHAR);
            callableStatement.setObject(6, dto.getTenThanhVien(),Types.VARCHAR);
            callableStatement.setObject(7, dto.getChinhSach().isEmpty() ? "null" : finalDsChinhSach, Types.VARCHAR);
            callableStatement.setObject(8, dto.getPhanLoaiHo().isEmpty() ? "null" : finalDsHoanCanh, Types.VARCHAR);
            callableStatement.setObject(9, dto.getNguoiNhanTCXH(), Types.BOOLEAN);
            callableStatement.setObject(10, dto.getDoiTuongBTXH(), Types.BOOLEAN);
            callableStatement.setObject(11, dto.getPageSize(), Types.INTEGER);
            callableStatement.setObject(12, dto.getPageNumber(), Types.INTEGER);
            callableStatement.setObject(13, dto.getTrangThaiXoa(), Types.INTEGER);
            return callableStatement;
        }, params);
        int totalPage = Integer.parseInt(result.get("P_SOTRANG").toString());
        int totalRows = Integer.parseInt(result.get("P_SODONG").toString());
        List<Map<String, Object>> temp = (List) result.get("#result-set-2");
        return PageDto.builder().totalRows(totalRows).totalPage(totalPage).pageSize(dto.getPageSize())
                .pageNumber(dto.getPageNumber()).data(temp).build();
    }

    @Override
    public ChiTietChinhSachThanhVienDto chiTietHuongCS(int maThanhVien, Integer idHuongCS) {
        String sqlThongTinChung = "{call ho_gia_dinh.sp_NhanKhau_ThongTinChung(?)}";
        Map<String, Object> thongTinChung = jdbcTemplate.queryForList(sqlThongTinChung, maThanhVien).get(0);
        return ChiTietChinhSachThanhVienDto.builder().thongTinChung(thongTinChung)
                .chinhSachThanhVien(chinhSachTheoThanhVien(maThanhVien, idHuongCS)).build();
    }

    @Override
    public List<Map<String, Object>> chinhSachTheoThanhVien(int maThanhVien, Integer idHuongCS) {
        String sql = "{call ho_gia_dinh.sp_NhanKhau_HuongChinhSach(?,?)}";
        return jdbcTemplate.queryForList(sql, maThanhVien, idHuongCS);
    }

    @Override
    public ThongTinThanhVienChiTietDto thongTinThanhVienChiTiet(int maThanhVien, Integer idHuongCS) {
        String sqlHN = "{call ho_gia_dinh.sp_NhanKhau_ThongTinHoNgheo(?)}";
        Map<String, Object> thongTinHoNgheo = jdbcTemplate.queryForList(sqlHN, maThanhVien).get(0);

        String sqlBTXH = "{call ho_gia_dinh.sp_NhanKhau_ThongTinBTXH(?)}";
        Map<String, Object> thongTinBTXH = jdbcTemplate.queryForList(sqlBTXH, maThanhVien).get(0);

        String sqlThongTinChung = "{call ho_gia_dinh.sp_NhanKhau_ThongTinChung(?)}";
        Map<String, Object> thongTinChung = jdbcTemplate.queryForList(sqlThongTinChung, maThanhVien).get(0);

        return ThongTinThanhVienChiTietDto.builder().thongTinChung(thongTinChung).thongTinHoNgheo(thongTinHoNgheo)
                .thongTinBTXH(thongTinBTXH).chinhSachThanhVien(chinhSachTheoThanhVien(maThanhVien, idHuongCS)).build();
    }

    @Override
    public ThongTinChungThanhVienDto thongTinChungThanhVien(int maThanhVien) {
        String sqlHN = "{call ho_gia_dinh.sp_NhanKhau_ThongTinHoNgheo(?)}";
        Map<String, Object> thongTinHoNgheo = jdbcTemplate.queryForMap(sqlHN, maThanhVien);

        String sqlBTXH = "{call ho_gia_dinh.sp_NhanKhau_ThongTinBTXH(?)}";
        Map<String, Object> thongTinBTXH = jdbcTemplate.queryForMap(sqlBTXH, maThanhVien);

        String sqlThongTinChung = "{call ho_gia_dinh.sp_NhanKhau_ThongTinChung(?)}";
        Map<String, Object> thongTinChung = jdbcTemplate.queryForMap(sqlThongTinChung, maThanhVien);

        return ThongTinChungThanhVienDto.builder()
                .thongTinBTXH(thongTinBTXH)
                .thongTinChung(thongTinChung)
                .thongTinHoNgheo(thongTinHoNgheo)
                .build();
    }

    @Override
    public DoiTuongHuongCSDto danhSachHuongCS(Integer maHo, Boolean HGD, Integer trangThaiHuong) {
        String sql = "{call ho_gia_dinh.sp_DoiTuong_DSHuongChinhSach(?,?,?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, maHo, HGD, trangThaiHuong);
        Map<String, Object> tongTienDangHuong = tongTienHuongCS_HGD(maHo, HGD);
        return DoiTuongHuongCSDto.builder().data(result)
                .tongTienHuong(Integer.parseInt(tongTienDangHuong.get("TONG_SO_TIEN_HUONG").toString())).build();
    }

    @Override
    public Map<String, Object> tongTienHuongCS_HGD(Integer maHoGD, Boolean HGD) {
        String sql = "{call ho_gia_dinh.sp_HGD_TongTienHuongCS(?,?)}";
        return jdbcTemplate.queryForList(sql, maHoGD, HGD).get(0);
    }

    @Override
    public Map<String, Object> tongTienHuongCS_ThanhVien(Integer idNhanKhau, Integer idHuongCS) {
        String sql = "{call ho_gia_dinh.sp_NhanKhau_TongTienHuongCS(?,?)}";
        return jdbcTemplate.queryForMap(sql, idNhanKhau, idHuongCS);
    }


    @Override
    public void ChuyenThanhVien(ThongTinChuyenTVDto ctv){
        String ctvSql = "{call ho_gia_dinh.sp_NhanKhau_ChuyenThanhVien(?, ?, ?)}";
        jdbcTemplate.update(ctvSql,ctv.getMaHo(),ctv.getMaQuanHe(),ctv.getIdNhanKhau());
    }

    @Override
    public void ExportMemberList(TimKiemThanhVienDto tktvdto, HttpServletResponse response){
        ThanhVienSearchDto dto = new ThanhVienSearchDto();
        List<ThanhVienSearchDto.DataList> exportDto = this.TimKiemThanhVienHoGiaDinh(tktvdto);
        dto.setData(exportDto);
        File file = this.templateExcelWriter.setSingleDataToTemplate(dto);
        excelService.response(response, file);
        return;
    }

    @Override
    public DoiTuongHuongCSDto chinhSachThanhVienDangHuong(Integer maThanhVien,
                                                          Integer idHuongCS) {
        Map<String, Object> tongTienDangHuong = tongTienHuongCS_ThanhVien(maThanhVien, idHuongCS);
        return DoiTuongHuongCSDto.builder().data(chinhSachTheoThanhVien(maThanhVien, idHuongCS))
                .tongTienHuong(Integer.parseInt(tongTienDangHuong.get("TONG_SO_TIEN_HUONG").toString())).build();
    }
}
