package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.dao.NguoiDungDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoidung.CapNhatNguoiDungDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoidung.ThemMoiNguoiDungDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoidung.TimKiemNguoiDungDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.nguoidung.ChiTietNguoiDungDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.CustomException;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author trinhctn
 */
@Repository
public class NguoiDungDaoImpl implements NguoiDungDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public Map<String, Object> findByUsername(String tenDangNhap){
        String sql = "{call spNguoiDung_DangNhap(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, tenDangNhap);
        if (result.size() == 0) {
            throw new CustomException("Tên đăng nhập không tồn tại");
        }
        return result.get(0);
    }

    public Map themMoiNguoiDung(ThemMoiNguoiDungDto dto){
        String sql = "{call spNguoiDung_ThemMoi(?,?,?,?,?,?,?,?,?,?,?,?)}";
        List<SqlParameter> params = Arrays.asList(
                new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.BOOLEAN),
                new SqlParameter(Types.BOOLEAN),
                new SqlParameter(Types.BOOLEAN),
                new SqlParameter(Types.BOOLEAN),
                new SqlParameter(Types.BOOLEAN),
                new SqlOutParameter("P_OUT", Types.INTEGER)
        );
        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setInt(1, dto.getMaPhongBan());
            callableStatement.setInt(2, dto.getMaChucDanh());
            callableStatement.setString(3, dto.getTenDangNhap());
            callableStatement.setString(4, dto.getMatKhau());
            callableStatement.setString(5, dto.getHoTen());
            callableStatement.setString(6, dto.getSdt());
            callableStatement.setBoolean(7, dto.isTrangThai());
            callableStatement.setBoolean(8, dto.getThem());
            callableStatement.setBoolean(9, dto.getSua());
            callableStatement.setBoolean(10, dto.getXoa());
            callableStatement.setBoolean(11, dto.getXem());
            return callableStatement; }, params);
        int id = Integer.parseInt(result.get("P_OUT").toString());
        if (id == 0){
            throw new CustomException("Người dùng đã tồn tại");
        }
        dto.getDsNhomQuyen().forEach(maNhomQuyen -> {
            themMoiPhanQuyen(id, maNhomQuyen);
        });
        return result;
    }

    @Override
    public void themMoiPhanQuyen(int maNguoiDung, int maNhomQuyen) {
        String sql = "{call sp_PhanQuyen_ThemMoi(?,?)}";

        jdbcTemplate.update(sql, maNguoiDung, maNhomQuyen);
    }

    @Override
    public void xoaPhanQuyen(String tenDangNhap) {
        String sql = "{call sp_PhanQuyen_Xoa(?)}";
        jdbcTemplate.update(sql, tenDangNhap);
    }

    @Override
    public List<Map<String, Object>> danhSachNhomQuyenNguoiDung(String tenDangNhap) {
        String sql = "{call spNguoiDung_DanhSachNhomQuyen(?)}";
        return jdbcTemplate.queryForList(sql, tenDangNhap);
    }

    @Override
    public void suaNguoiDung(CapNhatNguoiDungDto dto) {
        String sql = "{call spNguoiDung_SuaThongTin(?,?,?,?,?,?,?,?,?,?,?)}";
        jdbcTemplate.update(sql,
                dto.getMaPhongBan(), dto.getMaChucDanh(),
                dto.getTenDangNhap(), dto.getMatKhau(),
                dto.getHoTen(), dto.getSdt(),
                dto.isTrangThai(),
                dto.getThem(), dto.getSua(),
                dto.getXoa(), dto.getXem());
        if (dto.getDsNhomQuyen().size() > 0){
            xoaPhanQuyen(dto.getTenDangNhap());
            dto.getDsNhomQuyen().forEach(maNhomQuyen -> {
                themMoiPhanQuyen(dto.getMaNguoiDung(), maNhomQuyen);
            });
        }
    }

    @Override
    public void xoaNguoiDung(String tenDangNhap) {
        String sql = "{call spNguoiDung_Xoa(?)}";
        jdbcTemplate.update(sql, tenDangNhap);
    }

    @Override
    public void khoiPhucNguoiDung(String tenDangNhap){
        String sql = "{call spNguoiDung_KhoiPhuc(?)}";
        jdbcTemplate.update(sql, tenDangNhap);
    }

    @Override
    public List<Map<String, Object>> danhSachNguoiDung(TimKiemNguoiDungDto timKiemNguoiDungDto){
        String sql = "{call spNguoiDung_DanhSach(?,?,?,?,?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql,
                timKiemNguoiDungDto.getHoTen(),
                timKiemNguoiDungDto.getMaPhongBan(),
                timKiemNguoiDungDto.getMaChucDanh(),
                timKiemNguoiDungDto.getTrangThai(),
                timKiemNguoiDungDto.getMaDonVi());
        return result;
    }

    @Override
    public ChiTietNguoiDungDto chiTietNguoiDung(String tenDangNhap) {
        String sql = "{call spNguoiDung_DangNhap(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, tenDangNhap);
        if (result.size() == 0) {
            throw new CustomException("Tên đăng nhập không tồn tại");
        }
        result.get(0).put("danhSachNhomQuyen", danhSachNhomQuyenNguoiDung(tenDangNhap));
        ModelMapper modelMapper = new ModelMapper();
        return modelMapper.map(result.get(0), ChiTietNguoiDungDto.class);
    }

}
