package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.dao.NCCThongTinHoSoDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong.*;
import com.vnptit5.ptgp.ldtbxh.dto.request.treem.TimKiemTreEmDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.CustomException;
import com.vnptit5.ptgp.ldtbxh.dto.response.PageDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
public class NCCThongTinHoSoImpl implements NCCThongTinHoSoDao
{
    @Autowired
    private JdbcTemplate jdbcTemplate;
    //insert trang thong tin ho so - nguoi co cong voi cm
    public int tmThongTinHSNCC(TTNguoicocongCM TTHSNCC)
    {
        String tmChinhSach = "{call ncc.sp_NguoiCoCongGiupDoCM_Insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        int affectRow = jdbcTemplate.update(tmChinhSach, TTHSNCC.getBanSao(),TTHSNCC.getSoHoSoTinhQuanLy(), TTHSNCC.getSoHoSoBoQuanLy(),
                        TTHSNCC.getQuanHuyenTiepNhan(), TTHSNCC.getCumTo(),
                        TTHSNCC.getHoSoChuyenDenTuID(), TTHSNCC.getHoSoChuyenDiID(),
                        TTHSNCC.getHoSoQuanLy(), TTHSNCC.getPhuongXaTiepNhan(),
                        TTHSNCC.getSoBHXH(), TTHSNCC.getThoiGianChuyenDen(),TTHSNCC.getThoiGianChuyenDi(),
                        TTHSNCC.getHoVaTen(), TTHSNCC.getBiDanh(),TTHSNCC.getNgaySinh(),
                        TTHSNCC.getGioiTinh(), TTHSNCC.getDanToc(), TTHSNCC.getQueQuan(),TTHSNCC.getChoOHiennay(),TTHSNCC.getDuocNhaNuocPhongTang(),
                        TTHSNCC.getQuyetDinhSo(),TTHSNCC.getNgayRaQuyetDinh(),TTHSNCC.getThangRaQuyetDinh(),TTHSNCC.getNamRaQuyetDinh(),
                        TTHSNCC.getCheDoDuocHuong(), TTHSNCC.getHoanCanhHienTai(),TTHSNCC.getThoiKy(),TTHSNCC.getViTriLuuHoSo(),
                        TTHSNCC.getCacCheDoKhac(), TTHSNCC.getTinhTrangCanhan());
        if (affectRow == 0) {
            throw new CustomException("Hộ khẩu đã tồn tại");
        }

        return affectRow;
    }
    //update trang thong tin ho so - nguoi co cong voi cm
    @Override
    public int updateTTHSNCC (TTNguoicocongCMUpdate TTHSNCC) {
        String sql = "{call ncc.sp_NguoiCoCongGiupDoCM_Update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        int affectRow = jdbcTemplate.update(sql, TTHSNCC.getHoSoNguoiCoCongGiupCMID()
                ,TTHSNCC.getBanSao(),TTHSNCC.getSoHoSoTinhQuanLy(), TTHSNCC.getSoHoSoBoQuanLy(),
                TTHSNCC.getQuanHuyenTiepNhan(), TTHSNCC.getCumTo(),
                TTHSNCC.getHoSoChuyenDenTuID(), TTHSNCC.getHoSoChuyenDiID(),
                TTHSNCC.getHoSoQuanLy(), TTHSNCC.getPhuongXaTiepNhan(),
                TTHSNCC.getSoBHXH(),TTHSNCC.getThoiGianChuyenDen(),TTHSNCC.getThoiGianChuyenDi(),
                TTHSNCC.getHoVaTen(), TTHSNCC.getBiDanh(),TTHSNCC.getNgaySinh(),
                TTHSNCC.getGioiTinh(), TTHSNCC.getDanToc(), TTHSNCC.getQueQuan(),TTHSNCC.getChoOHiennay(),TTHSNCC.getDuocNhaNuocPhongTang(),
                TTHSNCC.getQuyetDinhSo(),TTHSNCC.getNgayRaQuyetDinh(),TTHSNCC.getThangRaQuyetDinh(),TTHSNCC.getNamRaQuyetDinh(),
                TTHSNCC.getCheDoDuocHuong(), TTHSNCC.getHoanCanhHienTai(),TTHSNCC.getThoiKy(),TTHSNCC.getViTriLuuHoSo(),
                TTHSNCC.getCacCheDoKhac(), TTHSNCC.getTinhTrangCanhan());
        if (affectRow == 0) {
            throw new CustomException("Cập nhật thất bại");
        }

        return affectRow;
    }

     @Override  //delete trang thong tin ho so - nguoi co cong voi cm
    public void deleteTTHSNCC(Integer HoSoNguoiCoCongGiupCMID)
     {
         String sql = "{call ncc.sp_NguoiCoCongGiupDoCM_Delete(?)}";
         int rs = jdbcTemplate.update(sql, HoSoNguoiCoCongGiupCMID);
     }

    @Override  //select trang thong tin ho so - nguoi co cong voi cm
    public  Map<String, Object>   selectTTHSNCC(Integer HosoID)
    {
        String sql = "{call ncc.sp_NguoiCoCongGiupDoCM_selectThongTinHoSo(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, HosoID);
        if (result.size() == 0) {
            throw new CustomException("Hồ Sơ không tồn tại");
        }
        return result.get(0);
    }

    @Override   //select trang thong tin tro cap - nguoi co cong voi cm
    public  List<Map<String, Object>> selectTTTrocap(Integer HosoID)
    {
        String sql = "{call ncc.sp_NguoiCoCongGiupDoCM_TTTroCap_selectTTTroCap(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, HosoID);
        if (result.size() == 0) {
            throw new CustomException("Hồ Sơ không tồn tại");
        }
        return result;
    }

    @Override //insert trang thong tin tro cap - nguoi co cong voi cm
    public int insertNCCTroCap(NCCTroCapInsert NCCTroCapInsert)
    {
        String sql = "{call ncc.sp_NguoiCoCongGiupDoCM_TTTroCap_Insert(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        int affectRow = jdbcTemplate.update(sql, NCCTroCapInsert.getHosoID(),NCCTroCapInsert.getNguoiDuocHuongTroCap()
                ,NCCTroCapInsert.getQuanHeVoiNguoiCoCong(),NCCTroCapInsert.getNamSinhNguoiHuong()
                ,NCCTroCapInsert.getNguyenQuanNguoiHuong(),NCCTroCapInsert.getTruQuanNguoiHuong(),NCCTroCapInsert.getMaiTangPhi()
                ,NCCTroCapInsert.getTroCapCacThang(), NCCTroCapInsert.getThoiGianHuongTroCap(), NCCTroCapInsert.getQuyetDinhHuongTroCap()
                ,NCCTroCapInsert.getNgayRaQuyetDinhHuongTroCap(),NCCTroCapInsert.getNoiRaQuyetDinhHuongTroCap(),NCCTroCapInsert.getSoTienTroCap()
                ,NCCTroCapInsert.getTruyLinhTuNgay(),NCCTroCapInsert.getTruyLinhDenNgay(), NCCTroCapInsert.getSoTienTruyLinh()
        );
        if (affectRow == 0) {
            throw new CustomException("Thông tin Đính kèm đã tồn tại");
        }

        return affectRow;
    }

    @Override //update trang thong tin tro cap - nguoi co cong voi cm
    public int updateNCCTroCap (NCCTroCapUpdate UpdateTroCap) {
        String sql = "{call ncc.sp_NguoiCoCongGiupDoCM_TTTroCap_Update(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        int affectRow = jdbcTemplate.update(
                sql, UpdateTroCap.getHosoID(),UpdateTroCap.getThongTinCheDoID(),UpdateTroCap.getNguoiDuocHuongTroCap()
                ,UpdateTroCap.getQuanHeVoiNguoiCoCong(),UpdateTroCap.getNamSinhNguoiHuong()
                ,UpdateTroCap.getNguyenQuanNguoiHuong(),UpdateTroCap.getTruQuanNguoiHuong(),UpdateTroCap.getMaiTangPhi()
                ,UpdateTroCap.getTroCapCacThang(), UpdateTroCap.getThoiGianHuongTroCap(), UpdateTroCap.getQuyetDinhHuongTroCap()
                ,UpdateTroCap.getNgayRaQuyetDinhHuongTroCap(),UpdateTroCap.getNoiRaQuyetDinhHuongTroCap(),UpdateTroCap.getSoTienTroCap()
                ,UpdateTroCap.getTruyLinhTuNgay(),UpdateTroCap.getTruyLinhDenNgay(), UpdateTroCap.getSoTienTruyLinh()
        );
        if (affectRow == 0) {
            throw new CustomException("Cập nhật thất bại");
        }
        return affectRow;
    }

    @Override  //delete trang thong tin tro cap  - nguoi co cong voi cm
    public void deleteNCCTrocap(Integer HosoID, Integer ThongTinCheDoID)
    {
        String sql = "{call ncc.sp_NguoiCoCongGiupDoCM_TTTroCap_Delete(?,?)}";
        int rs = jdbcTemplate.update(sql, HosoID, ThongTinCheDoID);
    }

    @Override //select trang dinh kem - nguoi co cong voi cm
    public List<Map<String, Object>> selectDinhkem(Integer HosoID)
    {
        String sql = "{call ncc.sp_NguoiCoCongGiupDoCM_TTDinhKem_selectTTDinhKem(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, HosoID);
        if (result.size() == 0) {
            throw new CustomException("Hồ Sơ không tồn tại");
        }
        return result;
    }

    @Override //insert trang dinh kem - nguoi co cong voi cm
    public int insertNCCTrocapDinhkem(NCCTroCapInsertDinhKem NCCDinhKemInsert)
    {
        String sql = "{call ncc.sp_NguoiCoCongGiupDoCM_TTDinhKem_Insert(?,?,?,?,?,?)}";
        int affectRow = jdbcTemplate.update(sql, NCCDinhKemInsert.getHosoID(),NCCDinhKemInsert.getTenFile()
                ,NCCDinhKemInsert.getNgayCapNhat(),NCCDinhKemInsert.getGhiChu(),NCCDinhKemInsert.getDuongDan(),NCCDinhKemInsert.getThuMuc()
        );
        if (affectRow == 0) {
            throw new CustomException("Thông tin Đính kèm đã tồn tại");
        }

        return affectRow;
    }

    @Override //update trang dinh kem - nguoi co cong voi cm
    public int updatenccDinhKem (NCCTroCapInsertDinhKem UpdateDinhKem) {
        String sql = "{call ncc.sp_NguoiCoCongGiupDoCM_TTDinhKem_Update(?,?,?,?,?,?)}";
        int affectRow = jdbcTemplate.update(
                sql, UpdateDinhKem.getHosoID(),UpdateDinhKem.getTenFile(),UpdateDinhKem.getNgayCapNhat()
                ,UpdateDinhKem.getGhiChu(),UpdateDinhKem.getDuongDan(),UpdateDinhKem.getThuMuc()
        );
        if (affectRow == 0) {
            throw new CustomException("Cập nhật thất bại");
        }
        return affectRow;
    }

    @Override  //xoa trang dinh kem - nguoi co cong voi cm
    public void deletenccDinhKem(Integer HosoID)
    {
        String sql = "{call ncc.sp_NguoiCoCongGiupDoCM_TTDinhKem_Delete(?)}";
        int rs = jdbcTemplate.update(sql, HosoID);
    }

    @Override   //select trang thong tin tro cap - nguoi co cong voi cm
    public  List<Map<String, Object>> selectDMDH()
    {
        String sql = "{call ncc.sp_dmCheDoDuocHuong}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql);
        if (result.size() == 0) {
            throw new CustomException("danh mục không tồn tại");
        }
        return result;
    }

    @Override   //select trang thong tin tro cap - nguoi co cong voi cm
    public  List<Map<String, Object>> selectDMTinhtrangcanhan()
    {
        String sql = "{call ncc.sp_dmTinhTrangCaNhan}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql);
        if (result.size() == 0) {
            throw new CustomException("danh mục không tồn tại");
        }
        return result;
    }


}
