package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.dao.DanhMucDao;
import com.vnptit5.ptgp.ldtbxh.utils.TreeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author trinhctn
 */
@Repository
public class DanhMucDaoImpl implements DanhMucDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;


    @Override
    public List<Map<String, Object>> danhMucHoanCanhGiaDinh() {
        String sql = "{call spHoanCanhGiaDinh_DanhSach()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucNhomQuyen() {
        String sql = "{call spNhomQuyen_DanhSach()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucChucDanh() {
        String sql = "{call spChucDanh_DanhSach()}";
        return jdbcTemplate.queryForList(sql);
    }


    @Override
    public List<Map<String, Object>> danhMucDonVi(String maTinhThanh,
                                                  String maQuanHuyen, String maXaPhuong) {
        String sql = "{call spDonVi_DanhSach(?,?,?)}";
        return jdbcTemplate.queryForList(sql,
                maTinhThanh, maQuanHuyen, maXaPhuong);
    }

    @Override
    public List<Map<String, Object>> danhMucTatCaDonVi() {
        String sql = "{call sp_DonVi_TatCa()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucPhongBan(int maDonVi) {
        String sql = "{call spPhongBan_DanhSach(?)}";
        return jdbcTemplate.queryForList(sql, maDonVi);
    }

    @Override
    public List<Map<String, Object>> danhMucChiTietHoanCanh() {
        String sql = "{call spHoanCanh_DanhSach(?)}";
        List< List<Map<String, Object>> > dsCapHoanCanh = new ArrayList<>();
        for (int cap = 1; cap <= 3; cap ++) {
            dsCapHoanCanh.add(jdbcTemplate.queryForList(sql, cap));
        }

        TreeUtils.buildTree(dsCapHoanCanh, "MA_CHI_TIET_HOAN_CANH", "ID_CHA");
        return dsCapHoanCanh.get(0);
    }

    @Override
    public List<Map<String, Object>> danhMucChiTietHoanCanhTheoLoai(Integer maLoaiHoanCanh, Integer maCap) {
        String sql = "{call spHoanCanh_DanhSach_TheoLoai(?,?)}";
        List< List<Map<String, Object>> > dsCapHoanCanhTheoLoai = new ArrayList<>();
        for (int cap = 1; cap <= 3; cap ++) {
            dsCapHoanCanhTheoLoai.add(jdbcTemplate.queryForList(sql,maLoaiHoanCanh, cap));
        }

        TreeUtils.buildTree(dsCapHoanCanhTheoLoai, "MA_CHI_TIET_HOAN_CANH", "ID_CHA");
        return dsCapHoanCanhTheoLoai.get(0);
    }

    @Override
    public List<Map<String, Object>> danhMucChiTietHuongTroGiup() {
        String sql = "{call spHuongTroGiup_DanhSach(?)}";
        List< List<Map<String, Object>> > dsCapHuongTroGiup = new ArrayList<>();
        for (int cap = 1; cap <= 3; cap ++) {
            dsCapHuongTroGiup.add(jdbcTemplate.queryForList(sql, cap));
        }

        TreeUtils.buildTree(dsCapHuongTroGiup, "MA_CHI_TIET_HUONG_TRO_GIUP", "ID_CHA");
        return dsCapHuongTroGiup.get(0);
    }

    @Override
    public List<Map<String, Object>> danhMucChiTietHTG() {
        String sql = "{call spHuongTroGiup()}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql);
        result.forEach(item ->{
            item.put("value", Integer.parseInt(item.get("MA_CHI_TIET_HUONG_TRO_GIUP").toString()) );
            if (item.get("ID_CHA") == null){
                item.remove("ID_CHA");
                item.put("ID_CHA", -1);
            }

        });
        return result;
    }

    @Override
    public List<Map<String, Object>> danhMucChiTietHC(Integer maLoaiHoanCanh, Integer cap) {
        String sql = "{call spHoanCanh_DanhSach_TheoLoai(?,?)}";
        List<Map<String, Object>> result =  jdbcTemplate.queryForList(sql,maLoaiHoanCanh, cap);
        result.forEach(item ->{
            item.put("value", Integer.parseInt(item.get("MA_CHI_TIET_HOAN_CANH").toString()) );
            if (item.get("ID_CHA") == null){
                item.remove("ID_CHA");
                item.put("ID_CHA", -1);
            }
        });
        return result;
    }

    @Override
    public List<Map<String, Object>> danhMucPhanLoaiHoNgheo() {
        String sql = "{call ho_gia_dinh.sp_DanhMuc_HoanCanhGiaDinh()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucChiTietHoNgheo() {
        String sql = "{call ho_gia_dinh.sp_DanhMuc_ChiTietHoNgheo()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucDanToc() {
        String sql = "{call ho_gia_dinh.sp_DanhMuc_DanToc()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucTinhTrangNhaO() {
        String sql = "{call ho_gia_dinh.sp_DanhMuc_TinhTrangNhaO()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucLoaiHoXi() {
        String sql = "{call ho_gia_dinh.sp_DanhMuc_LoaiHoXi()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucLoaiDien() {
        String sql = "{call ho_gia_dinh.sp_DanhMuc_LoaiDien()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucSoHuuNha() {
        String sql = "{call ho_gia_dinh.sp_DanhMuc_SoHuuNha()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucNuocSinhHoat() {
        String sql = "{call ho_gia_dinh.sp_DanhMuc_NuocSinhHoat()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucQuanHe() {
        String sql = "{call ho_gia_dinh.sp_DanhMuc_QuanHe()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucDoiTuong(int idLoaiDoiTuong){
        String dmdt = "{call ho_gia_dinh.sp_DanhMuc_DoiTuong(?)}";
        return jdbcTemplate.queryForList(dmdt, idLoaiDoiTuong);
    }

    @Override
    public List<Map<String, Object>> danhMucCapHoc() {
        String sql = "{call ho_gia_dinh.sp_DanhMuc_CapHoc()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucTrinhDo() {
        String sql = "{call ho_gia_dinh.sp_DanhMuc_TrinhDoHocVan()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucDTCS() {
        String sql = "{call ho_gia_dinh.sp_DanhMuc_DoiTuongChinhSach()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucTinhTrangViecLam() {
        String sql = "{call ho_gia_dinh.sp_DanhMuc_TinhTrangViecLam()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucTinhTrangHonNhan() {
        String sql = "{call ho_gia_dinh.sp_DanhMuc_TinhTrangHonNhan()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucLoaiDoiTuong(Integer idLoaiCS, Integer doiTuong) {
        String sql = "{call ho_gia_dinh.sp_DanhMuc_LoaiDoiTuong(?,?)}";
        return jdbcTemplate.queryForList(sql, idLoaiCS, doiTuong);
    }

    @Override
    public List<Map<String, Object>> danhMucLoaiChiTieu() {
        String sqlLoaiChiTieu = "{call tre_em.sp_LoaiChiTieu_DS()}";
        List<Map<String, Object>> danhMucLoaiChiTieu = jdbcTemplate.queryForList(sqlLoaiChiTieu);
        List<Map<String, Object>> danhMucChiTieu = danhMucChiTieu();
        List<Map<String, Object>> listChiTieu = TreeUtils.buildList(danhMucChiTieu, danhMucLoaiChiTieu,
                "MA_LOAI_HOAN_CANH", "MA_LOAI_HOAN_CANH", "SHEET_NAME");

        for (Map<String, Object> item:listChiTieu){
            item.remove("SHEET_NAME");
        }
        return listChiTieu;
    }

    @Override
    public List<Map<String, Object>> danhMucChiTieu() {
        String sql = "{call tre_em.sp_ChiTieu_DanhSach(?)}";
        List< List<Map<String, Object>> > dsCapHoanCanh = new ArrayList<>();
        for (int cap = 1; cap <= 3; cap ++) {
            dsCapHoanCanh.add(jdbcTemplate.queryForList(sql, cap));
        }

        TreeUtils.buildTree(dsCapHoanCanh, "MA_CHI_TIET_HOAN_CANH", "ID_CHA");
        return dsCapHoanCanh.get(0);
    }

    @Override
    public String danhSachHoanCanhCon(Integer maHC, Integer loaiHC) {
        String sqlHC = "{call tre_em.sp_HoanCanh_DanhSach_Con(?,?)}";
        List<Map<String, Object>> childHC = jdbcTemplate.queryForList(sqlHC, maHC, loaiHC);
        String listHCString = childHC.stream().map(e -> e.get("MA_CHI_TIET_HOAN_CANH").toString()).collect(Collectors.joining(","));
        if (maHC != null){
            String sqlHCNhieu = "{call tre_em.sp_HoanCanh_DanhSach_Con_Nhieu(?)}";
            List<Map<String, Object>> rs = jdbcTemplate.queryForList(sqlHCNhieu, listHCString);
            return rs.stream().map(e -> e.get("MA_CHI_TIET_HOAN_CANH").toString()).collect(Collectors.joining(","));
        }
        return listHCString;
    }

    @Override
    public String danhSachHuongTroGiupCon(Integer maHTG) {
        String sqlHTG = "{call tre_em.sp_HuongTroGiup_DanhSach_Con(?)}";
        List<Map<String, Object>> childHTG = jdbcTemplate.queryForList(sqlHTG, maHTG);
        String listHTGString = childHTG.stream().map(e -> e.get("MA_CHI_TIET_HUONG_TRO_GIUP").toString()).collect(Collectors.joining(","));
        if (maHTG != null){
            String sqlHTGNhieu = "{call tre_em.sp_HuongTroGiup_DanhSach_Con_Nhieu(?)}";
            List<Map<String, Object>> rs = jdbcTemplate.queryForList(sqlHTGNhieu, listHTGString);
            return rs.stream().map(e -> e.get("MA_CHI_TIET_HUONG_TRO_GIUP").toString()).collect(Collectors.joining(","));
        }
        return listHTGString;
    }

    @Override
    public List<Map<String, Object>> danhMucTrangThaiHCS() {
        String sql = "{call ho_gia_dinh.sp_DanhMuc_TrangThaiHCS()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucLoaiTheBhyt() {
        String sql = "{call ho_gia_dinh.sp_DanhMuc_LoaiTheBHYT()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucLyDoKhongKCB() {
        String sql = "{call ho_gia_dinh.sp_DanhMuc_LyDoKhongKCB()}";
        return jdbcTemplate.queryForList(sql);
    }

    @Override
    public List<Map<String, Object>> danhMucMucDoKhuyetTat() {
        String sql = "{call ho_gia_dinh.sp_DanhMuc_MucDoKhuyetTat()}";
        return jdbcTemplate.queryForList(sql);
    }
}
