package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.dao.FileStorageDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.CustomException;
import com.vnptit5.ptgp.ldtbxh.exception.FileNotFoundException;
import com.vnptit5.ptgp.ldtbxh.exception.FileStorageException;
import com.vnptit5.ptgp.ldtbxh.property.FileStorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Benjamin Lam on 10/21/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */
@Repository
public class FileStorageDaoImpl implements FileStorageDto {

    private Path fileStorageLocation;

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public FileStorageDaoImpl(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    @Override
    public String uploadAttachedProfile(MultipartFile file, Integer heroID, String fileName, String folderName, String note) {
        // Normalize file name
        Date date = new Date();
        String savedFileName = StringUtils.cleanPath(date.getTime() + "_" + file.getOriginalFilename());

        try {
            // Check if the file's name contains invalid characters
            if (savedFileName.contains("..")) {
                throw new FileStorageException("Sorry! Filename contains invalid path sequence " + fileName);
            }

            // Create sub folder
            Path savePath = Paths.get(fileStorageLocation.toAbsolutePath().toString() + File.separatorChar + folderName);
            try {
                Files.createDirectories(savePath);
            } catch (Exception ex) {
                throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
            }

            // Copy file to the target location (Replacing existing file with the same name)
            Path targetLocation = savePath.resolve(savedFileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);

            String sql = "{call ncc.sp_AttachedFile_addNew(?,?,?,?,?,?,?)}";
            int affectRow = jdbcTemplate.update(sql, heroID, fileName, savedFileName, file.getContentType(), note, savePath.toAbsolutePath().toString(), folderName);

            if (affectRow == 0) {
                throw new CustomException("Cập nhật thất bại");
            }

            return savedFileName;
        } catch (IOException ex) {
            throw new FileStorageException("Could not store file " + fileName + ". Please try again!", ex);
        }
    }

    @Override
    public List<Map<String, Object>> getListAttachedFiles(Integer heroID) {
        String sql = "{call ncc.sp_AttachedFile_getAll(?)}";
        return jdbcTemplate.queryForList(sql, heroID);
    }

    @Override
    public int deleteAttachedFile(Integer fileID) {
        String sql = "{call ncc.sp_AttachedFile_delete(?)}";
        int affectRow = jdbcTemplate.update(sql, fileID);
        if (affectRow == 0) {
            throw new CustomException("Xoá thất bại");
        }
        return affectRow;
    }

    @Override
    public Resource downloadAttachedFile(String fileName, String folderName) {
        try {
            Path getStorageLocation = Paths.get(fileStorageLocation.toAbsolutePath().toString()
                    + File.separatorChar + folderName);
            Path filePath = getStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if (resource.exists()) {
                return resource;
            } else {
                throw new FileNotFoundException("File not found " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new FileNotFoundException("File not found " + fileName, ex);
        }
    }
}
