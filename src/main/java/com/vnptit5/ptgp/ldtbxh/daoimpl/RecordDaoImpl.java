package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.authentication.CustomUserDetailsService;
import com.vnptit5.ptgp.ldtbxh.dao.DanTocDao;
import com.vnptit5.ptgp.ldtbxh.dao.ExcelService;
import com.vnptit5.ptgp.ldtbxh.dao.HuyenDao;
import com.vnptit5.ptgp.ldtbxh.dao.RecordDao;
import com.vnptit5.ptgp.ldtbxh.dao.RecordTypeDao;
import com.vnptit5.ptgp.ldtbxh.dto.export.RecordExcelExportDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.record.RecordCreateRequestDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.record.RecordRequestDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.treem.TimKiemTreEmDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.PageDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.nguoicocong.RecordCategory;
import com.vnptit5.ptgp.ldtbxh.utils.DatetimeUtil;
import com.vnptit5.ptgp.ldtbxh.utils.excel.annotation.TemplateExcelWriter;
import java.io.File;
import java.sql.CallableStatement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletResponse;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/*
 * Author: SonNVT
 * Date: 15/10/2020
 * */
@Repository
public class RecordDaoImpl implements RecordDao {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private ExcelService excelService;

    @Autowired
    private CustomUserDetailsService userService;

    @Autowired
    private TemplateExcelWriter templateExcelWriter;

    @Autowired
    private DanTocDao danTocDao;

    @Autowired
    private HuyenDao huyenDao;

    @Autowired
    private RecordTypeDao recordTypeDao;

    @Override
    public List<Map<String, Object>> searchRecord(RecordRequestDto recordRequestDto) {
        String listRecord = "{call ncc.sp_HoSo_TimKiem(?,?,?,?,?,?,?,?,?,?,?,?,?)}";
//        String listRecord = "{call sp_HoSo_TimKiem_1()}";
        return jdbcTemplate.queryForList(listRecord,
                recordRequestDto.recordTypeId,
                recordRequestDto.numRecordProvince,
                recordRequestDto.provinceId,
                recordRequestDto.districtId,
                recordRequestDto.townId,
                recordRequestDto.fullName,
                recordRequestDto.birthDay,
                recordRequestDto.gender,
                recordRequestDto.ethnicId,
                recordRequestDto.isCopy,
                recordRequestDto.numHealthInsurance,
                recordRequestDto.page,
                recordRequestDto.pageSize);
//        return jdbcTemplate.queryForList(listRecord);
    }

    /**
     * Get list profile with page
     * @param dto
     * @return
     */
    @Override
    public PageDto searchRecordPage (RecordRequestDto dto) {
        String sql = "{call ncc.sp_HoSo_TimKiem(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        List<SqlParameter> params = Arrays.asList(new SqlParameter(Types.INTEGER), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR), new SqlParameter(Types.BOOLEAN),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.INTEGER), new SqlParameter(Types.INTEGER),
                new SqlOutParameter("P_SODONG", Types.INTEGER),
                new SqlOutParameter("P_SOTRANG", Types.INTEGER));
        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setObject(1, dto.getRecordTypeId(), Types.INTEGER);
            callableStatement.setObject(2, dto.getNumRecordProvince(), Types.VARBINARY);
            callableStatement.setObject(3, dto.getProvinceId(), Types.INTEGER);
            callableStatement.setObject(4, dto.getDistrictId(), Types.INTEGER);
            callableStatement.setObject(5, dto.getTownId(), Types.INTEGER);
            callableStatement.setObject(6, dto.getFullName(), Types.VARCHAR);
            callableStatement.setObject(7, dto.getBirthDay(), Types.VARCHAR);
            callableStatement.setObject(8, dto.getGender(), Types.TINYINT);
            callableStatement.setObject(9, dto.getEthnicId(), Types.INTEGER);
            callableStatement.setObject(10, dto.getIsCopy(), Types.TINYINT);
            callableStatement.setObject(11, dto.getNumHealthInsurance(), Types.VARCHAR);
            callableStatement.setObject(12, dto.getPage(), Types.INTEGER);
            callableStatement.setObject(13, dto.getPageSize(), Types.INTEGER);
            return callableStatement;
        }, params);
        int totalPage = Integer.parseInt(result.get("P_SOTRANG").toString());
        int totalRows = Integer.parseInt(result.get("P_SODONG").toString());
        List<Map<String, Object>> temp = (List) result.get("#result-set-1");
        PageDto pageDto = PageDto.builder().totalRows(totalRows).totalPage(totalPage).pageSize(dto.pageSize)
                .pageNumber(dto.page).data(temp).build();
        return pageDto;
    }

    /**
     * Export report with template and condition search
     * @param recordRequestDto
     * @param response
     */
    @Override
    public void exportReport001(RecordRequestDto recordRequestDto, HttpServletResponse response) {
        RecordExcelExportDto dto = new RecordExcelExportDto();
        List<RecordExcelExportDto.RecordData> exportDto = this.searchReport(recordRequestDto);
        dto.setData(exportDto);
        dto.setDate(DatetimeUtil.getCurrentDatetimeInFormat(DatetimeUtil.DATE_TIME_FORMAT_3));
        dto.setName(userService.getCurrentUser().getHoTen());
        File file = this.templateExcelWriter.setSingleDataToTemplate(dto);
        excelService.response(response, file);
        return;
    }

    /**
     * Export data with condition search
     * @param dto
     * @return
     */
    public List<RecordExcelExportDto.RecordData> searchReport (RecordRequestDto dto) {
        try {
            String sql = "{call ncc.sp_HoSo_TimKiem_Report(?,?,?,?,?,?,?,?,?,?,?)}";
            List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, dto.getRecordTypeId(), dto.getNumRecordProvince(),
                    dto.getProvinceId(), dto.getDistrictId(), dto.getTownId(), dto.getFullName(),
                    dto.getBirthDay(), dto.getGender(), dto.getEthnicId(), dto.getIsCopy(),
                    dto.getNumHealthInsurance());
            ModelMapper modelMapper = new ModelMapper();
            return result.stream().map(e -> modelMapper.map(e, RecordExcelExportDto.RecordData.class))
                    .collect(Collectors.toList());
        } catch (Exception ex) {
            return new ArrayList<>();
        }
    }

    /**
     * Get all category for profile
     * @return
     */
    public RecordCategory getCategory() {
        RecordCategory result = new RecordCategory();
        result.setPROFILE_TYPE(this.recordTypeDao.getListRecordType());
        result.setPROVINCE(this.huyenDao.getListTinh());
        result.setNATION(danTocDao.getListDanToc());
        result.setPERIOD(this.getPeriod());
        result.setSUBJECT(this.getSubject());
        result.setTOTAL_MARTYRS(this.getTotalMartyrsInFamily());
        return result;
    }

    public List<Map<String, Object>> getPeriod() {
        try {
            String sql = "{call ncc.sp_Danhmuc_ThoiKy()}";
            List<Map<String, Object>> result = jdbcTemplate.queryForList(sql);
            return result;
        } catch (Exception ex) {
            return new ArrayList<>();
        }
    }

    public List<Map<String, Object>> getSubject() {
        try {
            String sql = "{call ncc.sp_DanhMuc_DoiTuong()}";
            List<Map<String, Object>> result = jdbcTemplate.queryForList(sql);
            return result;
        } catch (Exception ex) {
            return new ArrayList<>();
        }
    }

    public List<Map<String, Object>> getTotalMartyrsInFamily() {
        try {
            String sql = "{call ncc.sp_DanhMuc_TongSolietSy()}";
            List<Map<String, Object>> result = jdbcTemplate.queryForList(sql);
            return result;
        } catch (Exception ex) {
            return new ArrayList<>();
        }
    }

    public boolean createProfileMartyrs(RecordCreateRequestDto request) {
        try {
            if (request.getLoaiHoSoID() == 0) {
                return false;
            }

            String sql = "{call ncc.sp_create_HSLietSy(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
            jdbcTemplate.update(sql,
                    request.getLoaiHoSoID(), request.getBanSao(), request.getSoHoSoTinhQuanLy(),
                    request.getSoHoSoBoQuanLy(), request.getHuyenTiepNhanID(), request.getXaTiepNhanID(),
                    request.getCumToTiepNhan(), request.getHoSoChuyenDenTuID(), request.getThoiGianChuyenDen(),
                    request.getHoSoChuyenDiID(), request.getThoiGianChuyenDi(), request.getHoVaTen(),
                    request.getBiDanh(), request.getNgaySinh(), request.getGioiTinh(), request.getDanTocID(),
                    request.getQueQuan(), request.getNgayNhapNgu(), request.getCapBacTruocKhiHySinh(),
                    request.getLietSyLaAnhHung(), request.getDonViKhiHySinh(), request.getNgayHySinh(),
                    request.getThoiKyHySinhID(), request.getTruongHopHySinh(), request.getNoiHySinh(),
                    request.getThoCung(), request.getGiayBaoTuSo(), request.getDonViCapGiayBaoTu(),
                    request.getSoBangToQuocGhiCong(), request.getQuyetDinhCapBangSo(), request.getDoiTuongID(),
                    request.getTongSoLietSyTrongGiaDinhID(), request.getThuocHoGiaDinh(),
                    request.getCanBoQuanLy(), request.getViTriLuuTruHoSo(), request.getCacCheDoKhac(),
                    userService.getCurrentUser().getHoTen());
            return true;
        } catch (Exception ex) {
            return false;
        }
    }

    /**
     * Get information of profile
     * @return
     */
    public List<Map<String, Object>> getProfileInfo(int hoSoID) {
        try {
            String sql = "{call ncc.sp_HoSo_LietSy_Info(?)}";
            List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, hoSoID);
            return result;
        } catch (Exception ex) {
            return new ArrayList<>();
        }
    }

    public boolean update(RecordCreateRequestDto request) {
        try {
            String sql = "{call ncc.sp_update_HSLietSy(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
            jdbcTemplate.update(sql,
                    request.getLoaiHoSoID(), request.getBanSao(), request.getSoHoSoTinhQuanLy(),
                    request.getSoHoSoBoQuanLy(), request.getHuyenTiepNhanID(), request.getXaTiepNhanID(),
                    request.getCumToTiepNhan(), request.getHoSoChuyenDenTuID(), request.getThoiGianChuyenDen(),
                    request.getHoSoChuyenDiID(), request.getThoiGianChuyenDi(), request.getHoVaTen(),
                    request.getBiDanh(), request.getNgaySinh(), request.getGioiTinh(), request.getDanTocID(),
                    request.getQueQuan(), request.getNgayNhapNgu(), request.getCapBacTruocKhiHySinh(),
                    request.getLietSyLaAnhHung(), request.getDonViKhiHySinh(), request.getNgayHySinh(),
                    request.getThoiKyHySinhID(), request.getTruongHopHySinh(), request.getNoiHySinh(),
                    request.getThoCung(), request.getGiayBaoTuSo(), request.getDonViCapGiayBaoTu(),
                    request.getSoBangToQuocGhiCong(), request.getQuyetDinhCapBangSo(), request.getDoiTuongID(),
                    request.getTongSoLietSyTrongGiaDinhID(), request.getThuocHoGiaDinh(),
                    request.getCanBoQuanLy(), request.getViTriLuuTruHoSo(), request.getCacCheDoKhac(),
                    userService.getCurrentUser().getHoTen(), request.getHoSoID());
            return true;
        } catch (Exception ex) {
            return false;
        }
    }
}
