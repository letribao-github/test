package com.vnptit5.ptgp.ldtbxh.daoimpl;

import com.vnptit5.ptgp.ldtbxh.dao.ChinhSachDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach.SuaChinhSachDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach.SuaDoiTuongDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach.ThemChinhSachDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach.ThemDoiTuongDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.CustomException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.stereotype.Repository;

import java.sql.CallableStatement;
import java.sql.Types;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

@Repository
public class ChinhSachDaoImpl implements ChinhSachDao {
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Override
    public List<Map<String, Object>> danhMucChinhSach(Integer doiTuong) {
        String dmcs = "{call ho_gia_dinh.sp_LoaiChinhSach_DanhSach(?)}";
        return jdbcTemplate.queryForList(dmcs, doiTuong);
    }

    @Override
    public int themMoiChinhSach(ThemChinhSachDto csdto) {
        String tmChinhSach = "{call ho_gia_dinh.sp_LoaiChinhSach_ThemMoi(?,?,?,?,?,?)}";
        List<SqlParameter> params = Arrays.asList(new SqlParameter(Types.VARCHAR), new SqlParameter(Types.VARCHAR),
                new SqlParameter(Types.BOOLEAN), new SqlParameter(Types.INTEGER), new SqlParameter(Types.BOOLEAN),
                new SqlOutParameter("P_OUT", Types.INTEGER));
        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(tmChinhSach);
            callableStatement.setString(1, csdto.getTenChinhSach());
            callableStatement.setString(2, csdto.getMaChinhSach());
            callableStatement.setBoolean(3, csdto.getTrangThai());
            callableStatement.setInt(4, csdto.getMucHuong());
            callableStatement.setBoolean(5, csdto.getIsHGD());
            return callableStatement;
        }, params);
        return Integer.parseInt(result.get("P_OUT").toString());
    }

    @Override
    public Map<String, Object> thongTinChinhSachTheoDoiTuong(Integer idLoaiCS, Integer idDoiTuong) {
        String sql = "{call ho_gia_dinh.sp_SoTienHuong_TheoDoiTuong(?,?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, idLoaiCS, idDoiTuong);
        if (result.size() == 0) {
            throw new CustomException("Chính sách không tồn tại");
        }
        return result.get(0);
    }

    @Override
    public List<Map<String, Object>> getLoaiChinhSachs(Integer doiTuong) {
        String sql = "call ho_gia_dinh.sp_LoaiChinhSach_DanhSach(?)";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, doiTuong);
        return result;
    }

    @Override
    public List<Map<String, Object>> getAllLoaiDoiTuong() {
        String sql = "call ho_gia_dinh.sp_LoaiDoiTuong_GetAll()";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql);
        return result;
    }

    @Override
    public List<Map<String, Object>> findLoaiChinhSach(String name) {
        String sql = "call ho_gia_dinh.sp_LoaiChinhSach_findByName(?)";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, name);
        return result;
    }

    @Override
    public List<Map<String, Object>> searchDoiTuong(Integer idLoaiCs, String tenDoiTuong) {
        String sql = "call ho_gia_dinh.sp_DoiTuong_searchDoiTuong(?,?)";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, idLoaiCs, tenDoiTuong);
        return result;
    }

    @Override
    public void updatePolicy(SuaChinhSachDto dto) {
        String sql = "{call ho_gia_dinh.sp_LoaiChinhSach_Update(?,?,?,?,?,?)}";
        jdbcTemplate.update(sql, dto.getIdChinhSach(), dto.getTenChinhSach(), dto.getMaChinhSach(), dto.getMucHuong(),
                dto.getTrangThai(), dto.getIsHGD());
    }

    @Override
    public int themDoiTuong(ThemDoiTuongDto dto) {
        String sql = "{call ho_gia_dinh.sp_DoiTuong_ThemDoiTuong(?,?,?,?,?)}";
        String sql1 = "{call ho_gia_dinh.sp_DoiTuong_CapNhatTrangThaiDoiTuongBangChinhSach(?)}";
        jdbcTemplate.update(sql1, dto.getIdDoiTuong());
        List<SqlParameter> params = Arrays.asList(new SqlParameter(Types.VARCHAR), new SqlParameter(Types.INTEGER),
                new SqlParameter(Types.DOUBLE), new SqlParameter(Types.INTEGER),
                new SqlOutParameter("P_OUT", Types.INTEGER));
        Map result = jdbcTemplate.call(con -> {
            CallableStatement callableStatement = con.prepareCall(sql);
            callableStatement.setString(1, dto.getTenDoiTuong());
            callableStatement.setDouble(2, dto.getMucHuong());
            callableStatement.setInt(3, dto.getIdLoaiCs());
            callableStatement.setInt(4, dto.getIdDoiTuong());
            return callableStatement;
        }, params);
        return Integer.parseInt(result.get("P_OUT").toString());
    }

    @Override
    public void updateDoiTuong(SuaDoiTuongDto dto) {
        String sql = "{call ho_gia_dinh.sp_DoiTuong_UpdateDoiTuong(?,?,?,?,?)}";
        jdbcTemplate.update(sql, dto.getIdDoiTuong(), dto.getTenDoiTuong(), dto.getHeSo(), dto.getIsHGD(),
                dto.getTrangThai());
    }

    @Override
    public Map<String, Object> getRelatedInfoDoiTuong(Integer idDoiTuong) {
        String sql = "{call ho_gia_dinh.sp_DoiTuong_GetInfo(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, idDoiTuong);
        if (result.size() == 0) {
            throw new CustomException("Đối tượng không tồn tại");
        }
        return result.get(0);
    }

    @Override
    public Map<String, Object> findByIdChinhSach(Integer idChinhSach) {
        String sql = "{call ho_gia_dinh.sp_LoaiChinhSach_findById(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, idChinhSach);
        if (result.size() == 0) {
            throw new CustomException("Chính sách không tồn tại");
        }
        return result.get(0);
    }

    @Override
    public Map<String, Object> findByIdDoiTuong(Integer idDoituong) {
        String sql = "{call ho_gia_dinh.sp_DoiTuong_FindById(?)}";
        List<Map<String, Object>> result = jdbcTemplate.queryForList(sql, idDoituong);
        if (result.size() == 0) {
            throw new CustomException("Đối tượng không tồn tại");
        }
        return result.get(0);
    }

}
