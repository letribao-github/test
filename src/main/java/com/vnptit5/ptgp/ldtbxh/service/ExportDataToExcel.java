package com.vnptit5.ptgp.ldtbxh.service;

import com.google.common.base.Strings;
import com.vnptit5.ptgp.ldtbxh.dao.TreEmDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.ExcelType;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author trinhctn
 */
@Slf4j
@Component
public class  ExportDataToExcel {

    @Autowired
    private TreEmDao treEmDao;

    public ResponseEntity<ByteArrayResource> responseExcel(ByteArrayResource byteArrayResource, String fileName, ExcelType type) {
        final HttpHeaders httpHeaders = new HttpHeaders();
        fileName = fileName + "_" + LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyyMMddHHmmss"))
                + "." + type.toString().toLowerCase();
        ContentDisposition contentDisposition = ContentDisposition.builder("attachment")
                .filename(fileName)
                .build();
        httpHeaders.setContentDisposition(contentDisposition);
        httpHeaders.setContentType(type.getMediaType());
        return ResponseEntity.ok()
                .headers(httpHeaders)
                .body(byteArrayResource);
    }


    public ByteArrayResource exportDanhSachTreEm(List<Map<String, Object>> DSTreEmBinhThuong, ExcelType type) throws IOException, NullPointerException {
        Workbook workbook = type == ExcelType.XLSX ?
                new XSSFWorkbook() : new HSSFWorkbook();
        CellStyle titleCellStyle = workbook.createCellStyle();
        titleCellStyle.setAlignment(HorizontalAlignment.CENTER);
        Font titleFont = workbook.createFont();
        titleFont.setBold(true);
        titleFont.setFontHeightInPoints((short) 20);
        Font tongTEFont = workbook.createFont();
        titleFont.setBold(true);
        titleFont.setFontHeightInPoints((short) 10);
        DSTreEmBinhThuong.forEach(e ->{
            int rowTitle = 0;
            int rowTong = 2;
            int rowName = 1;
            int rowNum = 3;

            Cell cell; Row row;
            Sheet sheet = workbook.createSheet(
                    e.get("SHEET_NAME").toString().equals("TEN_THON_XOM") ?
                            e.get("TEN_THON_XOM").toString() :  e.get("TEN_XA_PHUONG").toString());

            sheet.addMergedRegion(CellRangeAddress.valueOf("A1:G1"));
            sheet.addMergedRegion(CellRangeAddress.valueOf("A2:H2"));
            sheet.addMergedRegion(CellRangeAddress.valueOf("A3:C3"));

            List<Map<String, Object>> list = (List<Map<String, Object>>) e.get("DATA");

            String tenKhuPho = e.get("SHEET_NAME").toString().equals("TEN_THON_XOM") ? e.get("TEN_THON_XOM").toString() : "";
            String tenXaPhuong = e.get("TEN_XA_PHUONG").toString();
            String tenQuanHuyen = e.get("TEN_QUAN_HUYEN").toString();
            String tenTinhThanh = e.get("TEN_TINH_THANH").toString();

            String donVi = e.get("SHEET_NAME").toString().equals("TEN_THON_XOM") ?
                    tenKhuPho.concat("-").concat(tenXaPhuong).concat(" - ").concat(tenQuanHuyen).concat(" - ").concat(tenTinhThanh)
                    : tenXaPhuong.concat(" - ").concat(tenQuanHuyen).concat(" - ").concat(tenTinhThanh);

            row = sheet.createRow(rowTitle);
            cell = row.createCell(0, CellType.STRING);
            cell.setCellValue(donVi);
            titleCellStyle.setFont(tongTEFont);

            cell.setCellStyle(titleCellStyle);
            row = sheet.createRow(rowTong);
            cell = row.createCell(0, CellType.STRING);
            cell.setCellValue("Tổng số trẻ em: " +list.size());
            titleCellStyle.setFont(titleFont);
            cell.setCellStyle(titleCellStyle);

            row = sheet.createRow(rowName);
            cell = row.createCell(0, CellType.STRING);
            cell.setCellValue("DANH SÁCH TRẺ EM");
            titleCellStyle.setFont(titleFont);
            cell.setCellStyle(titleCellStyle);
            List<String> listKey = Arrays.asList("MÃ TRẺ EM", "TÊN TRẺ EM", "NGÀY SINH", "DÂN TỘC",
                    "GIỚI TÍNH", "HỌ TÊN CHA", "HỌ TÊN MẸ", "HỌ TÊN NGƯỜI NUÔI DƯỠNNG",
                    "HỌ TÊN NGƯỜI GIÁM HỘ", "ĐỊA CHỈ");
            row = sheet.createRow(rowNum);
            int index = 0;
            for (String key:listKey){
                cell = row.createCell(index, CellType.STRING);
                cell.setCellValue(key);
                cell.setCellStyle(titleCellStyle);
                index++;
            }
            try {
                for (Map<String, Object> emp:list){
                    rowNum++;
                    row = sheet.createRow(rowNum);

                    String fromCell = "K" + rowNum;
                    String toCell = "U" + rowNum;
                    sheet.addMergedRegion(CellRangeAddress.valueOf(fromCell.concat(":").concat(toCell)));

                    cell = row.createCell(0, CellType.STRING);
                    cell.setCellValue(emp.get("MA_TRE_EM").toString());

                    cell = row.createCell(1, CellType.STRING);
                    cell.setCellValue(emp.get("TEN_TRE_EM").toString());

                    cell = row.createCell(2, CellType.STRING);
                    cell.setCellValue(emp.get("NGAY_SINH").toString());

                    cell = row.createCell(3, CellType.STRING);
                    cell.setCellValue(emp.get("TEN_DAN_TOC").toString());

                    cell = row.createCell(4, CellType.STRING);
                    cell.setCellValue(emp.get("GIOI_TINH").toString().equals("0") ? "Nữ" : "Nam");

                    cell = row.createCell(5, CellType.STRING);
                    cell.setCellValue(emp.get("HO_TEN_CHA").toString());

                    cell = row.createCell(6, CellType.STRING);
                    cell.setCellValue(emp.get("HO_TEN_ME").toString());

                    cell = row.createCell(7, CellType.STRING);
                    cell.setCellValue(emp.get("TEN_NGUOI_NUOI_DUONG").toString());

                    cell = row.createCell(8, CellType.STRING);
                    cell.setCellValue(emp.get("TEN_NGUOI_GIAM_HO").toString());

                    cell = row.createCell(9, CellType.STRING);
                    cell.setCellValue(emp.get("DIA_CHI").toString());
                }
            }catch (NullPointerException ex){
                ex.printStackTrace();
            }
        });
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        workbook.write(out);
        return new ByteArrayResource(out.toByteArray());
    };

    public ByteArrayResource exportDSTreEmTheoChiTieu(List<Map<String, Object>> DSTreEm, ExcelType type, String tenLoaiChiTieu,
                                                      List<Map<String, Object>> listChiTieuCap1) throws IOException, NullPointerException{
        Workbook workbook = type == ExcelType.XLSX ?
                new XSSFWorkbook() : new HSSFWorkbook();
        CellStyle titleCellStyle = workbook.createCellStyle();
        titleCellStyle.setAlignment(HorizontalAlignment.CENTER);
        Font titleFont = workbook.createFont();
        titleFont.setBold(true);
        titleFont.setFontHeightInPoints((short) 20);
        Font tongTEFont = workbook.createFont();
        titleFont.setBold(true);
        titleFont.setFontHeightInPoints((short) 10);
        for (Map<String, Object> e : DSTreEm) {
            int rowTitle = 0;
            int rowTong = 2;
            int rowName = 1;
            int rowNum = 3;
            int rowHC = 3;
            Cell cell;
            Row row;
            Sheet sheet = workbook.createSheet(
                    e.get("SHEET_NAME").toString().equals("TEN_THON_XOM") ?
                            e.get("TEN_THON_XOM").toString() : e.get("TEN_XA_PHUONG").toString());
            sheet.addMergedRegion(CellRangeAddress.valueOf("A1:G1"));
            sheet.addMergedRegion(CellRangeAddress.valueOf("A2:H2"));
            sheet.addMergedRegion(CellRangeAddress.valueOf("A3:C3"));
            List<Map<String, Object>> list = (List<Map<String, Object>>) e.get("DATA");
            String tenKhuPho = e.get("SHEET_NAME").toString().equals("TEN_THON_XOM") ? e.get("TEN_THON_XOM").toString() : "";
            String tenXaPhuong = e.get("TEN_XA_PHUONG").toString();
            String tenQuanHuyen = e.get("TEN_QUAN_HUYEN").toString();
            String tenTinhThanh = e.get("TEN_TINH_THANH").toString();
            String donVi = e.get("SHEET_NAME").toString().equals("TEN_THON_XOM") ?
                    tenKhuPho.concat(" - ").concat(tenXaPhuong)
                            .concat(" - ").concat(tenQuanHuyen).concat(" - ").concat(tenTinhThanh)
                    : tenXaPhuong.concat(" - ").concat(tenQuanHuyen).concat(" - ").concat(tenTinhThanh);
            row = sheet.createRow(rowTitle);
            cell = row.createCell(0, CellType.STRING);
            cell.setCellValue(donVi);
            titleCellStyle.setFont(tongTEFont);
            cell.setCellStyle(titleCellStyle);
            row = sheet.createRow(rowTong);
            cell = row.createCell(0, CellType.STRING);
            cell.setCellValue("Tổng số trẻ em: " + list.size());
            titleCellStyle.setFont(titleFont);
            cell.setCellStyle(titleCellStyle);
            row = sheet.createRow(rowName);
            cell = row.createCell(0, CellType.STRING);
            cell.setCellValue("DANH SÁCH TRẺ EM");
            titleCellStyle.setFont(titleFont);
            cell.setCellStyle(titleCellStyle);
            List<String> listKey = Arrays.asList("MÃ TRẺ EM", "TÊN TRẺ EM", "NGÀY SINH", "DÂN TỘC",
                    "GIỚI TÍNH", "HỌ TÊN CHA", "HỌ TÊN MẸ", "HỌ TÊN NGƯỜI NUÔI DƯỠNNG",
                    "HỌ TÊN NGƯỜI GIÁM HỘ", "ĐỊA CHỈ");
            row = sheet.createRow(rowNum);
            int index = 0;
            for (String key : listKey) {
                cell = row.createCell(index, CellType.STRING);
                sheet.addMergedRegion(new CellRangeAddress(3, 4, index, index));
                cell.setCellValue(key);
                cell.setCellStyle(titleCellStyle);
                index++;
            }
            cell = row.createCell(10, CellType.STRING);
            cell.setCellValue(tenLoaiChiTieu);
            cell.setCellStyle(titleCellStyle);
            int rowCT = 4;
            int indexCT = 10;
            row = sheet.createRow(rowCT);
            for (Map<String, Object> keyCT : listChiTieuCap1) {
                cell = row.createCell(indexCT, CellType.STRING);
                cell.setCellValue(keyCT.get("TEN_CHI_TIET_HOAN_CANH").toString());
                cell.setCellStyle(titleCellStyle);
                indexCT++;
            }
            int rowData = 5;
            try {
                for (Map<String, Object> emp : list) {
                    row = sheet.createRow(rowData);
                    cell = row.createCell(0, CellType.STRING);
                    cell.setCellValue(emp.get("MA_TRE_EM").toString());

                    cell = row.createCell(1, CellType.STRING);
                    cell.setCellValue(emp.get("TEN_TRE_EM").toString());

                    cell = row.createCell(2, CellType.STRING);
                    cell.setCellValue(Strings.nullToEmpty(emp.get("NGAY_SINH").toString()));

                    cell = row.createCell(3, CellType.STRING);
                    cell.setCellValue(Strings.nullToEmpty(emp.get("TEN_DAN_TOC").toString()));

                    cell = row.createCell(4, CellType.STRING);
                    cell.setCellValue(Strings.nullToEmpty(emp.get("GIOI_TINH").toString())
                            .equals("0")
                            ? "Nữ" : "Nam");

                    cell = row.createCell(5, CellType.STRING);
                    cell.setCellValue(Strings.nullToEmpty(emp.get("HO_TEN_CHA").toString()));

                    cell = row.createCell(6, CellType.STRING);
                    cell.setCellValue(Strings.nullToEmpty(emp.get("HO_TEN_ME").toString()));

                    cell = row.createCell(7, CellType.STRING);
                    cell.setCellValue(Strings.nullToEmpty(emp.get("TEN_NGUOI_NUOI_DUONG").toString()));

                    cell = row.createCell(8, CellType.STRING);
                    cell.setCellValue(Strings.nullToEmpty(emp.get("TEN_NGUOI_GIAM_HO").toString()));

                    cell = row.createCell(9, CellType.STRING);
                    cell.setCellValue(Strings.nullToEmpty(emp.get("DIA_CHI").toString()));

                    int col = 10;
                    List<Map<String, Object>> listDSHC = (List) emp.get("DSHC");
                    for (Map<String, Object> ct:listDSHC){
                        cell = row.createCell(col, CellType.STRING);
                        cell.setCellValue(ct.get("VALUE").toString().equals("true") ? 1 : 0);
                        col++;
                    }
                    rowData++;

                }
            } catch (NullPointerException ex) {
                ex.printStackTrace();
            }
        }
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        workbook.write(out);
        return new ByteArrayResource(out.toByteArray());
    }
}
