package com.vnptit5.ptgp.ldtbxh.service;

import com.vnptit5.ptgp.ldtbxh.client.VndisClient;
import com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh.TraCuuHoGiaDinhDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.treem.TraCuuTreEmDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh.TraCuuTheoMaDinhDanh;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author trinhctn
 */
@Service
public class VndisService {

    @Autowired
    private VndisClient vndisClient;

    public List<Map<String, Object>> getTreEmPhatSinh(TraCuuTreEmDto traCuuTreEmDto) {
        Map<String, Object> getTokenRequestBody = new HashMap<>();
        getTokenRequestBody.put("user_name", "ADMIN");
        getTokenRequestBody.put("pass_word", "4da33780eee4609ebcd0be6ab3d82e8d");

        Map<String, Object> responseToken = vndisClient.takeToken(getTokenRequestBody).getBody();
        String access_token = ((Map) responseToken.get("apiKey"))
                .get("access_token").toString();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(access_token);

        return vndisClient.getTreEmPhatSinh(httpHeaders, traCuuTreEmDto).getBody();
    }

    public List<Map<String, Object>> traCuuHoGiaDinh(TraCuuHoGiaDinhDto hoGiaDinhDto) {
        Map<String, Object> getTokenRequestBody = new HashMap<>();
        getTokenRequestBody.put("user_name", "ADMIN");
        getTokenRequestBody.put("pass_word", "4da33780eee4609ebcd0be6ab3d82e8d");

        Map<String, Object> responseToken = vndisClient.takeToken(getTokenRequestBody).getBody();

        String access_token = ((Map) responseToken.get("apiKey"))
                .get("access_token").toString();

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(access_token);

        return vndisClient.traCuuHoGiaDinh(httpHeaders, hoGiaDinhDto).getBody();
    }

    public String getAccessToken(String username, String password){
        Map<String, Object> getTokenRequestBody = new HashMap<>();
        getTokenRequestBody.put("user_name", username);
        getTokenRequestBody.put("pass_word", password);
        Map<String, Object> responseToken = vndisClient.takeToken(getTokenRequestBody).getBody();
        return ((Map) responseToken.get("apiKey"))
                .get("access_token").toString();
    }

    public Map<String, Object> traCuuNhanh(String maDinhDanh) {
        String access_token = getAccessToken("ADMIN", "4da33780eee4609ebcd0be6ab3d82e8d");

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(access_token);

        Map<String, Object> body = new HashMap<>();
        body.put("maDinhDanh", maDinhDanh.toString());
        return vndisClient.traCuuNhanh(httpHeaders, body).getBody();
    }

    public List<Map<String, Object>> traCuuHoTheoMaDinhDanh(TraCuuTheoMaDinhDanh dto) {
        String access_token = getAccessToken("ADMIN", "4da33780eee4609ebcd0be6ab3d82e8d");

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setBearerAuth(access_token);

        return vndisClient.traCuuHoTheoMaDinhDanh(httpHeaders, dto).getBody();
    }

}
