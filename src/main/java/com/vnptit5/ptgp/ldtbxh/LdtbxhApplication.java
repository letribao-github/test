package com.vnptit5.ptgp.ldtbxh;

import com.vnptit5.ptgp.ldtbxh.property.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
@EnableConfigurationProperties(FileStorageProperties.class)
public class LdtbxhApplication {

	public static void main(String[] args) {
		SpringApplication.run(LdtbxhApplication.class, args);
	}

}
