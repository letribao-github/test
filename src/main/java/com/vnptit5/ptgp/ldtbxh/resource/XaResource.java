package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.dao.XaDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import com.vnptit5.ptgp.ldtbxh.common.UrlConstants;

@RestController
@CrossOrigin
@RequestMapping(UrlConstants.XA)
public class XaResource {
    @Autowired
    private XaDao xaDao;

    @GetMapping(UrlConstants.GET_LIST_XA)
    public ResponseEntity listXa(@RequestParam Integer huyenID) {
        List<Map<String, Object>> listXa = xaDao.getListXa(huyenID);
        return ResponseEntity.ok()
                .body(listXa);
    }
}

