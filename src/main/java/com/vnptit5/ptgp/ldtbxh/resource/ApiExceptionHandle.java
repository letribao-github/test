package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.dto.response.CustomException;
import com.vnptit5.ptgp.ldtbxh.dto.response.MessageDto;
import io.jsonwebtoken.ExpiredJwtException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author trinhctn
 */
@RestControllerAdvice
public class ApiExceptionHandle {
    /*
      Tất cả các Exception không được khai báo sẽ được xử lý tại đây
     */
    @ExceptionHandler(Exception.class)
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public MessageDto handleException(Exception ex, WebRequest request) {
        ex.printStackTrace();
        //Quá trình kiểm soát lỗi
        return new MessageDto(1000, ex.getLocalizedMessage());
    }

    @ExceptionHandler(CustomException.class)
    @ResponseStatus(value = HttpStatus.CONFLICT)
    public MessageDto ahandleException(Exception ex, WebRequest request) {
        //Quá trình kiểm soát lỗi
        return new MessageDto(HttpStatus.CONFLICT.value(), ex.getLocalizedMessage());
    }

    /*
        IndexOutOfBoundsException sẽ được xử lý riêng tại đây
     */
    // Xử lý check hợp lệ dữ liệu
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    public MessageDto checkValidException(MethodArgumentNotValidException ex) {
        List<ObjectError> errors = ex.getBindingResult().getAllErrors();
        String message = errors.stream().map(DefaultMessageSourceResolvable::getDefaultMessage).collect(Collectors.joining(". "));
        return new MessageDto(HttpStatus.BAD_REQUEST.value(), StringUtils.capitalize(message));
    }

    //Xử lý lỗi Login
    @ExceptionHandler({AuthenticationException.class, ExpiredJwtException.class})
    @ResponseStatus(value = HttpStatus.UNAUTHORIZED)
    public MessageDto LoginException(Exception ex, WebRequest request) {
        return new MessageDto(HttpStatus.UNAUTHORIZED.value(), "Sai tên đăng nhập hoặc mật khẩu");
    }

}
