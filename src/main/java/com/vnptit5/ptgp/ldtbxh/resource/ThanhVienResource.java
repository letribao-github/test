package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.dao.ThanhVienDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.thanhvien.*;
import com.vnptit5.ptgp.ldtbxh.dto.response.CustomException;
import com.vnptit5.ptgp.ldtbxh.dto.response.DangKyDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.MessageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;

/**
 * @author trinhctn
 */

@RestController
@RequestMapping("/sldtbxh/thanh-vien")
@CrossOrigin
public class ThanhVienResource {

    @Autowired
    private ThanhVienDao thanhVienDao;

    @PostMapping("/them-moi")
    public ResponseEntity themMoiThanhVien(@RequestBody ThemMoiThanhVienDto dto) {
        int idNhanKhau = thanhVienDao.themMoiThanhVien(dto);
        return ResponseEntity.ok().body(DangKyDto.builder().id(idNhanKhau).thongBao("Thêm mới thành công")
                .statusCode(HttpStatus.OK.value()).build());
    }

    @PutMapping("/cap-nhat")
    public ResponseEntity capNhatThanhVien(@RequestBody CapNhatThanhVienDto dto) {
        thanhVienDao.capNhatThanhVien(dto);
        return ResponseEntity.ok()
                .body(MessageDto.builder().message("Cập nhật thành viên thành công").build());
    }

    @DeleteMapping("/xoa")
    public ResponseEntity xoaThanhVien(@RequestParam Integer idNhanKhau, @RequestParam Integer maHoGD) {
        int isDelete = thanhVienDao.xoaThanhVien(idNhanKhau, maHoGD);
        if (isDelete == 0){
            throw new CustomException("Xóa không thành công");
        }
        return ResponseEntity.ok().body(
                MessageDto.builder().message("Xóa thành viên thành công").statusCode(HttpStatus.OK.value()).build());
    }

    @PutMapping("/khoi-phuc")
    public ResponseEntity khoiPhucThanhVien(@RequestParam Integer idNhanKhau, @RequestParam Integer maHoGD) {
        int isRescover = thanhVienDao.khoiPhucThanhVien(idNhanKhau, maHoGD);
        if (isRescover == 0){
            throw new CustomException("Khôi phục không thành công");
        }
        return ResponseEntity.ok().body(
                MessageDto.builder().message("Khôi phục thành viên thành công").statusCode(HttpStatus.OK.value()).build());
    }

    @GetMapping("/danh-sach")
    public ResponseEntity danhSachThanhVien(@RequestParam Integer maHoGD) {
        return ResponseEntity.ok().body(thanhVienDao.danhSachThanhVien(maHoGD));
    }

    @PostMapping("/tim-kiem")
    public ResponseEntity traCuuThongTinThanhVienHGD(@RequestBody TimKiemThanhVienDto nkdt){
        return ResponseEntity.ok().body(thanhVienDao.TimKiemThanhVienHoGiaDinh(nkdt));
    }

    @PostMapping("tim-kiem-phan-trang")
    public ResponseEntity timKiemThanhVienPhanTrang(@RequestBody TimKiemThanhVienPhanTrangDto dto){
        return ResponseEntity.ok().body(thanhVienDao.timKiemThanhVienPhanTrang(dto));
    }

    @GetMapping("/chi-tiet-chinh-sach-dang-huong")
    public ResponseEntity chiTietChinhSachThanhVien(@RequestParam Integer maThanhVien,
                                                    @RequestParam(required = false) Integer idHuongCS) {
        return ResponseEntity.ok().body(thanhVienDao.chiTietHuongCS(maThanhVien,idHuongCS));
    }

    @GetMapping("/thong-tin-chi-tiet")
    public ResponseEntity chinhSachTheoThanhVien(@RequestParam Integer idNhanKhau,
                                                 @RequestParam(required = false) Integer idHuongCS) {
        return ResponseEntity.ok().body(thanhVienDao.thongTinThanhVienChiTiet(idNhanKhau, idHuongCS));
    }

    @GetMapping("/thong-tin-chung")
    public ResponseEntity thongTinChungThanhVien(@RequestParam Integer idNhanKhau ) {
        return ResponseEntity.ok().body(thanhVienDao.thongTinChungThanhVien(idNhanKhau));
    }

    @GetMapping("/chinh-sach-dang-huong")
    public ResponseEntity chinhSachThanhVien(@RequestParam Integer idNhanKhau,
                                             @RequestParam(required = false) Integer idHuongCS) {
        return ResponseEntity.ok().body(thanhVienDao.chinhSachThanhVienDangHuong(idNhanKhau, idHuongCS));
    }

    @PostMapping("/chuyen-ho")
    public ResponseEntity ChuyenThanhVienHo(@RequestBody ThongTinChuyenTVDto ctv){
        thanhVienDao.ChuyenThanhVien(ctv);
        return ResponseEntity.ok().body(new MessageDto(HttpStatus.OK.value(), "Chuyển thành viên hộ thành công"));
    }

    @GetMapping("/xuat-danh-sach")
    public void ExportReportMemberList(TimKiemThanhVienDto tktvdto, HttpServletResponse response){
        thanhVienDao.ExportMemberList(tktvdto, response);
    }
}
