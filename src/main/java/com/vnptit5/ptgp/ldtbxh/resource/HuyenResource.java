package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.dao.HuyenDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import com.vnptit5.ptgp.ldtbxh.common.UrlConstants;

@RestController
@CrossOrigin
@RequestMapping(UrlConstants.HUYEN)
public class HuyenResource {
    @Autowired
    private HuyenDao huyenDao;

    @GetMapping(UrlConstants.GET_LIST_HUYEN)
    public ResponseEntity listHuyen(@RequestParam Integer tinhID) {
        List<Map<String, Object>> listHuyen = huyenDao.getListHuyen(tinhID);
        return ResponseEntity.ok()
                .body(listHuyen);
    }

    @GetMapping(UrlConstants.GET_LIST_TINH)
    public ResponseEntity listTinh() {
        List<Map<String, Object>> listTinh = huyenDao.getListTinh();
        return ResponseEntity.ok().body(listTinh);
    }
}

