package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.dao.DiaPhuongDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.diaphuong.CapNhatThonDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.diaphuong.ThemMoiThonDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.diaphuong.TimKiemDiaPhuongDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.*;
import com.vnptit5.ptgp.ldtbxh.dto.response.diaphuong.HuyenDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.diaphuong.XaDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author trinhctn
 */

@RestController
@RequestMapping("/sldtbxh/dia-phuong")
@CrossOrigin
public class DiaPhuongResource {

    @Autowired
    private DiaPhuongDao diaPhuongDao;

    @GetMapping("/danh-sach-tinh")
    public ResponseEntity danhSachTinh(){
        List<Map<String, Object>> list = diaPhuongDao.danhSachTinh();
        return ResponseEntity.ok()
                .body(list);
    }

    @GetMapping("/danh-sach-huyen")
    public ResponseEntity danhSachQuanHuyen(@RequestParam String maTinh){
        List<Map<String, Object>> list = diaPhuongDao.danhSachHuyen(maTinh);

        ModelMapper modelMapper = new ModelMapper();
        List<HuyenDto> result = list.stream()
                .map(e -> modelMapper.map(e, HuyenDto.class))
                .collect(Collectors.toList());

        return ResponseEntity.ok()
                .body(result);
    }

    @GetMapping("/danh-sach-xa")
    public ResponseEntity danhSachXaPhuong(@RequestParam String maTinh, String maHuyen){
        List<Map<String, Object>> list = diaPhuongDao.danhSachXa(maTinh, maHuyen);

        ModelMapper modelMapper = new ModelMapper();
        List<XaDto> result = list.stream()
                .map(e -> modelMapper.map(e, XaDto.class))
                .collect(Collectors.toList());

        return ResponseEntity.ok()
                .body(result);
    }

    @GetMapping("/danh-sach-thon")
    public ResponseEntity danhSachThonXom(@RequestParam String maTinh, String maHuyen, String maXa){
        List<Map<String, Object>> list = diaPhuongDao.danhSachThon(maTinh, maHuyen, maXa);

        ModelMapper modelMapper = new ModelMapper();
        List<ThonDto> result = list.stream()
                .map(e -> modelMapper.map(e, ThonDto.class))
                .collect(Collectors.toList());

        return ResponseEntity.ok()
                .body(result);
    }

    @GetMapping("/thon")
    public ResponseEntity thongTinThon(@RequestParam String maThon){
        return ResponseEntity.ok()
                .body(diaPhuongDao.thongTinThong(maThon));
    }


    @PostMapping("/thon/them-moi")
    public ResponseEntity themMoiThonXom(@RequestBody ThemMoiThonDto dto){
        int id = diaPhuongDao.themMoiThon(dto);
        if (id == 0){
            throw new CustomException("Thôn đã tồn tại");
        }
        return ResponseEntity.ok()
                .body(DangKyDto.builder().id(id).thongBao("Thêm mới thành công")
                        .statusCode(HttpStatus.OK.value()).build());
    }

    @PutMapping("/thon/sua")
    public ResponseEntity suaThon(@RequestBody CapNhatThonDto dto){
        diaPhuongDao.suaThon(dto);
        return ResponseEntity.ok()
                .body(new MessageDto(HttpStatus.OK.value(),
                        "Cập nhật thông tin thôn thành công"));
    }

    @DeleteMapping("/thon/xoa")
    public ResponseEntity xoaThon(@RequestParam String maThon){
        diaPhuongDao.xoaThon(maThon);
        return ResponseEntity.ok()
                .body(new MessageDto(HttpStatus.OK.value(), "Xoá thành công"));
    }

    @GetMapping("/thon/khoi-phuc")
    public ResponseEntity khoiPhucNguoiDung(@RequestParam String maThon){
        diaPhuongDao.khoiPhucThon(maThon);
        return ResponseEntity.ok()
                .body(new MessageDto(HttpStatus.OK.value(), "Khôi phục thành công"));
    }

    @PostMapping("/tim-kiem")
    public ResponseEntity timKiemDiaPhuong(@RequestBody TimKiemDiaPhuongDto dto){
        return ResponseEntity.ok()
                .body(diaPhuongDao.timKiemDiaPhuong(dto));
    }
}
