package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.dao.DanhMucDao;
import com.vnptit5.ptgp.ldtbxh.dao.TreEmDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.*;
import com.vnptit5.ptgp.ldtbxh.dto.request.treem.*;
import com.vnptit5.ptgp.ldtbxh.dto.response.DangKyDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.MessageDto;
import com.vnptit5.ptgp.ldtbxh.service.ExportDataToExcel;
import com.vnptit5.ptgp.ldtbxh.service.VndisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author trinhctn
 */

@RestController
@RequestMapping("/sldtbxh/tre-em")
@CrossOrigin
public class TreEmResource {

    @Autowired
    private TreEmDao treEmDao;

    @Autowired
    private DanhMucDao danhMucDao;

    @Autowired
    private VndisService vndisService;

    @Autowired
    private ExportDataToExcel exportDataToExcel;

    @PostMapping("/phat-sinh")
    public ResponseEntity<List<Map<String, Object>>> phatSinhTreEm(@RequestBody @Valid TraCuuTreEmDto traCuuTreEmDto) {
        List<Map<String, Object>> result = vndisService.getTreEmPhatSinh(traCuuTreEmDto);
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/them-moi")
    public ResponseEntity themMoiTreEm(@RequestBody CapNhatTreEmDto dto) {
        int id = treEmDao.themMoiTreEm(dto);
        return ResponseEntity.ok()
                .body(DangKyDto.builder().id(id).thongBao("Thêm mới thành công")
                .statusCode(HttpStatus.OK.value()).build());
    }

    @PutMapping("/sua")
    public ResponseEntity suaTreEm(@RequestBody CapNhatTreEmDto dto) {
        treEmDao.suaTreEm(dto);
        return ResponseEntity.ok()
                .body(new MessageDto(HttpStatus.OK.value(), "Cập nhật trẻ em thành công"));
    }

    @DeleteMapping("/xoa")
    public ResponseEntity xoaTreEm(@RequestParam int maTreEm) {
        treEmDao.xoaTreEm(maTreEm);
        return ResponseEntity.ok().body(new MessageDto(HttpStatus.OK.value(), "Xoá thành công"));
    }

    @PostMapping("/khoi-phuc")
    public ResponseEntity khoiPhucTreEm(@RequestParam int maTreEm) {
        treEmDao.khoiPhucTreEm(maTreEm);
        return ResponseEntity.ok().body(new
                MessageDto(HttpStatus.OK.value(), "Khôi phục thành công"));
    }

    @PostMapping("/danh-sach")
    public ResponseEntity danhSachTreEmPhanTrang(@RequestBody TimKiemTreEmDto dto) {
        return ResponseEntity.ok().body(treEmDao.danhSachTreEmPhanTrang(dto));
    }

    @GetMapping("chi-tiet")
    public ResponseEntity chiTietTreEm(@RequestParam int maTreEm,
            @RequestParam(required = false) Integer maLoaiHoanCanh) {
        return ResponseEntity.ok().body(treEmDao.chiTietTreEm(maTreEm, maLoaiHoanCanh));
    }

    @PostMapping("hoan-canh")
    public ResponseEntity quanLyTreEmHoanCanh(@RequestBody QuanLyTreEmHoanCanhDto dto) {
        return ResponseEntity.ok().body(treEmDao.quanLyTreEmHoanCanh(dto));
    }

    @PostMapping("huong-tro-giup")
    public ResponseEntity quanLyTreEmHuongTroGiup(@RequestBody QuanLyTreEmHuongTroGiupDto dto) {
        return ResponseEntity.ok().body(treEmDao.quanLyTreEmHuongTroGiup(dto));
    }

    @PostMapping("bao-cao/danh-sach-tre-em")
    public ResponseEntity<ByteArrayResource> baoCaoDanhSachTreEm(@RequestBody BaoCaoTreEmDto dto) throws IOException {
        ByteArrayResource byteArrayResource = exportDataToExcel.exportDanhSachTreEm(treEmDao.baoCaoDanhSachTreEm(dto),
                dto.getExcelType() == null ? ExcelType.XLS : dto.getExcelType());
        return exportDataToExcel.responseExcel(byteArrayResource, "DanhSachTreEm",
                dto.getExcelType() == null ? ExcelType.XLS : dto.getExcelType());
    }

    @PostMapping("bao-cao/danh-sach-tre-em-binh-thuong")
    public ResponseEntity<ByteArrayResource> baoCaoDanhSachTreEmBinhThuong(@RequestBody BaoCaoTreEmDto dto) throws IOException {
        ByteArrayResource byteArrayResource = exportDataToExcel.exportDanhSachTreEm(treEmDao.baoCaoDanhSachTreEmBinhThuong(dto),
                dto.getExcelType() == null ? ExcelType.XLS : dto.getExcelType());
        return exportDataToExcel.responseExcel(byteArrayResource, "DSTreEmBinhThuong",
                dto.getExcelType() == null ? ExcelType.XLS : dto.getExcelType());
    }

    @PostMapping("bao-cao/danh-sach-tre-em-theo-chi-tieu")
    public ResponseEntity<ByteArrayResource> baoCaoDanhSachTreEmTheoChiTieu(@RequestBody TreEmTheoChiTieuDto dto) throws IOException {
        String tenLoaiChiTieu = treEmDao.chiTietChiTieu(dto.getMaLoaiHC()).get("TEN_LOAI_HOAN_CANH").toString();
        List<Map<String, Object>> listChiTieuCap1 = treEmDao.danhMucChiTieuTheoLoai(dto.getMaLoaiHC(), 1);
        List<Map<String, Object>> chiTieuCha = treEmDao.chiTieuCha(dto.getMaChiTietHC());
        ByteArrayResource byteArrayResource = exportDataToExcel.exportDSTreEmTheoChiTieu(treEmDao.baoCaoDanhSachTreEmTheoChiTieu(dto),
                dto.getExcelType() == null ? ExcelType.XLS : dto.getExcelType(), tenLoaiChiTieu,
                dto.getMaChiTietHC() == null ? listChiTieuCap1 : chiTieuCha);
        return exportDataToExcel.responseExcel(byteArrayResource, "DSTreEmTheoChiTieu",
                dto.getExcelType() == null ? ExcelType.XLS : dto.getExcelType());
    }

    @GetMapping("baocao-thong-ke-tre-em-hcdb")
    public ResponseEntity baoCaoThongKeTreEmHCDB(@RequestParam int year) {
        return ResponseEntity.ok().body(treEmDao.baocaothongkeTEHCDB(year));
    }

    @GetMapping("baocao-thong-ke-tre-em-theo-nam")
    public ResponseEntity baoCaoThongKeTreEmTheoNam() {
        return ResponseEntity.ok().body(treEmDao.baocaothongkeTETheoNam());
    }
}
