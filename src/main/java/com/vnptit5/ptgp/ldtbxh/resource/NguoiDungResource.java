package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.authentication.CustomUserDetails;
import com.vnptit5.ptgp.ldtbxh.authentication.CustomUserDetailsService;
import com.vnptit5.ptgp.ldtbxh.authentication.JwtTokenProvider;
import com.vnptit5.ptgp.ldtbxh.dao.NguoiDungDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoidung.CapNhatNguoiDungDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoidung.GetTokenDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoidung.ThemMoiNguoiDungDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoidung.TimKiemNguoiDungDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.*;
import com.vnptit5.ptgp.ldtbxh.dto.response.nguoidung.ChiTietNguoiDungDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.nguoidung.TokenDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author trinhctn
 */
@RestController
@RequestMapping("/sldtbxh/nguoi-dung")
@CrossOrigin
public class NguoiDungResource {

    @Value("${jwt.default-password}")
    private String DEFAULT_PASSWORD;

    @Autowired
    private CustomUserDetailsService userDetailsService;

    @Autowired
    private NguoiDungDao nguoiDungDao;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenProvider tokenProvider;


    @PostMapping("/dang-nhap")
    public ResponseEntity dangNhap(@RequestBody @Valid GetTokenDto getTokenDto){
        String username = getTokenDto.getTenDangNhap();
        String password = getTokenDto.getMatKhau();
        String jwt;
        if (passwordEncoder.matches(password, DEFAULT_PASSWORD))
        {
            UserDetails userDetails = userDetailsService.loadUserByUsername(username);
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            SecurityContextHolder.getContext().setAuthentication(authentication);
            jwt = tokenProvider.generateToken((CustomUserDetails) authentication.getPrincipal());
        }
        else {
            Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(username, password)
            );
            SecurityContextHolder.getContext().setAuthentication(authentication);
            jwt = tokenProvider.generateToken((CustomUserDetails) authentication.getPrincipal());
        }
        TokenDto tokenDto = new TokenDto(jwt,tokenProvider.getExpFromJwt(jwt), tokenProvider.getIssueAtFromJwt(jwt));
        tokenDto.setStatusCode(HttpStatus.OK.value());
        return ResponseEntity.ok()
                .body(tokenDto);
    }

    @PostMapping("/danh-sach")
    public ResponseEntity danhSachNguoiDung(@RequestBody TimKiemNguoiDungDto timKiemNguoiDungDto){
        List<Map<String, Object>> userList = nguoiDungDao.danhSachNguoiDung(timKiemNguoiDungDto);

        ModelMapper modelMapper = new ModelMapper();
        List<ThongTinNguoiDungDto> result = userList.stream()
                                            .map(e -> modelMapper.map(e, ThongTinNguoiDungDto.class))
                                            .collect(Collectors.toList());
        return ResponseEntity.ok()
                .body(result);
    }

    @PostMapping("/dang-ky")
    public ResponseEntity themMoiNguoiDung(@RequestBody ThemMoiNguoiDungDto dto){
        dto.setMatKhau(passwordEncoder.encode(dto.getMatKhau()));
        int id = Integer.parseInt(nguoiDungDao.themMoiNguoiDung(dto).get("P_OUT").toString());
        if (id == 0){
            throw new CustomException("Người dùng đã tồn tại");
        }
        return ResponseEntity.ok()
                .body(DangKyDto.builder().id(id).thongBao("Thêm mới thành công")
                        .statusCode(HttpStatus.OK.value()).build());
    }

    @PutMapping("/sua")
    public ResponseEntity suaNguoiDung(@RequestBody CapNhatNguoiDungDto capNhatNguoiDungDto){
        if (capNhatNguoiDungDto.getMatKhau() != null){
            capNhatNguoiDungDto.setMatKhau(passwordEncoder.encode(capNhatNguoiDungDto.getMatKhau()));
        }
        nguoiDungDao.suaNguoiDung(capNhatNguoiDungDto);
        return ResponseEntity.ok()
                .body(new MessageDto(HttpStatus.OK.value(), "Cập nhật người dùng thành công"));
    }

    @DeleteMapping("/xoa")
    public ResponseEntity xoaNguoiDung(@RequestParam String tenDangNhap){
        nguoiDungDao.xoaNguoiDung(tenDangNhap);
        return ResponseEntity.ok()
                .body(new MessageDto(HttpStatus.OK.value(), "Xoá thành công"));
    }

    @GetMapping("/khoi-phuc")
    public ResponseEntity khoiPhucNguoiDung(@RequestParam String tenDangNhap){
        nguoiDungDao.khoiPhucNguoiDung(tenDangNhap);
        return ResponseEntity.ok()
                .body(new MessageDto(HttpStatus.OK.value(), "Khôi phục thành công"));
    }

    @GetMapping("chi-tiet")
    public ResponseEntity chiTietNguoiDung(){
       ChiTietNguoiDungDto result = userDetailsService.getCurrentUser();
        return ResponseEntity.ok()
                .body(result);
    }

    @GetMapping("thong-tin-chi-tiet")
    public ResponseEntity chiTietNguoiDungTheoTenDangNhap(@RequestParam String tenDangNhap){
        ChiTietNguoiDungDto result = nguoiDungDao.chiTietNguoiDung(tenDangNhap);
        return ResponseEntity.ok()
                .body(result);
    }


}
