package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.common.UrlConstants;
import com.vnptit5.ptgp.ldtbxh.dao.MartyrsProfileRelativeDao;
import com.vnptit5.ptgp.ldtbxh.dto.base.PostResponseBaseDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong.MartyrsProfileRelativeDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ToiTV
 * 2020-10-23
 */

@RestController
@RequestMapping(UrlConstants.RELATIVE_INFORMATION)
@CrossOrigin
public class MartyrsProfileRelativeResource {
    @Autowired
    private MartyrsProfileRelativeDao martyrsProfileRelativeDao;

    /**
     * Get relative information
     * @return
     */
    @GetMapping(UrlConstants.RELATIVE_INFORMATION_INFO)
    public ResponseEntity getRelativeInfo(@RequestParam Integer hoSoID) {
        return ResponseEntity.ok()
                .body(martyrsProfileRelativeDao.getRelativeInfo(hoSoID));
    }

    /**
     * Insert or update relative information
     * @return
     */
    @PostMapping(UrlConstants.RELATIVE_INFORMATION_UPDATE)
    public ResponseEntity updateRelativeInfo(@RequestBody MartyrsProfileRelativeDto request) {
        boolean result = martyrsProfileRelativeDao.updateRelativeInfo(request);
        return result ?
                ResponseEntity.ok().body(PostResponseBaseDto.builder().message("Cập nhật thành công")
                        .statusCode(HttpStatus.OK.value()).build()) :
                ResponseEntity.badRequest().body(PostResponseBaseDto.builder().message("Cập nhật không thành công")
                        .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value()).build());
    }

    /**
     * Delete relative information
     * @return
     */
    @DeleteMapping(UrlConstants.RELATIVE_INFORMATION_DELETE)
    public ResponseEntity deleteRelativeInfo(@RequestParam Integer thanNhanLietSyID) {
        boolean result = martyrsProfileRelativeDao.deleteRelativeInfo(thanNhanLietSyID);
        return result ?
                ResponseEntity.ok().body(PostResponseBaseDto.builder().message("Xóa thành công")
                        .statusCode(HttpStatus.OK.value()).build()) :
                ResponseEntity.badRequest().body(PostResponseBaseDto.builder().message("Xóa không thành công")
                        .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value()).build());
    }
}
