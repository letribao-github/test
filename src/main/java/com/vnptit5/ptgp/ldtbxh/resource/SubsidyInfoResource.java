package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.dao.HuyenDao;
import com.vnptit5.ptgp.ldtbxh.dao.SubsidyInfoDao;
import com.vnptit5.ptgp.ldtbxh.dto.base.PostResponseBaseDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.subsidy.SubsidyDtoRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import com.vnptit5.ptgp.ldtbxh.common.UrlConstants;

/**
 * @author ToiTV
 * 2020-10-21
 */

@RestController
@CrossOrigin
@RequestMapping(UrlConstants.SUBSIDY_INFORMATION_PAGE)
public class SubsidyInfoResource {
    @Autowired
    private SubsidyInfoDao subsidyInfoDao;

    @GetMapping(UrlConstants.SUBSIDY_CATEGORY)
    public  ResponseEntity getCategory() {
        return ResponseEntity.ok().body(subsidyInfoDao.getCategory());
    }

    @GetMapping(UrlConstants.SUBSIDY_INFORMATION)
    public ResponseEntity getInfo(@RequestParam Integer hoSoID) {
        return ResponseEntity.ok().body(subsidyInfoDao.getInfo(hoSoID));
    }

    @PostMapping(UrlConstants.SUBSIDY_INSERT_OR_UPDATE)
    public ResponseEntity insertOrUpdate(@RequestBody SubsidyDtoRequest request) {
        return subsidyInfoDao.insertOrUpdate(request) ?
                ResponseEntity.ok().body(PostResponseBaseDto.builder().message("Thêm mới thành công")
                        .statusCode(HttpStatus.OK.value()).build()) :
                ResponseEntity.badRequest().body(PostResponseBaseDto.builder().message("Thêm mới không thành công")
                        .statusCode(HttpStatus.BAD_REQUEST.value()).build());
    }

    @DeleteMapping(UrlConstants.SUBSIDY_DELETE)
    public ResponseEntity delete(@RequestParam int thongTinCheDoID) {
        return subsidyInfoDao.delete(thongTinCheDoID) ?
                ResponseEntity.ok().body(PostResponseBaseDto.builder().message("Xóa thành công")
                        .statusCode(HttpStatus.OK.value()).build()) :
                ResponseEntity.badRequest().body(PostResponseBaseDto.builder().message("Xóa không thành công")
                        .statusCode(HttpStatus.BAD_REQUEST.value()).build());
    }
}

