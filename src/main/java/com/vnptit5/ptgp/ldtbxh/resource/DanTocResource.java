package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.dao.DanTocDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import com.vnptit5.ptgp.ldtbxh.common.UrlConstants;

@RestController
@CrossOrigin
@RequestMapping(UrlConstants.DAN_TOC)
public class DanTocResource {
    @Autowired
    private DanTocDao danTocDao;

    @GetMapping(UrlConstants.GET_LIST_DAN_TOC)
    public ResponseEntity listDanToc() {
        List<Map<String, Object>> listDanToc = danTocDao.getListDanToc();
        return ResponseEntity.ok()
                .body(listDanToc);
    }
}

