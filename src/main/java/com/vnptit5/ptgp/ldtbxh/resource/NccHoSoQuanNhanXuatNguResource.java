package com.vnptit5.ptgp.ldtbxh.resource;
import com.vnptit5.ptgp.ldtbxh.dao.NccHoSoQuanNhanXuatNguDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong.HoSoQuanNhanXuatNguDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.DangKyDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.MessageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.vnptit5.ptgp.ldtbxh.common.UrlConstants;

import java.util.List;
import java.util.Map;


@RestController
@RequestMapping(UrlConstants.HO_SO_QUAN_NHAN_XUAT_NGU)
@CrossOrigin
public class NccHoSoQuanNhanXuatNguResource {
    @Autowired
    private NccHoSoQuanNhanXuatNguDao nccHoSoQuanNhanXuatNguDao;
    @GetMapping(UrlConstants.HO_SO_QNXN_XEM_THONG_TIN_HO_SO)
    public ResponseEntity getThongtinHoSoByID(@RequestParam Integer hosoID){
        return ResponseEntity.ok()
                .body(nccHoSoQuanNhanXuatNguDao.getThongTinHoSoByID(hosoID));
    }
    @GetMapping(UrlConstants.HO_SO_QNXN_XEM_THONG_TIN_TRO_CAP_PHU_CAP)
    public ResponseEntity getThongtinTroCapPhuCapByID(@RequestParam Integer hosoID){
        return ResponseEntity.ok()
                .body(nccHoSoQuanNhanXuatNguDao.getThongtinTroCapPhuCapByID(hosoID));
    }
    @GetMapping(UrlConstants.HO_SO_QNXN_XEM_QUA_TRINH_CONG_TAC)
    public ResponseEntity getQuaTrinhCongTacByID(@RequestParam Integer hosoID){
        return ResponseEntity.ok()
                .body(nccHoSoQuanNhanXuatNguDao.getQuaTrinhCongTacByID(hosoID));
    }
    @GetMapping(UrlConstants.HO_SO_QNXN_XEM_TRO_CAP_KHI_QUA_DOI)
    public ResponseEntity getTroCapKhiQuaDoiByID(@RequestParam Integer hosoID){
        return ResponseEntity.ok()
                .body(nccHoSoQuanNhanXuatNguDao.getTroCapKhiQuaDoiByID(hosoID));
    }
    @PostMapping(UrlConstants.HO_SO_QNXN_THEM_THONG_TIN_HO_SO)
    public ResponseEntity addThongTinHoSo(@RequestBody HoSoQuanNhanXuatNguDto dto){
        int id = nccHoSoQuanNhanXuatNguDao.addThongTinHoSo(dto);
        return ResponseEntity.ok()
                .body(DangKyDto.builder().id(id).thongBao("Thêm mới thành công")
                        .statusCode(HttpStatus.OK.value()).build());
    }
    @PostMapping(UrlConstants.HO_SO_QNXN_THEM_THONG_TIN_TRO_CAP_PHU_CAP)
    public ResponseEntity addThongtinTroCapPhuCap(@RequestBody HoSoQuanNhanXuatNguDto dto){
        int id = nccHoSoQuanNhanXuatNguDao.addThongTinTroCapPhuCap(dto);
        return ResponseEntity.ok()
                .body(DangKyDto.builder().id(id).thongBao("Thêm mới thành công")
                        .statusCode(HttpStatus.OK.value()).build());
    }
    @PostMapping(UrlConstants.HO_SO_QNXN_THEM_QUA_TRINH_CONG_TAC)
    public ResponseEntity addQuaTrinhCongTac(@RequestBody HoSoQuanNhanXuatNguDto dto){
        int id = nccHoSoQuanNhanXuatNguDao.addQuaTrinhCongTac(dto);
        return ResponseEntity.ok()
                .body(DangKyDto.builder().id(id).thongBao("Thêm mới thành công")
                        .statusCode(HttpStatus.OK.value()).build());
    }
    @PostMapping(UrlConstants.HO_SO_QNXN_THEM_TRO_CAP_KHI_QUA_DOI)
    public ResponseEntity addTroCapKhiQuaDoi(@RequestBody HoSoQuanNhanXuatNguDto dto){
        int id = nccHoSoQuanNhanXuatNguDao.addTroCapKhiQuaDoi(dto);
        return ResponseEntity.ok()
                .body(DangKyDto.builder().id(id).thongBao("Thêm mới thành công")
                        .statusCode(HttpStatus.OK.value()).build());
    }
    @PutMapping(UrlConstants.HO_SO_QNXN_SUA_THONG_TIN_HO_SO)
    public ResponseEntity updateThongTinHoSo(@RequestBody HoSoQuanNhanXuatNguDto dto){
        nccHoSoQuanNhanXuatNguDao.updateThongTinHoSo(dto);
        return ResponseEntity.ok().body(new MessageDto(HttpStatus.OK.value(), "Cập nhật chính sách thành công"));
    }
    @PutMapping(UrlConstants.HO_SO_QNXN_SUA_THONG_TIN_TRO_CAP_PHU_CAP)
    public ResponseEntity updateThongtinTroCapPhuCap(@RequestBody HoSoQuanNhanXuatNguDto dto){
        nccHoSoQuanNhanXuatNguDao.updateThongTinTroCapPhuCap(dto);
        return ResponseEntity.ok().body(new MessageDto(HttpStatus.OK.value(), "Cập nhật chính sách thành công"));
    }
    @PutMapping(UrlConstants.HO_SO_QNXN_SUA_QUA_TRINH_CONG_TAC)
    public ResponseEntity updateQuaTrinhCongTac(@RequestBody HoSoQuanNhanXuatNguDto dto){
        nccHoSoQuanNhanXuatNguDao.updateQuaTrinhCongTac(dto);
        return ResponseEntity.ok().body(new MessageDto(HttpStatus.OK.value(), "Cập nhật chính sách thành công"));
    }
    @PutMapping(UrlConstants.HO_SO_QNXN_SUA_TRO_CAP_KHI_QUA_DOI)
    public ResponseEntity updateTroCapKhiQuaDoi(@RequestBody HoSoQuanNhanXuatNguDto dto){
        nccHoSoQuanNhanXuatNguDao.updateTroCapKhiQuaDoi(dto);
        return ResponseEntity.ok().body(new MessageDto(HttpStatus.OK.value(), "Cập nhật chính sách thành công"));
    }
    @DeleteMapping(UrlConstants.HO_SO_QNXN_XOA_THONG_TIN_HO_SO)
    public ResponseEntity deleteThongTinHoSo(@RequestParam Integer hosoID){
        nccHoSoQuanNhanXuatNguDao.deleteThongTinHoSo(hosoID);
        return ResponseEntity.ok().body(new MessageDto(HttpStatus.OK.value(), "Xoá thành công"));
    }
    @DeleteMapping(UrlConstants.HO_SO_QNXN_XOA_THONG_TIN_TRO_CAP_PHU_CAP)
    public ResponseEntity deleteThongtinTroCapPhuCap(@RequestParam Integer thongtinchedoID){
        nccHoSoQuanNhanXuatNguDao.deleteThongtinTroCapPhuCap(thongtinchedoID);
        return ResponseEntity.ok().body(new MessageDto(HttpStatus.OK.value(), "Xoá thành công"));
    }
    @DeleteMapping(UrlConstants.HO_SO_QNXN_XOA_QUA_TRINH_CONG_TAC)
    public ResponseEntity deleteQuaTrinhCongTac(@RequestParam Integer quatrinhcongtacID){
        nccHoSoQuanNhanXuatNguDao.deleteQuaTrinhCongTac(quatrinhcongtacID);
        return ResponseEntity.ok().body(new MessageDto(HttpStatus.OK.value(), "Xoá thành công"));
    }
    @DeleteMapping(UrlConstants.HO_SO_QNXN_XOA_TRO_CAP_KHI_QUA_DOI)
    public ResponseEntity deleteTroCapKhiQuaDoi(@RequestParam Integer ID){
        nccHoSoQuanNhanXuatNguDao.deleteTroCapKhiQuaDoi(ID);
        return ResponseEntity.ok().body(new MessageDto(HttpStatus.OK.value(), "Xoá thành công"));
    }
}
