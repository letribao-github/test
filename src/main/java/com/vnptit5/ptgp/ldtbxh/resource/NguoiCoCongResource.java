package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.common.UrlConstants;
import com.vnptit5.ptgp.ldtbxh.dao.FileStorageDto;
import com.vnptit5.ptgp.ldtbxh.dao.NCCThongTinHoSoDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong.*;
import com.vnptit5.ptgp.ldtbxh.dto.response.CustomException;
import com.vnptit5.ptgp.ldtbxh.dto.response.DangKyDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.MessageDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.UploadFileResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

@RestController
@RequestMapping("/sldtbxh/nguoi-co-cong")
@CrossOrigin

public class NguoiCoCongResource {
    @Autowired
    private NCCThongTinHoSoDao nccThongTinHoSoDao;

    //insert trang thong tin ho so - nguoi co cong voi cm

    @PostMapping("/them-moi")
    public ResponseEntity tmThongTinHSNCC(@RequestBody TTNguoicocongCM TTHSNCC) {
        int macs = nccThongTinHoSoDao.tmThongTinHSNCC(TTHSNCC);
        if (macs == 0) {
            throw new CustomException("Người có công đã tồn tại");
        }
        return ResponseEntity.ok().body(
                DangKyDto.builder().id(macs).thongBao("Thêm mới thành công").statusCode(HttpStatus.OK.value()).build());
    }

    //update trang thong tin ho so - nguoi co cong voi cm

    @PutMapping("/cap-nhat-thanh-vien")
    public ResponseEntity updateTTHSNCC(@RequestBody TTNguoicocongCMUpdate TTHSNCC) {
        nccThongTinHoSoDao.updateTTHSNCC(TTHSNCC);
        MessageDto messageDto = MessageDto.builder().message("Cập nhật Người có công thành công").statusCode(HttpStatus.OK.value())
                .build();
        return ResponseEntity.ok().body(messageDto);
    }

    //delete trang thong tin ho so - nguoi co cong voi cm

    @DeleteMapping("/xoa")
    public ResponseEntity deleteTTHSNCC(@RequestParam Integer HoSoNguoiCoCongGiupCMID)
    {   nccThongTinHoSoDao.deleteTTHSNCC(HoSoNguoiCoCongGiupCMID);
        return ResponseEntity.ok()
                .body(new MessageDto(HttpStatus.OK.value(), "Xoá thành công"));
    }

    //select trang thong tin ho so - nguoi co cong voi cm

    @GetMapping("/thong-tin")
    public ResponseEntity selectTTHSNCC(@RequestParam(required = false) Integer HosoID) {
        return ResponseEntity.ok().body(nccThongTinHoSoDao.selectTTHSNCC(HosoID));
    }

    //select trang thong tin tro cap - nguoi co cong voi cm

    @GetMapping("/thong-tin-tro-cap")
    public ResponseEntity selectTTTrocap(@RequestParam(required = false) Integer HosoID) {
        return ResponseEntity.ok().body(nccThongTinHoSoDao.selectTTTrocap(HosoID));
    }

    //insert trang thong tin tro cap - nguoi co cong voi cm

    @PostMapping("/them-moi-tro-cap")
    public ResponseEntity insertNCCTroCap (@RequestBody NCCTroCapInsert NCCTroCapInsert) {
        int macs = nccThongTinHoSoDao.insertNCCTroCap(NCCTroCapInsert);
        if (macs == 0) {
            throw new CustomException("Thông tin trợ cấp đã tồn tại");
        }
        return ResponseEntity.ok().body(
                DangKyDto.builder().id(macs).thongBao("Thêm mới thành công").statusCode(HttpStatus.OK.value()).build());
    }

    //update trang thong tin tro cap - nguoi co cong voi cm

    @PutMapping("/cap-nhat-tro-cap")
    public ResponseEntity updateNCCTroCap(@RequestBody NCCTroCapUpdate UpdateTroCap) {
        nccThongTinHoSoDao.updateNCCTroCap(UpdateTroCap);
        MessageDto messageDto = MessageDto.builder().message("Cập nhật Người có công thành công").statusCode(HttpStatus.OK.value())
                .build();
        return ResponseEntity.ok().body(messageDto);
    }

    //delete trang thong tin tro cap - nguoi co cong voi cm

    @DeleteMapping("/xoa-tro-cap")
    public ResponseEntity deleteNCCTrocap(@RequestParam Integer HosoID, Integer ThongTinCheDoID)
    {   nccThongTinHoSoDao.deleteNCCTrocap(HosoID,ThongTinCheDoID);
        return ResponseEntity.ok()
                .body(new MessageDto(HttpStatus.OK.value(), "Xoá thành công"));
    }

    //select trang dinh kem - nguoi co cong voi cm

    @GetMapping("/thong-tin-dinh-kem")
    public ResponseEntity selectDinhkem(@RequestParam(required = false) Integer HosoID) {
        return ResponseEntity.ok().body(nccThongTinHoSoDao.selectDinhkem(HosoID));
    }

    //insert trang dinh kem - nguoi co cong voi cm

    @PostMapping("/them-moi-dinh-kem")
    public ResponseEntity insertNCCTrocapDinhkem(@RequestBody NCCTroCapInsertDinhKem NCCDinhKemInsert) {
        int macs = nccThongTinHoSoDao.insertNCCTrocapDinhkem(NCCDinhKemInsert);
        if (macs == 0) {
            throw new CustomException("Thông tin đính kèm đã tồn tại");
        }
        return ResponseEntity.ok().body(
                DangKyDto.builder().id(macs).thongBao("Thêm mới thành công").statusCode(HttpStatus.OK.value()).build());
    }

    //update trang dinh kem - nguoi co cong voi cm

    @PutMapping("/cap-nhat-dinh-kem")
    public ResponseEntity updatenccDinhKem(@RequestBody NCCTroCapInsertDinhKem UpdateDinhKem) {
        nccThongTinHoSoDao.updatenccDinhKem(UpdateDinhKem);
        MessageDto messageDto = MessageDto.builder().message("Cập nhật Người có công thành công").statusCode(HttpStatus.OK.value())
                .build();
        return ResponseEntity.ok().body(messageDto);
    }

    //xoa trang dinh kem - nguoi co cong voi cm

    @DeleteMapping("/xoa-dinh-kem")
    public ResponseEntity deletenccDinhKem(@RequestParam Integer HosoID)
    {   nccThongTinHoSoDao.deletenccDinhKem(HosoID);
        return ResponseEntity.ok()
                .body(new MessageDto(HttpStatus.OK.value(), "Xoá thành công"));
    }
    @GetMapping("/thong-tin-che-do-duoc-huong")
    public ResponseEntity selectDMDH() {
        return ResponseEntity.ok().body(nccThongTinHoSoDao.selectDMDH());
    }

    @GetMapping("/thong-tin-tinh-trang-ca-nhan")
    public ResponseEntity selectDMTinhtrangcanhan() {
        return ResponseEntity.ok().body(nccThongTinHoSoDao.selectDMTinhtrangcanhan());
    }

}
