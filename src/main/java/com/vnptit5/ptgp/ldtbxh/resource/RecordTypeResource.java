package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.dao.RecordTypeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

import com.vnptit5.ptgp.ldtbxh.common.UrlConstants;

@RestController
@RequestMapping(UrlConstants.RECORD_TYPE)
@CrossOrigin
public class RecordTypeResource {
    @Autowired
    private RecordTypeDao recordTypeDao;

    @GetMapping(UrlConstants.GET_LIST_RECORD_TYPE)
    public ResponseEntity getListRecordType() {
        List<Map<String, Object>> listRecordType = recordTypeDao.getListRecordType();
        return ResponseEntity.ok()
                .body(listRecordType);
    }
}
