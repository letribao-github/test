package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.dao.DiaPhuongDao;
import com.vnptit5.ptgp.ldtbxh.dao.HoGiaDinhDao;
import com.vnptit5.ptgp.ldtbxh.dao.ThanhVienDao;
import com.vnptit5.ptgp.ldtbxh.dao.TreEmDao;
import com.vnptit5.ptgp.ldtbxh.dto.request.CatChetNhanKhauDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh.*;
import com.vnptit5.ptgp.ldtbxh.dto.request.hogiadinh.chitra.ChiTraDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.thanhvien.DanhSachNhanKhauHuongChinhSachDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.CustomException;
import com.vnptit5.ptgp.ldtbxh.dto.response.DangKyDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.MessageDto;
import com.vnptit5.ptgp.ldtbxh.service.VndisService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author trinhctn
 */

@RestController
@RequestMapping("/sldtbxh")
@CrossOrigin
public class HoGiaDinhResource {
    @Autowired
    private VndisService vndisService;

    @Autowired
    private TreEmDao treEmDao;

    @Autowired
    private HoGiaDinhDao hoGiaDinhDao;

    @Autowired
    private ThanhVienDao thanhVienDao;

    @Autowired
    private DiaPhuongDao diaPhuongDao;

    @PostMapping("/ho-gia-dinh/tra-cuu")
    public ResponseEntity<List<Map<String, Object>>> traCuuHoGiaDinh(
            @RequestBody TraCuuHoGiaDinhDto traCuuHoGiaDinhDto) {
        List<Map<String, Object>> result = vndisService.traCuuHoGiaDinh(traCuuHoGiaDinhDto);
        return ResponseEntity.ok().body(result);
    }

    @GetMapping("/ho-gia-dinh/tra-cuu-nhanh")
    public ResponseEntity<Map<String, Object>> traCuuNhanh(@RequestParam String maDinhDanh) {
        Map<String, Object> result = vndisService.traCuuNhanh(maDinhDanh);
        return ResponseEntity.ok().body(result);
    }

    @PostMapping("/ho-gia-dinh/tra-cuu-theo-ma-dinh-danh")
    public ResponseEntity<List<Map<String, Object>>> traCuuTheoMaDinhDanh(@RequestBody TraCuuTheoMaDinhDanh dto) {
        return ResponseEntity.ok().body(vndisService.traCuuHoTheoMaDinhDanh(dto));
    }

    @PostMapping("/ho-gia-dinh/them-moi")
    public ResponseEntity themMoiHoGiaDinh(@RequestBody ThemMoiHoGiaDinhDto dto) {
        int id = treEmDao.themMoiHoGiaDinh(dto);
        return ResponseEntity.ok().body(
                DangKyDto.builder().id(id).thongBao("Thêm mới thành công").statusCode(HttpStatus.OK.value()).build());
    }

    @PostMapping("/ho-gia-dinh/danh-sach-chi-tra")
    public ResponseEntity danhSachChiTra(@RequestBody ChiTraDto dto) {
        return ResponseEntity.ok().body(hoGiaDinhDao.danhSachChiTra(dto));
    }

    @PostMapping("/nhan-khau/cat-chet")
    public ResponseEntity catChetNhanKhau(@RequestBody CatChetNhanKhauDto dto) {
        return ResponseEntity.ok().body(hoGiaDinhDao.catChetNhanKhau(dto));
    }

    @PutMapping("/ho-gia-dinh/chi-tra")
    public ResponseEntity chiTra(@RequestParam Integer idNhanKhau, @RequestParam Integer soTienHuong,
            @RequestParam String namHuong, @RequestParam String thangHuong) {
        hoGiaDinhDao.chiTra(idNhanKhau, soTienHuong, namHuong, thangHuong);
        MessageDto messageDto = MessageDto.builder().message("Chi trả thành công").statusCode(HttpStatus.OK.value())
                .build();
        return ResponseEntity.ok().body(messageDto);
    }

    @PutMapping("/ho-gia-dinh/khoi-phuc-chi-tra")
    public ResponseEntity khoiPhucChiTra(@RequestParam Integer idNhanKhau, @RequestParam Integer soTienHuong,
            @RequestParam String namHuong, @RequestParam String thangHuong) {
        hoGiaDinhDao.khoiPhucChiTra(idNhanKhau, soTienHuong, namHuong, thangHuong);
        MessageDto messageDto = MessageDto.builder().message("Khôi phục thành công").statusCode(HttpStatus.OK.value())
                .build();
        return ResponseEntity.ok().body(messageDto);
    }

    @GetMapping("/ho-gia-dinh/thong-ke-chi-tra")
    public ResponseEntity thongKeChiTra(@RequestParam String maTinh, @RequestParam String maHuyen,
            @RequestParam(required = false) String maXa, @RequestParam(required = false) String maThon,
            @RequestParam String namHuong, @RequestParam(required = false) String thangHuong) {
        return ResponseEntity.ok()
                .body(hoGiaDinhDao.thongKeChiTra(maTinh, maHuyen, maXa, maThon, namHuong, thangHuong));
    }

    @GetMapping("/ho-gia-dinh/thong-ke-chi-tra-chi-tiet")
    public ResponseEntity thongKeChiTraChiTiet(@RequestParam(required = false) String maXa,
            @RequestParam(required = false) String maThon, @RequestParam(required = false) String namHuong,
            @RequestParam(required = false) String thangHuong) {
        if (maThon == null) {
            Map<String, Object> thongTinXa = diaPhuongDao.thongTinXa(maXa);
            String maTinh = thongTinXa.get("MA_TINH_BHXH").toString();
            String maHuyen = thongTinXa.get("MA_QUAN_HUYEN_BHXH").toString();
            return ResponseEntity.ok()
                    .body(hoGiaDinhDao.thongKeChiTra(maTinh, maHuyen, maXa, maThon, namHuong, thangHuong));
        } else {
            return ResponseEntity.ok().body(hoGiaDinhDao.thongKeChiTraChiTiet(maXa, maThon, namHuong, thangHuong));
        }
    }

    @GetMapping("/ho-gia-dinh/thong-ke-chi-tra-chi-tiet-doi-tuong")
    public ResponseEntity thongKeChiTraChiTietDoiTuong(@RequestParam Integer idNhanKhau, @RequestParam String namHuong,
            @RequestParam String thangHuong) {
        return ResponseEntity.ok().body(hoGiaDinhDao.thongKeChiTraChiTietDoiTuong(idNhanKhau, namHuong, thangHuong));
    }


    @PostMapping("/ho-gia-dinh/them-moi-dac-diem")
    public ResponseEntity themMoiDacDiemHGD(@RequestBody DacDiemHGDDto dto) {
        int idNhanKhau = hoGiaDinhDao.themMoiDacDiemHGD(dto);
        return ResponseEntity.ok().body(DangKyDto.builder().id(idNhanKhau).thongBao("Thêm mới thành công")
                .statusCode(HttpStatus.OK.value()).build());
    }

    @PostMapping("/ho-gia-dinh/dang-ky")
    public ResponseEntity themMoiHGD(@RequestBody ThemMoiHoGiaDinhDto dto) {
        int idNhanKhau = hoGiaDinhDao.themMoiHGD(dto);
        return ResponseEntity.ok().body(DangKyDto.builder().id(idNhanKhau).thongBao("Thêm mới thành công")
                .statusCode(HttpStatus.OK.value()).build());
    }
    @PutMapping("/ho-gia-dinh/cap-nhat")
    public ResponseEntity capNhatHGD(@RequestBody CapNhatHoGiaDinhDto dto) {
        hoGiaDinhDao.capNhatHGD(dto);
        return ResponseEntity.ok().body(MessageDto.builder().message("Cập nhật hộ gia đình thành công")
                .statusCode(HttpStatus.OK.value()).build());
    }

    @DeleteMapping("/ho-gia-dinh/xoa-khoi-he-thong")
    public ResponseEntity xoaHGD(@RequestParam Integer maHoGD){
        int result = hoGiaDinhDao.xoaHGD(maHoGD);
        if (result == 0){
            throw new CustomException("Xóa không thành công");
        }
        return ResponseEntity.ok().body(
                MessageDto.builder().message("Xóa hộ gia đình thành công").statusCode(HttpStatus.OK.value()).build());
    }

    @PutMapping("ho-gia-dinh/khoi-phuc")
    public ResponseEntity khoiPhucHGD(@RequestParam Integer maHoGD){
        int result = hoGiaDinhDao.khoiPhucHGD(maHoGD);
        if (result == 0){
            throw new CustomException("Khôi phục không thành công");
        }
        return ResponseEntity.ok().body(
                MessageDto.builder().message("Khôi phục hộ gia đình thành công").statusCode(HttpStatus.OK.value()).build());
    }

    @GetMapping("ho-gia-dinh/chinh-sach-dang-huong")
    public ResponseEntity chinhSachHo(@RequestParam Integer maHoGD,
            @RequestParam(required = false) Integer trangThaiHuong) {
        return ResponseEntity.ok().body(thanhVienDao.danhSachHuongCS(maHoGD, true, trangThaiHuong));
    }


    @GetMapping("/ho-gia-dinh/thong-tin-chung")
    public ResponseEntity thongTinHGD(@RequestParam Integer maHoGD) {
        return ResponseEntity.ok().body(hoGiaDinhDao.thongTinHGD(maHoGD));
    }

    @PostMapping("/ho-gia-dinh/ds-nhan-khau-huong-chinh-sach")
    public ResponseEntity dsDoiTuong(@RequestBody DanhSachNhanKhauHuongChinhSachDto dto) {
        return ResponseEntity.ok().body(hoGiaDinhDao.dsNhanKhauHuongChinhSach(dto));
    }
    
    @PostMapping("/ho-gia-dinh/them-moi-chinh-sach-doi-tuong")
    public ResponseEntity themMoiChinhSachDoiTuong(@RequestBody CapNhatChinhSachDoiTuong chinhSachDoiTuong) {
        int id = hoGiaDinhDao.themMoiChinhSachDoiTuong(chinhSachDoiTuong);
        return ResponseEntity.ok().body(DangKyDto.builder().thongBao("Thêm mới chính sách thành công").id(id)
                .statusCode(HttpStatus.OK.value()).build());
    }


    @PostMapping("ho-gia-dinh/tim-kiem")
    public ResponseEntity timKiemHoGiaDinh(@RequestBody TimKiemHoGiaDinhBTXHDto dto) {
        return ResponseEntity.ok().body(hoGiaDinhDao.timKiemHoGiaDinh(dto));
    }

    @PostMapping("ho-gia-dinh/tim-kiem-phan-trang")
    public ResponseEntity timKiemHoGiaDinhPhanTrang(@RequestBody TimKiemHoGiaDinhPhanTrangDto dto) {
        return ResponseEntity.ok().body(hoGiaDinhDao.timKiemHoGiaDinhPhanTrang(dto));
    }

    @PutMapping("/ho-gia-dinh/cap-nhat-chinh-sach-doi-tuong")
    public ResponseEntity capNhatChinhSachDoiTuong(@RequestBody CapNhatChinhSachDoiTuong chinhSachDoiTuong) {
        int row = hoGiaDinhDao.capNhatChinhSachDoiTuong(chinhSachDoiTuong);
        if (row == 0){
            throw new CustomException("Cập nhật không thành công");
        }
        return ResponseEntity.ok().body(MessageDto.builder().message("Cập nhật chính sách đối tượng thành công")
                .statusCode(HttpStatus.OK.value()).build());
    }

    @PostMapping("/doi-tuong/danh-sach-cat-chet")
    public ResponseEntity danhSachCatChet(@RequestBody DanhSachCatChetDto dto) {
        return ResponseEntity.ok().body(hoGiaDinhDao.danhSachCatChet(dto));

    }

    @PostMapping("/ho-gia-dinh/chuyen-vung")
    public ResponseEntity chuyenVungHGD(@RequestBody ChuyenVungHoGiaDinhDto dto) {
        hoGiaDinhDao.chuyenVungHGD(dto);
        return ResponseEntity.ok().body(MessageDto.builder().message("Chuyển vùng hộ thành công")
                            .statusCode(HttpStatus.OK.value()).build() );
    }

    @GetMapping("/doi-tuong/chinh-sach-dang-huong")
    public ResponseEntity chinhSachDoiTuong(Integer idHuongChinhSach, Integer idNhanKhau){
        return ResponseEntity.ok().body(hoGiaDinhDao.chinhSachDoiTuong(idHuongChinhSach, idNhanKhau));
    }

}
