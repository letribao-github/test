package com.vnptit5.ptgp.ldtbxh.resource;


import com.vnptit5.ptgp.ldtbxh.common.UrlConstants;
import com.vnptit5.ptgp.ldtbxh.dao.MartyrsProfileFamilyDao;
import com.vnptit5.ptgp.ldtbxh.dto.base.PostResponseBaseDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong.MartyrsProfileFamilyInfoDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ToiTV
 * 2020-10-22
 */

@RestController
@RequestMapping(UrlConstants.FAMILY_INFORMATION)
@CrossOrigin
public class MartyrsProfileFamilyResource {

    @Autowired
    private MartyrsProfileFamilyDao martyrsProfileDao;

    /**
     * Get family information
     * @return
     */
    @GetMapping(UrlConstants.FAMILY_INFORMATION_INFO)
    public ResponseEntity getFamilyInfo(@RequestParam Integer hoSoID) {
        return ResponseEntity.ok()
                .body(martyrsProfileDao.getFamilyInfo(hoSoID));
    }

    /**
     * Update family information
     * @return
     */
    @PostMapping(UrlConstants.FAMILY_INFORMATION_UPDATE)
    public ResponseEntity updateFamilyInfo(@RequestBody MartyrsProfileFamilyInfoDto request) {
        boolean result = martyrsProfileDao.updateFamilyInfo(request);
        return result ?
                ResponseEntity.ok().body(PostResponseBaseDto.builder().message("Cập nhật thành công")
                        .statusCode(HttpStatus.OK.value()).build()) :
                ResponseEntity.badRequest().body(PostResponseBaseDto.builder().message("Cập nhật không thành công")
                        .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value()).build());
    }

}
