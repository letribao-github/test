package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.common.UrlConstants;
import com.vnptit5.ptgp.ldtbxh.dao.FileStorageDto;
import com.vnptit5.ptgp.ldtbxh.dto.base.PostResponseBaseDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.UploadFileResponseDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.attachedfile.AttachedFileListGetResponseDto;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.engine.jdbc.StreamUtils;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * Created by Benjamin Lam on 10/22/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */
@RestController
@RequestMapping(UrlConstants.FILE_MAPPING)
@CrossOrigin
@Slf4j
public class FileStorageResource {
    @Autowired
    private FileStorageDto attachedProfileDto;

    /**
     * @param file
     * @param heroID
     * @param fileName
     * @param folderName
     * @param note
     * @return
     */
    @PostMapping(UrlConstants.FILE_STORAGE_UPLOAD_FILE)
    public UploadFileResponseDto uploadFile(@RequestParam MultipartFile file,
                                            @RequestParam Integer heroID,
                                            @RequestParam String fileName,
                                            @RequestParam String folderName,
                                            @RequestParam(required = false) String note) {
        String savedFileName = attachedProfileDto.uploadAttachedProfile(file, heroID, fileName, folderName, note);

        String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(UrlConstants.FILE_STORAGE_PATH_DOWNLOAD)
                .path(savedFileName)
                .toUriString();

        return new UploadFileResponseDto(savedFileName, fileDownloadUri,
                file.getContentType(), file.getSize());
    }

    /**
     * @param fileName
     * @param request
     * @param folderName
     * @return
     */
    @GetMapping("/downloadFile/{fileName:.+}")
    public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request,
                                                 @RequestParam String folderName) {
        // Load file as Resource
        Resource resource = attachedProfileDto.downloadAttachedFile(fileName, folderName);

        // Try to determine file's content type
        String contentType = null;
        try {
            contentType = request.getServletContext().getMimeType(resource.getFile().getAbsolutePath());
        } catch (IOException ex) {
            log.info("Could not determine file type.");
        }

        // Fallback to the default content type if type could not be determined
        if (contentType == null) {
            contentType = "application/octet-stream";
        }

        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType(contentType))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"")
                .body(resource);
    }

    /**
     * @param fileID
     * @return
     */
    @DeleteMapping(UrlConstants.FILE_STORAGE_DELETE_FILE)
    public ResponseEntity deleteAttachedFile(@RequestParam Integer fileID) {
        attachedProfileDto.deleteAttachedFile(fileID);
        return ResponseEntity.ok().body(PostResponseBaseDto.builder().message("Xoá file thành công")
                .statusCode(HttpStatus.OK.value()).build());
    }

    /**
     * @param heroID
     * @return
     */
    @GetMapping(UrlConstants.FILE_STORAGE_LIST_FILE)
    public ResponseEntity getListAttachedFiles(@RequestParam Integer heroID) {

        ModelMapper modelMapper = new ModelMapper();

        List<AttachedFileListGetResponseDto> listResponse =
                attachedProfileDto.getListAttachedFiles(heroID).stream().map(e -> modelMapper.map(e, AttachedFileListGetResponseDto.class))
                        .collect(Collectors.toList());

        return ResponseEntity.ok()
                .body(listResponse);
    }

    /**
     * @param heroID
     * @param response
     */
    @GetMapping(UrlConstants.FILE_STORAGE_LIST_ZIP_FILE)
    public void downloadZipFile(@RequestParam Integer heroID, HttpServletResponse response) {

        ModelMapper modelMapper = new ModelMapper();

        List<AttachedFileListGetResponseDto> listResponse =
                attachedProfileDto.getListAttachedFiles(heroID).stream().map(e -> modelMapper.map(e, AttachedFileListGetResponseDto.class))
                        .collect(Collectors.toList());

        response.setContentType("application/octet-stream");
        response.setHeader("Content-Disposition", "attachment;filename=download.zip");
        response.setStatus(HttpServletResponse.SC_OK);

        List<String> fileNames = listResponse.stream()
                .map(AttachedFileListGetResponseDto::toFullFilePath)
                .collect(Collectors.toList());

        log.info("############# file size ###########");
        log.info(String.valueOf(fileNames.size()));

        try (ZipOutputStream zippedOut = new ZipOutputStream(response.getOutputStream())) {
            for (String file : fileNames) {
                FileSystemResource resource = new FileSystemResource(file);

                ZipEntry e = new ZipEntry(Objects.requireNonNull(resource.getFilename()));
                // Configure the zip entry, the properties of the file
                e.setSize(resource.contentLength());
                e.setTime(System.currentTimeMillis());
                // etc.
                zippedOut.putNextEntry(e);
                // And the content of the resource:
                StreamUtils.copy(resource.getInputStream(), zippedOut);
                zippedOut.closeEntry();
            }
            zippedOut.finish();
        } catch (Exception e) {
            // Exception handling goes here
        }
    }
}
