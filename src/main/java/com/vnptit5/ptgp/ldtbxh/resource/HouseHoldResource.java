package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.dao.HouseHoldDao;
import com.vnptit5.ptgp.ldtbxh.dto.base.PostResponseBaseDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.household.HouseHoldPutDto;
import com.vnptit5.ptgp.ldtbxh.dto.pojo.household.MemberHouseHoldGetDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.household.MemberHouseHoldPutDto;
import com.vnptit5.ptgp.ldtbxh.dto.pojo.household.OwnerHouseHoldGetDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.household.MemberHouseHoldPostDto;
import com.vnptit5.ptgp.ldtbxh.dto.base.GetResponseBaseDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.household.HouseHoldListGetResponseDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.household.HouseHoldPostDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.MessageDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.household.HouseHoldGetResponseDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Benjamin Lam on 10/14/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */
@RestController
@RequestMapping("/sldtbxh/ho-khau")
@CrossOrigin
public class HouseHoldResource {
    @Autowired
    private HouseHoldDao houseHoldDao;

    @PostMapping("/them-moi")
    public ResponseEntity addNewHouseHold(@RequestBody HouseHoldPostDto houseHoldPostDto) {
        houseHoldDao.addNewHouseHold(houseHoldPostDto);
        return ResponseEntity.ok().body(PostResponseBaseDto.builder().message("Thêm mới hộ khẩu thành công")
                .statusCode(HttpStatus.OK.value()).build());
    }

    @PostMapping("/them-moi-thanh-vien")
    public ResponseEntity addNewHouseHoldMember(@RequestBody MemberHouseHoldPostDto memberHouseHoldPostDto) {
        houseHoldDao.addNewHouseHoldMember(memberHouseHoldPostDto);
        return ResponseEntity.ok().body(PostResponseBaseDto.builder().message("Thêm mới thành viên thành công")
                .statusCode(HttpStatus.OK.value()).build());
    }

    @PutMapping("/cap-nhat")
    public ResponseEntity updateHouseHold(@RequestBody HouseHoldPutDto houseHoldPutDto) {
        houseHoldDao.updateHouseHold(houseHoldPutDto);
        MessageDto messageDto = MessageDto.builder().message("Cập nhật thành công").statusCode(HttpStatus.OK.value())
                .build();
        return ResponseEntity.ok().body(messageDto);
    }

    @PutMapping("/cap-nhat-thanh-vien")
    public ResponseEntity updateHouseHoldMember(@RequestBody MemberHouseHoldPutDto memberHouseHoldPutDto) {
        houseHoldDao.updateHouseHoldMember(memberHouseHoldPutDto);
        MessageDto messageDto = MessageDto.builder().message("Cập nhật thành viên thành công").statusCode(HttpStatus.OK.value())
                .build();
        return ResponseEntity.ok().body(messageDto);
    }

    @GetMapping("/tra-cuu")
    public ResponseEntity getHouseHold(@RequestParam String identityID) {
        HouseHoldGetResponseDto houseHold = new HouseHoldGetResponseDto();

        ModelMapper modelMapper = new ModelMapper();

        OwnerHouseHoldGetDto owner =
                modelMapper.map(houseHoldDao.getHouseHoldOwner(identityID), OwnerHouseHoldGetDto.class);
        owner.setIdentityNumber(identityID);

        List<MemberHouseHoldGetDto> memberList =
                houseHoldDao.getHouseHoldMember(identityID).stream().map(e -> modelMapper.map(e, MemberHouseHoldGetDto.class))
                        .collect(Collectors.toList());

        houseHold.setOwner(owner);
        houseHold.setMemberList(memberList);

        return ResponseEntity.ok()
                .body(houseHold);
    }

    @GetMapping("/danh-sach")
    public ResponseEntity getListHouseHold() {

        ModelMapper modelMapper = new ModelMapper();

        List<HouseHoldListGetResponseDto> memberList =
                houseHoldDao.getListHouseHold().stream().map(e -> modelMapper.map(e, HouseHoldListGetResponseDto.class))
                        .collect(Collectors.toList());

        return ResponseEntity.ok()
                .body(memberList);
    }

    @DeleteMapping("/xoa-ho-khau")
    public ResponseEntity deleteHouseHold(@RequestParam String identityID, @RequestParam String reason) {
        houseHoldDao.deleteHouseHold(identityID, reason);
        MessageDto messageDto = MessageDto.builder().message("Xoá thành công").statusCode(HttpStatus.OK.value())
                .build();
        return ResponseEntity.ok().body(messageDto);
    }

    @DeleteMapping("/xoa-thanh-vien")
    public ResponseEntity deleteMemberInHouseHold(@RequestParam String identityID,
                                                  @RequestParam int memberID,
                                                  @RequestParam String reason) {
        houseHoldDao.deleteMemberInHouseHold(identityID, memberID, reason);
        MessageDto messageDto = MessageDto.builder().message("Xoá thành công").statusCode(HttpStatus.OK.value())
                .build();
        return ResponseEntity.ok().body(messageDto);
    }

    @GetMapping("/danh-muc-cap-bac-quan-doi")
    public ResponseEntity getListArmyLevel() {

        ModelMapper modelMapper = new ModelMapper();

        List<GetResponseBaseDto> memberList =
                houseHoldDao.getListArmyLevel().stream().map(e -> modelMapper.map(e, GetResponseBaseDto.class))
                        .collect(Collectors.toList());

        return ResponseEntity.ok()
                .body(memberList);
    }

    @GetMapping("/danh-muc-cap-bac-co-quan-cong-an")
    public ResponseEntity getListPoliceLevel() {

        ModelMapper modelMapper = new ModelMapper();

        List<GetResponseBaseDto> memberList =
                houseHoldDao.getListPoliceLevel().stream().map(e -> modelMapper.map(e, GetResponseBaseDto.class))
                        .collect(Collectors.toList());

        return ResponseEntity.ok()
                .body(memberList);
    }

    @GetMapping("/danh-muc-dan-toc")
    public ResponseEntity getListNationLevel() {

        ModelMapper modelMapper = new ModelMapper();

        List<GetResponseBaseDto> memberList =
                houseHoldDao.getListNationLevel().stream().map(e -> modelMapper.map(e, GetResponseBaseDto.class))
                        .collect(Collectors.toList());

        return ResponseEntity.ok()
                .body(memberList);
    }
}