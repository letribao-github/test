package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.common.UrlConstants;
import com.vnptit5.ptgp.ldtbxh.dao.RecordDao;
import com.vnptit5.ptgp.ldtbxh.dto.base.PostResponseBaseDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.record.RecordCreateRequestDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.record.RecordRequestDto;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(UrlConstants.RECORD)
@CrossOrigin
public class RecordResource {

    @Autowired
    private RecordDao recordDao;

    @GetMapping(UrlConstants.SEARCH_RECORD)
    public ResponseEntity getListRecordType(RecordRequestDto recordRequestDto) {
        return ResponseEntity.ok()
                .body(recordDao.searchRecordPage(recordRequestDto));
    }

    /**
     * @author ToiTV
     * 2020-10-17
     */
    @RequestMapping(value = UrlConstants.PROFILE_INFORMATION_EXPORT_GENERAL,
                    produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public void exportGeneral(RecordRequestDto recordRequestDto, HttpServletResponse response) {
        recordDao.exportReport001(recordRequestDto, response);
    }

    /**
     * @author ToiTV
     * 2020-10-18
     */
    @GetMapping(value = UrlConstants.CATEGORY)
    public ResponseEntity getCategory() {
        return ResponseEntity.ok()
                .body(recordDao.getCategory());
    }

    /**
     * @author ToiTV
     * 2020-10-18
     */
    @PostMapping(value = UrlConstants.CREATE_PROFILE_MARTYRS)
    public ResponseEntity createProfileMartyrs(@RequestBody RecordCreateRequestDto recordRequestDto) {
        boolean result = recordDao.createProfileMartyrs(recordRequestDto);
        return result ?
                ResponseEntity.ok().body(PostResponseBaseDto.builder().message("Thêm mới hồ sơ thành công")
                                .statusCode(HttpStatus.OK.value()).build()) :
                ResponseEntity.badRequest().body(PostResponseBaseDto.builder().message("Thêm mới hồ sơ không thành công")
                                .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value()).build());
    }

    /**
     * @author ToiTV
     * Get information of profile
     * @param hoSoID
     * @return
     */
    @GetMapping(value = UrlConstants.INFORMATION)
    public ResponseEntity getProfileInfo(@RequestParam Integer hoSoID) {
        return ResponseEntity.ok()
                .body(recordDao.getProfileInfo(hoSoID));
    }

    /**
     * @author ToiTV
     * Update information of profile
     */
    @PostMapping(value = UrlConstants.UPDATE_PROFILE)
    public ResponseEntity update(@RequestBody RecordCreateRequestDto request) {
        return ResponseEntity.ok()
                .body(recordDao.update(request));
    }

}
