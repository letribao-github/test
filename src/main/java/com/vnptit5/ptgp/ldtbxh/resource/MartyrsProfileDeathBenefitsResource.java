package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.common.UrlConstants;
import com.vnptit5.ptgp.ldtbxh.dao.MartyrsProfileDeathBenefitsDao;
import com.vnptit5.ptgp.ldtbxh.dto.base.PostResponseBaseDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.nguoicocong.MartyrsProfileDeathBenefitsDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ToiTV
 * 2020-10-22
 */

@RestController
@RequestMapping(UrlConstants.DEATH_BENEFITS_INFORMATION)
@CrossOrigin
public class MartyrsProfileDeathBenefitsResource {

    @Autowired
    private MartyrsProfileDeathBenefitsDao martyrsProfileDeathBenefitsDao;

    /**
     * Get death benefits information
     * @return
     */
    @GetMapping(UrlConstants.DEATH_BENEFITS_INFORMATION_INFO)
    public ResponseEntity getDeathInfo(@RequestParam Integer hoSoID) {
        return ResponseEntity.ok()
                .body(martyrsProfileDeathBenefitsDao.getDeathInfo(hoSoID));
    }

    /**
     * Insert or update death benefits information
     * @return
     */
    @PostMapping(UrlConstants.DEATH_BENEFITS_INFORMATION_UPDATE)
    public ResponseEntity updateRelativeInfo(@RequestBody MartyrsProfileDeathBenefitsDto request) {
        boolean result = martyrsProfileDeathBenefitsDao.updateDeathBenefitsInfo(request);
        return result ?
                ResponseEntity.ok().body(PostResponseBaseDto.builder().message("Cập nhật thành công")
                        .statusCode(HttpStatus.OK.value()).build()) :
                ResponseEntity.badRequest().body(PostResponseBaseDto.builder().message("Cập nhật không thành công")
                        .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value()).build());
    }

    /**
     * Delete death benefits information
     * @return
     */
    @DeleteMapping(UrlConstants.DEATH_BENEFITS_INFORMATION_DELETE)
    public ResponseEntity deleteRelativeInfo(@RequestParam Integer iD) {
        boolean result = martyrsProfileDeathBenefitsDao.deleteDeathBenefitsInfo(iD);
        return result ?
                ResponseEntity.ok().body(PostResponseBaseDto.builder().message("Xóa thành công")
                        .statusCode(HttpStatus.OK.value()).build()) :
                ResponseEntity.badRequest().body(PostResponseBaseDto.builder().message("Xóa không thành công")
                        .statusCode(HttpStatus.INTERNAL_SERVER_ERROR.value()).build());
    }
}
