package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.common.UrlConstants;
import com.vnptit5.ptgp.ldtbxh.dao.LifeInfoDao;
import com.vnptit5.ptgp.ldtbxh.dto.base.GetResponseBaseDto;
import com.vnptit5.ptgp.ldtbxh.dto.base.PostResponseBaseDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.lifeinfo.LifeInfoPostDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.lifeinfo.IncomeTypeHomeStatusGetResponseDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.lifeinfo.LifeInfoGetResponseDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Benjamin Lam on 10/19/20.
 * VNPT-IT KV5 LTD
 * binhldq.tgg@vnpt.vn
 */
@RestController
@RequestMapping(UrlConstants.LIFE_INFO_MAPPING)
@CrossOrigin
public class LifeInfoResource {
    @Autowired
    private LifeInfoDao lifeInfoDao;

    // TODO: TASK IT5CQS-401
    @GetMapping(UrlConstants.LIFE_INFO_SEARCH)
    public ResponseEntity getHeroLifeInfo(@RequestParam Integer heroID) {
        ModelMapper modelMapper = new ModelMapper();

        LifeInfoGetResponseDto lifeInfo = modelMapper.map(lifeInfoDao.getHeroLifeInfo(heroID), LifeInfoGetResponseDto.class);
        return ResponseEntity.ok()
                .body(lifeInfo);
    }

    @GetMapping(UrlConstants.LIFE_INFO_MENU)
    public ResponseEntity getListIncomeTypeAndHouseStatus() {
        IncomeTypeHomeStatusGetResponseDto responseDto = new IncomeTypeHomeStatusGetResponseDto();

        ModelMapper modelMapper = new ModelMapper();

        List<GetResponseBaseDto> listIncomeType =
                lifeInfoDao.getListIncomeType().stream().map(e -> modelMapper.map(e, GetResponseBaseDto.class))
                        .collect(Collectors.toList());
        responseDto.setListIncomeType(listIncomeType);
        List<GetResponseBaseDto> listHomeStatus =
                lifeInfoDao.getListHouseStatus().stream().map(e -> modelMapper.map(e, GetResponseBaseDto.class))
                        .collect(Collectors.toList());
        responseDto.setListHomeStatus(listHomeStatus);

        return ResponseEntity.ok()
                .body(responseDto);
    }

    @PostMapping(UrlConstants.LIFE_INFO_UPDATE)
    public ResponseEntity addNewHouseHold(@RequestBody LifeInfoPostDto lifeInfoPostDto) {
        lifeInfoDao.modifyHeroLifeInfo(lifeInfoPostDto);
        return ResponseEntity.ok().body(PostResponseBaseDto.builder().message("Cập nhật thông tin đời sống thành công")
                .statusCode(HttpStatus.OK.value()).build());
    }
}