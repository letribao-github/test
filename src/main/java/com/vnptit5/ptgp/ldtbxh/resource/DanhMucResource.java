package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.dao.DanhMucDao;
import com.vnptit5.ptgp.ldtbxh.dto.response.DoiTuongDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.PhongBanDto;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author trinhctn
 */

@RestController
@RequestMapping("/sldtbxh/danh-muc")
@CrossOrigin
public class DanhMucResource{

    @Autowired
    DanhMucDao danhMucDao;

    @GetMapping("/chuc-danh")
    public ResponseEntity danhMucChucDanh(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucChucDanh());
    }

    @GetMapping("/don-vi")
    public ResponseEntity danhMucDonVi(@RequestParam(required = false) String maTinh,
                                       @RequestParam(required = false) String maHuyen,
                                       @RequestParam(required = false) String maXa){
        return ResponseEntity.ok()
                .body((maTinh == null && maHuyen == null && maXa == null)
                        ? danhMucDao.danhMucTatCaDonVi()
                        : danhMucDao.danhMucDonVi(maTinh, maHuyen,maXa));
    }

    @GetMapping("/phong-ban")
    public ResponseEntity danhMucPhongBan(int maDonVi){
        List<Map<String, Object>> list = danhMucDao.danhMucPhongBan(maDonVi);

        ModelMapper modelMapper = new ModelMapper();
        List<PhongBanDto> result = list.stream()
                .map(e -> modelMapper.map(e, PhongBanDto.class))
                .collect(Collectors.toList());

        return ResponseEntity.ok()
                .body(result);
    }

    @GetMapping("/chi-tiet-hoan-canh")
    public ResponseEntity danhMucChiTietHoanCanh(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucChiTietHoanCanh());
    }

    @GetMapping("/chi-tiet-hoan-canh-theo-loai")
    public ResponseEntity danhMucChiTietHoanCanhTheoLoai(@RequestParam int maLoaiHoanCanh, @RequestParam(required = false) Integer maCap){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucChiTietHoanCanhTheoLoai(maLoaiHoanCanh, maCap));
    }

    @GetMapping("/chi-tiet-huong-tro-giup")
    public ResponseEntity danhMucChiTietHuongTroGiup(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucChiTietHuongTroGiup());
    }

    @GetMapping("/hoan-canh-theo-loai")
    public ResponseEntity danhMucHoanCanhTheoLoai(@RequestParam int maLoaiHoanCanh, @RequestParam(required = false) Integer maCap){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucChiTietHC(maLoaiHoanCanh, maCap));
    }

    @GetMapping("/huong-tro-giup")
    public ResponseEntity danhMucHuongTroGiup(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucChiTietHTG());
    }

    @GetMapping("/nhom-quyen")
    public ResponseEntity danhMucNhomQuyen(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucNhomQuyen());
    }

    @GetMapping("/hoan-canh-gia-dinh")
    public ResponseEntity danhMucHoanCanhGiaDinh(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucHoanCanhGiaDinh());
    }

    @GetMapping("/phan-loai-ho-ngheo")
    public ResponseEntity danhMucPhanLoaiHoNgheo(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucPhanLoaiHoNgheo());
    }

    @GetMapping("/chi-tiet-ho-ngheo")
    public ResponseEntity danhMucChiTietHoNgheo(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucChiTietHoNgheo());
    }

    @GetMapping("/dan-toc")
    public ResponseEntity danhMucDanToc(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucDanToc());
    }

    @GetMapping("/tinh-trang-nha-o")
    public ResponseEntity danhMucTinhTrangNhaO(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucTinhTrangNhaO());
    }

    @GetMapping("/loai-ho-xi")
    public ResponseEntity danhMucLoaiHoXi(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucLoaiHoXi());
    }

    @GetMapping("/loai-dien")
    public ResponseEntity danhMucLoaiDien(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucLoaiDien());
    }

    @GetMapping("/hinh-thuc-so-huu-nha")
    public ResponseEntity danhMucSoHuuNha(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucSoHuuNha());
    }

    @GetMapping("/nuoc-sinh-hoat")
    public ResponseEntity danhMucNuocSinhHoat(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucNuocSinhHoat());
    }

    @GetMapping("/quan-he")
    public ResponseEntity danhMucQuanHe(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucQuanHe());
    }

    @GetMapping("/doi-tuong")
    public ResponseEntity danhSachDoiTuong(int idLoaiDoiTuong){
        List<Map<String, Object>> dsdt = danhMucDao.danhMucDoiTuong(idLoaiDoiTuong);
        ModelMapper modelMapper = new ModelMapper();
        List<DoiTuongDto> result = dsdt.stream()
                .map(e -> modelMapper.map(e, DoiTuongDto.class))
                .collect(Collectors.toList());

        return ResponseEntity.ok()
                .body(result);
    }
    
    @GetMapping("/cap-hoc")
    public ResponseEntity danhMucCapHoc(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucCapHoc());
    }

    @GetMapping("/trinh-do-hoc-van")
    public ResponseEntity danhMucTrinhDo(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucTrinhDo());
    }
    @GetMapping("/doi-tuong-chinh-sach")
    public ResponseEntity danhMucDTCS(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucDTCS());
    }

    @GetMapping("/tinh-trang-viec-lam")
    public ResponseEntity danhMucTTVL(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucTinhTrangViecLam());
    }

    @GetMapping("/tinh-trang-hon-nhan")
    public ResponseEntity danhMucTTHN(){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucTinhTrangHonNhan());
    }

    @GetMapping("/loai-doi-tuong")
    public ResponseEntity danhMucLoaiDT(@RequestParam Integer maLoaiChinhSach,
                                            @RequestParam(required = false) Integer doiTuong){
        return ResponseEntity.ok()
                .body(danhMucDao.danhMucLoaiDoiTuong(maLoaiChinhSach, doiTuong));
    }

    @GetMapping("/chi-tieu")
    public ResponseEntity danhMucChiTieu(){
        return ResponseEntity.ok().body(danhMucDao.danhMucLoaiChiTieu());
    }

    @GetMapping("/trang-thai-huong-chinh-sach")
    public ResponseEntity danhMucTrangThaiHCS(){
        return ResponseEntity.ok().body(danhMucDao.danhMucTrangThaiHCS());
    }

    @GetMapping("/loai-the-bhyt")
    public ResponseEntity danhMucLoaiTheBhyt(){
        return ResponseEntity.ok().body(danhMucDao.danhMucLoaiTheBhyt());
    }

    @GetMapping("/ly-do-khong-kcb")
    public ResponseEntity danhMucLyDoKhongKCB(){
        return ResponseEntity.ok().body(danhMucDao.danhMucLyDoKhongKCB());
    }

    @GetMapping("/muc-do-khuyet-tat")
    public ResponseEntity danhMucMucDoKhuyetTat(){
        return ResponseEntity.ok().body(danhMucDao.danhMucMucDoKhuyetTat());
    }
}
