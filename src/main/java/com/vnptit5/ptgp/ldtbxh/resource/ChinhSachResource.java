package com.vnptit5.ptgp.ldtbxh.resource;

import com.vnptit5.ptgp.ldtbxh.dao.ChinhSachDao;

import com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach.SuaChinhSachDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach.SuaDoiTuongDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach.ThemChinhSachDto;
import com.vnptit5.ptgp.ldtbxh.dto.request.chinhsach.ThemDoiTuongDto;
import com.vnptit5.ptgp.ldtbxh.dto.response.CustomException;
import com.vnptit5.ptgp.ldtbxh.dto.response.DangKyDto;
import org.springframework.beans.factory.annotation.Autowired;
import com.vnptit5.ptgp.ldtbxh.dto.response.MessageDto;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/sldtbxh")
@CrossOrigin
public class ChinhSachResource {
    @Autowired
    ChinhSachDao chinhSachDao;

    @GetMapping("/chinh-sach/danh-sach")
    public ResponseEntity danhMucChinhSach(@RequestParam(required = false) Integer doiTuong) {
        return ResponseEntity.ok().body(chinhSachDao.danhMucChinhSach(doiTuong));
    }

    @PostMapping("/chinh-sach/them-moi")
    public ResponseEntity themMoiChinhSach(@RequestBody ThemChinhSachDto csdto) {
        int macs = chinhSachDao.themMoiChinhSach(csdto);
        if (macs == 0) {
            throw new CustomException("Chính sách đã tồn tại");
        }
        return ResponseEntity.ok().body(
                DangKyDto.builder().id(macs).thongBao("Thêm mới thành công").statusCode(HttpStatus.OK.value()).build());
    }

    @GetMapping("/chinh-sach/thong-tin-theo-doi-tuong")
    public ResponseEntity thongTinChinhSachTheoDoiTuong(@RequestParam Integer idLoaiCS,
            @RequestParam Integer idDoiTuong) {
        return ResponseEntity.ok().body(chinhSachDao.thongTinChinhSachTheoDoiTuong(idLoaiCS, idDoiTuong));
    }

    @GetMapping("/chinh-sach/get-loai-chinh-sachs")
    public ResponseEntity getLoaiChinhSachs(@RequestParam(required = false) Integer doiTuong) {
        return ResponseEntity.ok().body(chinhSachDao.getLoaiChinhSachs(doiTuong));
    }

    @GetMapping("/loai-doi-tuong/get-all")
    public ResponseEntity getAllLoaiDoituong() {
        return ResponseEntity.ok().body(chinhSachDao.getAllLoaiDoiTuong());
    }

    @GetMapping("/chinh-sach/find-loai-chinh-sach")
    public ResponseEntity findLoaiChinhSachs(@RequestParam String name) {
        return ResponseEntity.ok().body(chinhSachDao.findLoaiChinhSach(name));
    }

    @GetMapping("/doi-tuong/search-doi-tuong")
    public ResponseEntity searchDoiTuong(@RequestParam Integer idLoaiCs, @RequestParam String tenDoiTuong) {
        return ResponseEntity.ok().body(chinhSachDao.searchDoiTuong(idLoaiCs, tenDoiTuong));
    }

    @PutMapping("/chinh-sach/update")
    public ResponseEntity suaTreEm(@RequestBody SuaChinhSachDto dto) {
        chinhSachDao.updatePolicy(dto);
        return ResponseEntity.ok().body(new MessageDto(HttpStatus.OK.value(), "Cập nhật chính sách thành công"));
    }

    @GetMapping("/doi-tuong/get-related-info-of-doi-tuong")
    public ResponseEntity getRelatedInfoDoiTuong(@RequestParam Integer idDoiTuong) {
        return ResponseEntity.ok().body(chinhSachDao.getRelatedInfoDoiTuong(idDoiTuong));
    }

    @GetMapping("/chinh-sach/find-by-id")
    public ResponseEntity findByIdChinhSach(@RequestParam Integer idChinhSach) {
        return ResponseEntity.ok().body(chinhSachDao.findByIdChinhSach(idChinhSach));
    }

    @GetMapping("/doi-tuong/find-by-id")
    public ResponseEntity findByIdDoiTuong(@RequestParam Integer idDoiTuong) {
        return ResponseEntity.ok().body(chinhSachDao.findByIdDoiTuong(idDoiTuong));
    }

    @PutMapping("/doi-tuong/update")
    public ResponseEntity updateDoiTuong(@RequestBody SuaDoiTuongDto dto) {
        chinhSachDao.updateDoiTuong(dto);
        return ResponseEntity.ok().body(new MessageDto(HttpStatus.OK.value(), "Cập nhật đối tượng thành công"));
    }

    @PostMapping("/doi-tuong/add")
    public ResponseEntity themDoiTuong(@RequestBody ThemDoiTuongDto dto) {
        int madt = chinhSachDao.themDoiTuong(dto);
        if (madt == 0) {
            throw new CustomException("Đối tượng đã tồn tại");
        }
        return ResponseEntity.ok().body(
                DangKyDto.builder().id(madt).thongBao("Thêm mới thành công").statusCode(HttpStatus.OK.value()).build());
    }
}
