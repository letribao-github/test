package com.vnptit5.ptgp.ldtbxh.common;

public class UrlConstants {

    /* ROOT*/
    public static final String ROOT = "/sldtbxh/ncc";

    /* RECORD TYPE AREA*/
    public static final String RECORD_TYPE = ROOT + "/record-type";
    public static final String GET_LIST_RECORD_TYPE = "list";

    /* RECORD AREA*/
    public static final String RECORD = ROOT + "/record";
    public static final String SEARCH_RECORD = "search";
    public static final String PROFILE_INFORMATION_EXPORT_GENERAL = "export";
    public static final String CREATE_PROFILE_MARTYRS = "martyrs";
    public static final String CATEGORY = "category";
    public static final String INFORMATION = "info";
    public static final String UPDATE_PROFILE = "update";

    /* DAN TOC AREA */
    public static final String DAN_TOC = ROOT + "/dan-toc";
    public static final String GET_LIST_DAN_TOC = "list";

    /* HUYEN AREA */
    public static final String HUYEN = ROOT + "/huyen";
    public static final String GET_LIST_HUYEN = "list";
    public static final String GET_LIST_TINH = "tinh";

    /* XA AREA */
    public static final String XA = ROOT + "/xa";
    public static final String GET_LIST_XA = "list";


    /* HO SO QUAN NHAN XUAT NGU AREA */
    public static final String HO_SO_QUAN_NHAN_XUAT_NGU = ROOT + "/quannhanxuatngu";
    public static final String HO_SO_QNXN_XEM_THONG_TIN_HO_SO =  "/xem-thong-tin-ho-so";
    public static final String HO_SO_QNXN_XEM_THONG_TIN_TRO_CAP_PHU_CAP = "/xem-thong-tin-tro-cap-phu-cap";
    public static final String HO_SO_QNXN_XEM_QUA_TRINH_CONG_TAC = "/xem-qua-trinh-cong-tac";
    public static final String HO_SO_QNXN_XEM_TRO_CAP_KHI_QUA_DOI = "/xem-tro-cap-khi-qua-doi";
    public static final String HO_SO_QNXN_THEM_THONG_TIN_HO_SO = "/them-thong-tin-ho-so";
    public static final String HO_SO_QNXN_THEM_THONG_TIN_TRO_CAP_PHU_CAP = "/them-thong-tin-tro-cap-phu-cap";
    public static final String HO_SO_QNXN_THEM_QUA_TRINH_CONG_TAC = "/them-qua-trinh-cong-tac";
    public static final String HO_SO_QNXN_THEM_TRO_CAP_KHI_QUA_DOI = "/them-tro-cap-khi-qua-doi";
    public static final String HO_SO_QNXN_SUA_THONG_TIN_HO_SO = "/sua-thong-tin-ho-so";
    public static final String HO_SO_QNXN_SUA_THONG_TIN_TRO_CAP_PHU_CAP = "/sua-thong-tin-tro-cap-phu-cap";
    public static final String HO_SO_QNXN_SUA_QUA_TRINH_CONG_TAC = "/sua-qua-trinh-cong-tac";
    public static final String HO_SO_QNXN_SUA_TRO_CAP_KHI_QUA_DOI = "/sua-tro-cap-khi-qua-doi";
    public static final String HO_SO_QNXN_XOA_THONG_TIN_HO_SO = "/xoa-thong-tin-ho-so";
    public static final String HO_SO_QNXN_XOA_THONG_TIN_TRO_CAP_PHU_CAP = "/xoa-thong-tin-tro-cap-phu-cap";
    public static final String HO_SO_QNXN_XOA_QUA_TRINH_CONG_TAC = "/xoa-qua-trinh-cong-tac";
    public static final String HO_SO_QNXN_XOA_TRO_CAP_KHI_QUA_DOI = "/xoa-tro-cap-khi-qua-doi";

    /* SUBSIDY INFORMATION */
    public static final String SUBSIDY_INFORMATION_PAGE = ROOT + "/liet-si/subsidy";
    public static final String SUBSIDY_INFORMATION = "info";
    public static final String SUBSIDY_CATEGORY = "category";
    public static final String SUBSIDY_INSERT_OR_UPDATE = "update";
    public static final String SUBSIDY_DELETE = "delete";

    /* HERO LIFE INFORMATION */
    public static final String LIFE_INFO_MAPPING = ROOT + "/liet-si/doi-song";
    public static final String LIFE_INFO_SEARCH = "/tra-cuu";
    public static final String LIFE_INFO_MENU = "/danh-muc";
    public static final String LIFE_INFO_UPDATE = "/cap-nhat";

    /* FILE STORAGE */
    public static final String FILE_MAPPING = ROOT + "/file";
    public static final String FILE_STORAGE_UPLOAD_FILE = "/uploadFile";
    public static final String FILE_STORAGE_PATH_DOWNLOAD = FILE_MAPPING + "/downloadFile/";
    public static final String FILE_STORAGE_DELETE_FILE = "/deleteFile";
    public static final String FILE_STORAGE_LIST_FILE = "/listFile";
    public static final String FILE_STORAGE_LIST_ZIP_FILE = "/zipFile";

    /* MARTYRS FAMILY INFORMATION */
    public static final String FAMILY_INFORMATION = ROOT + "/liet-si/family";
    public static final String FAMILY_INFORMATION_INFO = "/info";
    public static final String FAMILY_INFORMATION_UPDATE = "/update";

    /* MARTYRS RELATIVE INFORMATION */
    public static final String RELATIVE_INFORMATION = ROOT + "/liet-si/relative";
    public static final String RELATIVE_INFORMATION_INFO = "/info";
    public static final String RELATIVE_INFORMATION_UPDATE = "/update";
    public static final String RELATIVE_INFORMATION_DELETE = "/delete";

    /* MARTYRS DEATH BENEFITS INFORMATION */
    public static final String DEATH_BENEFITS_INFORMATION = ROOT + "/liet-si/death-benefits";
    public static final String DEATH_BENEFITS_INFORMATION_INFO = "/info";
    public static final String DEATH_BENEFITS_INFORMATION_UPDATE = "/update";
    public static final String DEATH_BENEFITS_INFORMATION_DELETE = "/delete";

}
