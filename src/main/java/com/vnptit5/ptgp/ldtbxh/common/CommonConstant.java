package com.vnptit5.ptgp.ldtbxh.common;

public interface CommonConstant {
    String EXCEL_TEMPLATE_FOLDER = "template/excel/";

    interface MimeType {
        String EXCEL = "application/vnd.ms-excel";
    }
}
