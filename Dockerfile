FROM openjdk:11
ADD build/libs/ROOT.jar ROOT.jar
EXPOSE 9000
ENTRYPOINT [ "java","-jar","ROOT.jar" ]
