## Hệ thống thông tin chuyên ngành Lao động Thương Binh Xã hội VNPT - LIMS (Back end)

#### Giới thiệu chung
**VNPT LIMS:** Là hệ thống dùng chung, quản lý tất cả các lĩnh vực chuyên ngành Lao động Thương binh Xã hội. Hệ thống bao gồm các phân hệ:
- Các phân hệ chung:
	- Quản trị người dùng
	- Quản trị địa phương
- Các phân hệ chính:
	- Trẻ em và Bình đẳng giới
	- Bảo trợ xã hội
	- Người có công

####Các yêu cầu run procject:
- Máy tính đã cài đặt java 8.
(Tài liệu hướng dẫn cài đặt Java 8 tại [đây](https://gitlab.com/ctntrinh/ldtbxh/-/blob/dev/doc/H%C6%B0%E1%BB%9Bng%20d%E1%BA%ABn%20c%C3%A0i%20%C4%91%E1%BA%B7t%20Java%208.docx))
- Có các IDE hỗ trợ code: IntelliJ IDEA (Recommend), Visual Code
- Ứng dụng quản lý phiên bản source code: SourceTree(Recommend), Git Desktop

####Các bước run procject:
1. Download source theo [link](https://gitlab.com/ctntrinh/ldtbxh)
2. Mở project bằng IDE Intelij.
3. Chọn RUN để chạy procject

####Các bước khi tham gia code 1 tính năng mới:
1. Clone / Pull source mới nhất từ nhánh DEV về máy
2. Check out nhánh DEV
3. Tạo nhánh mới từ nhánh DEV
4. Thực hiện code trên nhánh vừa tạo
5. Commit code
6. Push code to origin
7. Tạo Merge Request vào nhánh DEV


