set year=%DATE:~10,4%
set day=%DATE:~7,2%
set mnt=%DATE:~4,2%
set hr=%TIME:~0,2%
set min=%TIME:~3,2%

IF %day% LSS 10 SET day=0%day:~1,1%
IF %mnt% LSS 10 SET mnt=0%mnt:~1,1%
IF %hr% LSS 10 SET hr=0%hr:~1,1%
IF %min% LSS 10 SET min=0%min:~1,1%

set backuptime=%year%-%day%-%mnt%-%hr%-%min%

:: MySQL EXE Path
set mysqldumpexe="C:\Program Files\MySQL\MySQL Server 8.0\bin\mysqldump.exe"

:: Name of the database user with rights to all tables
set dbuser=trinhctn

:: Password for the database user
set dbpass=Vnpttg@2020#

:: Error log path - Important in debugging your issues
set errorLogPath="D:\dumperrors.txt"

:: Error log path
set backupfldr=D:\Backup_Lims\


:: Path to data folder which may differ from install dir
set datafldr="C:\ProgramData\MySQL\MySQL Server 8.0\data"


:: DONE WITH SETTINGS
pushd %datafldr%

@echo off

FOR /D %%F IN (*) DO (

IF NOT [%%F]==[performance_schema] (
SET %%F=!%%F:@002d=-!
%mysqldumpexe% --user=%dbuser% --password=%dbpass% --databases --routines --log-error=%errorLogPath% %%F > "%backupfldr%%%F.%backuptime%.sql"
) ELSE (
echo Skipping DB backup for performance_schema
)
)